/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! A test server to echo communication between clients and a server.

use std::{
    net::{SocketAddr, UdpSocket},
    time::{Duration, SystemTime},
};

use renet::{ConnectionConfig, DefaultChannel, RenetClient, RenetServer, ServerEvent};
use renet_netcode::{
    ClientAuthentication, NetcodeClientTransport, NetcodeServerTransport, ServerAuthentication,
    ServerConfig,
};

/// Entrypoint to create a client or server.
fn main() {
    println!("Usage: server [SERVER_PORT] or client [SERVER_ADDR] [USER_NAME]");
    let args: Vec<String> = std::env::args().collect();

    let exec_type = args.get(1).unwrap();
    match exec_type.as_str() {
        "client" => {
            let server_addr: SocketAddr = args.get(2).unwrap().parse().unwrap();
            client(server_addr);
        }
        "server" => {
            let server_addr: SocketAddr =
                format!("0.0.0.0:{}", args.get(2).unwrap()).parse().unwrap();
            server(server_addr);
        }
        _ => {
            println!("Invalid argument, first one must be \"client\" or \"server\".");
        }
    }
}

/// Create a client and connect to the given server address.
///
/// # Panics
///
/// Will panic if the UDP socket cannot be bound to the localhost address.
///
/// This may also panic if the netcode transport is unable to bind with the server configuration and the UDP socket.
fn client(server_addr: SocketAddr) {
    let mut client = RenetClient::new(ConnectionConfig::default());

    // Setup transport layer
    let socket = UdpSocket::bind("127.0.0.1:0").unwrap();
    let current_time = SystemTime::now()
        .duration_since(SystemTime::UNIX_EPOCH)
        .unwrap();
    let authentication = ClientAuthentication::Unsecure {
        server_addr,
        client_id: 0,
        user_data: None,
        protocol_id: 0,
    };

    let mut transport = NetcodeClientTransport::new(current_time, authentication, socket).unwrap();

    // Your gameplay loop
    loop {
        let delta_time = Duration::from_millis(16);
        // Receive new messages and update client
        client.update(delta_time);
        transport.update(delta_time, &mut client).unwrap();

        if client.is_connected() {
            // Receive message from server
            while let Some(message) = client.receive_message(DefaultChannel::ReliableOrdered) {
                // Handle received message
                println!("Server message: {message:?}");
            }

            // Send message
            client.send_message(DefaultChannel::ReliableOrdered, "client text");
        }

        // Send packets to server using the transport layer
        transport.send_packets(&mut client).unwrap();

        std::thread::sleep(delta_time); // Running at 60hz
    }
}

/// Create a server and listen at the given socket address.
///
/// # Panics
///
/// Will panic if the UDP socket cannot be bound to the server address.
///
/// This may also panic if the netcode transport is unable to bind with the server configuration and the UDP socket.
fn server(public_addr: SocketAddr) {
    let mut server = RenetServer::new(ConnectionConfig::default());

    // Setup transport layer
    let socket: UdpSocket = UdpSocket::bind(public_addr).unwrap();
    let server_config = ServerConfig {
        current_time: SystemTime::now()
            .duration_since(SystemTime::UNIX_EPOCH)
            .unwrap(),
        max_clients: 64,
        protocol_id: 0,
        public_addresses: vec![public_addr],
        authentication: ServerAuthentication::Unsecure,
    };
    let mut transport = NetcodeServerTransport::new(server_config, socket).unwrap();

    // Your gameplay loop
    loop {
        let delta_time = Duration::from_millis(16);
        // Receive new messages and update clients
        server.update(delta_time);
        transport.update(delta_time, &mut server).unwrap();

        // Check for client connections/disconnections
        while let Some(event) = server.get_event() {
            match event {
                ServerEvent::ClientConnected { client_id } => {
                    println!("Client {client_id} connected");
                }
                ServerEvent::ClientDisconnected { client_id, reason } => {
                    println!("Client {client_id} disconnected: {reason}");
                }
            }
        }

        // Receive message from channel
        for client_id in server.clients_id() {
            // The enum DefaultChannel describe the channels used by the default configuration
            while let Some(message) =
                server.receive_message(client_id, DefaultChannel::ReliableOrdered)
            {
                // Handle received message
                println!("Client message: {message:?}");
            }
        }

        // Send a text message for all clients
        server.broadcast_message(DefaultChannel::ReliableOrdered, "server message");

        let client_id = 0;
        // Send a text message for all clients except for Client 0
        server.broadcast_message_except(
            client_id,
            DefaultChannel::ReliableOrdered,
            "server message",
        );

        // Send message to only one client
        server.send_message(client_id, DefaultChannel::ReliableOrdered, "server message");

        // Send packets to clients using the transport layer
        transport.send_packets(&mut server);

        std::thread::sleep(delta_time); // Running at 60hz
    }
}
