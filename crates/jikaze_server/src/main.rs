/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Entrypoint to run the server.

use std::net::{IpAddr, Ipv4Addr, SocketAddr};
use std::time::{Duration, Instant};

use jikaze_riichi_core::tile::Tile;
use jikaze_server::server::server;
use log::{info, trace};
use renet::{DefaultChannel, ServerEvent};

/// Entrypoint to run the server.
fn main() {
    env_logger::init();

    // TODO: Remove this debug assignment.
    let tile = Tile::WindEast;

    const SERVER_ADDR: SocketAddr = SocketAddr::new(IpAddr::V4(Ipv4Addr::new(127, 0, 0, 1)), 5000);
    let (mut server, mut transport) = server(SERVER_ADDR);
    trace!("Server listening on {}", SERVER_ADDR);

    let mut last_updated = Instant::now();
    // Your gameplay loop.
    loop {
        // Update server time.
        let now = Instant::now();
        let delta_time = now - last_updated;
        server.update(delta_time);
        transport.update(delta_time, &mut server).unwrap();
        last_updated = now;

        // Check for client connections/disconnections.
        while let Some(event) = server.get_event() {
            match event {
                ServerEvent::ClientConnected { client_id } => {
                    info!("Client {client_id} connected");
                }
                ServerEvent::ClientDisconnected { client_id, reason } => {
                    info!("Client {client_id} disconnected: {reason}");
                }
            }
        }

        // Receive message from channel.
        for client_id in server.clients_id() {
            // The enum DefaultChannel describe the channels used by the default configuration.
            while let Some(_message) =
                server.receive_message(client_id, DefaultChannel::ReliableOrdered)
            {
                // Handle received message.
            }
        }

        // Send a text message for all clients.
        server.broadcast_message(DefaultChannel::ReliableOrdered, format!("Drew tile {tile}"));

        let client_id = 0;
        // Send a text message for all clients except for Client 0.
        server.broadcast_message_except(
            client_id,
            DefaultChannel::ReliableOrdered,
            "server message",
        );

        // Send message to only one client.
        server.send_message(client_id, DefaultChannel::ReliableOrdered, "server message");

        // Send packets to clients using the transport layer.
        transport.send_packets(&mut server);

        // Running at 60hz.
        std::thread::sleep(Duration::from_millis(50));
    }
}
