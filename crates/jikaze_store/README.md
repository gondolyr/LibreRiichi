<!--
SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

Library to manage the game state, events, and more.
