/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Structs to track the state of the game.

use std::collections::HashMap;

use jikaze_riichi_core::game_rules::{GameLength, GameRules, Renchan};
use jikaze_riichi_core::hand::{Hand, HandVisibility};
use jikaze_riichi_core::meld::{KanType, Meld};
use jikaze_riichi_core::player::player_round_state::{Furiten, PlayerRoundState, Riichi};
use jikaze_riichi_core::round::{LastDiscard, LastKan, RoundContext};
use jikaze_riichi_core::tile::{Tile, TileSuit};
use jikaze_riichi_core::utils::ceilu64;
use jikaze_riichi_core::wall::{Wall, create_unshuffled_tiles_3p, create_unshuffled_tiles_4p};
use jikaze_riichi_core::win_declaration::WinDeclaration;
use jikaze_riichi_core::wind::Wind;
use rand::seq::{IteratorRandom, SliceRandom};

use crate::error::{ConnectionErrorReason, GameEventError};
use crate::event::GameEvent;
use crate::jikaze_rand;
use crate::player::{Player, PlayerId};

/// Reasons why a round can end.
#[derive(Debug, Clone, Copy, PartialEq)]
pub enum EndRoundReason {
    /// When all of the live tiles in the wall are exhaused.
    ExhaustiveDraw,
    /// A player declared a win.
    PlayerWon {
        /// Player who won.
        winner: PlayerId,
        /// How the win was achieved.
        win_declaration: WinDeclaration,
    },
}

/// The different stages a game can be in.
///
/// _Not to be confused with the entire [`GameState`]._
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum Stage {
    /// Players are in a lobby/matchmaking.
    PreGame,
    /// Players are in an active game.
    InGame,
    /// The game has ended.
    Ended,
}

/// The different stages a round can be in.
#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub enum RoundStage {
    /// Round is being set up.
    PreRound,
    /// Round is active.
    InGame,
    /// The round has ended.
    Ended,
}

/// Stores the state of the game.
#[derive(Debug)]
pub struct GameState {
    /// Current player that is active and should discard a tile.
    active_player: PlayerId,
    /// Available seat winds.
    ///
    /// Only used in the pre-game stage to determine what seat each player receives.
    available_seats: Vec<Wind>,
    /// Rules that define how the game should be played.
    game_rules: GameRules,
    /// History of events that have been added to this state (i.e. the sequence of events).
    history: Vec<GameEvent>,
    /// Number of repeat counters.
    honba: u16,
    /// Number of players in the game.
    ///
    /// This affects some gameplay rules.
    number_of_players: u8,
    /// Players in the game.
    players: HashMap<PlayerId, Player>,
    /// Number of leftover riichi bets.
    riichi_bets: u16,
    /// Context around the round.
    ///
    /// The tiles in the wall should be shuffled.
    round_context: RoundContext,
    /// Round number for the wind.
    ///
    /// This should have a maximum value equal to the number of players in the game, as every player has a chance to be the dealer.
    round_number: u16,
    /// Stage the round is in.
    round_stage: RoundStage,
    /// Stage the game is in.
    stage: Stage,
}

impl Default for GameState {
    fn default() -> Self {
        let game_rules = GameRules::new_4p_default();

        let mut rng = jikaze_rand::rng();
        let mut tiles = create_unshuffled_tiles_4p(game_rules.akadora());
        tiles.shuffle(&mut rng);

        let wall = Wall::new(Box::new(tiles));

        Self::new_with_wall(game_rules, 4, wall, rng.get_seed())
    }
}

impl GameState {
    /// Get the active player.
    pub const fn active_player(&self) -> &PlayerId {
        &self.active_player
    }

    /// Get a mutable reference to the active player.
    pub fn active_player_mut(&mut self) -> &mut PlayerId {
        &mut self.active_player
    }

    /// Try to consume an event by first validating it.
    ///
    /// # Errors
    ///
    /// Will return `Err` if the event fails validation.
    pub fn dispatch(&mut self, event: &GameEvent) -> Result<(), GameEventError> {
        self.validate(event)?;

        self.consume(event);

        Ok(())
    }

    /// Get the game rules.
    pub const fn game_rules(&self) -> &GameRules {
        &self.game_rules
    }

    /// Get the list of events that occurred during the game.
    pub const fn history(&self) -> &Vec<GameEvent> {
        &self.history
    }

    /// Get the number of repeat counters.
    pub const fn honba(&self) -> u16 {
        self.honba
    }

    /// Get a mutable reference to the number of repeat counters.
    pub fn honba_mut(&mut self) -> &mut u16 {
        &mut self.honba
    }

    /// Create a new instance of `GameState`.
    pub fn new(game_rules: GameRules, number_of_players: u8) -> Self {
        let mut rng = jikaze_rand::rng();

        let wall = if number_of_players == 3 {
            let mut tiles = create_unshuffled_tiles_3p(game_rules.akadora());
            tiles.shuffle(&mut rng);

            Wall::new(Box::new(tiles))
        } else {
            // Default to 4 players.
            let mut tiles = create_unshuffled_tiles_4p(game_rules.akadora());
            tiles.shuffle(&mut rng);

            Wall::new(Box::new(tiles))
        };

        Self::new_with_wall(game_rules, number_of_players, wall, rng.get_seed())
    }

    /// Create a new instance of `GameState` with the defined wall.
    pub fn new_with_wall(
        game_rules: GameRules,
        number_of_players: u8,
        wall: Wall,
        seed: [u8; 32],
    ) -> Self {
        let available_seats = if number_of_players == 3 {
            vec![Wind::East, Wind::South, Wind::West]
        } else {
            vec![Wind::East, Wind::South, Wind::West, Wind::North]
        };

        Self {
            active_player: 0,
            available_seats,
            game_rules,
            history: Vec::new(),
            honba: 0,
            number_of_players,
            players: HashMap::new(),
            riichi_bets: 0,
            round_context: RoundContext::new(wall, Wind::East, seed),
            round_number: 1,
            round_stage: RoundStage::PreRound,
            stage: Stage::PreGame,
        }
    }

    /// Get the number of players for the game.
    pub const fn number_of_players(&self) -> u8 {
        self.number_of_players
    }

    /// Get the players in the game.
    pub const fn players(&self) -> &HashMap<PlayerId, Player> {
        &self.players
    }

    /// Get a mutable reference to the players in the game.
    ///
    /// This should only be used to add or remove players from a lobby and should not be used during an active game (kicking a player for cheating would be an acceptable exception).
    pub fn players_mut(&mut self) -> &mut HashMap<PlayerId, Player> {
        &mut self.players
    }

    /// Consume an event, modifying the `GameState`, and adding the event to its history.
    ///
    /// <div class="warning">
    ///
    /// This assumes the event has already been validated, and will accept **any** event passed to it.
    ///
    /// </div>
    fn consume(&mut self, valid_event: &GameEvent) {
        match valid_event {
            GameEvent::PlayerJoined {
                player_id,
                username,
            } => self.consume_player_joined(player_id, username),
            GameEvent::BeginGame => self.consume_begin_game(),
            GameEvent::EndGame => self.consume_end_game(),
            GameEvent::DiscardTile { player_id, at } => self.consume_discard_tile(player_id, at),
            GameEvent::EndRound { reason } => self.consume_end_round(reason),
            GameEvent::Meld { player_id, meld } => self.consume_meld(player_id, meld),
            GameEvent::PlayerDisconnected { player_id } => {
                self.consume_player_disconnected(player_id)
            }
            GameEvent::Riichi { player_id } => self.consume_riichi(player_id),
        }

        self.history.push(valid_event.clone());
    }

    /// Consume the [`GameEvent::BeginGame`] event.
    fn consume_begin_game(&mut self) {
        *self.stage_mut() = Stage::InGame;
    }

    /// Consume the [`GameEvent::DiscardTile`] event.
    fn consume_discard_tile(&mut self, player_id: &PlayerId, at: &usize) {
        let round_context = self.round_context().clone();

        // We can unwrap safely because the `validate()` method ensures this player exists.
        {
            let player = self.players_mut().get_mut(player_id).unwrap();
            let discarded_tile = if *at == player.hand().tiles().len() {
                // Player discarded their drawn tile.
                let discarded_tile = match (player.hand().drawn_tile(), player.hand().rinshanpai())
                {
                    (Some(tile), None) => *tile,
                    (None, Some(rinshanpai)) => *rinshanpai,
                    (Some(tile), Some(_rinshanpai)) => *tile,
                    (None, None) => unreachable!(
                        "discarding the drawn tile should have been checked during validation"
                    ),
                };

                *player.hand_mut().drawn_tile_mut() = None;
                *player.hand_mut().rinshanpai_mut() = None;

                match player.player_round_state().furiten() {
                    Some(Furiten::Temporary) => {
                        *player.player_round_state_mut().furiten_mut() = None;
                    }
                    Some(Furiten::Discard) | Some(Furiten::Permanent) | None => {}
                }

                discarded_tile
            } else {
                // Player discarded a tile from within their hand.
                let discarded_tile = player.hand_mut().tiles_mut().remove(*at);

                // Add the drawn tile to the hand.
                let drawn_tile = match (player.hand().drawn_tile(), player.hand().rinshanpai()) {
                    (Some(tile), None) => *tile,
                    (None, Some(rinshanpai)) => *rinshanpai,
                    (Some(tile), Some(_rinshanpai)) => *tile,
                    (None, None) => unreachable!(
                        "adding the drawn tile to the hand should have been checked during validation"
                    ),
                };
                player.hand_mut().tiles_mut().push(&drawn_tile);
                player.hand_mut().tiles_mut().sort();
                *player.hand_mut().drawn_tile_mut() = None;

                discarded_tile
            };

            // The player's complete list of discards.
            player.hand_mut().discards_mut().push(&discarded_tile);
            // The player's displayed discards. Meld calls on this player's discarded tiles will remove them from this field.
            player.discards_mut().push(&discarded_tile);

            // The `last_meld` field is used for validating that that the kuikae rule is not violated and needs to be reset to prevent false flags during validation.
            *player.player_round_state_mut().last_meld_mut() = None;
        }

        // The discarded tile is in this player's discard pile so unwrapping is safe here.
        let discarded_tile = *self
            .players()
            .get(player_id)
            .unwrap()
            .hand()
            .discards()
            .last()
            .unwrap();

        if round_context.first_uninterrupted_turn()
            && self.players().values().all(|player| {
                if matches!(discarded_tile.suit(), TileSuit::Wind) {
                    match player.hand().discards().first() {
                        Some(tile) => *tile == discarded_tile,
                        None => false,
                    }
                } else {
                    false
                }
            })
        {
            // All players discarded the same wind in the first turn -- abortive draw.
            // 1. No points are exchanged.
            // 2. Hands are **not** revealed.
            // 3. All riichi bets remain on the table.
            // 4. Honba count increases by one.
            // 5. Seats do not rotate.

            *self.round_stage_mut() = RoundStage::Ended;

            *self.honba_mut() = self.honba() + 1;

            // TODO: Send an event to the clients letting them know about the abortive draw.
        } else {
            let player = self.players_mut().get_mut(player_id).unwrap();

            if *at < player.hand().tiles().len() {
                let player_waits = player.hand().get_waits(&round_context);
                match player.player_round_state().furiten() {
                    Some(Furiten::Discard) => {
                        let player_discards_distinct = player.hand().discards().distinct();
                        // Check if any of the new waits are in the player's discards. If not, then the player is no longer in furiten.
                        if player_waits.iter().all(|wait| {
                            !player_discards_distinct
                                .iter()
                                .any(|discard| discard == wait)
                        }) {
                            *player.player_round_state_mut().furiten_mut() = None;
                        }
                    }
                    Some(Furiten::Temporary) => {
                        *player.player_round_state_mut().furiten_mut() = None;
                    }
                    Some(Furiten::Permanent) => {
                        unreachable!(
                            "player cannot be be in permanent furiten (riichi) and discard a tile from their hand"
                        )
                    }
                    None => {}
                }
            }

            // The player did not win with rinshan kaihou (after a kan).
            *player.player_round_state_mut().last_meld_mut() = None;

            let player_wind = *player.player_round_state().wind();
            *self.round_context_mut().last_discard_mut() =
                Some(LastDiscard::new(player_wind, discarded_tile));

            if self.round_context().first_uninterrupted_turn()
                && (self.round_context().wall().current_tile_idx()
                    >= self.number_of_players().into())
            {
                // Everyone has taken their first turn.
                *self.round_context_mut().first_uninterrupted_turn_mut() = false;
            }
        }

        if let Some(last_kan) = self.round_context().last_kan() {
            match last_kan.kan() {
                KanType::Ankan { tiles: _ } => {
                    // Another dora indicator tile should have been revealed already.
                }
                KanType::Daiminkan {
                    claimed_tile: _,
                    relative_player: _,
                    tiles: _,
                }
                | KanType::Shominkan {
                    claimed_tile: _,
                    relative_player: _,
                    tiles: _,
                } => {
                    self.round_context_mut()
                        .wall_mut()
                        .increment_num_revealed_dora()
                        .unwrap();
                }
            }
            *self.round_context_mut().last_kan_mut() = None;
        }
    }

    /// Consume the [`GameEvent::EndGame`] event.
    fn consume_end_game(&mut self) {
        *self.stage_mut() = Stage::Ended;
    }

    /// Consume the [`GameEvent::EndRound`] event.
    ///
    /// # Panics
    ///
    /// Will panic if the payment conversion from a `u64` to a `i64` fails.
    fn consume_end_round(&mut self, reason: &EndRoundReason) {
        // Determine if the player seats need to be rotated.
        let mut rotate_seats = true;

        match reason {
            EndRoundReason::ExhaustiveDraw => {
                let number_of_players = self.number_of_players();
                let round_context = self.round_context();

                let players_in_tenpai: Box<[u64]> = self
                    .players()
                    .iter()
                    .filter_map(|(player_id, player)| {
                        if !player.hand().get_waits(round_context).is_empty() {
                            if (matches!(self.game_rules().renchan(), Renchan::Tenpai)
                                && player.player_round_state().is_dealer())
                                || matches!(self.game_rules().renchan(), Renchan::Ryuukyoku)
                            {
                                rotate_seats = false;
                            }

                            Some(*player_id)
                        } else {
                            None
                        }
                    })
                    .collect();
                let players_not_in_tenpai: Box<[u64]> = self
                    .players()
                    .iter()
                    .filter_map(|(player_id, player)| {
                        if player.hand().get_waits(round_context).is_empty() {
                            Some(*player_id)
                        } else {
                            None
                        }
                    })
                    .collect();

                // Number of points each player in "noten" has to pay.
                let (noten_payment, tenpai_payment) = if players_in_tenpai.len() > 0
                    && players_in_tenpai.len() < number_of_players.into()
                {
                    (
                        3000 / std::convert::TryInto::<u32>::try_into(players_not_in_tenpai.len())
                            .unwrap(),
                        3000 / std::convert::TryInto::<u32>::try_into(players_in_tenpai.len())
                            .unwrap(),
                    )
                } else {
                    (0, 0)
                };
                let noten_payment: i64 = noten_payment.into();
                let tenpai_payment: i64 = tenpai_payment.into();

                for player_id in &players_in_tenpai {
                    let player = self.players_mut().get_mut(player_id).unwrap();
                    *player.points_mut() = player.points() + tenpai_payment;
                }
                for player_id in &players_not_in_tenpai {
                    let player = self.players_mut().get_mut(player_id).unwrap();
                    *player.points_mut() = player.points() - noten_payment;
                }

                if rotate_seats {
                    if players_in_tenpai.is_empty() {
                        *self.honba_mut() = self.honba() + 1;
                    }
                } else {
                    *self.honba_mut() = self.honba() + 1;
                }
            }
            EndRoundReason::PlayerWon {
                winner,
                win_declaration,
            } => {
                {
                    let winner = self.players_mut().get_mut(winner).unwrap();

                    *winner.hand_mut().win_declaration_mut() = Some(*win_declaration);
                    let hand_groups = winner.hand().find_groups();
                    *winner.hand_mut().groups_mut() = hand_groups;
                }
                let score = self.players().get(winner).unwrap().hand().score(
                    self.game_rules(),
                    self.round_context(),
                    self.players().get(winner).unwrap().player_round_state(),
                );
                let riichi_bet_value: i64 = self.game_rules().riichi_bet_value().into();
                let riichi_bets: i64 = self.riichi_bets().into();
                // The number of points each honba (repeat counter) is worth.
                let tsumibou: i64 = self.game_rules().tsumibou().into();
                let honba: i64 = self.honba().into();
                if self
                    .players()
                    .get(winner)
                    .unwrap()
                    .player_round_state()
                    .is_dealer()
                {
                    *self.honba_mut() = self.honba() + 1;
                    rotate_seats = false;
                }
                // The winning player will collect all riichi bets, so reset this counter.
                *self.riichi_bets_mut() = 0;

                match win_declaration {
                    WinDeclaration::Ron {
                        deal_in_player,
                        tile: _,
                    } => {
                        let base_value_multiplier = if self
                            .players()
                            .get(winner)
                            .unwrap()
                            .player_round_state()
                            .is_dealer()
                        {
                            6
                        } else {
                            4
                        };
                        let payment: i64 = ceilu64(score.base_value() * base_value_multiplier, 2)
                            .try_into()
                            .unwrap();

                        for player_deal_in in self.players_mut().values_mut() {
                            if player_deal_in.player_round_state().wind() == deal_in_player {
                                *player_deal_in.points_mut() =
                                    player_deal_in.points() - payment - (tsumibou * honba);
                            }
                        }

                        let winner_payment = self.players().get(winner).unwrap().points()
                            + payment
                            + (riichi_bet_value * riichi_bets)
                            + (tsumibou * honba);
                        // Update the winner's points.
                        *self.players_mut().get_mut(winner).unwrap().points_mut() = winner_payment;
                    }
                    WinDeclaration::Tsumo(_) => {
                        let number_of_players: i64 = self.number_of_players().into();

                        let honba_payment: i64 = ceilu64(
                            ((tsumibou * honba) / (number_of_players - 1))
                                .try_into()
                                .unwrap(),
                            2,
                        )
                        .try_into()
                        .unwrap();

                        let winner_is_dealer = self
                            .players()
                            .get(winner)
                            .unwrap()
                            .player_round_state()
                            .is_dealer();

                        let dealer_payment: i64 =
                            ceilu64(score.base_value() * 2, 2).try_into().unwrap();
                        let payment_multiplier = if winner_is_dealer { 2 } else { 1 };
                        let non_dealer_payment: i64 =
                            ceilu64(score.base_value() * payment_multiplier, 2)
                                .try_into()
                                .unwrap();

                        for (player_id, player) in self.players_mut() {
                            if player_id == winner {
                                let payment = if player.player_round_state().is_dealer() {
                                    let player_payment: i64 =
                                        ceilu64(score.base_value() * 2, 2).try_into().unwrap();

                                    player_payment * (number_of_players - 1)
                                } else {
                                    // The (players - 2) is to exclude the winner and the dealer, as the dealer is added in separately.
                                    (non_dealer_payment * (number_of_players - 2)) + dealer_payment
                                };

                                let winner_payment = player.points()
                                    + payment
                                    + (riichi_bet_value * riichi_bets)
                                    + (tsumibou * honba);
                                *player.points_mut() = winner_payment;
                            } else if player.player_round_state().is_dealer() {
                                *player.points_mut() =
                                    player.points() - dealer_payment - honba_payment;
                            } else {
                                *player.points_mut() =
                                    player.points() - non_dealer_payment - honba_payment;
                            }
                        }
                    }
                }
            }
        }

        if self.game_rules().tobi() && self.players().iter().any(|(_, player)| player.points() < 0)
        {
            *self.stage_mut() = Stage::Ended;
            *self.round_stage_mut() = RoundStage::Ended;
        } else {
            let mut rng = jikaze_rand::rng();

            let mut tiles: Box<[Tile]> = {
                let number_of_akadora = self.game_rules().akadora();
                if self.number_of_players() == 3 {
                    create_unshuffled_tiles_3p(number_of_akadora).into()
                } else {
                    create_unshuffled_tiles_4p(number_of_akadora).into()
                }
            };
            tiles.shuffle(&mut rng);

            if rotate_seats {
                for player in self.players_mut().values_mut() {
                    // Rotate the seats anti-clockwise.
                    *player.player_round_state_mut() =
                        PlayerRoundState::new(player.player_round_state().wind().previous_wind());
                }

                if self.round_number() == self.number_of_players().into() {
                    self.round_number_set(1);
                    let round_wind = self.round_context().wind().next_wind();
                    *self.round_context_mut() =
                        RoundContext::new(Wall::new(tiles), round_wind, rng.get_seed());
                } else {
                    self.round_number_set(self.round_number() + 1);
                    let round_wind = *self.round_context().wind();
                    *self.round_context_mut() =
                        RoundContext::new(Wall::new(tiles), round_wind, rng.get_seed());
                }
            } else {
                let round_wind = *self.round_context().wind();
                *self.round_context_mut() =
                    RoundContext::new(Wall::new(tiles), round_wind, rng.get_seed());
            }

            *self.round_stage_mut() = RoundStage::PreRound;
        }
    }

    /// Consume the [`GameEvent::Meld`] event.
    fn consume_meld(&mut self, player_id: &PlayerId, meld: &Meld) {
        // Apply meld to player's hand.
        {
            let round_context = self.round_context().clone();
            let player_round_state = self
                .players()
                .get(player_id)
                .unwrap()
                .player_round_state()
                .clone();

            if let Err(error) = self
                .players_mut()
                .get_mut(player_id)
                .unwrap()
                .hand_mut()
                .apply_meld(&round_context, &player_round_state, meld)
            {
                // `validate()` should catch an invalid meld. If it's a valid meld and there's a problem, it's with `apply_meld()`.
                unreachable!(
                    "unable to apply meld to the player's hand; this should not happen: {error}"
                );
            }

            *self
                .players_mut()
                .get_mut(player_id)
                .unwrap()
                .player_round_state_mut()
                .last_meld_mut() = Some(*meld);
        }

        *self.active_player_mut() = *player_id;

        match meld {
            Meld::Chii {
                claimed_tile: _,
                relative_player,
                tiles: _,
            }
            | Meld::Pon {
                claimed_tile: _,
                relative_player,
                tiles: _,
            } => {
                // Remove the claimed tile from the opponent's visible discard pile.
                let opponent = {
                    let relative_player_wind = self
                        .players()
                        .get(player_id)
                        .unwrap()
                        .player_round_state()
                        .wind()
                        .from_relative_position(relative_player);
                    let (_player_id, player) = self
                        .players_mut()
                        .iter_mut()
                        .find(|(_player_id, player)| {
                            player.player_round_state().wind() == &relative_player_wind
                        })
                        .unwrap();

                    player
                };
                // This removes the called opponent's last discard from the visible discard pile, but not the source of truth (the player's discard pile associated with their hand).
                let _opponent_last_discard = opponent.discards_mut().pop();
            }
            Meld::Kan(kan_type) => {
                {
                    let player = self.players().get(player_id).unwrap();
                    let player_wind = *player.player_round_state().wind();
                    *self.round_context_mut().last_kan_mut() =
                        Some(LastKan::new(player_wind, *kan_type));
                }

                {
                    self.round_context_mut()
                        .wall_mut()
                        .increment_num_kans_called()
                        .unwrap();
                    self.round_context_mut()
                        .wall_mut()
                        .increment_num_rinshanpai_drawn()
                        .unwrap();
                }

                {
                    let rinshanpai = *self.round_context().wall().rinshanpai();
                    *self
                        .players_mut()
                        .get_mut(player_id)
                        .unwrap()
                        .hand_mut()
                        .rinshanpai_mut() = Some(rinshanpai);
                }

                match kan_type {
                    KanType::Ankan { tiles: _ } => {
                        // This specific type of kan reveals the next dora indicator before the player's discard.
                        self.round_context_mut()
                            .wall_mut()
                            .increment_num_revealed_dora()
                            .unwrap();
                    }
                    KanType::Daiminkan {
                        claimed_tile: _,
                        relative_player,
                        tiles: _,
                    } => {
                        // Remove the claimed tile from the opponent's visible discard pile.
                        let opponent = {
                            let relative_player_wind = self
                                .players()
                                .get(player_id)
                                .unwrap()
                                .player_round_state()
                                .wind()
                                .from_relative_position(relative_player);
                            let (_player_id, player) = self
                                .players_mut()
                                .iter_mut()
                                .find(|(_player_id, player)| {
                                    player.player_round_state().wind() == &relative_player_wind
                                })
                                .unwrap();

                            player
                        };
                        // This removes the called opponent's last discard from the visible discard pile, but not the source of truth (the player's discard pile associated with their hand).
                        let _opponent_last_discard = opponent.discards_mut().pop();
                    }
                    KanType::Shominkan {
                        claimed_tile: _,
                        relative_player: _,
                        tiles: _,
                    } => {
                        // Nothing to do here.
                    }
                }
            }
        }
    }

    /// Consume the [`GameEvent::PlayerDisconnected`] event.
    fn consume_player_disconnected(&mut self, player_id: &PlayerId) {
        match self.stage() {
            Stage::PreGame => {
                // Add the player's assigned seat back into the pool.
                self.available_seats.push(
                    *self
                        .players()
                        .get(player_id)
                        .unwrap()
                        .player_round_state()
                        .wind(),
                );
                let _ = self.players_mut().remove(player_id);
            }
            Stage::Ended => {
                let _ = self.players_mut().remove(player_id);
            }
            Stage::InGame => {
                let player = self.players_mut().get_mut(player_id).unwrap();
                player.disconnect();
            }
        }
    }

    /// Consume the [`GameEvent::PlayerJoined`] event.
    fn consume_player_joined(&mut self, player_id: &PlayerId, username: &str) {
        match self.players_mut().get_mut(player_id) {
            Some(player) => {
                if !player.connected() {
                    player.reconnect();
                }
            }
            None => {
                if matches!(*self.stage(), Stage::PreGame) {
                    let mut rng = jikaze_rand::rng();

                    let seat_wind_idx = (0..self.available_seats.len()).choose(&mut rng).unwrap();
                    let seat_wind = self.available_seats.swap_remove(seat_wind_idx);

                    self.players.insert(
                        *player_id,
                        Player::new(*player_id, Hand::default(), seat_wind, username.to_string()),
                    );
                }
            }
        }
    }

    /// Consume the [`GameEvent::Riichi`] event.
    fn consume_riichi(&mut self, player_id: &PlayerId) {
        {
            let first_uninterrupted_turn = self.round_context().first_uninterrupted_turn();

            // We can unwrap safely because the `validate()` method ensures this player exists.
            let player = self.players_mut().get_mut(player_id).unwrap();

            let riichi = if first_uninterrupted_turn {
                Riichi::DoubleRiichi
            } else {
                Riichi::Riichi
            };
            *player.player_round_state_mut().riichi_mut() = Some(riichi);
        }

        if self
            .players
            .values()
            .all(|player| player.player_round_state().riichi().is_some())
        {
            // All players declared riichi without a win -- abortive draw.
            // 1. No points are exchanged.
            // 2. Everyone reveals their hand.
            // 3. All riichi bets remain on the table.
            // 4. Honba count increases by one.
            // 5. Seats do not rotate.
            // NOTE: The last player to declare riichi does **not** place their bet because the round ends before the bet placement step.

            *self.round_stage_mut() = RoundStage::Ended;

            *self.honba_mut() = self.honba() + 1;

            // TODO: Send an event to the clients letting them know about the abortive draw.
        } else {
            let player_waits = self
                .players()
                .get(player_id)
                .unwrap()
                .hand()
                .get_waits(self.round_context());
            let riichi_bet_value: i64 = self.game_rules().riichi_bet_value().into();

            // We can unwrap safely because the `validate()` method ensures this player exists.
            let player = self.players_mut().get_mut(player_id).unwrap();

            let player_discards_distinct = player.hand().discards().distinct();
            // Check if any of the waits are in the player's discards. If so, then the player is in permanent furiten.
            if player_waits.iter().any(|wait| {
                player_discards_distinct
                    .iter()
                    .any(|discard| discard == wait)
            }) {
                // Player is in discard furiten, but permanent, because they have declared riichi.
                *player.player_round_state_mut().furiten_mut() = Some(Furiten::Permanent);
            }

            // FIXME: The riichi bet should not be put on the table until the discard has passed with no one claiming a win on it.
            *player.points_mut() = player.points() - riichi_bet_value;
            *self.riichi_bets_mut() = self.riichi_bets() + 1;
        }
    }

    /// Get the number of leftover riichi bets.
    pub const fn riichi_bets(&self) -> u16 {
        self.riichi_bets
    }

    /// Get a mutable reference to the number of leftover riichi bets.
    pub fn riichi_bets_mut(&mut self) -> &mut u16 {
        &mut self.riichi_bets
    }

    /// Get the round context.
    pub const fn round_context(&self) -> &RoundContext {
        &self.round_context
    }

    /// Get a mutable reference to the round context.
    pub fn round_context_mut(&mut self) -> &mut RoundContext {
        &mut self.round_context
    }

    /// Get the wind round number.
    pub const fn round_number(&self) -> u16 {
        self.round_number
    }

    /// Get a mutable reference to the wind round number.
    pub fn round_number_mut(&mut self) -> &mut u16 {
        &mut self.round_number
    }

    /// Set the wind round number.
    ///
    /// This function provides some error correction when:
    ///
    /// - The round number is set to 0; in which case the round number is set to 1.
    /// - The round number exceeds the number of players; in which case the round number is set to 1.
    pub fn round_number_set(&mut self, round_number: u16) {
        let round_number = {
            if round_number == 0 || (round_number > self.number_of_players().into()) {
                1
            } else {
                round_number
            }
        };

        self.round_number = round_number;
    }

    /// Get the round stage the game is in.
    pub const fn round_stage(&self) -> &RoundStage {
        &self.round_stage
    }

    /// Get a mutable reference to the round stage the game is in.
    pub fn round_stage_mut(&mut self) -> &mut RoundStage {
        &mut self.round_stage
    }

    /// Get the stage the game is in.
    pub const fn stage(&self) -> &Stage {
        &self.stage
    }

    /// Get a mutable reference to the stage the game is in.
    pub fn stage_mut(&mut self) -> &mut Stage {
        &mut self.stage
    }

    /// Determine if the event is valid on the current `GameState`.
    ///
    /// # Errors
    ///
    /// Will return `Err` if the event fails validation.
    fn validate(&self, event: &GameEvent) -> Result<(), GameEventError> {
        // Try to invalidate the event.
        match event {
            GameEvent::PlayerJoined {
                player_id,
                username,
            } => self.validate_player_joined(player_id, username)?,
            GameEvent::BeginGame => self.validate_begin_game()?,
            GameEvent::EndGame => self.validate_end_game()?,
            GameEvent::DiscardTile { player_id, at } => {
                self.validate_discard_tile(player_id, at)?
            }
            GameEvent::EndRound { reason } => self.validate_end_round(reason)?,
            GameEvent::Meld { player_id, meld } => self.validate_meld(player_id, meld)?,
            GameEvent::PlayerDisconnected { player_id } => {
                self.validate_player_disconnected(player_id)?
            }
            GameEvent::Riichi { player_id } => self.validate_riichi(player_id)?,
        }

        // If we can't find something that is wrong with the event, then it must be okay.
        Ok(())
    }

    /// Validate the [`GameEvent::BeginGame`] event.
    ///
    /// # Errors
    ///
    /// Will return `Err` if the number of players in the game does not match the expected number of players.
    ///
    /// For example, a 4-player game cannot start with fewer than 4 players.
    fn validate_begin_game(&self) -> Result<(), GameEventError> {
        if self.players().len() != self.number_of_players().into() {
            return Err(GameEventError::InvalidBeginGame);
        }

        Ok(())
    }

    /// Validate the [`GameEvent::DiscardTile`] event.
    ///
    /// # Errors
    ///
    /// Will return `Err` if:
    ///
    /// - The game has not started.
    /// - The game has ended.
    /// - The round is being set up.
    /// - The round has ended.
    /// - The player is not in the game.
    /// - It is not the player's turn.
    /// - The player attempted to discard a drawn tile when they have none (they called a meld and must discard).
    /// - The player attempted to discard a tile from their hand after they have declared riichi.
    /// - The tile index (`at`) is not a valid index in the collection of tiles.
    /// - The "kuikae" rule is not in play (nashi) and the player attempted to discard a tile would have completed the meld.
    fn validate_discard_tile(
        &self,
        player_id: &PlayerId,
        at: &usize,
    ) -> Result<(), GameEventError> {
        if matches!(*self.stage(), Stage::PreGame | Stage::Ended)
            || matches!(
                *self.round_stage(),
                RoundStage::PreRound | RoundStage::Ended
            )
        {
            return Err(GameEventError::NotInGame);
        }

        let player = match self.players().get(player_id) {
            Some(player) => player,
            None => return Err(GameEventError::InvalidPlayer(*player_id)),
        };

        if self.active_player() != player_id {
            return Err(GameEventError::NotPlayerTurn(*player_id));
        }

        let in_riichi = player.player_round_state().riichi().is_some();
        let player_has_not_drawn_tile =
            player.hand().drawn_tile().is_none() && player.hand().rinshanpai().is_none();
        let tile_idx_outside_hand_length = *at > player.hand().tiles().len();
        let tile_is_drawn_tile = *at == player.hand().tiles().len();
        let tile_is_from_hand = *at < player.hand().tiles().len();

        // NOTE: Having comments between the conditions prevents rustfmt from working.
        //    See: https://github.com/rust-lang/rustfmt/issues/6246
        // Player cannot discard a drawn tile when they have none.
        // Player cannot discard tiles from their hand after declaring riichi. Checking against the hand length accounts for any ankan calls.
        if tile_idx_outside_hand_length
            || (tile_is_drawn_tile && player_has_not_drawn_tile)
            || (in_riichi && tile_is_from_hand)
        {
            return Err(GameEventError::InvalidTileDiscard(*at));
        }

        let violated_kuikae = {
            if !self.game_rules().kuikae() {
                match player.player_round_state().last_meld() {
                    Some(meld) => {
                        // The `at` index has been validated, so the tile can be obtained safely.
                        let discarded_tile = match player.hand().tiles().get(*at) {
                            Some(discarded_tile) => discarded_tile,
                            None => {
                                if tile_is_drawn_tile {
                                    match (player.hand().drawn_tile(), player.hand().rinshanpai()) {
                                        (Some(discarded_tile), None) => discarded_tile,
                                        (None, Some(rinshanpai)) => rinshanpai,
                                        (Some(discarded_tile), Some(_rinshanpai)) => discarded_tile,
                                        (None, None) => unreachable!(),
                                    }
                                } else {
                                    unreachable!();
                                }
                            }
                        };

                        match meld {
                            Meld::Chii {
                                claimed_tile,
                                relative_player: _,
                                tiles,
                            } => {
                                let discarded_is_same_as_claimed = discarded_tile == claimed_tile;
                                let discarded_completes_lower_end = claimed_tile
                                    == tiles.get(2).unwrap()
                                    && discarded_tile == &tiles.first().unwrap().previous_tile();
                                let discarded_completed_higher_end = claimed_tile
                                    == tiles.first().unwrap()
                                    && discarded_tile == &tiles.get(2).unwrap().next_tile();

                                discarded_is_same_as_claimed
                                    || discarded_completes_lower_end
                                    || discarded_completed_higher_end
                            }
                            Meld::Pon {
                                claimed_tile,
                                relative_player: _,
                                tiles: _,
                            } => discarded_tile == claimed_tile,
                            Meld::Kan(_) => {
                                // No restrictions on what is not allowed to be discarded.
                                false
                            }
                        }
                    }
                    None => false,
                }
            } else {
                false
            }
        };
        if violated_kuikae {
            return Err(GameEventError::InvalidTileDiscard(*at));
        }

        Ok(())
    }

    /// Validate the [`GameEvent::EndGame`] event.
    ///
    /// # Errors
    ///
    /// Will return `Err` if:
    ///
    /// - The round has not ended.
    /// - The tobi rule is enabled and none of the players have fewer than 0 points and the game has not reach the game length.
    /// - None of the players have met the minimum point threshold to end the game.
    /// - If the dealer won the round and is not the point leader.
    fn validate_end_game(&self) -> Result<(), GameEventError> {
        // The following checks must be done in order:
        //   1. Check that the round ended.
        //   2. Check if the round is East 4/South 4 (or later).
        //     a. If a player is below zero points, end the game.
        //   3. If a player is below zero points, end the game.

        if !matches!(self.round_stage(), RoundStage::Ended) {
            return Err(GameEventError::InvalidGameEnd);
        }

        // The game does **not** end if:
        //   - The tobi rule is not enabled.
        //   - The tobi rule is enabled and none of the players have fewer than 0 points.
        let tobi_enabled_and_any_player_score_below_zero = self.game_rules().tobi()
            && self
                .players()
                .iter()
                .any(|(_id, player)| player.points() < 0);

        let game_not_reached_game_length = {
            // NOTE: In the rare chance the round rotation goes back around to `East`, this logic wouldn't work.
            //   Does the game just end after West 4 then, no matter what every player's scores are?
            let round_wind_within_game_length = match self.game_rules().game_length() {
                GameLength::Tonpuusen => {
                    matches!(self.round_context().wind(), Wind::East)
                }
                GameLength::Hanchan => {
                    matches!(self.round_context().wind(), Wind::East | Wind::South)
                }
            };
            let round_number_not_at_end = self.round_number() < self.number_of_players().into();

            round_wind_within_game_length && round_number_not_at_end
        };

        if !tobi_enabled_and_any_player_score_below_zero {
            if game_not_reached_game_length {
                return Err(GameEventError::InvalidGameEnd);
            }

            // At least one player must be above the minimum point threshold to end the game.
            if self
                .players()
                .values()
                .all(|player| player.points() < self.game_rules().minimum_points_to_win().into())
            {
                return Err(GameEventError::InvalidGameEnd);
            }
        }

        // Check if the dealer won or is in tenpai.
        if self.history().len() > self.number_of_players().into() {
            // Up to the last `n` win events, where `n` is the number of players in the game.
            let last_n_win_events = self
                .history()
                .iter()
                .rev()
                .take(self.number_of_players().into())
                .filter(|event| {
                    matches!(
                        event,
                        GameEvent::EndRound {
                            reason: EndRoundReason::PlayerWon {
                                winner: _,
                                win_declaration: _
                            }
                        }
                    )
                });
            for event in last_n_win_events {
                match event {
                    GameEvent::EndRound {
                        reason:
                            EndRoundReason::PlayerWon {
                                winner,
                                win_declaration: _,
                            },
                    } => {
                        let player = match self.players().get(winner) {
                            Some(player) => player,
                            None => return Err(GameEventError::InvalidPlayer(*winner)),
                        };

                        let mut current_max_points = 0;
                        let mut non_dealer_has_max_points = false;
                        for player in self.players().values() {
                            if player.points() >= current_max_points {
                                current_max_points = player.points();

                                if !player.player_round_state().is_dealer() {
                                    non_dealer_has_max_points = true;
                                }
                            }
                        }
                        // NOTE: If the current dealer matches another player's points, maybe the game _does_ end.
                        if player.player_round_state().is_dealer()
                            && player.points() < current_max_points
                            || ((player.points() == current_max_points)
                                && non_dealer_has_max_points)
                        {
                            return Err(GameEventError::InvalidGameEnd);
                        }
                    }
                    GameEvent::EndRound {
                        reason: EndRoundReason::ExhaustiveDraw,
                    } => {
                        // There is always a dealer so calling `unwrap()` here is safe.
                        let dealer = self
                            .players()
                            .values()
                            .find(|player| player.player_round_state().is_dealer())
                            .unwrap();
                        let mut current_max_points = 0;
                        let mut non_dealer_has_max_points = false;
                        for player in self.players().values() {
                            if player.points() >= current_max_points {
                                current_max_points = player.points();

                                if !player.player_round_state().is_dealer() {
                                    non_dealer_has_max_points = true;
                                }
                            }
                        }
                        let in_tenpai = !dealer.hand().get_waits(self.round_context()).is_empty();

                        // NOTE: If the current dealer matches another player's points, maybe the game _does_ end.
                        if in_tenpai && dealer.points() < current_max_points
                            || ((dealer.points() == current_max_points)
                                && non_dealer_has_max_points)
                        {
                            return Err(GameEventError::InvalidGameEnd);
                        }
                    }
                    GameEvent::BeginGame
                    | GameEvent::DiscardTile { .. }
                    | GameEvent::EndGame
                    | GameEvent::Meld { .. }
                    | GameEvent::PlayerDisconnected { .. }
                    | GameEvent::PlayerJoined { .. }
                    | GameEvent::Riichi { .. } => {
                        unreachable!("last few game events were filtered to only have `EndRound`")
                    }
                }
            }
        }

        Ok(())
    }

    /// Validate the [`GameEvent::EndRound`] event.
    ///
    /// # Errors
    ///
    /// Will return `Err` if:
    ///
    /// - The game has not started.
    /// - The game has ended.
    /// - The round is being set up.
    /// - The round has ended.
    /// - The end round reason is for exhaustive draw and there are still live tiles in the wall.
    /// - The end round reason is that a player(s) won but isn't a player in the game.
    /// - The end round reason is that a player(s) won but the win is invalid.
    /// - The player attempted to call "ron" on a winning tile but is in furiten.
    fn validate_end_round(&self, reason: &EndRoundReason) -> Result<(), GameEventError> {
        if matches!(*self.stage(), Stage::PreGame | Stage::Ended)
            || matches!(
                *self.round_stage(),
                RoundStage::PreRound | RoundStage::Ended
            )
        {
            return Err(GameEventError::InvalidGameEnd);
        }

        match reason {
            EndRoundReason::ExhaustiveDraw => {
                if !self.round_context().wall().is_wall_exhausted() {
                    return Err(GameEventError::InvalidGameEnd);
                }
            }
            EndRoundReason::PlayerWon {
                winner,
                win_declaration,
            } => {
                let winning_player = match self.players().get(winner) {
                    Some(player) => player,
                    None => return Err(GameEventError::InvalidPlayer(*winner)),
                };

                let winning_player_waits = winning_player.hand().get_waits(self.round_context());
                if winning_player_waits.is_empty() {
                    // Player is not in tenpai.
                    return Err(GameEventError::InvalidGameEnd);
                }

                match win_declaration {
                    WinDeclaration::Ron {
                        deal_in_player,
                        tile,
                    } => match self.round_context().last_discard() {
                        Some(last_discard) => {
                            let mut winning_hand = Hand::new(winning_player.hand().tiles().clone());
                            *winning_hand.win_declaration_mut() = Some(*win_declaration);
                            let winning_hand_yaku = winning_hand.calculate_yaku(
                                self.game_rules(),
                                self.round_context(),
                                winning_player.player_round_state(),
                            );
                            let has_valid_yaku = winning_hand_yaku.len() > 0;
                            for wait in winning_player_waits.iter() {
                                if winning_player.hand().discards().binary_search(wait).is_ok() {
                                    // Player is in discard furiten.
                                    return Err(GameEventError::InvalidGameEnd);
                                }
                            }

                            if deal_in_player != last_discard.player()
                                || deal_in_player == winning_player.player_round_state().wind()
                                || tile != last_discard.tile()
                                || !winning_player_waits.contains(tile)
                                || winning_player.player_round_state().furiten().is_some()
                                || !has_valid_yaku
                            {
                                return Err(GameEventError::InvalidGameEnd);
                            }
                        }
                        None => return Err(GameEventError::InvalidGameEnd),
                    },
                    WinDeclaration::Tsumo(tile) => {
                        if self.active_player() != winner {
                            return Err(GameEventError::NotPlayerTurn(*winner));
                        }

                        match winning_player.hand().drawn_tile() {
                            Some(drawn_tile) => {
                                let mut winning_hand =
                                    Hand::new(winning_player.hand().tiles().clone());
                                *winning_hand.win_declaration_mut() = Some(*win_declaration);
                                let winning_hand_yaku = winning_hand.calculate_yaku(
                                    self.game_rules(),
                                    self.round_context(),
                                    winning_player.player_round_state(),
                                );
                                let has_valid_yaku = winning_hand_yaku.len() > 0;

                                if tile != drawn_tile
                                    || !winning_player_waits.contains(tile)
                                    || !has_valid_yaku
                                {
                                    return Err(GameEventError::InvalidGameEnd);
                                }
                            }
                            None => return Err(GameEventError::InvalidGameEnd),
                        }
                    }
                }
            }
        }

        Ok(())
    }

    /// Validate the [`GameEvent::Meld`] event.
    ///
    /// # Errors
    ///
    /// Will return `Err` if:
    ///
    /// - The game has not started.
    /// - The game has ended.
    /// - The round is being set up.
    /// - The round has ended.
    /// - The player for this event is not in the game.
    /// - There are no more live tiles in the wall (wall is exhausted).
    /// - In the 3-player variant (sanma), the calling player is making a "chii" call.
    /// - In the 3-player variant (sanma), the player the tile is being taken from is the North seat.
    /// - The meld is invalid given their hand.
    /// - The player calls a meld on themselves (other than ankan).
    /// - Kuikae is not in play (nashi) and melding would violate it.
    /// - It is not the player's turn when calling ankan.
    /// - If the player has declared riichi and is not making an ankan meld.
    ///     - If making the ankan meld would change the shape of the hand.
    fn validate_meld(&self, player_id: &PlayerId, meld: &Meld) -> Result<(), GameEventError> {
        if matches!(*self.stage(), Stage::PreGame | Stage::Ended)
            || matches!(
                *self.round_stage(),
                RoundStage::PreRound | RoundStage::Ended
            )
        {
            return Err(GameEventError::NotInGame);
        }

        let player = match self.players().get(player_id) {
            Some(player) => player,
            None => return Err(GameEventError::InvalidPlayer(*player_id)),
        };

        if self.round_context().wall().is_wall_exhausted() {
            return Err(GameEventError::InvalidMeld(*player_id, *meld));
        }

        // 3-player: Check that the player the tile is claimed from is not from the "North" seat and that the meld type is not a "chii".
        if self.number_of_players() == 3 {
            match meld {
                Meld::Chii {
                    claimed_tile: _,
                    relative_player: _,
                    tiles: _,
                } => return Err(GameEventError::InvalidMeld(*player_id, *meld)),
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles: _ } => {}
                    KanType::Daiminkan {
                        claimed_tile: _,
                        relative_player,
                        tiles: _,
                    } => {
                        if player
                            .player_round_state()
                            .wind()
                            .from_relative_position(relative_player)
                            == Wind::North
                        {
                            return Err(GameEventError::InvalidMeld(*player_id, *meld));
                        }
                    }
                    KanType::Shominkan {
                        claimed_tile: _,
                        relative_player,
                        tiles: _,
                    } => {
                        if player
                            .player_round_state()
                            .wind()
                            .from_relative_position(relative_player)
                            == Wind::North
                        {
                            return Err(GameEventError::InvalidMeld(*player_id, *meld));
                        }
                    }
                },
                Meld::Pon {
                    claimed_tile: _,
                    relative_player,
                    tiles: _,
                } => {
                    if player
                        .player_round_state()
                        .wind()
                        .from_relative_position(relative_player)
                        == Wind::North
                    {
                        return Err(GameEventError::InvalidMeld(*player_id, *meld));
                    }
                }
            }
        }

        let declared_riichi = player.player_round_state().riichi().is_some();

        match meld {
            Meld::Chii {
                claimed_tile,
                relative_player: _,
                tiles,
            } => {
                let meld_options = player
                    .hand()
                    .chii_options(self.round_context(), player.player_round_state());
                if meld_options.is_none()
                    || !meld_options
                        .unwrap()
                        .iter()
                        .any(|meld_option| meld == meld_option)
                    || declared_riichi
                {
                    return Err(GameEventError::InvalidMeld(*player_id, *meld));
                }

                // Player has called 3 times and is left with 4 tiles in the hand.
                // The expected shape is a set of tiles in sequence such as `2345m` and this is checking that the call doesn't violate kuikae with a claimed tile on either end such as either `2m` or `5m`.
                if player.hand().tiles().len() == 4 {
                    let violated_kuikae = {
                        if !self.game_rules().kuikae() {
                            // Tiles that would be left over for discarding.
                            let remaining_tiles = {
                                let hand = {
                                    // Temporary copy of the tiles in the hand to filter out the tiles that would be included in the meld.
                                    let mut hand = player.hand().clone();

                                    hand.apply_meld(
                                        self.round_context(),
                                        player.player_round_state(),
                                        meld,
                                    )
                                    .unwrap();

                                    hand
                                };

                                hand.tiles().clone()
                            };

                            // This allows for a hand shape such as `3455p` and calling a `4m` to be left with `4m` and `5m`.
                            // The player would be forced to discard the `5m` in the example.
                            let remaining_is_same_as_claimed = remaining_tiles
                                .as_ref()
                                .iter()
                                .all(|tile| tile == claimed_tile);
                            let remaining_completes_lower_end = claimed_tile
                                == tiles.get(2).unwrap()
                                && remaining_tiles
                                    .binary_search(&tiles.first().unwrap().previous_tile())
                                    .is_ok();
                            let remaining_completed_higher_end = claimed_tile
                                == tiles.first().unwrap()
                                && remaining_tiles
                                    .binary_search(&tiles.get(2).unwrap().next_tile())
                                    .is_ok();

                            remaining_is_same_as_claimed
                                || remaining_completes_lower_end
                                || remaining_completed_higher_end
                        } else {
                            false
                        }
                    };
                    if violated_kuikae {
                        return Err(GameEventError::InvalidMeld(*player_id, *meld));
                    }
                }
            }
            Meld::Kan(kan_type) => {
                match player
                    .hand()
                    .kan_option(self.round_context(), player.player_round_state())
                {
                    Some(Meld::Kan(calculated_kan_type)) => {
                        if *kan_type != calculated_kan_type {
                            return Err(GameEventError::InvalidMeld(*player_id, *meld));
                        }

                        // There are no differences between 3-player and 4-player for "ankan".
                        match kan_type {
                            KanType::Ankan { tiles } => {
                                if self.active_player() != player_id {
                                    return Err(GameEventError::NotPlayerTurn(*player_id));
                                }

                                if declared_riichi {
                                    let current_waits =
                                        player.hand().get_waits(self.round_context());

                                    let waits_after_meld = {
                                        let mut temp_hand = player.hand().clone();
                                        temp_hand.melds_mut().push(*meld);

                                        // The tile should exist in the hand because of earlier validation so unwrapping should be okay here.
                                        // Remove the 4 copies of the called meld tiles from the hand.
                                        for _ in 0..3 {
                                            let tile_idx = temp_hand
                                                .tiles()
                                                .binary_search(tiles.first().unwrap())
                                                .unwrap();
                                            temp_hand.tiles_mut().remove(tile_idx);
                                        }

                                        temp_hand.get_waits(self.round_context())
                                    };

                                    if waits_after_meld != current_waits {
                                        return Err(GameEventError::InvalidMeld(*player_id, *meld));
                                    }
                                }
                            }
                            _ => {
                                if declared_riichi {
                                    return Err(GameEventError::InvalidMeld(*player_id, *meld));
                                }
                            }
                        }
                    }
                    _ => return Err(GameEventError::InvalidMeld(*player_id, *meld)),
                }
            }
            Meld::Pon {
                claimed_tile: _,
                relative_player: _,
                tiles: _,
            } => {
                let meld_options = player
                    .hand()
                    .pon_options(self.round_context(), player.player_round_state());
                if meld_options.is_none()
                    || !meld_options
                        .unwrap()
                        .iter()
                        .any(|meld_option| meld == meld_option)
                    || declared_riichi
                {
                    return Err(GameEventError::InvalidMeld(*player_id, *meld));
                }
            }
        }

        Ok(())
    }

    /// Validate the [`GameEvent::PlayerDisconnected`] event.
    ///
    /// # Errors
    ///
    /// Will return `Err` if the player is not in the game.
    fn validate_player_disconnected(&self, player_id: &PlayerId) -> Result<(), GameEventError> {
        if !self.players().contains_key(player_id) {
            return Err(GameEventError::InvalidPlayer(*player_id));
        }

        Ok(())
    }

    /// Validate the [`GameEvent::PlayerJoined`] event.
    ///
    /// # Errors
    ///
    /// Will return `Err` if:
    ///
    /// - The game has ended.
    /// - The player is already in the game and is attempting to join again.
    /// - The player is attempting to join the game when the game has already started.
    /// - The number of players in the game equals or exceeds the expected number of players.
    fn validate_player_joined(
        &self,
        player_id: &PlayerId,
        username: &str,
    ) -> Result<(), GameEventError> {
        if *self.stage() == Stage::Ended {
            return Err(GameEventError::JoinGameError(
                ConnectionErrorReason::GameEnded,
            ));
        }

        match self.players().get(player_id) {
            Some(player) => {
                // The same player cannot join multiple times.
                if player.connected() {
                    return Err(GameEventError::JoinGameError(
                        ConnectionErrorReason::DuplicatePlayer(*player_id, username.to_string()),
                    ));
                }
            }
            None => {
                if *self.stage() != Stage::PreGame {
                    return Err(GameEventError::InvalidPlayer(*player_id));
                }
            }
        }

        if self.players().len() >= self.number_of_players().into() {
            return Err(GameEventError::JoinGameError(
                ConnectionErrorReason::GameFull,
            ));
        }

        Ok(())
    }

    /// Validate the [`GameEvent::Riichi`] event.
    ///
    /// # Errors
    ///
    /// Will return `Err` if:
    ///
    /// - The game has not started.
    /// - The game has ended.
    /// - The round is being set up.
    /// - The round has ended.
    /// - The player declaring riichi is not in the game.
    /// - It is not the player's turn.
    /// - The player's hand is not concealed.
    /// - The player's hand is not in tenpai.
    /// - If the tobi rule is enabled (ari) and the player does not have enough points to bet.
    /// - There are not enough live tiles for the player to be able to draw one last time. For a 4-player game, there must be 4 remaining live tiles.
    fn validate_riichi(&self, player_id: &PlayerId) -> Result<(), GameEventError> {
        if matches!(*self.stage(), Stage::PreGame | Stage::Ended)
            || matches!(
                *self.round_stage(),
                RoundStage::PreRound | RoundStage::Ended
            )
        {
            return Err(GameEventError::NotInGame);
        }

        let player = match self.players().get(player_id) {
            Some(player) => player,
            None => return Err(GameEventError::InvalidPlayer(*player_id)),
        };

        if self.active_player() != player_id {
            return Err(GameEventError::NotPlayerTurn(*player_id));
        }

        let hand_not_in_tenpai = player.hand().get_waits(self.round_context()).is_empty();
        let not_enough_points_to_bet = self.game_rules().tobi()
            && player.points() < self.game_rules().riichi_bet_value().into();
        // There must be enough live tiles for the player to be able to draw at least one more tile in an uninterrupted set of turns.
        let not_enough_live_tiles_left = (self.round_context().wall().last_live_tile_idx()
            - self.round_context().wall().current_tile_idx())
            < self.number_of_players().into();
        if player.hand().hand_visibility() == HandVisibility::Open
            || hand_not_in_tenpai
            || not_enough_points_to_bet
            || not_enough_live_tiles_left
        {
            return Err(GameEventError::InvalidRiichi(*player_id));
        }

        Ok(())
    }
}

#[cfg(test)]
mod tests {
    use jikaze_riichi_core::error::IncrementWallIndexError;
    use jikaze_riichi_core::game_rules::NumberOfAkadora;
    use jikaze_riichi_core::player::player_round_state::Furiten;
    use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    use jikaze_riichi_core::round::{LastDiscard, LastKan};
    use jikaze_riichi_core::tile::{Tile, TileSet};

    use super::*;

    #[test]
    fn dispatch_on_player_joined_adds_player_to_pool_in_stage_pre_game()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;
        *game_state.round_stage_mut() = RoundStage::PreRound;

        let player_id = 1;

        game_state.dispatch(&GameEvent::PlayerJoined {
            player_id,
            username: String::from("testusername1"),
        })?;

        let actual_player = game_state.players().get(&player_id).unwrap();

        assert_eq!(actual_player.id(), player_id);
        assert_eq!(*actual_player.hand(), Hand::default());
        assert_eq!(actual_player.username(), "testusername1");
        // Since the seat is randomly assigned, there's little point in checking it.
        dbg!(actual_player.player_round_state().wind());

        Ok(())
    }

    #[test]
    fn dispatch_on_two_players_joined_assigns_unique_seats() -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;
        *game_state.round_stage_mut() = RoundStage::PreRound;

        let player1_id = 1;
        game_state.dispatch(&GameEvent::PlayerJoined {
            player_id: player1_id,
            username: String::from("testusername1"),
        })?;
        let player2_id = 2;
        game_state.dispatch(&GameEvent::PlayerJoined {
            player_id: player2_id,
            username: String::from("testusername2"),
        })?;

        let actual_player1 = game_state.players().get(&player1_id).unwrap();
        assert_eq!(actual_player1.id(), player1_id);
        assert_eq!(*actual_player1.hand(), Hand::default());
        assert_eq!(actual_player1.username(), "testusername1");

        let actual_player2 = game_state.players().get(&player2_id).unwrap();
        assert_eq!(actual_player2.id(), player2_id);
        assert_eq!(*actual_player2.hand(), Hand::default());
        assert_eq!(actual_player2.username(), "testusername2");

        // NOTE: This test is technically flaky since the randomization means that they could potentially collide "randomly".
        // The `consume()` code should prevent the seats from ever colliding so theoretically this should always pass.
        assert_ne!(
            actual_player1.player_round_state().wind(),
            actual_player2.player_round_state().wind()
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_player_joined_reconnects_player_if_previously_disconnected()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;
        *game_state.round_stage_mut() = RoundStage::PreRound;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.disconnect();

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::PlayerJoined {
            player_id: player1_id,
            username: String::from("testusername1"),
        })?;

        let actual = game_state.players();

        assert!(actual.get(&player1_id).unwrap().connected());

        Ok(())
    }

    #[test]
    fn dispatch_on_begin_game_works_with_4_players() -> Result<(), GameEventError> {
        let mut game_state = GameState::default();

        let player1 = { Player::new(1, Hand::default(), Wind::East, "testusername1".to_string()) };
        game_state.players_mut().insert(1, player1);
        let player2 = { Player::new(2, Hand::default(), Wind::East, "testusername2".to_string()) };
        game_state.players_mut().insert(2, player2);
        let player3 = { Player::new(3, Hand::default(), Wind::East, "testusername3".to_string()) };
        game_state.players_mut().insert(3, player3);
        let player4 = { Player::new(4, Hand::default(), Wind::East, "testusername4".to_string()) };
        game_state.players_mut().insert(4, player4);

        game_state.dispatch(&GameEvent::BeginGame)?;

        let actual_stage = game_state.stage();

        let expected_stage = Stage::InGame;
        assert_eq!(actual_stage, &expected_stage);

        Ok(())
    }

    #[test]
    fn dispatch_on_end_game_works_when_player_below_zero_points() -> Result<(), GameEventError> {
        let mut game_state = GameState::default();

        *game_state.round_stage_mut() = RoundStage::Ended;

        let player1 = {
            let mut player =
                Player::new(1, Hand::default(), Wind::East, "testusername1".to_string());

            *player.points_mut() = -1000;

            player
        };
        game_state.players_mut().insert(1, player1);
        let player2 = { Player::new(2, Hand::default(), Wind::East, "testusername2".to_string()) };
        game_state.players_mut().insert(2, player2);
        let player3 = { Player::new(3, Hand::default(), Wind::East, "testusername3".to_string()) };
        game_state.players_mut().insert(3, player3);
        let player4 = { Player::new(4, Hand::default(), Wind::East, "testusername4".to_string()) };
        game_state.players_mut().insert(4, player4);

        game_state.dispatch(&GameEvent::EndGame)?;

        let actual_stage = game_state.stage();

        let expected_stage = Stage::Ended;
        assert_eq!(actual_stage, &expected_stage);

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_works() -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 3,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_hand = TileSet::new(vec![
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
        ]);

        // Player's hand should have the new tile.
        assert_eq!(*actual_player.hand().tiles(), expected_hand);
        // Player's discards should have the discarded tile.
        // `matches!()` must be used because of the custom `PartialEq` trait implementation for the 5s and red 5s.
        assert!(matches!(
            actual_player.hand().discards().as_ref().first().unwrap(),
            Tile::Manzu5
        ));
        // Ensure that the display discard pile matches the source of truth, the discards in the `Hand`.
        // The display discard pile may get desynced from the source of truth from meld calls but this is intentional.
        assert_eq!(actual_player.hand().discards(), actual_player.discards());
        // The player's last drawn tile is in the hand and shouldn't be in this field.
        assert!(actual_player.hand().drawn_tile().is_none());

        let actual_last_discard = game_state.round_context().last_discard().unwrap();
        let expected_last_discard = LastDiscard::new(Wind::East, Tile::Manzu5);
        // The discard should be in the round context.
        assert_eq!(actual_last_discard, expected_last_discard);

        let actual_first_uninterrupted_turn = game_state.round_context().first_uninterrupted_turn();
        // Not everyone has taken their first uninterrupted turn yet.
        assert!(actual_first_uninterrupted_turn);

        let expected_furiten = None;
        assert_eq!(
            *actual_player.player_round_state().furiten(),
            expected_furiten
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_tile_discard_sets_last_meld_none() -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);
            *player.player_round_state_mut().last_meld_mut() = Some(Meld::Chii {
                claimed_tile: Tile::Manzu3,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 3,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_last_meld = None;

        assert_eq!(
            *actual_player.player_round_state().last_meld(),
            expected_last_meld
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_removes_temporary_furiten_with_drawn_tile()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);
            *player.player_round_state_mut().furiten_mut() = Some(Furiten::Temporary);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 3,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_furiten = None;
        assert_eq!(
            *actual_player.player_round_state().furiten(),
            expected_furiten
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_removes_temporary_furiten_with_tile_from_hand()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);
            *player.player_round_state_mut().furiten_mut() = Some(Furiten::Temporary);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = 1;

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 3,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_furiten = None;
        assert_eq!(
            *actual_player.player_round_state().furiten(),
            expected_furiten
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_maintains_discard_furiten_with_drawn_tile()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);
            *player.player_round_state_mut().furiten_mut() = Some(Furiten::Discard);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 13,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_furiten = Some(Furiten::Discard);
        assert_eq!(
            *actual_player.player_round_state().furiten(),
            expected_furiten
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_maintains_discard_furiten_with_wait_tile_from_hand()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);
            *player.player_round_state_mut().furiten_mut() = Some(Furiten::Discard);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 3,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_furiten = Some(Furiten::Discard);
        assert_eq!(
            *actual_player.player_round_state().furiten(),
            expected_furiten
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_removes_discard_furiten_with_tile_from_hand()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu2,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().discards_mut().push(&Tile::Manzu4);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu8);
            *player.player_round_state_mut().furiten_mut() = Some(Furiten::Discard);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        // Change from a 4-7m wait to a 7m wait by discarding the 5m.
        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 2,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_furiten = None;
        assert_eq!(
            *actual_player.player_round_state().furiten(),
            expected_furiten
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_maintains_permanent_furiten_with_tile_drawn()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.player_round_state_mut().furiten_mut() = Some(Furiten::Permanent);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 13,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_furiten = Some(Furiten::Permanent);
        assert_eq!(
            *actual_player.player_round_state().furiten(),
            expected_furiten
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_sets_first_uninterrupted_turn_to_false_after_everyone_has_taken_their_turn()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        // "Take" everyone's first turn (4-player game) without interruptions (i.e. melds).
        for _ in 0..4 {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 3,
        })?;

        let actual_first_uninterrupted_turn = game_state.round_context().first_uninterrupted_turn();
        assert!(!actual_first_uninterrupted_turn);

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_removes_drawn_tile_from_hand_and_adds_to_discard_pile()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = 1;

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 13,
        })?;

        let actual = game_state.players().get(&player1_id).unwrap();
        let expected_hand = TileSet::new(vec![
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
        ]);

        assert_eq!(*actual.hand().tiles(), expected_hand);
        // `matches!()` must be used because of the custom `PartialEq` trait implementation for the 5s and red 5s.
        assert!(matches!(
            actual.hand().discards().as_ref().first().unwrap(),
            Tile::ManzuRed5
        ));
        assert!(actual.hand().drawn_tile().is_none());

        Ok(())
    }

    #[test]
    fn dispatch_on_discard_tile_ends_round_with_abortive_draw_with_all_players_discarding_same_wind_in_first_turn()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::WindWest);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername2"),
            );
            player.hand_mut().discards_mut().push(&Tile::WindWest);

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername3"),
            );
            player.hand_mut().discards_mut().push(&Tile::WindWest);

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername4"),
            );
            player.hand_mut().discards_mut().push(&Tile::WindWest);

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 13,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_hand = TileSet::new(vec![
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
        ]);

        let actual_round_stage = game_state.round_stage();
        let expected_round_stage = RoundStage::Ended;
        assert_eq!(*actual_round_stage, expected_round_stage);

        // Player's hand should have the new tile.
        assert_eq!(*actual_player.hand().tiles(), expected_hand);
        // Player's discards should have the discarded tile.
        // `matches!()` must be used because of the custom `PartialEq` trait implementation for the 5s and red 5s.
        assert!(matches!(
            actual_player.hand().discards().as_ref().first().unwrap(),
            Tile::WindWest
        ));
        // The player's last drawn tile is in the hand and shouldn't be in this field.
        assert!(actual_player.hand().drawn_tile().is_none());

        Ok(())
    }

    #[test]
    fn dispatch_on_tile_discard_reveals_new_dora_with_last_kan_daiminkan()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().rinshanpai_mut() = Some(Tile::ManzuRed5);
            *player.player_round_state_mut().last_meld_mut() =
                Some(Meld::Kan(KanType::Daiminkan {
                    claimed_tile: Tile::Manzu3,
                    relative_player: PlayerRelativePosition::Kamicha,
                    tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
                }));

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;
        *game_state.round_context_mut().last_kan_mut() = Some(LastKan::new(
            Wind::East,
            KanType::Daiminkan {
                claimed_tile: Tile::Manzu3,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            },
        ));

        let expected_last_live_tile_idx = 121;
        assert_eq!(
            game_state.round_context().wall().last_live_tile_idx(),
            expected_last_live_tile_idx
        );

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 3,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_last_meld = None;

        assert_eq!(
            *actual_player.player_round_state().last_meld(),
            expected_last_meld
        );

        let expected_last_kan = None;
        assert_eq!(game_state.round_context().last_kan(), &expected_last_kan);

        let expected_last_live_tile_idx = 120;
        assert_eq!(
            game_state.round_context().wall().last_live_tile_idx(),
            expected_last_live_tile_idx
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_tile_discard_reveals_new_dora_with_last_kan_shominkan()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().rinshanpai_mut() = Some(Tile::ManzuRed5);
            *player.player_round_state_mut().last_meld_mut() =
                Some(Meld::Kan(KanType::Shominkan {
                    claimed_tile: Tile::Manzu3,
                    relative_player: PlayerRelativePosition::Kamicha,
                    tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
                }));

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;
        *game_state.round_context_mut().last_kan_mut() = Some(LastKan::new(
            Wind::East,
            KanType::Shominkan {
                claimed_tile: Tile::Manzu3,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            },
        ));

        let expected_last_live_tile_idx = 121;
        assert_eq!(
            game_state.round_context().wall().last_live_tile_idx(),
            expected_last_live_tile_idx
        );

        game_state.dispatch(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 3,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_last_meld = None;

        assert_eq!(
            *actual_player.player_round_state().last_meld(),
            expected_last_meld
        );

        let expected_last_kan = None;
        assert_eq!(game_state.round_context().last_kan(), &expected_last_kan);

        let expected_last_live_tile_idx = 120;
        assert_eq!(
            game_state.round_context().wall().last_live_tile_idx(),
            expected_last_live_tile_idx
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_exhaustive_draw_rotates_seat_order_with_no_one_in_tenpai()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 2);
        assert_eq!(game_state.honba(), 1);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_exhaustive_draw_distributes_points_with_one_player_in_tenpai()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 2);
        assert_eq!(game_state.honba(), 0);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points + 3000);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_exhaustive_draw_distributes_points_with_two_players_in_tenpai()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 2);
        assert_eq!(game_state.honba(), 0);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points - 1500);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points + 1500);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points + 1500);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points - 1500);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_exhaustive_draw_distributes_points_with_three_players_in_tenpai()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 2);
        assert_eq!(game_state.honba(), 0);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points - 3000);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points + 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points + 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points + 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_exhaustive_draw_maintains_seats_with_four_players_in_tenpai()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 1);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_player_won_takes_points_from_everyone_with_dealer_tsumo()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::from(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu2),
            },
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 1);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 6000);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 2000);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points - 2000);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points - 2000);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_player_won_takes_points_from_last_discarder_with_dealer_ron()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::from(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player2_id;
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu2,
                },
            },
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 1);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 3900);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 3900);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_player_won_takes_points_from_everyone_with_non_dealer_tsumo()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::from(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu2),
            },
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 2);
        assert_eq!(game_state.honba(), 0);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 4000);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 2000);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_player_won_takes_points_from_last_discarder_with_non_dealer_ron()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::from(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player2_id;
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::East, Tile::Manzu2));

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::East,
                    tile: Tile::Manzu2,
                },
            },
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 2);
        assert_eq!(game_state.honba(), 0);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 2600);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 2600);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_player_won_adds_honba_with_tsumo() -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::from(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;
        *game_state.honba_mut() = 2;
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu2),
            },
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 3);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 6600);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 2200);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points - 2200);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points - 2200);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_player_won_adds_honba_with_ron() -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::from(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player2_id;
        *game_state.honba_mut() = 2;
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu2,
                },
            },
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 3);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 4500);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 4500);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_player_won_adds_riichi_bets_with_tsumo()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::from(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;
        *game_state.riichi_bets_mut() = 2;
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu2),
            },
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 1);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 8000);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 2000);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points - 2000);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points - 2000);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_with_player_won_adds_riichi_bets_with_ron()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::from(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player2_id;
        *game_state.riichi_bets_mut() = 2;
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu2,
                },
            },
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 1);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 5900);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 3900);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_rotates_seats_and_round_when_in_last_wind_round()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        game_state.round_number_set(4);

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::South);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 0);
        assert_eq!(game_state.riichi_bets(), 0);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points + 3000);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_maintains_seats_with_renchan_and_dealer_in_tenpai_with_riichi()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.riichi_bets_mut() = 1;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = starting_points;
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        })?;

        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 1);
        assert_eq!(game_state.riichi_bets(), 1);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 3000);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_keeps_game_going_when_player_reaches_0_points()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.riichi_bets_mut() = 1;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = starting_points;
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = 1000;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        })?;

        assert_eq!(*game_state.stage(), Stage::InGame);
        assert_eq!(*game_state.round_stage(), RoundStage::PreRound);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 1);
        assert_eq!(game_state.riichi_bets(), 1);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 3000);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), 0);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_end_round_ends_game_when_player_falls_below_0_points()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.riichi_bets_mut() = 1;

        let starting_points = game_state.game_rules().starting_points().into();

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = starting_points;
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::South,
                String::from("testusername2"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player2_id, player2);
        let player3_id = 3;
        let player3 = {
            let mut player = Player::new(
                player3_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::West,
                String::from("testusername3"),
            );
            *player.points_mut() = starting_points;

            player
        };
        game_state.players_mut().insert(player3_id, player3);
        let player4_id = 4;
        let player4 = {
            let mut player = Player::new(
                player4_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::WindWest,
                    Tile::WindNorth,
                ])),
                Wind::North,
                String::from("testusername4"),
            );
            *player.points_mut() = 0;

            player
        };
        game_state.players_mut().insert(player4_id, player4);

        *game_state.active_player_mut() = player1_id;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        game_state.dispatch(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        })?;

        assert_eq!(*game_state.stage(), Stage::Ended);
        assert_eq!(*game_state.round_stage(), RoundStage::Ended);
        assert_eq!(*game_state.round_context().wind(), Wind::East);
        assert_eq!(game_state.round_number(), 1);
        assert_eq!(game_state.honba(), 1);
        assert_eq!(game_state.riichi_bets(), 1);
        {
            let player = game_state.players().get(&player1_id).unwrap();

            assert_eq!(player.points(), starting_points + 3000);
            assert_eq!(*player.player_round_state().wind(), Wind::East);
        }
        {
            let player = game_state.players().get(&player2_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::South);
        }
        {
            let player = game_state.players().get(&player3_id).unwrap();

            assert_eq!(player.points(), starting_points - 1000);
            assert_eq!(*player.player_round_state().wind(), Wind::West);
        }
        {
            let player = game_state.players().get(&player4_id).unwrap();

            assert_eq!(player.points(), -1000);
            assert_eq!(*player.player_round_state().wind(), Wind::North);
        }

        Ok(())
    }

    #[test]
    fn dispatch_on_meld_chii_removes_tile_from_opponent_and_hand_and_adds_meld()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            );
            player.discards_mut().push(&Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        game_state.dispatch(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        })?;

        assert_eq!(game_state.active_player(), &player1_id);

        let actual_player1 = game_state.players().get(&player1_id).unwrap();
        let actual_player2 = game_state.players().get(&player2_id).unwrap();

        let expected_last_meld = Some(Meld::Chii {
            claimed_tile: Tile::Manzu2,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
        });
        assert_eq!(
            actual_player1.player_round_state().last_meld(),
            &expected_last_meld
        );

        let expected_last_kan = None;
        assert_eq!(game_state.round_context().last_kan(), &expected_last_kan);

        let expected_melds = vec![Meld::Chii {
            claimed_tile: Tile::Manzu2,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
        }];
        assert_eq!(actual_player1.hand().melds(), &expected_melds);

        let expected_player1_hand_tiles = TileSet::new(vec![
            Tile::Manzu1,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
        ]);
        assert_eq!(actual_player1.hand().tiles(), &expected_player1_hand_tiles);

        let expected_player2_discards = TileSet::new(Vec::new());
        assert_eq!(actual_player2.hand().discards(), &expected_player2_discards);

        Ok(())
    }

    #[test]
    fn dispatch_on_meld_pon_removes_tile_from_opponent_and_hand_and_adds_meld()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu2,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            );
            player.discards_mut().push(&Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        game_state.dispatch(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Pon {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            },
        })?;

        assert_eq!(game_state.active_player(), &player1_id);

        let actual_player1 = game_state.players().get(&player1_id).unwrap();
        let actual_player2 = game_state.players().get(&player2_id).unwrap();

        let expected_last_meld = Some(Meld::Pon {
            claimed_tile: Tile::Manzu2,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
        });
        assert_eq!(
            actual_player1.player_round_state().last_meld(),
            &expected_last_meld
        );

        let expected_last_kan = None;
        assert_eq!(game_state.round_context().last_kan(), &expected_last_kan);

        let expected_melds = vec![Meld::Pon {
            claimed_tile: Tile::Manzu2,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
        }];
        assert_eq!(actual_player1.hand().melds(), &expected_melds);

        let expected_player1_hand_tiles = TileSet::new(vec![
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
        ]);
        assert_eq!(actual_player1.hand().tiles(), &expected_player1_hand_tiles);

        let expected_player2_discards = TileSet::new(Vec::new());
        assert_eq!(actual_player2.hand().discards(), &expected_player2_discards);

        Ok(())
    }

    #[test]
    fn dispatch_on_meld_ankan_removes_tiles_from_hand_and_adds_meld() -> Result<(), GameEventError>
    {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::new(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu2,
                    Tile::Manzu2,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Ankan {
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            }),
        })?;

        assert_eq!(game_state.active_player(), &player1_id);

        let actual_player1 = game_state.players().get(&player1_id).unwrap();

        let expected_last_meld = Some(Meld::Kan(KanType::Ankan {
            tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
        }));
        assert_eq!(
            actual_player1.player_round_state().last_meld(),
            &expected_last_meld
        );

        let expected_last_kan = Some(LastKan::new(
            Wind::East,
            KanType::Ankan {
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            },
        ));
        assert_eq!(game_state.round_context().last_kan(), &expected_last_kan);

        let expected_melds = vec![Meld::Kan(KanType::Ankan {
            tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
        })];
        assert_eq!(actual_player1.hand().melds(), &expected_melds);

        let expected_player1_hand_tiles = TileSet::new(vec![
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
        ]);
        assert_eq!(actual_player1.hand().tiles(), &expected_player1_hand_tiles);

        let expected_rinshanpai = Some(Tile::DragonRed);
        assert_eq!(actual_player1.hand().rinshanpai(), &expected_rinshanpai);

        let expected_dora_indicators = vec![
            (Tile::DragonGreen, Tile::DragonGreen),
            (Tile::DragonGreen, Tile::DragonGreen),
        ];
        assert_eq!(
            game_state.round_context().wall().dora_indicator_tiles(),
            expected_dora_indicators
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_meld_daiminkan_removes_tile_from_opponent_and_hand_and_adds_meld()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::new(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu2,
                    Tile::Manzu2,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            );
            player.hand_mut().discards_mut().push(&Tile::Manzu2);
            player.discards_mut().push(&Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        game_state.dispatch(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            }),
        })?;

        assert_eq!(game_state.active_player(), &player1_id);

        let actual_player1 = game_state.players().get(&player1_id).unwrap();
        let actual_player2 = game_state.players().get(&player2_id).unwrap();

        let expected_last_meld = Some(Meld::Kan(KanType::Daiminkan {
            claimed_tile: Tile::Manzu2,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
        }));
        assert_eq!(
            actual_player1.player_round_state().last_meld(),
            &expected_last_meld
        );

        let expected_last_kan = Some(LastKan::new(
            Wind::East,
            KanType::Daiminkan {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            },
        ));
        assert_eq!(game_state.round_context().last_kan(), &expected_last_kan);

        let expected_melds = vec![Meld::Kan(KanType::Daiminkan {
            claimed_tile: Tile::Manzu2,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
        })];
        assert_eq!(actual_player1.hand().melds(), &expected_melds);

        let expected_player1_hand_tiles = TileSet::new(vec![
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
        ]);
        assert_eq!(actual_player1.hand().tiles(), &expected_player1_hand_tiles);

        let expected_player2_discards = TileSet::new(vec![Tile::Manzu2]);
        assert!(actual_player2.discards().is_empty());
        assert_eq!(actual_player2.hand().discards(), &expected_player2_discards);

        let expected_rinshanpai = Some(Tile::DragonRed);
        assert_eq!(actual_player1.hand().rinshanpai(), &expected_rinshanpai);

        // Another dora indicator is revealed only after the player discards a tile.
        let expected_dora_indicators = vec![(Tile::DragonGreen, Tile::DragonGreen)];
        assert_eq!(
            game_state.round_context().wall().dora_indicator_tiles(),
            expected_dora_indicators
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_meld_shominkan_removes_tile_from_hand_and_adds_meld()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::new_with_wall(
            GameRules::new_4p_default(),
            4,
            Wall::new(Box::new(create_unshuffled_tiles_4p(
                &NumberOfAkadora::Three,
            ))),
            [0; 32],
        );
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            });
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            }),
        })?;

        assert_eq!(game_state.active_player(), &player1_id);

        let actual_player1 = game_state.players().get(&player1_id).unwrap();

        let expected_last_meld = Some(Meld::Kan(KanType::Shominkan {
            claimed_tile: Tile::Manzu2,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
        }));
        assert_eq!(
            actual_player1.player_round_state().last_meld(),
            &expected_last_meld
        );

        let expected_last_kan = Some(LastKan::new(
            Wind::East,
            KanType::Shominkan {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            },
        ));
        assert_eq!(game_state.round_context().last_kan(), &expected_last_kan);

        let expected_melds = vec![Meld::Kan(KanType::Shominkan {
            claimed_tile: Tile::Manzu2,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
        })];
        assert_eq!(actual_player1.hand().melds(), &expected_melds);

        let expected_player1_hand_tiles = TileSet::new(vec![
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
        ]);
        assert_eq!(actual_player1.hand().tiles(), &expected_player1_hand_tiles);

        let expected_rinshanpai = Some(Tile::DragonRed);
        assert_eq!(actual_player1.hand().rinshanpai(), &expected_rinshanpai);

        // Another dora indicator is revealed only after the player discards a tile.
        let expected_dora_indicators = vec![(Tile::DragonGreen, Tile::DragonGreen)];
        assert_eq!(
            game_state.round_context().wall().dora_indicator_tiles(),
            expected_dora_indicators
        );

        Ok(())
    }

    #[test]
    fn dispatch_on_player_disconnect_removes_player_in_pre_game_stage() -> Result<(), GameEventError>
    {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;
        game_state.available_seats = vec![Wind::West, Wind::North];

        let player1_id = 1;
        let player1 = Player::new(
            player1_id,
            Hand::new(TileSet::new(vec![
                Tile::Manzu1,
                Tile::Manzu3,
                Tile::Manzu4,
                Tile::Manzu5,
                Tile::Manzu6,
                Tile::Manzu7,
                Tile::Manzu8,
                Tile::Souzu4,
                Tile::Souzu5,
                Tile::Souzu6,
                Tile::Souzu8,
                Tile::Souzu8,
                Tile::Souzu8,
            ])),
            Wind::East,
            String::from("testusername1"),
        );
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = Player::new(
            player2_id,
            Hand::new(TileSet::new(vec![
                Tile::Manzu1,
                Tile::Manzu3,
                Tile::Manzu4,
                Tile::Manzu5,
                Tile::Manzu6,
                Tile::Manzu7,
                Tile::Manzu8,
                Tile::Souzu4,
                Tile::Souzu5,
                Tile::Souzu6,
                Tile::Souzu8,
                Tile::Souzu8,
                Tile::Souzu8,
            ])),
            Wind::South,
            String::from("testusername2"),
        );
        game_state.players_mut().insert(player2_id, player2.clone());

        game_state.dispatch(&GameEvent::PlayerDisconnected {
            player_id: player1_id,
        })?;

        let actual = game_state.players();

        let expected = HashMap::from([(player2_id, player2)]);
        assert_eq!(*actual, expected);

        let expected_available_seats = vec![Wind::West, Wind::North, Wind::East];
        assert_eq!(game_state.available_seats, expected_available_seats);

        Ok(())
    }

    #[test]
    fn dispatch_on_player_disconnect_sets_player_as_disconnected_in_in_game_stage()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;

        let player1_id = 1;
        let player1 = Player::new(
            player1_id,
            Hand::new(TileSet::new(vec![
                Tile::Manzu1,
                Tile::Manzu3,
                Tile::Manzu4,
                Tile::Manzu5,
                Tile::Manzu6,
                Tile::Manzu7,
                Tile::Manzu8,
                Tile::Souzu4,
                Tile::Souzu5,
                Tile::Souzu6,
                Tile::Souzu8,
                Tile::Souzu8,
                Tile::Souzu8,
            ])),
            Wind::East,
            String::from("testusername1"),
        );
        let mut player1_disconnected = player1.clone();
        player1_disconnected.disconnect();
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = Player::new(
            player2_id,
            Hand::new(TileSet::new(vec![
                Tile::Manzu1,
                Tile::Manzu3,
                Tile::Manzu4,
                Tile::Manzu5,
                Tile::Manzu6,
                Tile::Manzu7,
                Tile::Manzu8,
                Tile::Souzu4,
                Tile::Souzu5,
                Tile::Souzu6,
                Tile::Souzu8,
                Tile::Souzu8,
                Tile::Souzu8,
            ])),
            Wind::East,
            String::from("testusername2"),
        );
        game_state.players_mut().insert(player2_id, player2.clone());

        game_state.dispatch(&GameEvent::PlayerDisconnected {
            player_id: player1_id,
        })?;

        let actual = game_state.players();
        let expected = HashMap::from([(player1_id, player1_disconnected), (player2_id, player2)]);

        assert_eq!(*actual, expected);

        Ok(())
    }

    #[test]
    fn dispatch_on_player_disconnect_removes_player_in_ended_stage() -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::Ended;

        let player1_id = 1;
        let player1 = Player::new(
            player1_id,
            Hand::new(TileSet::new(vec![
                Tile::Manzu1,
                Tile::Manzu3,
                Tile::Manzu4,
                Tile::Manzu5,
                Tile::Manzu6,
                Tile::Manzu7,
                Tile::Manzu8,
                Tile::Souzu4,
                Tile::Souzu5,
                Tile::Souzu6,
                Tile::Souzu8,
                Tile::Souzu8,
                Tile::Souzu8,
            ])),
            Wind::East,
            String::from("testusername1"),
        );
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = Player::new(
            player2_id,
            Hand::new(TileSet::new(vec![
                Tile::Manzu1,
                Tile::Manzu3,
                Tile::Manzu4,
                Tile::Manzu5,
                Tile::Manzu6,
                Tile::Manzu7,
                Tile::Manzu8,
                Tile::Souzu4,
                Tile::Souzu5,
                Tile::Souzu6,
                Tile::Souzu8,
                Tile::Souzu8,
                Tile::Souzu8,
            ])),
            Wind::East,
            String::from("testusername2"),
        );
        game_state.players_mut().insert(player2_id, player2.clone());

        game_state.dispatch(&GameEvent::PlayerDisconnected {
            player_id: player1_id,
        })?;

        let actual = game_state.players();
        let expected = HashMap::from([(player2_id, player2)]);

        assert_eq!(*actual, expected);

        Ok(())
    }

    #[test]
    fn dispatch_on_riichi_sets_riichi_true_when_first_uninterrupted_turn_is_false()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername2"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::Riichi {
            player_id: player1_id,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let actual_player_round_state = actual_player.player_round_state();
        let expected_riichi = Some(Riichi::Riichi);
        assert_eq!(actual_player_round_state.riichi(), &expected_riichi);
        assert!(actual_player_round_state.furiten().is_none());
        assert_eq!(actual_player.points(), 24_000);

        Ok(())
    }

    #[test]
    fn dispatch_on_riichi_sets_double_riichi_true_when_first_uninterrupted_turn_is_true()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername2"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::Riichi {
            player_id: player1_id,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let actual_player_round_state = actual_player.player_round_state();
        let expected_riichi = Some(Riichi::DoubleRiichi);
        assert_eq!(actual_player_round_state.riichi(), &expected_riichi);
        assert!(actual_player_round_state.furiten().is_none());
        assert_eq!(actual_player.points(), 24_000);

        Ok(())
    }

    #[test]
    fn dispatch_on_riichi_sets_furiten_status_to_permanent_with_wait_in_discard()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().discards_mut().push(&Tile::Manzu2);
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername2"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::Riichi {
            player_id: player1_id,
        })?;

        let actual = game_state
            .players()
            .get(&player1_id)
            .unwrap()
            .player_round_state();
        let expected_riichi = Some(Riichi::Riichi);
        assert_eq!(actual.riichi(), &expected_riichi);
        let expected = Some(Furiten::Permanent);
        assert_eq!(*actual.furiten(), expected);

        Ok(())
    }

    #[test]
    fn dispatch_on_riichi_sets_player_points_below_zero_when_tobi_is_nashi()
    -> Result<(), GameEventError> {
        let mut game_rules = GameRules::new_4p_default();
        // "Nashi" is when tobi is `false`.
        *game_rules.tobi_mut() = false;
        let mut game_state = GameState::new(game_rules, 4);
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = 500;

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername2"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::Riichi {
            player_id: player1_id,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let actual_player_round_state = actual_player.player_round_state();
        let expected_riichi = Some(Riichi::Riichi);
        assert_eq!(actual_player_round_state.riichi(), &expected_riichi);
        assert!(actual_player_round_state.furiten().is_none());
        assert_eq!(actual_player.points(), -500);

        Ok(())
    }

    #[test]
    fn dispatch_on_riichi_ends_round_in_abortive_draw_when_all_players_call_riichi()
    -> Result<(), GameEventError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));
        *game_state
            .round_context_mut()
            .first_uninterrupted_turn_mut() = false;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };

        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername2"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player1_id;

        game_state.dispatch(&GameEvent::Riichi {
            player_id: player1_id,
        })?;

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let actual_player_round_state = actual_player.player_round_state();
        let expected_riichi = Some(Riichi::Riichi);
        assert_eq!(actual_player_round_state.riichi(), &expected_riichi);
        assert!(actual_player_round_state.furiten().is_none());
        assert_eq!(actual_player.points(), 25_000);
        let expected_round_stage = RoundStage::Ended;
        assert_eq!(*game_state.round_stage(), expected_round_stage);

        Ok(())
    }

    #[test]
    fn validate_succeeds_on_tile_discard_at_start() -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );

        *game_state.active_player_mut() = player1_id;

        // Put 13 tiles into the hand.
        for _ in 0..13 {
            let current_wall_tile = *game_state.round_context().wall().current_tile();
            game_state
                .players_mut()
                .get_mut(&player1_id)
                .unwrap()
                .hand_mut()
                .tiles_mut()
                .push(&current_wall_tile);
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()?;
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 0,
        });

        assert!(actual.is_ok());

        Ok(())
    }

    #[test]
    fn validate_succeeds_on_tile_discard_for_drawn_tile() -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );

        *game_state.active_player_mut() = player1_id;

        // Put 13 tiles into the hand and draw 1 tile.
        for i in 0..14 {
            if i != 13 {
                let current_wall_tile = *game_state.round_context().wall().current_tile();
                game_state
                    .players_mut()
                    .get_mut(&player1_id)
                    .unwrap()
                    .hand_mut()
                    .tiles_mut()
                    .push(&current_wall_tile);
                game_state
                    .round_context_mut()
                    .wall_mut()
                    .increment_tile_idx()?;
            } else {
                let current_wall_tile = *game_state.round_context().wall().current_tile();
                *game_state
                    .players_mut()
                    .get_mut(&player1_id)
                    .unwrap()
                    .hand_mut()
                    .drawn_tile_mut() = Some(current_wall_tile);
            }
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 13,
        });

        assert!(actual.is_ok());

        Ok(())
    }

    #[test]
    fn validate_succeeds_on_tile_discard_for_drawn_tile_while_in_riichi()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        // Put 13 tiles into the hand and draw 1 tile.
        for i in 0..14 {
            if i != 13 {
                let current_wall_tile = *game_state.round_context().wall().current_tile();
                game_state
                    .players_mut()
                    .get_mut(&player1_id)
                    .unwrap()
                    .hand_mut()
                    .tiles_mut()
                    .push(&current_wall_tile);
                game_state
                    .round_context_mut()
                    .wall_mut()
                    .increment_tile_idx()?;
            } else {
                let current_wall_tile = *game_state.round_context().wall().current_tile();
                *game_state
                    .players_mut()
                    .get_mut(&player1_id)
                    .unwrap()
                    .hand_mut()
                    .drawn_tile_mut() = Some(current_wall_tile);
            }
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 13,
        });

        assert!(actual.is_ok());

        Ok(())
    }

    #[test]
    fn validate_succeeds_on_tile_discard_for_drawn_tile_while_in_riichi_and_ankan()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            player
                .hand_mut()
                .melds_mut()
                .push(Meld::Kan(KanType::Ankan {
                    tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
                }));

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        // Put 10 tiles into the hand and draw 1 tile.
        for i in 0..11 {
            if i != 10 {
                let current_wall_tile = *game_state.round_context().wall().current_tile();
                game_state
                    .players_mut()
                    .get_mut(&player1_id)
                    .unwrap()
                    .hand_mut()
                    .tiles_mut()
                    .push(&current_wall_tile);
                game_state
                    .round_context_mut()
                    .wall_mut()
                    .increment_tile_idx()?;
            } else {
                let current_wall_tile = *game_state.round_context().wall().current_tile();
                *game_state
                    .players_mut()
                    .get_mut(&player1_id)
                    .unwrap()
                    .hand_mut()
                    .drawn_tile_mut() = Some(current_wall_tile);
            }
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 10,
        });

        assert!(actual.is_ok());

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_during_pre_game_stage() -> Result<(), IncrementWallIndexError>
    {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );
        *game_state.active_player_mut() = player1_id;

        // Put 13 tiles into the hand.
        for _ in 0..13 {
            let current_wall_tile = *game_state.round_context().wall().current_tile();
            game_state
                .players_mut()
                .get_mut(&player1_id)
                .unwrap()
                .hand_mut()
                .tiles_mut()
                .push(&current_wall_tile);
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()?;
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 0,
        });

        let expected = GameEventError::NotInGame;
        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_during_pre_round_round_stage()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::PreRound;

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );

        *game_state.active_player_mut() = player1_id;

        // Put 13 tiles into the hand.
        for _ in 0..13 {
            let current_wall_tile = *game_state.round_context().wall().current_tile();
            game_state
                .players_mut()
                .get_mut(&player1_id)
                .unwrap()
                .hand_mut()
                .tiles_mut()
                .push(&current_wall_tile);
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()?;
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 0,
        });

        let expected = GameEventError::NotInGame;

        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_invalid_player() -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );

        *game_state.active_player_mut() = player1_id;

        // Put 13 tiles into the hand.
        for _ in 0..13 {
            let current_wall_tile = *game_state.round_context().wall().current_tile();
            game_state
                .players_mut()
                .get_mut(&player1_id)
                .unwrap()
                .hand_mut()
                .tiles_mut()
                .push(&current_wall_tile);
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()?;
        }

        let player2_id = 2;
        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player2_id,
            at: 0,
        });

        let expected = GameEventError::InvalidPlayer(player2_id);

        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_not_active_player_turn()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );
        let player2_id = 2;

        *game_state.active_player_mut() = player2_id;

        // Put 13 tiles into the hand.
        for _ in 0..13 {
            let current_wall_tile = *game_state.round_context().wall().current_tile();
            game_state
                .players_mut()
                .get_mut(&player1_id)
                .unwrap()
                .hand_mut()
                .tiles_mut()
                .push(&current_wall_tile);
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()?;
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 0,
        });

        let expected = GameEventError::NotPlayerTurn(player1_id);

        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_at_index_greater_than_tiles()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );

        *game_state.active_player_mut() = player1_id;

        // Put 13 tiles into the hand.
        for _ in 0..13 {
            let current_wall_tile = *game_state.round_context().wall().current_tile();
            game_state
                .players_mut()
                .get_mut(&player1_id)
                .unwrap()
                .hand_mut()
                .tiles_mut()
                .push(&current_wall_tile);
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()?;
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 15,
        });

        let expected = GameEventError::InvalidTileDiscard(15);

        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_drawn_tile_but_no_tile_drawn()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );

        *game_state.active_player_mut() = player1_id;

        // Put 13 tiles into the hand.
        for _ in 0..13 {
            let current_wall_tile = *game_state.round_context().wall().current_tile();
            game_state
                .players_mut()
                .get_mut(&player1_id)
                .unwrap()
                .hand_mut()
                .tiles_mut()
                .push(&current_wall_tile);
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()?;
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 14,
        });

        let expected = GameEventError::InvalidTileDiscard(14);
        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_tile_in_hand_while_in_riichi()
    -> Result<(), GameEventError> {
        // This scenario should not ever happen because the player has declared riichi, but make sure that this case is handled.
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::ManzuRed5);
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.player_round_state_mut().furiten_mut() = Some(Furiten::Permanent);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 3,
        });
        let expected = GameEventError::InvalidTileDiscard(3);
        assert_eq!(actual.expect_err("expected validate to fail"), expected);

        let actual_player = game_state.players().get(&player1_id).unwrap();
        let expected_furiten = Some(Furiten::Permanent);
        assert_eq!(
            *actual_player.player_round_state().furiten(),
            expected_furiten
        );

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_tile_in_hand_while_in_riichi_and_ankan()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            player
                .hand_mut()
                .melds_mut()
                .push(Meld::Kan(KanType::Ankan {
                    tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
                }));

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        // Put 10 tiles into the hand and draw 1 tile.
        for i in 0..11 {
            if i != 10 {
                let current_wall_tile = *game_state.round_context().wall().current_tile();
                game_state
                    .players_mut()
                    .get_mut(&player1_id)
                    .unwrap()
                    .hand_mut()
                    .tiles_mut()
                    .push(&current_wall_tile);
                game_state
                    .round_context_mut()
                    .wall_mut()
                    .increment_tile_idx()?;
            } else {
                let current_wall_tile = *game_state.round_context().wall().current_tile();
                *game_state
                    .players_mut()
                    .get_mut(&player1_id)
                    .unwrap()
                    .hand_mut()
                    .drawn_tile_mut() = Some(current_wall_tile);
            }
        }

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 0,
        });

        assert!(actual.is_err());

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_violating_kuikae_with_chii_with_tile_on_other_end()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Pinzu1,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().last_meld_mut() = Some(Meld::Chii {
                claimed_tile: Tile::Pinzu4,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu2, Tile::Pinzu3, Tile::Pinzu4],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 6,
        });

        let expected = GameEventError::InvalidTileDiscard(6);
        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_violating_kuikae_with_chii_with_middle_tile_same_as_claimed()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Pinzu3,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().last_meld_mut() = Some(Meld::Chii {
                claimed_tile: Tile::Pinzu3,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu2, Tile::Pinzu3, Tile::Pinzu4],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 6,
        });

        let expected = GameEventError::InvalidTileDiscard(6);
        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_violating_kuikae_with_chii_with_discarded_tile_same_as_claimed()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Pinzu2,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().last_meld_mut() = Some(Meld::Chii {
                claimed_tile: Tile::Pinzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu2, Tile::Pinzu3, Tile::Pinzu4],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 6,
        });

        let expected = GameEventError::InvalidTileDiscard(6);
        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_tile_discard_for_violating_kuikae_with_pon_with_discarded_tile_same_as_claimed()
    -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Pinzu2,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().last_meld_mut() = Some(Meld::Pon {
                claimed_tile: Tile::Pinzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu2, Tile::Pinzu2, Tile::Pinzu2],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::DiscardTile {
            player_id: player1_id,
            at: 6,
        });

        let expected = GameEventError::InvalidTileDiscard(6);
        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_succeeds_on_end_round_with_exhaustive_draw() -> Result<(), IncrementWallIndexError>
    {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()?;
        }

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        });

        assert!(actual.is_ok());

        Ok(())
    }

    #[test]
    fn validate_succeeds_on_end_round_with_declared_win_ron() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu2,
                },
            },
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_end_round_with_declared_win_tsumo() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu2),
            },
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_fails_on_end_round_in_pre_game_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        });
        let expected = GameEventError::InvalidGameEnd;

        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_in_ended_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::Ended;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        });
        let expected = GameEventError::InvalidGameEnd;

        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_in_pre_round_round_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::PreRound;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        });
        let expected = GameEventError::InvalidGameEnd;

        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_in_ended_round_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::Ended;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        });
        let expected = GameEventError::InvalidGameEnd;

        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_exhaustive_draw_but_remaining_tiles_in_wall() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::ExhaustiveDraw,
        });
        let expected = GameEventError::InvalidGameEnd;

        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_non_existent_player() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player2_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu2,
                },
            },
        });

        let expected = GameEventError::InvalidPlayer(player2_id);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_when_player_not_in_tenpai() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu9,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu2,
                },
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_declared_win_ron_and_player_deal_in_different_from_last_discard_player()
     {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::West, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    // West made the last discard to call "ron" off of but this event is claiming a win from the South player discard.
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu2,
                },
            },
        });
        let expected = GameEventError::InvalidGameEnd;

        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_declared_win_ron_and_winning_tile_different_from_last_discard_tile()
     {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu5,
                },
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_declared_win_ron_and_in_own_discard_furiten() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu5));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().discards_mut().push(&Tile::Manzu2);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu5,
                },
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_declared_win_ron_and_in_temporary_furiten() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu5));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().furiten_mut() = Some(Furiten::Temporary);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu5,
                },
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_declared_win_ron_and_in_permanent_furiten() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu5));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.player_round_state_mut().furiten_mut() = Some(Furiten::Permanent);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu5,
                },
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_declared_win_ron_and_invalid_winning_tile() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu3));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu3,
                },
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_declared_win_ron_and_no_valid_yaku() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu8));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu5,
                    Tile::Manzu7,
                    Tile::Manzu9,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 8 is a potential winning tile.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu8,
                },
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_declared_win_ron_and_no_last_discard() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu5,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu6);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 8 is a potential winning tile.
                win_declaration: WinDeclaration::Ron {
                    deal_in_player: Wind::South,
                    tile: Tile::Manzu6,
                },
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_tsumo_on_different_player_turn() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;

        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu2),
            },
        });

        let expected = GameEventError::NotPlayerTurn(player1_id);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_tsumo_and_different_drawn_and_winning_tiles() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu5);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu2),
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_tsumo_and_invalid_winning_tile() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu3);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu3),
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_tsumo_and_no_valid_yaku() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu5,
                    Tile::Manzu5,
                    Tile::Manzu5,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            });
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Manzu9);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 6 and 9 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu9),
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_round_with_tsumo_and_no_drawn_tile() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::South, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::EndRound {
            reason: EndRoundReason::PlayerWon {
                winner: player1_id,
                // The manzu 2, 5, and 8 are potential winning tiles.
                win_declaration: WinDeclaration::Tsumo(Tile::Manzu2),
            },
        });

        let expected = GameEventError::InvalidGameEnd;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_succeeds_on_chii_meld() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_pon_meld() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu5));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu5,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Pon {
                claimed_tile: Tile::Manzu5,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
            },
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_ankan_meld() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Souzu8);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_daiminkan_meld() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::East, Tile::Souzu8));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = Player::new(
            player2_id,
            Hand::new(TileSet::new(vec![
                Tile::Manzu1,
                Tile::Manzu3,
                Tile::Manzu4,
                Tile::Manzu5,
                Tile::Manzu6,
                Tile::Manzu7,
                Tile::Manzu8,
                Tile::Souzu4,
                Tile::Souzu5,
                Tile::Souzu6,
                Tile::Souzu8,
                Tile::Souzu8,
                Tile::Souzu8,
            ])),
            Wind::West,
            String::from("testusername2"),
        );
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player2_id,
            meld: Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_shominkan_meld() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            });
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Souzu8);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_chii_meld_when_one_remaining_tile_violates_kuikae_with_same_tile_but_other_does_not()
     {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu4));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu5,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
            });
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu5,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::SouzuRed5, Tile::Souzu5, Tile::Souzu5],
            });
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Pinzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        // After the meld, the player would be left with `4m` and `5m`, which would be valid and they would be required to discard the `5m`.
        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu4,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu3, Tile::Manzu4, Tile::Manzu5],
            },
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_ankan_meld_during_riichi() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Souzu8);
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_fails_on_meld_in_pre_game_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        });

        let expected = GameEventError::NotInGame;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_in_ended_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::Ended;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        });

        let expected = GameEventError::NotInGame;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_in_pre_round_round_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::PreRound;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        });

        let expected = GameEventError::NotInGame;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_in_ended_round_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::Ended;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        *game_state.active_player_mut() = 1;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: 1,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        });
        let expected = GameEventError::NotInGame;

        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_when_wall_exhausted() -> Result<(), IncrementWallIndexError> {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        // Exhaust the wall.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx()) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()?;
        }

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        );
        assert_eq!(actual.unwrap_err(), expected);

        Ok(())
    }

    #[test]
    fn validate_fails_on_meld_3_player_and_chii() {
        let mut game_state = GameState::new(GameRules::new_3p_default(), 3);
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::East, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player2_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        });

        let expected = GameEventError::InvalidMeld(
            player2_id,
            Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_3_player_and_pon_on_north_seat_discard() {
        let mut game_state = GameState::new(GameRules::new_3p_default(), 3);
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Souzu8));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Pon {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            },
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Pon {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            },
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_3_player_and_daiminkan_on_north_seat_discard() {
        let mut game_state = GameState::new(GameRules::new_3p_default(), 3);
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Souzu8));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_3_player_and_shominkan_on_north_seat_discard() {
        let mut game_state = GameState::new(GameRules::new_3p_default(), 3);
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Souzu8));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_chii_meld_during_riichi() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_pon_meld_during_riichi() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu5));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu5,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Pon {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
            },
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Pon {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
            },
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_ankan_meld_during_riichi_which_changes_waits() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Manzu8,
                    Tile::Souzu6,
                    Tile::Souzu7,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Souzu8);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_daiminkan_meld_during_riichi() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::East, Tile::Souzu8));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            let mut player = Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::West,
                String::from("testusername2"),
            );
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player2_id,
            meld: Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        let expected = GameEventError::InvalidMeld(
            player2_id,
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_shominkan_meld_during_riichi() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            });
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Souzu8);
            *player.player_round_state_mut().riichi_mut() = Some(Riichi::Riichi);

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_chii_meld_when_remaining_tile_violates_kuikae_with_opposite_end() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
            });
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu5,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::SouzuRed5, Tile::Souzu5, Tile::Souzu5],
            });
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Pinzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Chii {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4],
            },
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_chii_meld_when_remaining_tile_violates_kuikae_with_same_tile() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu5));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu5,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
            });
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu5,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::SouzuRed5, Tile::Souzu5, Tile::Souzu5],
            });
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Pinzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Manzu5,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu3, Tile::Manzu4, Tile::Manzu5],
            },
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Chii {
                claimed_tile: Tile::Manzu5,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu3, Tile::Manzu4, Tile::Manzu5],
            },
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_with_chii_and_claimed_tile_that_does_not_complete_sequence() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::East, Tile::Souzu9));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::South,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player2_id,
            meld: Meld::Chii {
                claimed_tile: Tile::Souzu9,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu7, Tile::Souzu8, Tile::Souzu9],
            },
        });

        let expected = GameEventError::InvalidMeld(
            player2_id,
            Meld::Chii {
                claimed_tile: Tile::Souzu9,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu7, Tile::Souzu8, Tile::Souzu9],
            },
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_pon_that_does_not_complete_triplet() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Manzu1));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Pon {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
            },
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Pon {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
            },
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_kan_that_does_not_complete_quadruplet() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Souzu8));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu7,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu7,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_ankan_when_not_player_turn() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Souzu8));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Souzu8);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu7,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        let expected = GameEventError::NotPlayerTurn(player1_id);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_meld_kan_when_type_does_not_match_calculated_type() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::Souzu8));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.hand_mut().drawn_tile_mut() = Some(Tile::Souzu8);

            player
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu7,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::North,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Meld {
            player_id: player1_id,
            meld: Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        });

        // The meld should be ankan with the context.
        let expected = GameEventError::InvalidMeld(
            player1_id,
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        );
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_succeeds_on_player_disconnect_in_pre_game_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        let actual = game_state.validate(&GameEvent::PlayerDisconnected {
            player_id: player1_id,
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_player_disconnect_in_in_game_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        let actual = game_state.validate(&GameEvent::PlayerDisconnected {
            player_id: player1_id,
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_player_disconnect_in_ended_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::Ended;

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        let actual = game_state.validate(&GameEvent::PlayerDisconnected {
            player_id: player1_id,
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_fails_on_player_disconnect_for_non_existent_player() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::Ended;

        let player1_id = 1;

        let actual = game_state.validate(&GameEvent::PlayerDisconnected {
            player_id: player1_id,
        });

        let expected = GameEventError::InvalidPlayer(player1_id);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_succeeds_on_player_join() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;

        let player1_id = 1;

        let actual = game_state.validate(&GameEvent::PlayerJoined {
            player_id: player1_id,
            username: String::from("testusername1"),
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_fails_on_player_join_when_game_is_ended() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::Ended;

        let player1_id = 1;

        let actual = game_state.validate(&GameEvent::PlayerJoined {
            player_id: player1_id,
            username: String::from("testusername1"),
        });

        let expected = GameEventError::JoinGameError(ConnectionErrorReason::GameEnded);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_player_join_when_in_game_and_player_not_found() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );
        let player2_id = 2;

        let actual = game_state.validate(&GameEvent::PlayerJoined {
            player_id: player2_id,
            username: String::from("testusername2"),
        });

        let expected = GameEventError::InvalidPlayer(player2_id);

        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_player_join_when_same_player_joins_again() {
        let mut game_state = GameState::default();

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );

        let actual = game_state.validate(&GameEvent::PlayerJoined {
            player_id: player1_id,
            username: String::from("testusername1"),
        });

        let expected = GameEventError::JoinGameError(ConnectionErrorReason::DuplicatePlayer(
            player1_id,
            String::from("testusername1"),
        ));
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_player_join_when_game_is_full() {
        let mut game_state = GameState::default();

        let player1_id = 1;
        game_state.players_mut().insert(
            player1_id,
            Player::new(
                player1_id,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            ),
        );
        let player2_id = 2;
        game_state.players_mut().insert(
            player2_id,
            Player::new(
                player2_id,
                Hand::default(),
                Wind::East,
                String::from("testusername2"),
            ),
        );
        let player3_id = 3;
        game_state.players_mut().insert(
            player3_id,
            Player::new(
                player3_id,
                Hand::default(),
                Wind::East,
                String::from("testusername3"),
            ),
        );
        let player4_id = 4;
        game_state.players_mut().insert(
            player4_id,
            Player::new(
                player4_id,
                Hand::default(),
                Wind::East,
                String::from("testusername4"),
            ),
        );
        let player5_id = 5;

        let actual = game_state.validate(&GameEvent::PlayerJoined {
            player_id: player5_id,
            username: String::from("testusername5"),
        });

        let expected = GameEventError::JoinGameError(ConnectionErrorReason::GameFull);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_begin_game_with_fewer_players() {
        // 4-player game.
        let mut game_state = GameState::default();
        let player1 = {
            Player::new(
                1,
                Hand::default(),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(1, player1);

        let actual = game_state.validate(&GameEvent::BeginGame);

        let expected = GameEventError::InvalidBeginGame;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_end_game_when_all_players_above_zero_points() -> Result<(), GameEventError>
    {
        let mut game_state = GameState::default();

        *game_state.round_stage_mut() = RoundStage::Ended;
        *game_state.stage_mut() = Stage::InGame;

        let player1 = { Player::new(1, Hand::default(), Wind::East, "testusername1".to_string()) };
        game_state.players_mut().insert(1, player1);
        let player2 = { Player::new(2, Hand::default(), Wind::East, "testusername2".to_string()) };
        game_state.players_mut().insert(2, player2);
        let player3 = { Player::new(3, Hand::default(), Wind::East, "testusername3".to_string()) };
        game_state.players_mut().insert(3, player3);
        let player4 = { Player::new(4, Hand::default(), Wind::East, "testusername4".to_string()) };
        game_state.players_mut().insert(4, player4);

        let result = game_state.dispatch(&GameEvent::EndGame).unwrap_err();

        let expected_error = GameEventError::InvalidGameEnd;
        assert_eq!(result, expected_error);

        let actual_stage = game_state.stage();

        let expected_stage = Stage::InGame;
        assert_eq!(actual_stage, &expected_stage);

        Ok(())
    }

    #[test]
    fn validate_succeeeds_on_riichi() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_riichi_with_ankan_meld() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player
                .hand_mut()
                .melds_mut()
                .push(Meld::Kan(KanType::Ankan {
                    tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
                }));
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeds_on_riichi_with_tobi_set_to_nashi_and_player_falls_below_zero() {
        let mut game_rules = GameRules::new_4p_default();
        // "Nashi" is when tobi is `false`.
        *game_rules.tobi_mut() = false;
        let mut game_state = GameState::new(game_rules, 4);
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player
                .hand_mut()
                .melds_mut()
                .push(Meld::Kan(KanType::Ankan {
                    tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
                }));
            *player.points_mut() = 500;

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_succeeeds_on_riichi_with_four_tiles_left_in_wall_in_four_player_game() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        // "Play" until there are 4 live tiles left.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx() - 4) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        assert!(actual.is_ok());
    }

    #[test]
    fn validate_fails_on_riichi_with_tobi_set_to_ari_and_player_would_fall_below_zero() {
        let mut game_rules = GameRules::new_4p_default();
        // "Ari" is when tobi is `true`.
        *game_rules.tobi_mut() = true;
        let mut game_state = GameState::new(game_rules, 4);
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player
                .hand_mut()
                .melds_mut()
                .push(Meld::Kan(KanType::Ankan {
                    tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
                }));
            *player.points_mut() = 500;

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        let expected = GameEventError::InvalidRiichi(player1_id);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_riichi_when_in_pre_game_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::PreGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        let expected = GameEventError::NotInGame;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_riichi_when_in_ended_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::Ended;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = Player::new(
            player1_id,
            Hand::new(TileSet::new(vec![
                Tile::Manzu2,
                Tile::Manzu3,
                Tile::Manzu4,
                Tile::Manzu5,
                Tile::Manzu6,
                Tile::Manzu7,
                Tile::Manzu8,
                Tile::Souzu4,
                Tile::Souzu5,
                Tile::Souzu6,
                Tile::Souzu8,
                Tile::Souzu8,
                Tile::Souzu8,
            ])),
            Wind::East,
            String::from("testusername1"),
        );
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        let expected = GameEventError::NotInGame;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_riichi_when_in_pre_round_round_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::PreRound;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        let expected = GameEventError::NotInGame;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_riichi_when_in_ended_round_stage() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::Ended;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        let expected = GameEventError::NotInGame;
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_riichi_when_not_player_turn() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);
        let player2_id = 2;
        let player2 = {
            Player::new(
                player2_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername2"),
            )
        };
        game_state.players_mut().insert(player2_id, player2);
        *game_state.active_player_mut() = player2_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        let expected = GameEventError::NotPlayerTurn(player1_id);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_riichi_when_not_in_tenpai() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Pinzu1,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            )
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        let expected = GameEventError::InvalidRiichi(player1_id);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_riichi_when_hand_is_open() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu1,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            player.hand_mut().melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Souzu8,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            });

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        let expected = GameEventError::InvalidRiichi(player1_id);
        assert_eq!(actual.unwrap_err(), expected);
    }

    #[test]
    fn validate_fails_on_riichi_with_fewer_than_four_tiles_left_in_wall_in_four_player_game() {
        let mut game_state = GameState::default();
        *game_state.stage_mut() = Stage::InGame;
        *game_state.round_stage_mut() = RoundStage::InGame;
        *game_state.round_context_mut().last_discard_mut() =
            Some(LastDiscard::new(Wind::North, Tile::WindEast));

        let player1_id = 1;
        let player1 = {
            let mut player = Player::new(
                player1_id,
                Hand::new(TileSet::new(vec![
                    Tile::Manzu2,
                    Tile::Manzu3,
                    Tile::Manzu4,
                    Tile::Manzu5,
                    Tile::Manzu6,
                    Tile::Manzu7,
                    Tile::Manzu8,
                    Tile::Souzu4,
                    Tile::Souzu5,
                    Tile::Souzu6,
                    Tile::Souzu8,
                    Tile::Souzu8,
                    Tile::Souzu8,
                ])),
                Wind::East,
                String::from("testusername1"),
            );
            *player.points_mut() = game_state.game_rules().starting_points().into();

            player
        };
        game_state.players_mut().insert(player1_id, player1);

        *game_state.active_player_mut() = player1_id;

        // "Play" until there are 4 live tiles left.
        for _ in 0..(game_state.round_context().wall().last_live_tile_idx() - 3) {
            game_state
                .round_context_mut()
                .wall_mut()
                .increment_tile_idx()
                .unwrap();
        }

        let actual = game_state.validate(&GameEvent::Riichi {
            player_id: player1_id,
        });

        let expected = GameEventError::InvalidRiichi(player1_id);
        assert_eq!(actual.unwrap_err(), expected);
    }
}
