/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Error types for the game state and game events.

use jikaze_riichi_core::meld::Meld;

use crate::Username;
use crate::player::PlayerId;

/// Error while processing a game event.
#[derive(Debug, PartialEq)]
pub enum GameEventError {
    /// An invalid call to end the game was made.
    InvalidGameEnd,
    /// An invalid call to begin the game was made.
    InvalidBeginGame,
    /// An invalid meld was called.
    InvalidMeld(PlayerId, Meld),
    /// An invalid player ID was provided.
    InvalidPlayer(PlayerId),
    /// An invalid riichi call was made.
    InvalidRiichi(PlayerId),
    /// An invalid tile index was provided to discard.
    InvalidTileDiscard(usize),
    /// Player failed to join a game for the specified reason.
    JoinGameError(ConnectionErrorReason),
    /// Game is not in play.
    NotInGame,
    /// It is not the player's turn.
    NotPlayerTurn(PlayerId),
}

impl std::error::Error for GameEventError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Self::InvalidGameEnd => None,
            Self::InvalidBeginGame => None,
            Self::InvalidMeld(..) => None,
            Self::InvalidPlayer(_) => None,
            Self::InvalidRiichi(_) => None,
            Self::InvalidTileDiscard(_) => None,
            Self::JoinGameError(_) => None,
            Self::NotInGame => None,
            Self::NotPlayerTurn(_) => None,
        }
    }
}

impl std::fmt::Display for GameEventError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Self::InvalidGameEnd => write!(f, "Game cannot end."),
            Self::InvalidBeginGame => write!(f, "Game cannot begin."),
            Self::InvalidMeld(player_id, meld) => {
                write!(
                    f,
                    "Meld call is not valid for player ID {player_id}: {meld:?}."
                )
            }
            Self::InvalidPlayer(player_id) => {
                write!(f, "Player {player_id} is not part of the game.")
            }
            Self::InvalidRiichi(player_id) => {
                write!(f, "Player {player_id} is not allowed to declare riichi.")
            }
            Self::InvalidTileDiscard(tile_idx) => {
                write!(f, "Tile at {tile_idx} is not valid.")
            }
            Self::JoinGameError(reason) => match reason {
                ConnectionErrorReason::DuplicatePlayer(player_id, username) => write!(
                    f,
                    "Player {player_id} ({username}) cannot join the same game."
                ),
                ConnectionErrorReason::GameEnded => write!(f, "Game has ended."),
                ConnectionErrorReason::GameFull => write!(f, "Game is full."),
                ConnectionErrorReason::Timeout => {
                    write!(f, "Player timed out connecting to lobby.")
                }
            },
            Self::NotInGame => write!(f, "Game is not in play."),
            Self::NotPlayerTurn(player_id) => write!(f, "It is not player {player_id}'s turn."),
        }
    }
}

/// Reason why the player failed to join a game.
#[derive(Debug, PartialEq)]
pub enum ConnectionErrorReason {
    /// Error when the same player tries to join the same game.
    DuplicatePlayer(PlayerId, Username),
    /// The game has ended.
    GameEnded,
    /// Game is full.
    GameFull,
    /// Error when the player timed out attempting to join the game.
    Timeout,
}
