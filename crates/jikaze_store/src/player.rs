/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Registered players on the server.

use jikaze_riichi_core::hand::Hand;
use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
use jikaze_riichi_core::tile::TileSet;
use jikaze_riichi_core::wind::Wind;

use crate::Username;

/// Unique player ID generated and stored on the server.
pub type PlayerId = u64;

/// Player state within a game.
#[derive(Debug, Clone, PartialEq)]
pub struct Player {
    /// Server ID of the player.
    id: PlayerId,
    /// State of whether or not the player is connected.
    connected: bool,
    /// Visible discards for display purposes.
    ///
    /// <div class="warning">
    ///
    /// All of the discards that the player has discarded exists in their [`Hand`](jikaze_riichi_core::hand::Hand#structfield.discards).
    /// Whenever other players make meld calls on the player's discarded tile, they get removed from this field, but not from their `Hand`.
    ///
    /// </div>
    discards: TileSet,
    /// Player's hand and melds.
    hand: Hand,
    /// Player context for the round.
    player_round_state: PlayerRoundState,
    /// Current number of points the player has in the game.
    points: i64,
    /// Name of the player.
    username: Username,
}

impl Player {
    /// Get the connection status of the player.
    pub const fn connected(&self) -> bool {
        self.connected
    }

    /// Set the player's `connected` state to `false` (disconnected).
    pub fn disconnect(&mut self) {
        self.connected = false;
    }

    /// Get the player's visible discards.
    ///
    /// If all of the player's discards are desired, use the list of discarded tiles in their [`Hand`](jikaze_riichi_core::hand::Hand#structfield.discards)
    pub const fn discards(&self) -> &TileSet {
        &self.discards
    }

    /// Get a mutable reference to the player's visible discards.
    ///
    /// If all of the player's discards are desired, use the list of discarded tiles in their [`Hand`](jikaze_riichi_core::hand::Hand#structfield.discards)
    pub fn discards_mut(&mut self) -> &mut TileSet {
        &mut self.discards
    }

    /// Get the player's hand.
    pub const fn hand(&self) -> &Hand {
        &self.hand
    }

    /// Get a mutable reference to the player's hand.
    pub fn hand_mut(&mut self) -> &mut Hand {
        &mut self.hand
    }

    /// Get the player ID.
    pub const fn id(&self) -> PlayerId {
        self.id
    }

    /// Create a new instance of [`Player`].
    pub fn new(id: PlayerId, hand: Hand, wind: Wind, username: Username) -> Self {
        Self {
            id,
            connected: true,
            discards: TileSet::default(),
            hand,
            player_round_state: PlayerRoundState::new(wind),
            points: 0,
            username,
        }
    }

    /// Get the player context.
    pub const fn player_round_state(&self) -> &PlayerRoundState {
        &self.player_round_state
    }

    /// Get a mutable reference to the player context.
    pub fn player_round_state_mut(&mut self) -> &mut PlayerRoundState {
        &mut self.player_round_state
    }

    /// Get the player's current point value.
    pub const fn points(&self) -> i64 {
        self.points
    }

    /// Get a mutable reference to the player's current point value.
    pub fn points_mut(&mut self) -> &mut i64 {
        &mut self.points
    }

    /// Set the player's `connected` state to `true` (connected).
    pub fn reconnect(&mut self) {
        self.connected = true;
    }

    /// Get the username of the player.
    pub const fn username(&self) -> &Username {
        &self.username
    }
}
