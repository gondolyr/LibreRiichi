/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Game events that occur during a game.

use jikaze_riichi_core::meld::Meld;

use crate::Username;
use crate::game_state::EndRoundReason;
use crate::player::PlayerId;

/// Events that may occur during a game.
#[derive(Debug, Clone)]
pub enum GameEvent {
    /// Start the game.
    BeginGame,
    /// A player has discarded a tile within their hand at the given index.
    DiscardTile {
        /// The player that discarded a tile.
        player_id: PlayerId,
        /// Index of the tile to discard from the player's hand.
        ///
        /// Providing an index value equal to the hand length will discard the drawn tile.
        ///
        /// Example: `123456789m 1z`, where `1z` is the drawn tile. Providing a value of `10` will discard the `1z` tile.
        at: usize,
    },
    /// End the game.
    EndGame,
    /// The round has ended for the specified reason.
    EndRound {
        /// The reason the round ended.
        reason: EndRoundReason,
    },
    /// A player called a meld.
    Meld {
        /// The ID of the player that called a meld.
        player_id: PlayerId,
        /// The type of meld that was called.
        meld: Meld,
    },
    /// A player has disconnected.
    PlayerDisconnected {
        /// The ID of the player that disconnected.
        player_id: PlayerId,
    },
    /// Event when a player has joined the game.
    PlayerJoined {
        /// The ID of the player that joined the game.
        player_id: PlayerId,
        /// The display name of the player that joined the game.
        username: Username,
    },
    /// A player has declared [riichi](jikaze_riichi_core::yaku::Yaku::Riichi).
    Riichi {
        /// The ID of the player that declared riichi.
        player_id: PlayerId,
    },
}
