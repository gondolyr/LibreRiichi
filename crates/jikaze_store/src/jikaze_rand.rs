/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Random number generator utilities.
//!
//! This module consists of wrappers around external crates to provide a stable API within the project.

use rand::SeedableRng;
use rand_chacha::ChaCha20Rng;

/// Get an RNG.
///
/// By default, this provides an RNG that uses the ChaCha algorithm.
///
/// <div class="warning">
///
/// Future changes may introduce breaking changes to this function by changing the RNG algorithm.
///
/// If stability is needed, use an RNG provider directly.
///
/// </div>
pub fn rng() -> ChaCha20Rng {
    chacha20_rng_from_os_rng()
}

/// Get an RNG that uses the ChaCha algorithm.
pub fn chacha20_rng_from_os_rng() -> ChaCha20Rng {
    ChaCha20Rng::from_os_rng()
}
