<!--
SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

<style>
.jikaze__red-dragon-tile {
  font-size: 1.65rem;
  margin: 0 -4px 0;
  text-shadow: 0 0 1px black;
}

.jikaze__yaku-concealed,
.jikaze__yaku-concealed > td,
.jikaze__yaku-concealed > th {
  background: #BBB8CC;
  color: #000000;
}

.jikaze__yaku-concealed > th::after {
  content: '👁️';
  display: block;
  filter: contrast(0);
}

.jikaze__yaku-open,
.jikaze__yaku-open > td,
.jikaze__yaku-open > th,
.jikaze__yaku-open-reduced > td,
.jikaze__yaku-open-reduced > th {
  background: #B6EAAC;
  color: #000000;
}

.jikaze__yaku-open > th::after {
  content: '👁️';
  display: block;
}

.jikaze__yaku-open-reduced > th::after {
  content: '👁️ ' attr(data-open-han-value);
  display: block;
}

.jikaze__yaku-example {
  font-size: 2rem;
  max-width: 100%;
  white-space: nowrap;
}
</style>

<table>
  <caption>
    Yaku Key
  <caption>
  <tbody>
    <tr class="jikaze__yaku-open">
      <th scope="row">Open Yaku</th>
      <td>Can be claimed after calling pon, chii, or non-concealed kan</td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row">Concealed Yaku</th>
      <td>Cannot be claimed after calling pon, chii, or non-concealed kan</td>
    </tr>
  </tbody>
</table>

<table>
  <caption>
    Yaku
  </caption>
  <thead>
    <tr>
      <th scope="col">Name</th>
      <th scope="col">Japanese</th>
      <th scope="col">Romaji</th>
      <th scope="col">Description</th>
      <th scope="col">Example</th>
    </tr>
  </thead>

  <tbody class="jikaze__yaku-han-separator">
    <tr>
      <th scope="row" colspan="5">1 han</th>
    </tr>
  </tbody>
  <tbody>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="1">Riichi</th>
      <td>リーチ</td>
      <td>Riichi</td>
      <td>Closed waiting hand declared with 1,000 of the caller's points put on the table.</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="1">Fully Concealed Hand</th>
      <td>ツモ</td>
      <td>Tsumo</td>
      <td>Winning with a self-drawn tile on a fully concealed hand.</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="1">Unbroken</th>
      <td>一発</td>
      <td>Ippatsu</td>
      <td>Win on the turn after declaring riichi. There must be no interruption from any call (except another riichi declaration).</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="1">All Simples</th>
      <td>断幺九</td>
      <td>Tanyao</td>
      <td>No terminal or honor tiles.</td>
      <td class="jikaze__yaku-example">🀋🀌🀍🀜🀝🀞🀠🀠🀑🀒🀓🀕🀕🀕</td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="1">Pinfu</th>
      <td>平和</td>
      <td>Pinfu</td>
      <td>Four sequences and a valueless pair. Must win with a two-sided wait on a sequence.</td>
      <td class="jikaze__yaku-example">🀋🀌🀍🀙🀚🀛🀜🀝🀠🀠🀐🀑🀒 🀞</td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="1">Pure Double Sequence</th>
      <td>一盃口</td>
      <td>Iipeikou</td>
      <td>Two identical sequences in the same suit.</td>
      <td class="jikaze__yaku-example">🀙🀚🀛🀙🀚🀛</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="1">Value Honor</th>
      <td>役牌</td>
      <td>Yakuhai</td>
      <td>Triplet (or quad) of dragon tiles, player's seat wind, or the round wind.</td>
      <td class="jikaze__yaku-example">🀅🀅🀅 / 🀃🀃🀃 / 🀀🀀🀀</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="1">Dead Wall Draw</th>
      <td>嶺上開花</td>
      <td>Rinshan Kaihou</td>
      <td>Win with a self-draw with the replacement tile after forming a kantsu (kan call).</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="1">Robbing a Kan</th>
      <td>搶槓</td>
      <td>Chankan</td>
      <td>Win off of another player upgrading a triplet call (pon) to an added kan (shominkan).</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="1">Under the Sea</th>
      <td>海底撈月</td>
      <td>Haitei Raoyue</td>
      <td>Win with a self-draw on the last drawable tile from the live wall.</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="1">Under the River</th>
      <td>河底撈魚</td>
      <td>Houtei Raoyui</td>
      <td>Win from the last discard of the round.</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
  </tbody>

  <tbody class="jikaze__yaku-han-separator">
    <tr>
      <th scope="row" colspan="5">2 han</th>
    </tr>
  </tbody>
  <tbody>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="2">Double Riichi</th>
      <td>ダブルリーチ</td>
      <td>Daburu Riichi</td>
      <td>Win with a riichi declared on your first discard.</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="2">Seven Pairs</th>
      <td>七対子</td>
      <td>Chiitoitsu</td>
      <td>Seven unique pairs.</td>
      <td class="jikaze__yaku-example">🀌🀌🀙🀙🀛🀛🀠🀠🀑🀑🀀🀀🀅🀅</td>
    </tr>
    <tr class="jikaze__yaku-open-reduced">
      <th scope="row" data-concealed-han-value="2" data-open-han-value="1">Pure Straight</th>
      <td>一通</td>
      <td>Ittsuu</td>
      <td>Three sequences of the same suit forming 1-2-3, 4-5-6, and 7-8-9.</td>
      <td class="jikaze__yaku-example">🀙🀚🀛🀜🀝🀞🀟🀠🀡</td>
    </tr>
    <tr class="jikaze__yaku-open-reduced">
      <th scope="row" data-concealed-han-value="2" data-open-han-value="1">Mixed Triple Sequences</th>
      <td>三色同順</td>
      <td>Sanshoku Doujun</td>
      <td>Same numerical sequence in each suit.</td>
      <td class="jikaze__yaku-example">🀇🀈🀉🀙🀚🀛🀐🀑🀒</td>
    </tr>
    <tr class="jikaze__yaku-open-reduced">
      <th scope="row" data-concealed-han-value="2" data-open-han-value="1">Half Outside Hand</th>
      <td>全帯</td>
      <td>Chanta</td>
      <td>All groups and the pair containing at least one terminal or honor tile. Contains at least one sequence.</td>
      <td class="jikaze__yaku-example">🀇🀈🀉🀙🀚🀛🀡🀡🀡🀖🀗🀘🀀🀀</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="2">Mixed Triple Triplets</th>
      <td>三色同刻</td>
      <td>Sanshoku Doukou</td>
      <td>Three triplets (or quads) of the same number in each suit.</td>
      <td class="jikaze__yaku-example">🀏🀏🀏🀡🀡🀡🀘🀘🀘</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="2">Three Concealed Triplets</th>
      <td>三暗刻</td>
      <td>Sanankou</td>
      <td>Three entirely concealed triplets (or quads) of any tile. May not ron on triplets. The rest of the hand may be called.</td>
      <td class="jikaze__yaku-example">🀌🀌🀌🀠🀠🀠🀓🀓🀓</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="2">Three Quads</th>
      <td>三槓子</td>
      <td>Sankantsu</td>
      <td>Three quads of any tile.</td>
      <td class="jikaze__yaku-example">🀌🀌🀌🀌🀠🀠🀠🀠🀓🀓🀓🀓</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="2">All Triplets</th>
      <td>対々</td>
      <td>Toitoi</td>
      <td>Four triplets (or quads) and a pair.</td>
      <td class="jikaze__yaku-example">🀌🀌🀌🀠🀠🀠🀓🀓🀓🀃🀃🀃🀆🀆</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="2">All Terminals and Honors</th>
      <td>混老頭</td>
      <td>Honroutou</td>
      <td>Only terminal and honor tiles.</td>
      <td class="jikaze__yaku-example">🀏🀏🀏🀡🀡🀡🀘🀘🀘🀃🀃🀃🀆🀆</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="2">Little Three Dragons</th>
      <td>小三元</td>
      <td>Shousangen</td>
      <td>Two dragon triplets (or quads) and a pair of dragon tiles.</td>
      <td class="jikaze__yaku-example">🀆🀆🀅🀅🀅<span class="jikaze__red-dragon-tile">🀄</span><span class="jikaze__red-dragon-tile">🀄</span><span class="jikaze__red-dragon-tile">🀄</span></td>
    </tr>
  </tbody>

  <tbody class="jikaze__yaku-han-separator">
    <tr>
      <th scope="row" colspan="5">3 han</th>
    </tr>
  </tbody>
  <tbody>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="3">Twice Pure Double Sequence</th>
      <td>二盃口</td>
      <td>Ryanpeikou</td>
      <td>Two Pure Double Sequences. Does not stack with Pure Double Sequences.</td>
      <td class="jikaze__yaku-example">🀈🀉🀊🀈🀉🀊🀙🀚🀛🀙🀚🀛</td>
    </tr>
    <tr class="jikaze__yaku-open-reduced">
      <th scope="row" data-concealed-han-value="3" data-open-han-value="2">Half Flush</th>
      <td>混一色</td>
      <td>Honitsu</td>
      <td>Groups and the pair containing only tiles from one suit and honor tiles.</td>
      <td class="jikaze__yaku-example">🀐🀑🀒🀓🀓🀓🀕🀕🀕🀖🀗🀘🀀🀀</td>
    </tr>
    <tr class="jikaze__yaku-open-reduced">
      <th scope="row" data-concealed-han-value="3" data-open-han-value="2">Full Outside Hand</th>
      <td>純全</td>
      <td>Junchan</td>
      <td>At least one terminal tile in each group.</td>
      <td class="jikaze__yaku-example">🀐🀑🀒🀍🀎🀏🀐🀐🀐🀖🀗🀘🀘🀘</td>
    </tr>
  </tbody>

  <tbody class="jikaze__yaku-han-separator">
    <tr>
      <th scope="row" colspan="5">5 han</th>
    </tr>
  </tbody>
  <tbody>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="5">Nagashi Mangan</th>
      <td>流し満貫</td>
      <td>Nagashi Mangan</td>
      <td>Reach exhaustive draw, where every discarded tile was a terminal or honor tile, no one claimed your tiles, and you did not claim anyone else's discards.</td>
      <td class="jikaze__yaku-example">🀈🀉🀊🀈🀉🀊🀙🀚🀛🀙🀚🀛</td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="5">Blessing of Man</th>
      <td>人和</td>
      <td>Renhou</td>
      <td>Winning on a discard in the first un-interrupted go-around and before drawing their first tile. Concealed kan is not allowed.</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
  </tbody>

  <tbody class="jikaze__yaku-han-separator">
    <tr>
      <th scope="row" colspan="5">6 han</th>
    </tr>
  </tbody>
  <tbody>
    <tr class="jikaze__yaku-open-reduced">
      <th scope="row" data-concealed-han-value="6" data-open-han-value="5">Full Flush</th>
      <td>清一色</td>
      <td>Chinitsu</td>
      <td>Only number tiles from one suit.</td>
      <td class="jikaze__yaku-example">🀐🀑🀒🀓🀓🀓🀔🀔🀕🀕🀕🀖🀗🀘</td>
    </tr>
  </tbody>

  <tbody class="jikaze__yaku-han-separator">
    <tr>
      <th scope="row" colspan="5">Yakuman</th>
    </tr>
  </tbody>
  <tbody>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="Yakuman">Thirteen Orphans</th>
      <td>国士無双</td>
      <td>Kokushi Musou</td>
      <td>One tile of each type of terminal and honor and one additional terminal or honor tile.</td>
      <td class="jikaze__yaku-example">🀇🀏🀙🀡🀐🀐🀘🀀🀁🀂🀃🀆🀅<span class="jikaze__red-dragon-tile">🀄</span></td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="Yakuman">Nine Gates</th>
      <td>九連宝燈</td>
      <td>Chuuren Poutou</td>
      <td>1112345678999 in the same suit, plus one additional tile of that suit.</td>
      <td class="jikaze__yaku-example">🀐🀐🀐🀑🀒🀓🀔🀕🀖🀗🀘🀘🀘 🀑</td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="Yakuman">Four Concealed Triplets</th>
      <td>四暗刻</td>
      <td>Suuankou</td>
      <td>Four triplets (or quads) without calling.</td>
      <td class="jikaze__yaku-example">🀚🀚🀚🀞🀞🀞🀖🀖🀖🀅🀅🀅</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="Yakuman">Four Quads</th>
      <td>四槓子</td>
      <td>Suukantsu</td>
      <td>Four quads.</td>
      <td class="jikaze__yaku-example">🀚🀚🀚🀚🀞🀞🀞🀞🀖🀖🀖🀖🀅🀅🀅🀅</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="Yakuman">All Green</th>
      <td>緑一色</td>
      <td>Ryuuiisou</td>
      <td>Only green tiles.</td>
      <td class="jikaze__yaku-example">🀑🀒🀓🀓🀓🀓🀕🀕🀕🀗🀗🀅🀅🀅</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="Yakuman">All Terminals</th>
      <td>清老頭</td>
      <td>Chinroutou</td>
      <td>Only green tiles.</td>
      <td class="jikaze__yaku-example">🀇🀇🀇🀏🀏🀏🀙🀙🀙🀡🀡🀡🀘🀘</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="Yakuman">All Honors</th>
      <td>字一色</td>
      <td>Tsuuiisou</td>
      <td>Only honor tiles.</td>
      <td class="jikaze__yaku-example">🀀🀀🀀🀁🀁🀁🀂🀂🀂🀆🀆🀆🀅🀅</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="Yakuman">Big Three Dragons</th>
      <td>大三元</td>
      <td>Daisangen</td>
      <td>Three triplets (or quads) of dragon tiles.</td>
      <td class="jikaze__yaku-example">🀆🀆🀆🀅🀅🀅<span class="jikaze__red-dragon-tile">🀄</span><span class="jikaze__red-dragon-tile">🀄</span><span class="jikaze__red-dragon-tile">🀄</span></td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="Yakuman">Little Winds</th>
      <td>小四喜</td>
      <td>Shousuushii</td>
      <td>Only honor tiles.</td>
      <td class="jikaze__yaku-example">🀀🀀🀀🀁🀁🀁🀂🀂🀂🀃🀃</td>
    </tr>
    <tr class="jikaze__yaku-open">
      <th scope="row" data-open-han-value="Yakuman">Big Winds</th>
      <td>大四喜</td>
      <td>Daisuushii</td>
      <td>Only honor tiles.</td>
      <td class="jikaze__yaku-example">🀀🀀🀀🀁🀁🀁🀂🀂🀂🀃🀃🀃</td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="Yakuman">Blessing of Heaven</th>
      <td>天和</td>
      <td>Tenhou</td>
      <td>Dealer winning on their initial draw. Concealed kan is not allowed. Does not combine with any other yaku.</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
    <tr class="jikaze__yaku-concealed">
      <th scope="row" data-concealed-han-value="Yakuman">Blessing of Earth</th>
      <td>地和</td>
      <td>Tenhou</td>
      <td>Winning on the self-draw on the initial un-interrupted go-around. Concealed kan is not allowed. Does not combine with any other yaku.</td>
      <td class="jikaze__yaku-example"></td>
    </tr>
  </tbody>
</table>
