/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module provides the `GameRules` struct to hold the state of which game rules are active.

use crate::yaku::RenhouValue;

/// Settings for how the game is set up and played.
#[derive(Debug, Clone, Copy)]
pub struct GameRules {
    /// Number of red five to include in each round.
    akadora: NumberOfAkadora,
    /// # Aotenjou 「青天井」
    ///
    /// If true, all scoring caps and limits are removed. The basic points have the freedom to be
    /// valued as high as possible. The exponential nature of the scoring equation can produce
    /// astronomically large point values. Points are even much higher in conjunction with the
    /// multipliers applied for the dealer, non-dealer, and ron vs. tsumo wins. All yaku retain
    /// their original values. In additionally, yakuman patterns are defaulted at 13 han per yakuman
    /// count; and they are treated like normal yaku when determining the overall han and fu values.
    /// Furthermore, any han applied by dora counts as well to further increase the point values.
    ///
    /// # Base value forumla
    ///
    /// The formula for calculating the base value without caps or limits applied (aotenjou)
    /// or han < 5 (no aotenjou) is:
    ///
    /// ```text
    /// fu * 2 ^ (2 + han)
    /// ```
    ///
    /// # Examples
    ///
    /// ## Kokushi Musou
    ///
    /// ```text
    /// 一九 ①⑨ 19 東南西北 白發發中 +①
    /// ```
    ///
    /// This hand is then scored as 13 han 30 or 40 fu. By dealer, it is worth 7,864,400 (ron) or
    /// 1,966,080 (tsumo).
    ///
    /// ## Shousuushii + Tsuuiisou
    ///
    /// ```text
    /// 東東東 南南 西西 中中中 ⮦北北北
    /// ```
    ///
    /// Waiting for: `南` or `西`
    ///
    /// Every aspect of this hand is considered, not just the yakuman patterns. This hand also
    /// includes the regular yaku of [toitoi] and [sanankou] as well as two or three [yakuhai]
    /// when applicable. This hand stands at 31 han and 50 fu via tsumo:
    ///
    ///   - 13 han from [shousuushii](crate::yaku::Yaku::Shousuushii)
    ///   - 13 han from [tsuuiisou](crate::yaku::Yaku::Tsuuiisou)
    ///   - 2 han from [sanankou]
    ///   - 3 han from the maximum number of [yakuhai] possible
    ///   - Basic points (approx.): 4.29 x 10^11
    ///
    /// Any dora to these examples increases the hand even further.
    ///
    /// [sanankou]: crate::yaku::Yaku::Sanankou
    /// [toitoi]: crate::yaku::Yaku::Toitoi
    /// [yakuhai]: crate::yaku::Yaku::Yakuhai
    aotenjou: bool,
    /// Number of rounds to play in a game.
    ///
    /// A typical game involves two rounds with the East and then South (i.e. hanchan).
    game_length: GameLength,
    /// # Kuikae 「喰い替え」
    ///
    /// Commonly referred to as "swap-calling".
    ///
    /// <div class="warning">
    ///
    /// For now, this field will always be set to `false`.
    ///
    /// </div>
    ///
    /// Not allowing this prevents:
    ///
    /// - A cheap defense against opponents' hands by avoiding a draw and discarding a tile that is suji to the discarded tile, while also not necessarily weakening the player's hand.
    /// - Players scaling their hands towards yaku such as tanyao (all simples) or chanta (half outside hand).
    /// - Players realigning their hands to form sanshoku (mixed triple sequence).
    /// - Skipping a draw by calling and discarding the same called tile.
    ///
    /// When this rule is not in play (nashi, `false`), the following rules are in effect when discarding after making meld call:
    ///
    /// 1. The discarded tile cannot be another copy of the just-called tile.
    /// 2. After a chii, the discarded tile cannot be a tile that would complete the just-called chii.
    ///
    /// ## Examples
    ///
    /// - With `456m` in the hand, a chii call on the left player's discarded `7m`, the calling player cannot discard the `4m` from their hand because it completes the `56m` on the other end.
    /// - With `567p` in the hand, a chii call on the left player's discarded `6p`, the calling player cannot discard the `6p` from their hand because it is another copy of the tile that was just called.
    /// - With `234s` in the hand, a chii call on the left player's discarded `2s`, the calling player cannot discard the `2s` from their hand because it is another copy of the tile that was just called.
    /// - With `444m` in the hand, a pon call on `4m`, the calling player cannot discard the `4m` from their hand because it is another copy of the tile that was just called.
    /// - After calling 3 times, the player has `3456s` in the hand. The left player discards `6s` and calling it would leave `36s` left over, but discarding either of these would be invalid, so this meld cannot be called.
    kuikae: bool,
    /// Kuitan 「喰い断」 is a rule allowing open tanyao. If kuitan is disallowed, then it would be specified as "kuitan nashi", and tanyao would be restricted as a closed-only yaku.
    ///
    /// See [Yaku::Tanyao](crate::yaku::Yaku#kuitan) for more information about this rule.
    kuitan: bool,
    /// Optional yaku that should be enabled.
    local_yaku: LocalYaku,
    /// Minimum number of points needed to win the game at the end of the last round.
    /// If all players have fewer points than this, the game will extend into the next wind round (South in an East-only game, West in a South game).
    ///
    /// The bare minimum is 100 points.
    minimum_points_to_win: u32,
    /// 「連荘」 Dealer repeat. When it occurs, the wind seating and round wind do not rotate.
    ///
    /// Under all rulesets, renchan occurs any time the dealer wins a hand.
    renchan: Renchan,
    /// Value for [Yaku::Renhou](crate::yaku::Yaku::Renhou) depending on the game rules.
    renhou_value: RenhouValue,
    /// Number of points a player must provide to declare riichi.
    riichi_bet_value: u32,
    /// It is normally applied for gambling settings, where one shugi is an added bonus.
    ///
    /// It is generally marked using poker chips, and each player starts with a base number of chips.
    /// Usually, each player begins with five chips.
    shugi: bool,
    /// Points each player will start with.
    starting_points: u32,
    /// When enabled, the game ends when a player falls below zero points.
    tobi: bool,
    /// Number of points each honba (repeat counter) is worth.
    tsumibou: u32,
}

impl GameRules {
    /// Create a new `GameRules`.
    #[allow(clippy::too_many_arguments)]
    pub const fn new(
        akadora: NumberOfAkadora,
        aotenjou: bool,
        game_length: GameLength,
        kuitan: bool,
        local_yaku: LocalYaku,
        minimum_points_to_win: u32,
        renchan: Renchan,
        renhou_value: RenhouValue,
        riichi_bet_value: u32,
        shugi: bool,
        starting_points: u32,
        tobi: bool,
        tsumibou: u32,
    ) -> Self {
        Self {
            akadora,
            aotenjou,
            game_length,
            kuikae: false,
            kuitan,
            minimum_points_to_win,
            local_yaku,
            renchan,
            renhou_value,
            riichi_bet_value,
            shugi,
            starting_points,
            tobi,
            tsumibou,
        }
    }

    /// Establish the rules for a 3-player game.
    #[allow(clippy::too_many_arguments)]
    pub const fn new_3p(
        akadora: NumberOfAkadora,
        aotenjou: bool,
        game_length: GameLength,
        kuitan: bool,
        local_yaku: LocalYaku,
        minimum_points_to_win: u32,
        renchan: Renchan,
        renhou_value: RenhouValue,
        riichi_bet_value: u32,
        shugi: bool,
        starting_points: u32,
        tobi: bool,
        tsumibou: u32,
    ) -> Self {
        Self::new(
            akadora,
            aotenjou,
            game_length,
            kuitan,
            local_yaku,
            minimum_points_to_win,
            renchan,
            renhou_value,
            riichi_bet_value,
            shugi,
            starting_points,
            tobi,
            tsumibou,
        )
    }

    /// Get the default rules for a 3-player game.
    pub fn new_3p_default() -> Self {
        let akadora = NumberOfAkadora::Three;
        let aotenjou = false;
        let game_length = GameLength::Hanchan;
        let kuitan = true;
        let local_yaku = LocalYaku::default();
        let minimum_points_to_win = 40_000;
        let renchan = Renchan::default();
        let renhou_value = RenhouValue::default();
        let riichi_bet_value = 1_000;
        let shugi = false;
        let starting_points = 35_000;
        let tobi = true;
        let tsumibou = 200;

        Self::new_3p(
            akadora,
            aotenjou,
            game_length,
            kuitan,
            local_yaku,
            minimum_points_to_win,
            renchan,
            renhou_value,
            riichi_bet_value,
            shugi,
            starting_points,
            tobi,
            tsumibou,
        )
    }

    /// Establish the rules for a 4-player game.
    #[allow(clippy::too_many_arguments)]
    pub const fn new_4p(
        akadora: NumberOfAkadora,
        aotenjou: bool,
        game_length: GameLength,
        kuitan: bool,
        local_yaku: LocalYaku,
        minimum_points_to_win: u32,
        renchan: Renchan,
        renhou_value: RenhouValue,
        riichi_bet_value: u32,
        shugi: bool,
        starting_points: u32,
        tobi: bool,
        tsumibou: u32,
    ) -> Self {
        Self::new(
            akadora,
            aotenjou,
            game_length,
            kuitan,
            local_yaku,
            minimum_points_to_win,
            renchan,
            renhou_value,
            riichi_bet_value,
            shugi,
            starting_points,
            tobi,
            tsumibou,
        )
    }

    /// Get the default rules for a 4-player game.
    pub fn new_4p_default() -> Self {
        let akadora = NumberOfAkadora::Three;
        let aotenjou = false;
        let game_length = GameLength::Hanchan;
        let kuitan = true;
        let local_yaku = LocalYaku::default();
        let minimum_points_to_win = 30_000;
        let renchan = Renchan::default();
        let renhou_value = RenhouValue::default();
        let riichi_bet_value = 1_000;
        let shugi = false;
        let starting_points = 25_000;
        let tobi = true;
        let tsumibou = 300;

        Self::new_4p(
            akadora,
            aotenjou,
            game_length,
            kuitan,
            local_yaku,
            minimum_points_to_win,
            renchan,
            renhou_value,
            riichi_bet_value,
            shugi,
            starting_points,
            tobi,
            tsumibou,
        )
    }

    /// Get how many akadora (red fives) are included in the game.
    pub const fn akadora(&self) -> &NumberOfAkadora {
        &self.akadora
    }

    /// Get the state of whether or not the aotenjou (no limits) scoring system is enabled.
    pub const fn aotenjou(&self) -> bool {
        self.aotenjou
    }

    /// Get the number of rounds a game should last.
    pub const fn game_length(&self) -> &GameLength {
        &self.game_length
    }

    /// Get the state of the kuikae (swap calling) rule.
    ///
    /// **NOTE**: For now, this field will always be set to `false`.
    ///
    /// `true` (ari) means that swap calling is allowed, while `false` (nashi) means swap calling is not allowed.
    pub const fn kuikae(&self) -> bool {
        self.kuikae
    }

    /// Get the state of the kuitan (open tanyao) rule.
    pub const fn kuitan(&self) -> bool {
        self.kuitan
    }

    /// Get the [LocalYaku] to see which yaku are enabled.
    pub const fn local_yaku(&self) -> &LocalYaku {
        &self.local_yaku
    }

    /// Get the number of points needed to win after the last round.
    pub const fn minimum_points_to_win(&self) -> u32 {
        self.minimum_points_to_win
    }

    /// Get the [Renchan] condition for exhaustive draw.
    pub const fn renchan(&self) -> &Renchan {
        &self.renchan
    }

    /// Get the value of the [renhou](crate::yaku::Yaku::Renhou) yaku for the game.
    pub const fn renhou_value(&self) -> &RenhouValue {
        &self.renhou_value
    }

    /// Get the number of points a riichi bet is worth.
    pub const fn riichi_bet_value(&self) -> u32 {
        self.riichi_bet_value
    }

    /// Get the state of the shugi feature.
    pub const fn shugi(&self) -> bool {
        self.shugi
    }

    /// Get the number of player starting points.
    pub const fn starting_points(&self) -> u32 {
        self.starting_points
    }

    /// Get the state of the tobi rule.
    ///
    /// Tobi: Rule where the game ends when a player's score falls below zero.
    ///
    /// When enabled (`true`), the game ends when a player falls below zero points.
    pub const fn tobi(&self) -> bool {
        self.tobi
    }

    /// Get a mutable reference to the `tobi` field.
    ///
    /// Tobi: Rule where the game ends when a player's score falls below zero.
    ///
    /// When enabled (`true`), the game ends when a player falls below zero points.
    pub fn tobi_mut(&mut self) -> &mut bool {
        &mut self.tobi
    }

    /// Get the number of points each honba is worth.
    pub const fn tsumibou(&self) -> u32 {
        self.tsumibou
    }
}

/// How many rounds a game should last.
#[derive(Debug, Default, Clone, Copy)]
pub enum GameLength {
    /// A game that ends after the East round.
    Tonpuusen,
    /// A game that ends after the East and then South rounds have been played.
    #[default]
    Hanchan,
}

/// Yaku that are used in the standard ruleset.
///
/// This contains the settings of which optional yaku are enabled for a game.
#[derive(Debug, Default, Clone, Copy)]
pub struct LocalYaku {
    /// Hand composed of seven pairs of honor tiles.
    ///
    /// See [Yaku::Daichiishin](crate::yaku::Yaku::Daichiishin) for more information about the yaku.
    daichiishin: bool,
    /// Allow revealing the hand during a round to gain an additional 1-han.
    ///
    /// See [Yaku::OpenRiichi](crate::yaku::Yaku::OpenRiichi) for more information about the yaku.
    open_riichi: bool,
}

impl LocalYaku {
    /// Create a new `LocalYaku`.
    pub const fn new(daichiishin: bool, open_riichi: bool) -> Self {
        Self {
            daichiishin,
            open_riichi,
        }
    }

    /// Get the setting of whether or not [Yaku::Daichiishin](crate::yaku::Yaku::Daichiishin) is enabled.
    pub const fn daichiishin(&self) -> bool {
        self.daichiishin
    }

    /// Get the setting for open riichi.
    pub const fn open_riichi(&self) -> bool {
        self.open_riichi
    }
}

/// Number of akadora (red fives) to include in a game.
#[derive(Debug, Default, Clone, Copy)]
#[repr(u8)]
pub enum NumberOfAkadora {
    /// No red fives.
    None = 0,
    /// Three red fives, one for each suit.
    ///
    /// In a 3-player game, two red fives are included instead, with the 5-man tile absent.
    #[default]
    Three = 3,
    /// Four red fives, one for each suit, plus an extra for the pinzu suit.
    ///
    /// In a 3-player game, three red fives are included instead, with the 5-man tile absent and with two 5-pin tiles.
    Four = 4,
}

/// Variations of the renchan rule for exhaustive draw.
///
/// Under all rulesets, renchan occurs any time the dealer wins a hand.
#[derive(Debug, Default, Clone, Copy)]
pub enum Renchan {
    /// No conditions for renchan, even at an exhaustive draw.
    None,
    /// The most common form, under which a renchan occurs at an exhaustive draw if the dealer declares a tenpai hand.
    #[default]
    Tenpai,
    /// The most permissive form, under which a renchan occurs at any exhaustive draw regardless of players' tenpai status.
    ///
    /// Usually, this variation is only found in the South round where the East round is played with [tenpai](Self::Tenpai) renchan.
    Ryuukyoku,
}
