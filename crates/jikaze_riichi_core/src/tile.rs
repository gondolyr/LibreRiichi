/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Module to handle all things related to tiles.

pub mod tile_set;
pub mod tile_suit;

use core::cmp::Ordering;
use core::convert::TryFrom;
use core::fmt;

pub use tile_set::TileSet;
pub use tile_suit::TileSuit;

/// The number of unique tile types for a 4-player game.
pub const NUM_UNIQUE_TILES_4P: usize = 34;
/// There are 4 copies of every unique tile.
pub const TOTAL_NUM_TILES_4P: usize = NUM_UNIQUE_TILES_4P * 4;
/// [`Tile`] variants in an iterable structure, excluding red fives.
pub const UNIQUE_TILES_4P: [Tile; NUM_UNIQUE_TILES_4P] = [
    Tile::Manzu1,
    Tile::Manzu2,
    Tile::Manzu3,
    Tile::Manzu4,
    Tile::Manzu5,
    Tile::Manzu6,
    Tile::Manzu7,
    Tile::Manzu8,
    Tile::Manzu9,
    Tile::Pinzu1,
    Tile::Pinzu2,
    Tile::Pinzu3,
    Tile::Pinzu4,
    Tile::Pinzu5,
    Tile::Pinzu6,
    Tile::Pinzu7,
    Tile::Pinzu8,
    Tile::Pinzu9,
    Tile::Souzu1,
    Tile::Souzu2,
    Tile::Souzu3,
    Tile::Souzu4,
    Tile::Souzu5,
    Tile::Souzu6,
    Tile::Souzu7,
    Tile::Souzu8,
    Tile::Souzu9,
    Tile::WindEast,
    Tile::WindSouth,
    Tile::WindWest,
    Tile::WindNorth,
    Tile::DragonWhite,
    Tile::DragonGreen,
    Tile::DragonRed,
];

/// The number of unique tile types for a 3-player game.
pub const NUM_UNIQUE_TILES_3P: usize = 27;
/// There are 4 copies of every unique tile.
pub const TOTAL_NUM_TILES_3P: usize = NUM_UNIQUE_TILES_3P * 4;
/// [`Tile`] variants in an iterable structure, excluding red fives.
pub const UNIQUE_TILES_3P: [Tile; NUM_UNIQUE_TILES_3P] = [
    Tile::Manzu1,
    Tile::Manzu9,
    Tile::Pinzu1,
    Tile::Pinzu2,
    Tile::Pinzu3,
    Tile::Pinzu4,
    Tile::Pinzu5,
    Tile::Pinzu6,
    Tile::Pinzu7,
    Tile::Pinzu8,
    Tile::Pinzu9,
    Tile::Souzu1,
    Tile::Souzu2,
    Tile::Souzu3,
    Tile::Souzu4,
    Tile::Souzu5,
    Tile::Souzu6,
    Tile::Souzu7,
    Tile::Souzu8,
    Tile::Souzu9,
    Tile::WindEast,
    Tile::WindSouth,
    Tile::WindWest,
    Tile::WindNorth,
    Tile::DragonWhite,
    Tile::DragonGreen,
    Tile::DragonRed,
];

/// Aggregation of the tile values.
///
/// | Encoding   |  Shorthand  | Category (EN) | Category (JP) |
/// |------------|-------------|---------------|---------------|
/// | 0  ..= 8   |  1m ..= 9m  | characters    | 萬子          |
/// | 9  ..= 17  |  1p ..= 9p  | dots          | 筒子          |
/// | 18 ..= 26  |  1s ..= 9s  | bamboos       | 索子          |
/// | 27 ..= 30  |  1z ..= 4z  | winds         | 風牌          |
/// | 31, 32, 33 |  5z, 6z, 7z | dragons       | 三元牌        |
/// | 34, 35, 36 |  0m, 0p, 0s | reds          | 赤牌          |
#[derive(Debug, Clone, Copy, Eq)]
#[repr(u8)]
pub enum Tile {
    // Character tile type.
    /// Ii man.
    Manzu1 = 0,
    /// Ryan man.
    Manzu2,
    /// San man.
    Manzu3,
    /// Suu man.
    Manzu4,
    /// Uu man.
    Manzu5,
    /// Rou man.
    Manzu6,
    /// Chii man.
    Manzu7,
    /// Paa man.
    Manzu8,
    /// Kyuu man.
    Manzu9,
    // Circle tile type.
    /// Ii pin.
    Pinzu1,
    /// Ryan pin.
    Pinzu2,
    /// San pin.
    Pinzu3,
    /// Suu pin.
    Pinzu4,
    /// Uu pin.
    Pinzu5,
    /// Rou pin.
    Pinzu6,
    /// Chii pin.
    Pinzu7,
    /// Paa pin.
    Pinzu8,
    /// Kyuu pin.
    Pinzu9,
    // Bamboo tile type.
    /// Ii sou.
    Souzu1,
    /// Ryan sou.
    Souzu2,
    /// San sou.
    Souzu3,
    /// Suu sou.
    Souzu4,
    /// Uu sou.
    Souzu5,
    /// Rou sou.
    Souzu6,
    /// Chii sou.
    Souzu7,
    /// Paa sou.
    Souzu8,
    /// Kyuu sou.
    Souzu9,
    // Wind tile type.
    //
    // Has four variants:
    //
    // - Ton (東, East)
    // - Nan (南, South)
    // - Shaa (西, West)
    // - Pei (北, North)
    /// 東 (Ton).
    WindEast,
    /// 南 (Nan).
    WindSouth,
    /// 西 (Shaa).
    WindWest,
    /// 北 (Pei).
    WindNorth,
    // Dragon tile type.
    //
    // Has three variants:
    //
    // - Haku (白, White)
    // - Hatsu (發, Green)
    // - Chun (中, Red)
    /// 白 (Haku).
    DragonWhite,
    /// 發 (Hatsu).
    DragonGreen,
    /// 中 (Chun).
    DragonRed,
    // Optional red 5 tiles.
    //
    // This should be sorted between the 4 and normal 5 tiles.
    // Use the following ordering key formula:
    //
    // ```rust
    // if (self as u8) <= 33 {
    //     (self as u8) * 2
    // } else {
    //     7 + ((self as u8) - 34) * 18
    // }
    // ```
    /// Aka uu man.
    ManzuRed5,
    /// Aka uu pin.
    PinzuRed5,
    /// Aka uu sou.
    SouzuRed5,
}

impl From<Tile> for u8 {
    #[allow(clippy::as_conversions)]
    fn from(value: Tile) -> Self {
        // Tile has `#[repr(u8)]` so it should be safe to use `as` here.
        value as u8
    }
}

impl From<&Tile> for u8 {
    fn from(value: &Tile) -> Self {
        u8::from(*value)
    }
}

// Implementing this here allows for the use of `==` without needing to manually implement the
// exception of equating a regular 5 to a red 5.
// Using `match` on the 5's will still distinguish a regular 5 and a red 5.
impl PartialEq for Tile {
    fn eq(&self, other: &Self) -> bool {
        match (self, other) {
            (Tile::Manzu5, Tile::ManzuRed5)
            | (Tile::ManzuRed5, Tile::Manzu5)
            | (Tile::Pinzu5, Tile::PinzuRed5)
            | (Tile::PinzuRed5, Tile::Pinzu5)
            | (Tile::Souzu5, Tile::SouzuRed5)
            | (Tile::SouzuRed5, Tile::Souzu5) => true,
            (this, other_tile) => u8::from(this) == u8::from(other_tile),
        }
    }
}

impl PartialOrd for Tile {
    fn partial_cmp(&self, other: &Self) -> Option<Ordering> {
        Some(self.cmp(other))
    }
}

// Implementing this permits the red 5s to be placed between the 4 and regular 5 when sorting
// and also allows for accurate comparisons, using the binary operators, to be made if needed.
impl Ord for Tile {
    fn cmp(&self, other: &Self) -> Ordering {
        let this_value = normalize_tile_value_for_ordering(self);
        let other_value = normalize_tile_value_for_ordering(other);

        this_value.cmp(&other_value)
    }
}

/// Normalize the tile value to compare against another tile for sorting/ordering.
///
/// # Panics
///
/// This should never panic unless the [`NUM_UNIQUE_TILES_4P`] constant is changed to a value that cannot be converted to a `u8`.
fn normalize_tile_value_for_ordering(tile: &Tile) -> u8 {
    const NUM_HONOR_TILES: u8 = 7;
    // Because the enum values start from 0, we need to add 1 for this formula to work.
    const OFFSET: u8 = 1;

    if u8::from(tile) < core::convert::TryInto::<u8>::try_into(NUM_UNIQUE_TILES_4P).unwrap() {
        u8::from(tile) * 2
    } else {
        (NUM_HONOR_TILES + OFFSET)
            + (u8::from(tile)
                - (core::convert::TryInto::<u8>::try_into(NUM_UNIQUE_TILES_4P).unwrap()))
                * 18
    }
}

impl TryFrom<u8> for Tile {
    type Error = ();

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        match value {
            0 => Ok(Self::Manzu1),
            1 => Ok(Self::Manzu2),
            2 => Ok(Self::Manzu3),
            3 => Ok(Self::Manzu4),
            4 => Ok(Self::Manzu5),
            5 => Ok(Self::Manzu6),
            6 => Ok(Self::Manzu7),
            7 => Ok(Self::Manzu8),
            8 => Ok(Self::Manzu9),

            9 => Ok(Self::Pinzu1),
            10 => Ok(Self::Pinzu2),
            11 => Ok(Self::Pinzu3),
            12 => Ok(Self::Pinzu4),
            13 => Ok(Self::Pinzu5),
            14 => Ok(Self::Pinzu6),
            15 => Ok(Self::Pinzu7),
            16 => Ok(Self::Pinzu8),
            17 => Ok(Self::Pinzu9),

            18 => Ok(Self::Souzu1),
            19 => Ok(Self::Souzu2),
            20 => Ok(Self::Souzu3),
            21 => Ok(Self::Souzu4),
            22 => Ok(Self::Souzu5),
            23 => Ok(Self::Souzu6),
            24 => Ok(Self::Souzu7),
            25 => Ok(Self::Souzu8),
            26 => Ok(Self::Souzu9),

            27 => Ok(Self::WindEast),
            28 => Ok(Self::WindSouth),
            29 => Ok(Self::WindWest),
            30 => Ok(Self::WindNorth),

            31 => Ok(Self::DragonWhite),
            32 => Ok(Self::DragonGreen),
            33 => Ok(Self::DragonRed),

            34 => Ok(Self::ManzuRed5),
            35 => Ok(Self::PinzuRed5),
            36 => Ok(Self::SouzuRed5),

            _ => Err(()),
        }
    }
}

impl Tile {
    /// Determine if the current tile type is green.
    ///
    /// Used to check for the All Greens yaku.
    pub const fn is_green(&self) -> bool {
        matches!(
            self,
            Self::Souzu2
                | Self::Souzu3
                | Self::Souzu4
                | Self::Souzu6
                | Self::Souzu8
                | Self::DragonGreen
        )
    }

    /// Determine if the current tile type is an honor tile.
    pub const fn is_honor(&self) -> bool {
        matches!(
            self,
            Self::WindEast
                | Self::WindSouth
                | Self::WindWest
                | Self::WindNorth
                | Self::DragonWhite
                | Self::DragonGreen
                | Self::DragonRed
        )
    }

    /// Determine if the current tile is a red five.
    pub const fn is_red_five(&self) -> bool {
        matches!(self, Self::ManzuRed5 | Self::PinzuRed5 | Self::SouzuRed5)
    }

    /// Determine if the current tile is a regular, non-red, five.
    pub const fn is_regular_five(&self) -> bool {
        matches!(self, Self::Manzu5 | Self::Pinzu5 | Self::Souzu5)
    }

    /// Determine if the current tile type is a terminal tile.
    pub const fn is_terminal(&self) -> bool {
        matches!(
            self,
            Self::Manzu1 | Self::Manzu9 | Self::Pinzu1 | Self::Pinzu9 | Self::Souzu1 | Self::Souzu9
        )
    }

    /// Determine if the current tile has a tile next in a sequence.
    ///
    /// 9s in suits and honor tiles will return `false`.
    pub const fn has_next_tile(&self) -> bool {
        if self.is_honor() {
            return false;
        }

        match self {
            Self::Manzu9 | Self::Pinzu9 | Self::Souzu9 => false,
            // Honor tiles are covered with the `is_honor` check above.
            _ => true,
        }
    }

    /// Get the next tile in the sequence.
    ///
    /// Useful for determining which tile is worth bonus points from the dora indicator.
    pub const fn next_tile(&self) -> Self {
        match self {
            Self::Manzu1 => Self::Manzu2,
            Self::Manzu2 => Self::Manzu3,
            Self::Manzu3 => Self::Manzu4,
            Self::Manzu4 => Self::Manzu5,
            Self::Manzu5 => Self::Manzu6,
            Self::Manzu6 => Self::Manzu7,
            Self::Manzu7 => Self::Manzu8,
            Self::Manzu8 => Self::Manzu9,
            Self::Manzu9 => Self::Manzu1,

            Self::Pinzu1 => Self::Pinzu2,
            Self::Pinzu2 => Self::Pinzu3,
            Self::Pinzu3 => Self::Pinzu4,
            Self::Pinzu4 => Self::Pinzu5,
            Self::Pinzu5 => Self::Pinzu6,
            Self::Pinzu6 => Self::Pinzu7,
            Self::Pinzu7 => Self::Pinzu8,
            Self::Pinzu8 => Self::Pinzu9,
            Self::Pinzu9 => Self::Pinzu1,

            Self::Souzu1 => Self::Souzu2,
            Self::Souzu2 => Self::Souzu3,
            Self::Souzu3 => Self::Souzu4,
            Self::Souzu4 => Self::Souzu5,
            Self::Souzu5 => Self::Souzu6,
            Self::Souzu6 => Self::Souzu7,
            Self::Souzu7 => Self::Souzu8,
            Self::Souzu8 => Self::Souzu9,
            Self::Souzu9 => Self::Souzu1,

            Self::WindEast => Self::WindSouth,
            Self::WindSouth => Self::WindWest,
            Self::WindWest => Self::WindNorth,
            Self::WindNorth => Self::WindEast,

            Self::DragonWhite => Self::DragonGreen,
            Self::DragonGreen => Self::DragonRed,
            Self::DragonRed => Self::DragonWhite,

            Self::ManzuRed5 => Self::Manzu6,
            Self::PinzuRed5 => Self::Pinzu6,
            Self::SouzuRed5 => Self::Souzu6,
        }
    }

    /// Get the previous tile in the sequence.
    pub const fn previous_tile(&self) -> Self {
        match self {
            Self::Manzu1 => Self::Manzu9,
            Self::Manzu2 => Self::Manzu1,
            Self::Manzu3 => Self::Manzu2,
            Self::Manzu4 => Self::Manzu3,
            Self::Manzu5 => Self::Manzu4,
            Self::Manzu6 => Self::Manzu5,
            Self::Manzu7 => Self::Manzu6,
            Self::Manzu8 => Self::Manzu7,
            Self::Manzu9 => Self::Manzu8,

            Self::Pinzu1 => Self::Pinzu9,
            Self::Pinzu2 => Self::Pinzu1,
            Self::Pinzu3 => Self::Pinzu2,
            Self::Pinzu4 => Self::Pinzu3,
            Self::Pinzu5 => Self::Pinzu4,
            Self::Pinzu6 => Self::Pinzu5,
            Self::Pinzu7 => Self::Pinzu6,
            Self::Pinzu8 => Self::Pinzu7,
            Self::Pinzu9 => Self::Pinzu8,

            Self::Souzu1 => Self::Souzu9,
            Self::Souzu2 => Self::Souzu1,
            Self::Souzu3 => Self::Souzu2,
            Self::Souzu4 => Self::Souzu3,
            Self::Souzu5 => Self::Souzu4,
            Self::Souzu6 => Self::Souzu5,
            Self::Souzu7 => Self::Souzu6,
            Self::Souzu8 => Self::Souzu7,
            Self::Souzu9 => Self::Souzu8,

            Self::WindEast => Self::WindNorth,
            Self::WindSouth => Self::WindEast,
            Self::WindWest => Self::WindSouth,
            Self::WindNorth => Self::WindWest,

            Self::DragonWhite => Self::DragonRed,
            Self::DragonGreen => Self::DragonWhite,
            Self::DragonRed => Self::DragonGreen,

            // There isn't a good way to go backwards into a red 5, though it probably doesn't matter.
            Self::ManzuRed5 => Self::Manzu4,
            Self::PinzuRed5 => Self::Pinzu4,
            Self::SouzuRed5 => Self::Souzu4,
        }
    }

    /// Get the Japanese phonetic representation (romaji) of the tile.
    pub const fn romaji(&self) -> &str {
        match self {
            Self::Manzu1 => "ii wan",
            Self::Manzu2 => "ryan wan",
            Self::Manzu3 => "san wan",
            Self::Manzu4 => "suu wan",
            Self::Manzu5 => "uu wan",
            Self::Manzu6 => "rou wan",
            Self::Manzu7 => "chii wan",
            Self::Manzu8 => "paa wan",
            Self::Manzu9 => "kyuu wan",

            Self::Pinzu1 => "ii pin",
            Self::Pinzu2 => "ryan pin",
            Self::Pinzu3 => "san pin",
            Self::Pinzu4 => "suu pin",
            Self::Pinzu5 => "uu pin",
            Self::Pinzu6 => "rou pin",
            Self::Pinzu7 => "chii pin",
            Self::Pinzu8 => "paa pin",
            Self::Pinzu9 => "kyuu pin",

            Self::Souzu1 => "ii sou",
            Self::Souzu2 => "ryan sou",
            Self::Souzu3 => "san sou",
            Self::Souzu4 => "suu sou",
            Self::Souzu5 => "uu sou",
            Self::Souzu6 => "rou sou",
            Self::Souzu7 => "chii sou",
            Self::Souzu8 => "paa sou",
            Self::Souzu9 => "kyuu sou",

            Self::WindEast => "ton",
            Self::WindSouth => "nan",
            Self::WindWest => "shaa",
            Self::WindNorth => "pei",

            Self::DragonWhite => "haku",
            Self::DragonGreen => "hatsu",
            Self::DragonRed => "chun",

            Self::ManzuRed5 => "akadora man",
            Self::PinzuRed5 => "akadora pin",
            Self::SouzuRed5 => "akadora sou",
        }
    }

    /// Get a symbolic character representation of the tile.
    pub const fn symbol(&self) -> &str {
        match self {
            Self::Manzu1 => "一",
            Self::Manzu2 => "ニ",
            Self::Manzu3 => "三",
            Self::Manzu4 => "四",
            // The '伍' character is usually used instead of '五'.
            Self::Manzu5 => "伍",
            Self::Manzu6 => "六",
            Self::Manzu7 => "七",
            Self::Manzu8 => "八",
            Self::Manzu9 => "九",

            Self::Pinzu1 => "①",
            Self::Pinzu2 => "②",
            Self::Pinzu3 => "③",
            Self::Pinzu4 => "④",
            Self::Pinzu5 => "⑤",
            Self::Pinzu6 => "⑥",
            Self::Pinzu7 => "⑦",
            Self::Pinzu8 => "⑧",
            Self::Pinzu9 => "⑨",

            Self::Souzu1 => "1",
            Self::Souzu2 => "2",
            Self::Souzu3 => "3",
            Self::Souzu4 => "4",
            Self::Souzu5 => "5",
            Self::Souzu6 => "6",
            Self::Souzu7 => "7",
            Self::Souzu8 => "8",
            Self::Souzu9 => "9",

            Self::WindEast => "東",
            Self::WindSouth => "南",
            Self::WindWest => "西",
            Self::WindNorth => "北",

            Self::DragonWhite => "白",
            Self::DragonGreen => "發",
            Self::DragonRed => "中",

            Self::ManzuRed5 => "赤牌萬",
            Self::PinzuRed5 => "赤牌筒",
            Self::SouzuRed5 => "赤牌索",
        }
    }

    /// Get the suit of the tile.
    ///
    /// Useful for checking things like flushes or half-flushes.
    pub const fn suit(&self) -> TileSuit {
        match self {
            Self::Manzu1
            | Self::Manzu2
            | Self::Manzu3
            | Self::Manzu4
            | Self::Manzu5
            | Self::ManzuRed5
            | Self::Manzu6
            | Self::Manzu7
            | Self::Manzu8
            | Self::Manzu9 => TileSuit::Manzu,
            Self::Pinzu1
            | Self::Pinzu2
            | Self::Pinzu3
            | Self::Pinzu4
            | Self::Pinzu5
            | Self::PinzuRed5
            | Self::Pinzu6
            | Self::Pinzu7
            | Self::Pinzu8
            | Self::Pinzu9 => TileSuit::Pinzu,
            Self::Souzu1
            | Self::Souzu2
            | Self::Souzu3
            | Self::Souzu4
            | Self::Souzu5
            | Self::SouzuRed5
            | Self::Souzu6
            | Self::Souzu7
            | Self::Souzu8
            | Self::Souzu9 => TileSuit::Souzu,
            Self::WindEast | Self::WindSouth | Self::WindWest | Self::WindNorth => TileSuit::Wind,
            Self::DragonWhite | Self::DragonGreen | Self::DragonRed => TileSuit::Dragon,
        }
    }

    /// Get the Unicode character of the tile.
    pub const fn unicode(&self) -> &str {
        match self {
            Self::Manzu1 => "🀇",
            Self::Manzu2 => "🀈",
            Self::Manzu3 => "🀉",
            Self::Manzu4 => "🀊",
            Self::Manzu5 => "🀋",
            Self::Manzu6 => "🀌",
            Self::Manzu7 => "🀍",
            Self::Manzu8 => "🀎",
            Self::Manzu9 => "🀏",

            Self::Pinzu1 => "🀙",
            Self::Pinzu2 => "🀚",
            Self::Pinzu3 => "🀛",
            Self::Pinzu4 => "🀜",
            Self::Pinzu5 => "🀝",
            Self::Pinzu6 => "🀞",
            Self::Pinzu7 => "🀟",
            Self::Pinzu8 => "🀠",
            Self::Pinzu9 => "🀡",

            Self::Souzu1 => "🀐",
            Self::Souzu2 => "🀑",
            Self::Souzu3 => "🀒",
            Self::Souzu4 => "🀓",
            Self::Souzu5 => "🀔",
            Self::Souzu6 => "🀕",
            Self::Souzu7 => "🀖",
            Self::Souzu8 => "🀗",
            Self::Souzu9 => "🀘",

            Self::WindEast => "🀀",
            Self::WindSouth => "🀁",
            Self::WindWest => "🀂",
            Self::WindNorth => "🀃",

            Self::DragonWhite => "🀆",
            Self::DragonGreen => "🀅",
            // The standard Unicode character is an emoji, 🀄, but we want a textual representation here,
            // which contains extra modifier characters.
            Self::DragonRed => "🀄︎",

            Self::ManzuRed5 => "🀋",
            Self::PinzuRed5 => "🀝",
            Self::SouzuRed5 => "🀔",
        }
    }

    /// Return the number part of the shorthand with the red dora represented as `0`.
    pub fn shorthand_num(&self) -> u8 {
        let num = u8::from(self);
        if num < 34 { num % 9 + 1 } else { 0 }
    }

    /// Get the character tile equivalent of the given suit tile.
    pub const fn manzu_tile_equivalent(&self) -> Option<Self> {
        match self {
            Self::Manzu1
            | Self::Manzu2
            | Self::Manzu3
            | Self::Manzu4
            | Self::Manzu5
            | Self::Manzu6
            | Self::Manzu7
            | Self::Manzu8
            | Self::Manzu9
            | Self::ManzuRed5 => Some(*self),
            Self::Pinzu1 => Some(Self::Manzu1),
            Self::Pinzu2 => Some(Self::Manzu2),
            Self::Pinzu3 => Some(Self::Manzu3),
            Self::Pinzu4 => Some(Self::Manzu4),
            Self::Pinzu5 | Self::PinzuRed5 => Some(Self::Manzu5),
            Self::Pinzu6 => Some(Self::Manzu6),
            Self::Pinzu7 => Some(Self::Manzu7),
            Self::Pinzu8 => Some(Self::Manzu8),
            Self::Pinzu9 => Some(Self::Manzu9),
            Self::Souzu1 => Some(Self::Manzu1),
            Self::Souzu2 => Some(Self::Manzu2),
            Self::Souzu3 => Some(Self::Manzu3),
            Self::Souzu4 => Some(Self::Manzu4),
            Self::Souzu5 | Self::SouzuRed5 => Some(Self::Manzu5),
            Self::Souzu6 => Some(Self::Manzu6),
            Self::Souzu7 => Some(Self::Manzu7),
            Self::Souzu8 => Some(Self::Manzu8),
            Self::Souzu9 => Some(Self::Manzu9),
            _ => None,
        }
    }

    /// Get the circle tile equivalent of the given suit tile.
    pub const fn pinzu_tile_equivalent(&self) -> Option<Self> {
        match self {
            Self::Manzu1 => Some(Self::Pinzu1),
            Self::Manzu2 => Some(Self::Pinzu2),
            Self::Manzu3 => Some(Self::Pinzu3),
            Self::Manzu4 => Some(Self::Pinzu4),
            Self::Manzu5 | Self::ManzuRed5 => Some(Self::Pinzu5),
            Self::Manzu6 => Some(Self::Pinzu6),
            Self::Manzu7 => Some(Self::Pinzu7),
            Self::Manzu8 => Some(Self::Pinzu8),
            Self::Manzu9 => Some(Self::Pinzu9),
            Self::Pinzu1
            | Self::Pinzu2
            | Self::Pinzu3
            | Self::Pinzu4
            | Self::Pinzu5
            | Self::Pinzu6
            | Self::Pinzu7
            | Self::Pinzu8
            | Self::Pinzu9
            | Self::PinzuRed5 => Some(*self),
            Self::Souzu1 => Some(Self::Pinzu1),
            Self::Souzu2 => Some(Self::Pinzu2),
            Self::Souzu3 => Some(Self::Pinzu3),
            Self::Souzu4 => Some(Self::Pinzu4),
            Self::Souzu5 | Self::SouzuRed5 => Some(Self::Pinzu5),
            Self::Souzu6 => Some(Self::Pinzu6),
            Self::Souzu7 => Some(Self::Pinzu7),
            Self::Souzu8 => Some(Self::Pinzu8),
            Self::Souzu9 => Some(Self::Pinzu9),
            _ => None,
        }
    }

    /// Get the bamboo tile equivalent of the given suit tile.
    pub const fn souzu_tile_equivalent(&self) -> Option<Self> {
        match self {
            Self::Manzu1 => Some(Self::Souzu1),
            Self::Manzu2 => Some(Self::Souzu2),
            Self::Manzu3 => Some(Self::Souzu3),
            Self::Manzu4 => Some(Self::Souzu4),
            Self::Manzu5 | Self::ManzuRed5 => Some(Self::Souzu5),
            Self::Manzu6 => Some(Self::Souzu6),
            Self::Manzu7 => Some(Self::Souzu7),
            Self::Manzu8 => Some(Self::Souzu8),
            Self::Manzu9 => Some(Self::Souzu9),
            Self::Pinzu1 => Some(Self::Souzu1),
            Self::Pinzu2 => Some(Self::Souzu2),
            Self::Pinzu3 => Some(Self::Souzu3),
            Self::Pinzu4 => Some(Self::Souzu4),
            Self::Pinzu5 | Self::PinzuRed5 => Some(Self::Souzu5),
            Self::Pinzu6 => Some(Self::Souzu6),
            Self::Pinzu7 => Some(Self::Souzu7),
            Self::Pinzu8 => Some(Self::Souzu8),
            Self::Pinzu9 => Some(Self::Souzu9),
            Self::Souzu1
            | Self::Souzu2
            | Self::Souzu3
            | Self::Souzu4
            | Self::Souzu5
            | Self::Souzu6
            | Self::Souzu7
            | Self::Souzu8
            | Self::Souzu9
            | Self::SouzuRed5 => Some(*self),
            _ => None,
        }
    }

    /// Get the wall code value for the tile.
    ///
    /// This can be used to generate a string representation of the wall for purposes such as wall integrity verification with hashing.
    ///
    /// # Glossary
    ///
    /// See [`Tile`] for a table lookup.
    ///
    /// - `m` represents Man. `1m` to `9m` represent 1-Man to 9-Man. `5m` stands for regular 5-Man while `0m` for red 5-Man.
    /// - `p` represents Pin. `1p` to `9p` represent 1-Pin to 9-Pin. `5p` stands for regular 5-Pin while `0p` for red 5-Pin.
    /// - `s` represents Sou. `1s` to `9s` represent 1-Sou to 9-Sou. `5s` stands for regular 5-Sou while `0s` for red 5-Sou.
    /// - `z` represents honor tiles. `1z` to `4z` represent East, South, West, and North respectively.  `5z` to `7z` represent White, Green, and Red dragons respectively.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use jikaze_riichi_core::tile::Tile;
    ///
    /// let tiles = vec![
    ///     Tile::Manzu4,
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Pinzu8,
    ///     Tile::Souzu2,
    ///     Tile::WindNorth,
    ///     Tile::DragonRed,
    /// ];
    ///
    /// let actual: Vec<&str> = tiles
    ///     .iter()
    ///     .map(|tile| tile.wall_code())
    ///     .collect();
    /// let expected = vec![
    ///     "4m",
    ///     "0m",
    ///     "5m",
    ///     "8p",
    ///     "2s",
    ///     "4z",
    ///     "7z",
    /// ];
    ///
    /// assert_eq!(actual, expected);
    /// ```
    pub const fn wall_code(&self) -> &str {
        match self {
            Tile::Manzu1 => "1m",
            Tile::Manzu2 => "2m",
            Tile::Manzu3 => "3m",
            Tile::Manzu4 => "4m",
            Tile::Manzu5 => "5m",
            Tile::Manzu6 => "6m",
            Tile::Manzu7 => "7m",
            Tile::Manzu8 => "8m",
            Tile::Manzu9 => "9m",
            Tile::Pinzu1 => "1p",
            Tile::Pinzu2 => "2p",
            Tile::Pinzu3 => "3p",
            Tile::Pinzu4 => "4p",
            Tile::Pinzu5 => "5p",
            Tile::Pinzu6 => "6p",
            Tile::Pinzu7 => "7p",
            Tile::Pinzu8 => "8p",
            Tile::Pinzu9 => "9p",
            Tile::Souzu1 => "1s",
            Tile::Souzu2 => "2s",
            Tile::Souzu3 => "3s",
            Tile::Souzu4 => "4s",
            Tile::Souzu5 => "5s",
            Tile::Souzu6 => "6s",
            Tile::Souzu7 => "7s",
            Tile::Souzu8 => "8s",
            Tile::Souzu9 => "9s",
            Tile::WindEast => "1z",
            Tile::WindSouth => "2z",
            Tile::WindWest => "3z",
            Tile::WindNorth => "4z",
            Tile::DragonWhite => "5z",
            Tile::DragonGreen => "6z",
            Tile::DragonRed => "7z",
            Tile::ManzuRed5 => "0m",
            Tile::PinzuRed5 => "0p",
            Tile::SouzuRed5 => "0s",
        }
    }
}

impl fmt::Display for Tile {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Manzu1 => "One of characters",
                Self::Manzu2 => "Two of characters",
                Self::Manzu3 => "Three of characters",
                Self::Manzu4 => "Four of characters",
                Self::Manzu5 => "Five of characters",
                Self::Manzu6 => "Six of characters",
                Self::Manzu7 => "Seven of characters",
                Self::Manzu8 => "Eight of characters",
                Self::Manzu9 => "Nine of characters",

                Self::Pinzu1 => "One of coins (circles)",
                Self::Pinzu2 => "Two of coins (circles)",
                Self::Pinzu3 => "Three of coins (circles)",
                Self::Pinzu4 => "Four of coins (circles)",
                Self::Pinzu5 => "Five of coins (circles)",
                Self::Pinzu6 => "Six of coins (circles)",
                Self::Pinzu7 => "Seven of coins (circles)",
                Self::Pinzu8 => "Eight of coins (circles)",
                Self::Pinzu9 => "Nine of coins (circles)",

                Self::Souzu1 => "One of bamboo",
                Self::Souzu2 => "Two of bamboo",
                Self::Souzu3 => "Three of bamboo",
                Self::Souzu4 => "Four of bamboo",
                Self::Souzu5 => "Five of bamboo",
                Self::Souzu6 => "Six of bamboo",
                Self::Souzu7 => "Seven of bamboo",
                Self::Souzu8 => "Eight of bamboo",
                Self::Souzu9 => "Nine of bamboo",

                Self::WindEast => "East Wind",
                Self::WindSouth => "South Wind",
                Self::WindWest => "West Wind",
                Self::WindNorth => "North Wind",

                Self::DragonWhite => "White Dragon",
                Self::DragonGreen => "Green Dragon",
                Self::DragonRed => "Red Dragon",

                Self::ManzuRed5 => "Red five of characters",
                Self::PinzuRed5 => "Red five of coins (circles)",
                Self::SouzuRed5 => "Red five of bamboo",
            }
        )
    }
}

#[cfg(test)]
mod tile_tests {
    use alloc::string::ToString;
    use alloc::vec::Vec;

    use super::*;

    #[test]
    fn is_green_works() {
        assert!(!Tile::Manzu1.is_green());
        assert!(!Tile::Manzu2.is_green());
        assert!(!Tile::Manzu3.is_green());
        assert!(!Tile::Manzu4.is_green());
        assert!(!Tile::Manzu5.is_green());
        assert!(!Tile::Manzu6.is_green());
        assert!(!Tile::Manzu7.is_green());
        assert!(!Tile::Manzu8.is_green());
        assert!(!Tile::Manzu9.is_green());

        assert!(!Tile::Pinzu1.is_green());
        assert!(!Tile::Pinzu2.is_green());
        assert!(!Tile::Pinzu3.is_green());
        assert!(!Tile::Pinzu4.is_green());
        assert!(!Tile::Pinzu5.is_green());
        assert!(!Tile::Pinzu6.is_green());
        assert!(!Tile::Pinzu7.is_green());
        assert!(!Tile::Pinzu8.is_green());
        assert!(!Tile::Pinzu9.is_green());

        assert!(!Tile::Souzu1.is_green());
        assert!(Tile::Souzu2.is_green());
        assert!(Tile::Souzu3.is_green());
        assert!(Tile::Souzu4.is_green());
        assert!(!Tile::Souzu5.is_green());
        assert!(Tile::Souzu6.is_green());
        assert!(!Tile::Souzu7.is_green());
        assert!(Tile::Souzu8.is_green());
        assert!(!Tile::Souzu9.is_green());

        assert!(!Tile::WindEast.is_green());
        assert!(!Tile::WindSouth.is_green());
        assert!(!Tile::WindWest.is_green());
        assert!(!Tile::WindNorth.is_green());

        assert!(!Tile::DragonWhite.is_green());
        assert!(Tile::DragonGreen.is_green());
        assert!(!Tile::DragonRed.is_green());

        assert!(!Tile::ManzuRed5.is_green());
        assert!(!Tile::PinzuRed5.is_green());
        assert!(!Tile::SouzuRed5.is_green());
    }

    #[test]
    fn is_honor_works() {
        assert!(!Tile::Manzu1.is_honor());
        assert!(!Tile::Manzu2.is_honor());
        assert!(!Tile::Manzu3.is_honor());
        assert!(!Tile::Manzu4.is_honor());
        assert!(!Tile::Manzu5.is_honor());
        assert!(!Tile::Manzu6.is_honor());
        assert!(!Tile::Manzu7.is_honor());
        assert!(!Tile::Manzu8.is_honor());
        assert!(!Tile::Manzu9.is_honor());

        assert!(!Tile::Pinzu1.is_honor());
        assert!(!Tile::Pinzu2.is_honor());
        assert!(!Tile::Pinzu3.is_honor());
        assert!(!Tile::Pinzu4.is_honor());
        assert!(!Tile::Pinzu5.is_honor());
        assert!(!Tile::Pinzu6.is_honor());
        assert!(!Tile::Pinzu7.is_honor());
        assert!(!Tile::Pinzu8.is_honor());
        assert!(!Tile::Pinzu9.is_honor());

        assert!(!Tile::Souzu1.is_honor());
        assert!(!Tile::Souzu2.is_honor());
        assert!(!Tile::Souzu3.is_honor());
        assert!(!Tile::Souzu4.is_honor());
        assert!(!Tile::Souzu5.is_honor());
        assert!(!Tile::Souzu6.is_honor());
        assert!(!Tile::Souzu7.is_honor());
        assert!(!Tile::Souzu8.is_honor());
        assert!(!Tile::Souzu9.is_honor());

        assert!(Tile::WindEast.is_honor());
        assert!(Tile::WindSouth.is_honor());
        assert!(Tile::WindWest.is_honor());
        assert!(Tile::WindNorth.is_honor());

        assert!(Tile::DragonWhite.is_honor());
        assert!(Tile::DragonGreen.is_honor());
        assert!(Tile::DragonRed.is_honor());

        assert!(!Tile::ManzuRed5.is_honor());
        assert!(!Tile::PinzuRed5.is_honor());
        assert!(!Tile::SouzuRed5.is_honor());
    }

    #[test]
    fn is_red_five_returns_true_for_red_fives() {
        assert!(Tile::ManzuRed5.is_red_five());
        assert!(Tile::PinzuRed5.is_red_five());
        assert!(Tile::SouzuRed5.is_red_five());
    }

    #[test]
    fn is_red_five_returns_false_for_regular_fives() {
        assert!(!Tile::Manzu5.is_red_five());
        assert!(!Tile::Pinzu5.is_red_five());
        assert!(!Tile::Souzu5.is_red_five());
    }

    #[test]
    fn is_regular_five_returns_true_for_regular_fives() {
        assert!(Tile::Manzu5.is_regular_five());
        assert!(Tile::Pinzu5.is_regular_five());
        assert!(Tile::Souzu5.is_regular_five());
    }

    #[test]
    fn is_regular_five_returns_false_for_red_fives() {
        assert!(!Tile::ManzuRed5.is_regular_five());
        assert!(!Tile::PinzuRed5.is_regular_five());
        assert!(!Tile::SouzuRed5.is_regular_five());
    }

    #[test]
    fn is_terminal_works() {
        assert!(Tile::Manzu1.is_terminal());
        assert!(!Tile::Manzu2.is_terminal());
        assert!(!Tile::Manzu3.is_terminal());
        assert!(!Tile::Manzu4.is_terminal());
        assert!(!Tile::Manzu5.is_terminal());
        assert!(!Tile::Manzu6.is_terminal());
        assert!(!Tile::Manzu7.is_terminal());
        assert!(!Tile::Manzu8.is_terminal());
        assert!(Tile::Manzu9.is_terminal());

        assert!(Tile::Pinzu1.is_terminal());
        assert!(!Tile::Pinzu2.is_terminal());
        assert!(!Tile::Pinzu3.is_terminal());
        assert!(!Tile::Pinzu4.is_terminal());
        assert!(!Tile::Pinzu5.is_terminal());
        assert!(!Tile::Pinzu6.is_terminal());
        assert!(!Tile::Pinzu7.is_terminal());
        assert!(!Tile::Pinzu8.is_terminal());
        assert!(Tile::Pinzu9.is_terminal());

        assert!(Tile::Souzu1.is_terminal());
        assert!(!Tile::Souzu2.is_terminal());
        assert!(!Tile::Souzu3.is_terminal());
        assert!(!Tile::Souzu4.is_terminal());
        assert!(!Tile::Souzu5.is_terminal());
        assert!(!Tile::Souzu6.is_terminal());
        assert!(!Tile::Souzu7.is_terminal());
        assert!(!Tile::Souzu8.is_terminal());
        assert!(Tile::Souzu9.is_terminal());

        assert!(!Tile::WindEast.is_terminal());
        assert!(!Tile::WindSouth.is_terminal());
        assert!(!Tile::WindWest.is_terminal());
        assert!(!Tile::WindNorth.is_terminal());

        assert!(!Tile::DragonWhite.is_terminal());
        assert!(!Tile::DragonGreen.is_terminal());
        assert!(!Tile::DragonRed.is_terminal());

        assert!(!Tile::ManzuRed5.is_terminal());
        assert!(!Tile::PinzuRed5.is_terminal());
        assert!(!Tile::SouzuRed5.is_terminal());
    }

    #[test]
    fn sorting_tiletype_collection_works() {
        let mut tiles = Vec::from([
            Tile::DragonGreen,
            Tile::WindNorth,
            Tile::WindSouth,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Souzu9,
            Tile::DragonWhite,
            Tile::Manzu1,
            Tile::Pinzu3,
            Tile::Souzu4,
            Tile::Pinzu4,
            Tile::Pinzu2,
            Tile::Souzu8,
        ]);
        let expected = Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Souzu4,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::WindSouth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonGreen,
        ]);

        tiles.sort();
        assert_eq!(tiles, expected);
    }

    #[test]
    fn next_tile_works() {
        assert_eq!(Tile::Manzu1.next_tile(), Tile::Manzu2);
        assert_eq!(Tile::Manzu2.next_tile(), Tile::Manzu3);
        assert_eq!(Tile::Manzu3.next_tile(), Tile::Manzu4);
        assert_eq!(Tile::Manzu4.next_tile(), Tile::Manzu5);
        assert_eq!(Tile::Manzu5.next_tile(), Tile::Manzu6);
        assert_eq!(Tile::Manzu6.next_tile(), Tile::Manzu7);
        assert_eq!(Tile::Manzu7.next_tile(), Tile::Manzu8);
        assert_eq!(Tile::Manzu8.next_tile(), Tile::Manzu9);
        assert_eq!(Tile::Manzu9.next_tile(), Tile::Manzu1);

        assert_eq!(Tile::Pinzu1.next_tile(), Tile::Pinzu2);
        assert_eq!(Tile::Pinzu2.next_tile(), Tile::Pinzu3);
        assert_eq!(Tile::Pinzu3.next_tile(), Tile::Pinzu4);
        assert_eq!(Tile::Pinzu4.next_tile(), Tile::Pinzu5);
        assert_eq!(Tile::Pinzu5.next_tile(), Tile::Pinzu6);
        assert_eq!(Tile::Pinzu6.next_tile(), Tile::Pinzu7);
        assert_eq!(Tile::Pinzu7.next_tile(), Tile::Pinzu8);
        assert_eq!(Tile::Pinzu8.next_tile(), Tile::Pinzu9);
        assert_eq!(Tile::Pinzu9.next_tile(), Tile::Pinzu1);

        assert_eq!(Tile::Souzu1.next_tile(), Tile::Souzu2);
        assert_eq!(Tile::Souzu2.next_tile(), Tile::Souzu3);
        assert_eq!(Tile::Souzu3.next_tile(), Tile::Souzu4);
        assert_eq!(Tile::Souzu4.next_tile(), Tile::Souzu5);
        assert_eq!(Tile::Souzu5.next_tile(), Tile::Souzu6);
        assert_eq!(Tile::Souzu6.next_tile(), Tile::Souzu7);
        assert_eq!(Tile::Souzu7.next_tile(), Tile::Souzu8);
        assert_eq!(Tile::Souzu8.next_tile(), Tile::Souzu9);
        assert_eq!(Tile::Souzu9.next_tile(), Tile::Souzu1);

        assert_eq!(Tile::WindEast.next_tile(), Tile::WindSouth);
        assert_eq!(Tile::WindSouth.next_tile(), Tile::WindWest);
        assert_eq!(Tile::WindWest.next_tile(), Tile::WindNorth);
        assert_eq!(Tile::WindNorth.next_tile(), Tile::WindEast);

        assert_eq!(Tile::DragonWhite.next_tile(), Tile::DragonGreen);
        assert_eq!(Tile::DragonGreen.next_tile(), Tile::DragonRed);
        assert_eq!(Tile::DragonRed.next_tile(), Tile::DragonWhite);
    }

    #[test]
    fn previous_tile_works() {
        assert_eq!(Tile::Manzu1.previous_tile(), Tile::Manzu9);
        assert_eq!(Tile::Manzu2.previous_tile(), Tile::Manzu1);
        assert_eq!(Tile::Manzu3.previous_tile(), Tile::Manzu2);
        assert_eq!(Tile::Manzu4.previous_tile(), Tile::Manzu3);
        assert_eq!(Tile::Manzu5.previous_tile(), Tile::Manzu4);
        assert_eq!(Tile::Manzu6.previous_tile(), Tile::Manzu5);
        assert_eq!(Tile::Manzu7.previous_tile(), Tile::Manzu6);
        assert_eq!(Tile::Manzu8.previous_tile(), Tile::Manzu7);
        assert_eq!(Tile::Manzu9.previous_tile(), Tile::Manzu8);

        assert_eq!(Tile::Pinzu1.previous_tile(), Tile::Pinzu9);
        assert_eq!(Tile::Pinzu2.previous_tile(), Tile::Pinzu1);
        assert_eq!(Tile::Pinzu3.previous_tile(), Tile::Pinzu2);
        assert_eq!(Tile::Pinzu4.previous_tile(), Tile::Pinzu3);
        assert_eq!(Tile::Pinzu5.previous_tile(), Tile::Pinzu4);
        assert_eq!(Tile::Pinzu6.previous_tile(), Tile::Pinzu5);
        assert_eq!(Tile::Pinzu7.previous_tile(), Tile::Pinzu6);
        assert_eq!(Tile::Pinzu8.previous_tile(), Tile::Pinzu7);
        assert_eq!(Tile::Pinzu9.previous_tile(), Tile::Pinzu8);

        assert_eq!(Tile::Souzu1.previous_tile(), Tile::Souzu9);
        assert_eq!(Tile::Souzu2.previous_tile(), Tile::Souzu1);
        assert_eq!(Tile::Souzu3.previous_tile(), Tile::Souzu2);
        assert_eq!(Tile::Souzu4.previous_tile(), Tile::Souzu3);
        assert_eq!(Tile::Souzu5.previous_tile(), Tile::Souzu4);
        assert_eq!(Tile::Souzu6.previous_tile(), Tile::Souzu5);
        assert_eq!(Tile::Souzu7.previous_tile(), Tile::Souzu6);
        assert_eq!(Tile::Souzu8.previous_tile(), Tile::Souzu7);
        assert_eq!(Tile::Souzu9.previous_tile(), Tile::Souzu8);

        assert_eq!(Tile::WindEast.previous_tile(), Tile::WindNorth);
        assert_eq!(Tile::WindSouth.previous_tile(), Tile::WindEast);
        assert_eq!(Tile::WindWest.previous_tile(), Tile::WindSouth);
        assert_eq!(Tile::WindNorth.previous_tile(), Tile::WindWest);

        assert_eq!(Tile::DragonWhite.previous_tile(), Tile::DragonRed);
        assert_eq!(Tile::DragonGreen.previous_tile(), Tile::DragonWhite);
        assert_eq!(Tile::DragonRed.previous_tile(), Tile::DragonGreen);
    }

    #[test]
    fn romaji_works() {
        assert_eq!(Tile::Manzu1.romaji(), "ii wan");
        assert_eq!(Tile::Manzu2.romaji(), "ryan wan");
        assert_eq!(Tile::Manzu3.romaji(), "san wan");
        assert_eq!(Tile::Manzu4.romaji(), "suu wan");
        assert_eq!(Tile::Manzu5.romaji(), "uu wan");
        assert_eq!(Tile::Manzu6.romaji(), "rou wan");
        assert_eq!(Tile::Manzu7.romaji(), "chii wan");
        assert_eq!(Tile::Manzu8.romaji(), "paa wan");
        assert_eq!(Tile::Manzu9.romaji(), "kyuu wan");

        assert_eq!(Tile::Pinzu1.romaji(), "ii pin");
        assert_eq!(Tile::Pinzu2.romaji(), "ryan pin");
        assert_eq!(Tile::Pinzu3.romaji(), "san pin");
        assert_eq!(Tile::Pinzu4.romaji(), "suu pin");
        assert_eq!(Tile::Pinzu5.romaji(), "uu pin");
        assert_eq!(Tile::Pinzu6.romaji(), "rou pin");
        assert_eq!(Tile::Pinzu7.romaji(), "chii pin");
        assert_eq!(Tile::Pinzu8.romaji(), "paa pin");
        assert_eq!(Tile::Pinzu9.romaji(), "kyuu pin");

        assert_eq!(Tile::Souzu1.romaji(), "ii sou");
        assert_eq!(Tile::Souzu2.romaji(), "ryan sou");
        assert_eq!(Tile::Souzu3.romaji(), "san sou");
        assert_eq!(Tile::Souzu4.romaji(), "suu sou");
        assert_eq!(Tile::Souzu5.romaji(), "uu sou");
        assert_eq!(Tile::Souzu6.romaji(), "rou sou");
        assert_eq!(Tile::Souzu7.romaji(), "chii sou");
        assert_eq!(Tile::Souzu8.romaji(), "paa sou");
        assert_eq!(Tile::Souzu9.romaji(), "kyuu sou");

        assert_eq!(Tile::WindEast.romaji(), "ton");
        assert_eq!(Tile::WindSouth.romaji(), "nan");
        assert_eq!(Tile::WindWest.romaji(), "shaa");
        assert_eq!(Tile::WindNorth.romaji(), "pei");

        assert_eq!(Tile::DragonWhite.romaji(), "haku");
        assert_eq!(Tile::DragonGreen.romaji(), "hatsu");
        assert_eq!(Tile::DragonRed.romaji(), "chun");
    }

    #[test]
    fn symbol_works() {
        assert_eq!(Tile::Manzu1.symbol(), "一");
        assert_eq!(Tile::Manzu2.symbol(), "ニ");
        assert_eq!(Tile::Manzu3.symbol(), "三");
        assert_eq!(Tile::Manzu4.symbol(), "四");
        assert_eq!(Tile::Manzu5.symbol(), "伍");
        assert_eq!(Tile::Manzu6.symbol(), "六");
        assert_eq!(Tile::Manzu7.symbol(), "七");
        assert_eq!(Tile::Manzu8.symbol(), "八");
        assert_eq!(Tile::Manzu9.symbol(), "九");

        assert_eq!(Tile::Pinzu1.symbol(), "①");
        assert_eq!(Tile::Pinzu2.symbol(), "②");
        assert_eq!(Tile::Pinzu3.symbol(), "③");
        assert_eq!(Tile::Pinzu4.symbol(), "④");
        assert_eq!(Tile::Pinzu5.symbol(), "⑤");
        assert_eq!(Tile::Pinzu6.symbol(), "⑥");
        assert_eq!(Tile::Pinzu7.symbol(), "⑦");
        assert_eq!(Tile::Pinzu8.symbol(), "⑧");
        assert_eq!(Tile::Pinzu9.symbol(), "⑨");

        assert_eq!(Tile::Souzu1.symbol(), "1");
        assert_eq!(Tile::Souzu2.symbol(), "2");
        assert_eq!(Tile::Souzu3.symbol(), "3");
        assert_eq!(Tile::Souzu4.symbol(), "4");
        assert_eq!(Tile::Souzu5.symbol(), "5");
        assert_eq!(Tile::Souzu6.symbol(), "6");
        assert_eq!(Tile::Souzu7.symbol(), "7");
        assert_eq!(Tile::Souzu8.symbol(), "8");
        assert_eq!(Tile::Souzu9.symbol(), "9");

        assert_eq!(Tile::WindEast.symbol(), "東");
        assert_eq!(Tile::WindSouth.symbol(), "南");
        assert_eq!(Tile::WindWest.symbol(), "西");
        assert_eq!(Tile::WindNorth.symbol(), "北");

        assert_eq!(Tile::DragonWhite.symbol(), "白");
        assert_eq!(Tile::DragonGreen.symbol(), "發");
        assert_eq!(Tile::DragonRed.symbol(), "中");
    }

    #[test]
    fn suit_returns_expected() {
        assert_eq!(Tile::Manzu1.suit(), TileSuit::Manzu);
        assert_eq!(Tile::Manzu2.suit(), TileSuit::Manzu);
        assert_eq!(Tile::Manzu3.suit(), TileSuit::Manzu);
        assert_eq!(Tile::Manzu4.suit(), TileSuit::Manzu);
        assert_eq!(Tile::Manzu5.suit(), TileSuit::Manzu);
        assert_eq!(Tile::Manzu6.suit(), TileSuit::Manzu);
        assert_eq!(Tile::Manzu7.suit(), TileSuit::Manzu);
        assert_eq!(Tile::Manzu8.suit(), TileSuit::Manzu);
        assert_eq!(Tile::Manzu9.suit(), TileSuit::Manzu);

        assert_eq!(Tile::Pinzu1.suit(), TileSuit::Pinzu);
        assert_eq!(Tile::Pinzu2.suit(), TileSuit::Pinzu);
        assert_eq!(Tile::Pinzu3.suit(), TileSuit::Pinzu);
        assert_eq!(Tile::Pinzu4.suit(), TileSuit::Pinzu);
        assert_eq!(Tile::Pinzu5.suit(), TileSuit::Pinzu);
        assert_eq!(Tile::Pinzu6.suit(), TileSuit::Pinzu);
        assert_eq!(Tile::Pinzu7.suit(), TileSuit::Pinzu);
        assert_eq!(Tile::Pinzu8.suit(), TileSuit::Pinzu);
        assert_eq!(Tile::Pinzu9.suit(), TileSuit::Pinzu);

        assert_eq!(Tile::Souzu1.suit(), TileSuit::Souzu);
        assert_eq!(Tile::Souzu2.suit(), TileSuit::Souzu);
        assert_eq!(Tile::Souzu3.suit(), TileSuit::Souzu);
        assert_eq!(Tile::Souzu4.suit(), TileSuit::Souzu);
        assert_eq!(Tile::Souzu5.suit(), TileSuit::Souzu);
        assert_eq!(Tile::Souzu6.suit(), TileSuit::Souzu);
        assert_eq!(Tile::Souzu7.suit(), TileSuit::Souzu);
        assert_eq!(Tile::Souzu8.suit(), TileSuit::Souzu);
        assert_eq!(Tile::Souzu9.suit(), TileSuit::Souzu);

        assert_eq!(Tile::WindEast.suit(), TileSuit::Wind);
        assert_eq!(Tile::WindSouth.suit(), TileSuit::Wind);
        assert_eq!(Tile::WindWest.suit(), TileSuit::Wind);
        assert_eq!(Tile::WindNorth.suit(), TileSuit::Wind);

        assert_eq!(Tile::DragonWhite.suit(), TileSuit::Dragon);
        assert_eq!(Tile::DragonGreen.suit(), TileSuit::Dragon);
        assert_eq!(Tile::DragonRed.suit(), TileSuit::Dragon);
    }

    #[test]
    fn to_string_returns_expected() {
        assert_eq!(Tile::Manzu1.to_string(), "One of characters".to_string());
        assert_eq!(Tile::Manzu2.to_string(), "Two of characters".to_string());
        assert_eq!(Tile::Manzu3.to_string(), "Three of characters".to_string());
        assert_eq!(Tile::Manzu4.to_string(), "Four of characters".to_string());
        assert_eq!(Tile::Manzu5.to_string(), "Five of characters".to_string());
        assert_eq!(Tile::Manzu6.to_string(), "Six of characters".to_string());
        assert_eq!(Tile::Manzu7.to_string(), "Seven of characters".to_string());
        assert_eq!(Tile::Manzu8.to_string(), "Eight of characters".to_string());
        assert_eq!(Tile::Manzu9.to_string(), "Nine of characters".to_string());

        assert_eq!(
            Tile::Pinzu1.to_string(),
            "One of coins (circles)".to_string()
        );
        assert_eq!(
            Tile::Pinzu2.to_string(),
            "Two of coins (circles)".to_string()
        );
        assert_eq!(
            Tile::Pinzu3.to_string(),
            "Three of coins (circles)".to_string()
        );
        assert_eq!(
            Tile::Pinzu4.to_string(),
            "Four of coins (circles)".to_string()
        );
        assert_eq!(
            Tile::Pinzu5.to_string(),
            "Five of coins (circles)".to_string()
        );
        assert_eq!(
            Tile::Pinzu6.to_string(),
            "Six of coins (circles)".to_string()
        );
        assert_eq!(
            Tile::Pinzu7.to_string(),
            "Seven of coins (circles)".to_string()
        );
        assert_eq!(
            Tile::Pinzu8.to_string(),
            "Eight of coins (circles)".to_string()
        );
        assert_eq!(
            Tile::Pinzu9.to_string(),
            "Nine of coins (circles)".to_string()
        );

        assert_eq!(Tile::Souzu1.to_string(), "One of bamboo".to_string());
        assert_eq!(Tile::Souzu2.to_string(), "Two of bamboo".to_string());
        assert_eq!(Tile::Souzu3.to_string(), "Three of bamboo".to_string());
        assert_eq!(Tile::Souzu4.to_string(), "Four of bamboo".to_string());
        assert_eq!(Tile::Souzu5.to_string(), "Five of bamboo".to_string());
        assert_eq!(Tile::Souzu6.to_string(), "Six of bamboo".to_string());
        assert_eq!(Tile::Souzu7.to_string(), "Seven of bamboo".to_string());
        assert_eq!(Tile::Souzu8.to_string(), "Eight of bamboo".to_string());
        assert_eq!(Tile::Souzu9.to_string(), "Nine of bamboo".to_string());

        assert_eq!(Tile::WindEast.to_string(), "East Wind".to_string());
        assert_eq!(Tile::WindSouth.to_string(), "South Wind".to_string());
        assert_eq!(Tile::WindWest.to_string(), "West Wind".to_string());
        assert_eq!(Tile::WindNorth.to_string(), "North Wind".to_string());

        assert_eq!(Tile::DragonWhite.to_string(), "White Dragon".to_string());
        assert_eq!(Tile::DragonGreen.to_string(), "Green Dragon".to_string());
        assert_eq!(Tile::DragonRed.to_string(), "Red Dragon".to_string());
    }
}

#[cfg(test)]
mod tile_eq_tests {
    use super::Tile;

    #[test]
    fn red_5_is_equal_to_regular_5() {
        assert_eq!(Tile::ManzuRed5, Tile::Manzu5);
        assert_eq!(Tile::PinzuRed5, Tile::Pinzu5);
        assert_eq!(Tile::SouzuRed5, Tile::Souzu5);
    }

    #[test]
    fn red_5_is_equal_to_same_suit_red_5() {
        assert_eq!(Tile::ManzuRed5, Tile::ManzuRed5);
        assert_eq!(Tile::PinzuRed5, Tile::PinzuRed5);
        assert_eq!(Tile::SouzuRed5, Tile::SouzuRed5);
    }

    #[test]
    fn red_5_is_not_equal_to_different_suit_red_5() {
        assert_ne!(Tile::ManzuRed5, Tile::PinzuRed5);
        assert_ne!(Tile::ManzuRed5, Tile::SouzuRed5);
        assert_ne!(Tile::PinzuRed5, Tile::ManzuRed5);
        assert_ne!(Tile::PinzuRed5, Tile::SouzuRed5);
        assert_ne!(Tile::SouzuRed5, Tile::ManzuRed5);
        assert_ne!(Tile::SouzuRed5, Tile::PinzuRed5);
    }

    #[test]
    fn red_5_is_not_equal_to_non_5_tile() {
        assert_ne!(Tile::ManzuRed5, Tile::WindEast);
        assert_ne!(Tile::PinzuRed5, Tile::WindEast);
        assert_ne!(Tile::SouzuRed5, Tile::WindEast);
    }
}

#[cfg(test)]
mod tile_ord_tests {
    use alloc::vec::Vec;

    use super::Tile;

    #[test]
    fn red_5_is_ordered_after_4() {
        let mut tiles = Vec::from([
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Pinzu4,
            Tile::PinzuRed5,
            Tile::Souzu4,
            Tile::SouzuRed5,
        ]);
        tiles.sort();

        let expected = Vec::from([
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Pinzu4,
            Tile::PinzuRed5,
            Tile::Souzu4,
            Tile::SouzuRed5,
        ]);

        assert_eq!(tiles, expected);
    }

    #[test]
    fn red_5_is_ordered_before_6() {
        let mut tiles = Vec::from([
            Tile::Manzu6,
            Tile::ManzuRed5,
            Tile::Pinzu6,
            Tile::PinzuRed5,
            Tile::Souzu6,
            Tile::SouzuRed5,
        ]);
        tiles.sort();

        let expected = Vec::from([
            Tile::ManzuRed5,
            Tile::Manzu6,
            Tile::PinzuRed5,
            Tile::Pinzu6,
            Tile::SouzuRed5,
            Tile::Souzu6,
        ]);

        assert_eq!(tiles, expected);
    }

    #[test]
    fn red_5_is_ordered_before_regular_5() {
        let mut tiles = Vec::from([
            Tile::Manzu5,
            Tile::ManzuRed5,
            Tile::Pinzu5,
            Tile::PinzuRed5,
            Tile::Souzu5,
            Tile::SouzuRed5,
        ]);
        tiles.sort();

        let expected = Vec::from([
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::PinzuRed5,
            Tile::Pinzu5,
            Tile::SouzuRed5,
            Tile::Souzu5,
        ]);

        assert_eq!(tiles, expected);
    }

    #[test]
    fn red_5_is_ordered_numerically_within_same_suit() {
        let mut tiles = Vec::from([
            Tile::Manzu9,
            Tile::Manzu8,
            Tile::Manzu7,
            Tile::Manzu6,
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu4,
            Tile::Manzu3,
            Tile::Manzu2,
            Tile::Manzu1,
        ]);
        tiles.sort();

        let expected = Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
        ]);

        assert_eq!(tiles, expected);
    }

    #[test]
    fn red_5_is_ordered_before_wind_tile() {
        let mut tiles = Vec::from([Tile::WindEast, Tile::ManzuRed5, Tile::Manzu5]);
        tiles.sort();

        let expected = Vec::from([Tile::ManzuRed5, Tile::Manzu5, Tile::WindEast]);

        assert_eq!(tiles, expected);
    }

    #[test]
    fn red_5_is_ordered_before_dragon_tile() {
        let mut tiles = Vec::from([Tile::DragonRed, Tile::ManzuRed5, Tile::Manzu5]);
        tiles.sort();

        let expected = Vec::from([Tile::ManzuRed5, Tile::Manzu5, Tile::DragonRed]);

        assert_eq!(tiles, expected);
    }
}
