/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module impls a `Wall` struct for players to draw tiles from.

use alloc::boxed::Box;
use alloc::vec::Vec;

use crate::error::{
    IncrementDoraIndicatorError, IncrementKanError, IncrementRinshanpaiDrawnError,
    IncrementWallIndexError,
};
use crate::game_rules::NumberOfAkadora;
use crate::tile::{TOTAL_NUM_TILES_4P, Tile};

/// Initial number of tiles in the dead wall.
pub const NUM_DEAD_WALL_TILES: usize = 14;
/// Maximum number of kan calls allowed.
///
/// Typically this is 4, however, when a single player has called kan 4 times, the game can continue.
pub const MAX_NUM_KAN_CALLS: u8 = 5;
/// Maximum number of dora indicator tiles that can be revealed in a round.
pub const MAX_NUM_REVEALED_DORA: u8 = 5;
/// Maximum number of replacement tiles that can be drawn after calling kan.
pub const MAX_NUM_RINSHANPAI: u8 = 4;

/// Set of tiles players draw tiles from.
#[derive(Debug, Clone)]
pub struct Wall {
    /// Currently drawn tile index.
    current_tile: usize,
    /// Dora and uradora indicator tiles.
    dora_indicator_tiles: Box<[(Tile, Tile)]>,
    /// Number of kans called.
    ///
    /// The fourth kan typically ends the round after the discard unless a player has made all 4 kan calls to reach tenpai for [suukantsu](crate::yaku::Yaku::Suukantsu).
    num_kans_called: u8,
    /// Number of revealed dora indicators.
    ///
    /// This can be desynced from the number of rinshanpai drawn because some rules reveal them
    /// immediately with ankan but only reveal them after the discard with daiminkan or shouminkan.
    ///
    /// A fourth kandora _can_ be revealed if an ankan rinshanpai or any kan discard results in a
    /// win, unless [chankan](crate::yaku::Yaku::Chankan) is declared.
    num_revealed_dora: u8,
    /// Number of replacement tiles drawn.
    num_rinshanpai_drawn: u8,
    /// Tiles in the wall.
    ///
    /// - 3-player: 29 unique tiles (2-8 manzu tiles omitted), with 4 copies each -> 108 tiles total.
    /// - 4-player: 36 unique tiles, with 4 copies of each -> 136 tiles total.
    tiles: Box<[Tile]>,
}

impl Wall {
    /// Create a new instance of `Wall`.
    ///
    /// The traditional number of tiles in the wall varies depending on the number of players as follows:
    ///
    ///   - 3-player: 27 unique tiles (2-8 manzu tiles omitted), with 4 copies each -> 108 tiles total.
    ///   - 4-player: 34 unique tiles, with 4 copies of each -> 136 tiles total.
    ///
    /// # Panics
    ///
    /// This could panic if there are too few tiles or there is a value set too high for [`MAX_NUM_RINSHANPAI`].
    /// This should never happen though.
    pub fn new(tiles: Box<[Tile]>) -> Self {
        let dora_indicator_tile = tiles
            .get((tiles.len() - 1) - core::convert::Into::<usize>::into(MAX_NUM_RINSHANPAI))
            .unwrap();
        let uradora_indicator_tile = tiles
            .get((tiles.len() - 1) - core::convert::Into::<usize>::into(MAX_NUM_RINSHANPAI) - 1)
            .unwrap();

        Self {
            current_tile: 0,
            dora_indicator_tiles: Box::new([(*dora_indicator_tile, *uradora_indicator_tile)]),
            num_kans_called: 0,
            num_revealed_dora: 1,
            num_rinshanpai_drawn: 0,
            tiles,
        }
    }
    /// Calculate the dora and uradora indicator tiles.
    ///
    /// The function returns a list of tile pairs of (dora, uradora) respectively.
    fn calculate_dora_indicator_tiles(&self) -> Box<[(Tile, Tile)]> {
        let mut dora_indicator_tiles = Vec::new();

        // Dora indicators are revealed anti-clockwise.
        let end_idx =
            (self.tiles.len() - 1) - core::convert::Into::<usize>::into(MAX_NUM_RINSHANPAI);
        let start_idx =
            end_idx - ((core::convert::Into::<usize>::into(self.num_revealed_dora) - 1) * 2);
        for idx in (start_idx..=end_idx).rev().step_by(2) {
            dora_indicator_tiles.push((
                *self.tiles.get(idx).unwrap(),
                *self.tiles.get(idx - 1).unwrap(),
            ));
        }

        dora_indicator_tiles.into()
    }

    /// Get the current live tile in the wall.
    ///
    /// # Panics
    ///
    /// Will panic if the current tile index is out of bounds for the `tiles` collection.
    pub fn current_tile(&self) -> &Tile {
        self.tiles.get(self.current_tile).unwrap()
    }

    /// Get the current live tile index in the wall.
    pub const fn current_tile_idx(&self) -> usize {
        self.current_tile
    }

    /// Get the tiles in the dead wall.
    ///
    /// # Panics
    ///
    /// Will panic if the last live tile index is out of bounds for the `tiles` collection.
    pub fn dead_wall(&self) -> &[Tile] {
        self.tiles.get((self.last_live_tile_idx() + 1)..).unwrap()
    }

    /// Get the current dora and uradora indicator tiles.
    ///
    /// The function returns a list of tile pairs of (dora, uradora) respectively.
    pub const fn dora_indicator_tiles(&self) -> &[(Tile, Tile)] {
        &self.dora_indicator_tiles
    }

    /// Increment the number of replacement tiles drawn.
    ///
    /// Typically the maximum number of calls allowed is 4, however, when a single player has called kan 4 times, the game can continue.
    ///
    /// # Errors
    ///
    /// Will return `Err` if this is called when the current number of kans called equals or exceeds [MAX_NUM_KAN_CALLS].
    pub fn increment_num_kans_called(&mut self) -> Result<(), IncrementKanError> {
        if self.num_kans_called >= MAX_NUM_KAN_CALLS {
            return Err(IncrementKanError::new(MAX_NUM_KAN_CALLS));
        }

        self.num_kans_called += 1;

        Ok(())
    }

    /// Increment the number of revealed dora indicators.
    ///
    /// This will also update the dora indicator tiles for [Self::dora_indicator_tiles()].
    ///
    /// # Errors
    ///
    /// Will return `Err` if this is called when the current number of revealed dora equals or exceeds [MAX_NUM_REVEALED_DORA].
    pub fn increment_num_revealed_dora(&mut self) -> Result<(), IncrementDoraIndicatorError> {
        if self.num_revealed_dora >= MAX_NUM_REVEALED_DORA {
            return Err(IncrementDoraIndicatorError::new(MAX_NUM_REVEALED_DORA));
        }

        self.num_revealed_dora += 1;
        self.dora_indicator_tiles = self.calculate_dora_indicator_tiles();

        Ok(())
    }

    /// Increment the number of replacement tiles drawn.
    ///
    /// # Errors
    ///
    /// Will return `Err` if this is called when the current number of rinshanpai (replacement tiles) equals or exceeds [MAX_NUM_RINSHANPAI].
    pub fn increment_num_rinshanpai_drawn(&mut self) -> Result<(), IncrementRinshanpaiDrawnError> {
        if self.num_rinshanpai_drawn >= MAX_NUM_RINSHANPAI {
            return Err(IncrementRinshanpaiDrawnError::new(MAX_NUM_RINSHANPAI));
        }

        self.num_rinshanpai_drawn += 1;

        Ok(())
    }

    /// Increment the current tile index to point to the next tile in the wall.
    ///
    /// # Errors
    ///
    /// Will return `Err` if there are no more live tiles in the wall (exhausted wall).
    pub fn increment_tile_idx(&mut self) -> Result<(), IncrementWallIndexError> {
        if self.is_wall_exhausted() {
            return Err(IncrementWallIndexError::new(self.last_live_tile_idx()));
        }

        self.current_tile += 1;

        Ok(())
    }

    /// Check if the live tiles in the wall are exhausted to end the round.
    pub fn is_wall_exhausted(&self) -> bool {
        self.current_tile == self.last_live_tile_idx()
    }

    /// Get the last live wall tile index.
    pub fn last_live_tile_idx(&self) -> usize {
        self.tiles.len()
            - (NUM_DEAD_WALL_TILES + core::convert::Into::<usize>::into(self.num_revealed_dora))
    }

    /// Get the number of kans called in the round.
    pub const fn num_kans_called(&self) -> u8 {
        self.num_kans_called
    }

    /// Get the replacement tile for calling kan.
    ///
    /// The last 4 tiles in the wall are used for the replacement tile, taken in reverse order (i.e. the last tile in the wall is the first replacement tile taken).
    ///
    /// # Panics
    ///
    /// Will panic if the rinshanpai index is out of bounds of the `tiles` collection.
    pub fn rinshanpai(&self) -> &Tile {
        // The rinshanpai are drawn in reverse, from the last tile first.
        let rinshanpai_idx =
            (self.tiles.len() - 1) - core::convert::Into::<usize>::into(self.num_rinshanpai_drawn);

        self.tiles.get(rinshanpai_idx).unwrap()
    }

    /// Get the tiles in the wall.
    pub const fn tiles(&self) -> &[Tile] {
        &self.tiles
    }
}

/// Create an unshuffled list of all tiles for a 3-player game.
///
/// The manzu tiles 2-8 are removed for this game mode.
///
/// It is expected that users will provide the RNG and shuffling method themselves.
///
/// # Examples
///
/// Use of the [`rand`](https://docs.rs/rand/latest/rand/) crate to shuffle. Another RNG backend may be used though.
///
/// ```rust,ignore
/// use jikaze_riichi_core::game_rules::NumberOfAkadora;
/// use jikaze_riichi_core::wall::{create_unshuffled_tiles_3p, Wall};
/// use rand::seq::{IteratorRandom, SliceRandom};
///
/// let mut rng = rand::rng();
///
/// let mut tiles = create_unshuffled_tiles_3p(&NumberOfAkadora::Three);
/// tiles.shuffle(&mut rng);
///
/// let mut wall = Wall::new(tiles);
/// // Do stuff with the wall...
/// ```
pub const fn create_unshuffled_tiles_3p(
    num_akadora: &NumberOfAkadora,
) -> [Tile; TOTAL_NUM_TILES_4P - (7 * 4)] {
    // The arrays are hard-coded to allow this function to be `const`.
    match num_akadora {
        NumberOfAkadora::None => [
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ],
        NumberOfAkadora::Three => [
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::PinzuRed5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::SouzuRed5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ],
        NumberOfAkadora::Four => [
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::PinzuRed5,
            Tile::PinzuRed5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::SouzuRed5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ],
    }
}

/// Create an unshuffled list of all tiles for a 4-player game.
///
/// It is expected that users will provide the RNG and shuffling method themselves.
///
/// # Examples
///
/// Use of the [`rand`](https://docs.rs/rand/latest/rand/) crate to shuffle. Another RNG backend may be used though.
///
/// ```rust,ignore
/// use jikaze_riichi_core::game_rules::NumberOfAkadora;
/// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
/// use rand::seq::{IteratorRandom, SliceRandom};
///
/// let mut rng = rand::rng();
///
/// let mut tiles = create_unshuffled_tiles_4p(&NumberOfAkadora::Three);
/// tiles.shuffle(&mut rng);
///
/// let mut wall = Wall::new(tiles);
/// // Do stuff with the wall...
/// ```
pub const fn create_unshuffled_tiles_4p(
    num_akadora: &NumberOfAkadora,
) -> [Tile; TOTAL_NUM_TILES_4P] {
    // The arrays are hard-coded to allow this function to be `const`.
    match num_akadora {
        NumberOfAkadora::None => [
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ],
        NumberOfAkadora::Three => [
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::PinzuRed5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::SouzuRed5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ],
        NumberOfAkadora::Four => [
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::PinzuRed5,
            Tile::PinzuRed5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::SouzuRed5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ],
    }
}

#[cfg(test)]
mod tests {
    use crate::game_rules::NumberOfAkadora;
    use crate::tile::Tile;

    use super::create_unshuffled_tiles_4p;

    #[test]
    fn create_unshuffled_tiles_works_with_no_akadora() {
        let expected = [
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ];

        assert_eq!(create_unshuffled_tiles_4p(&NumberOfAkadora::None), expected);
    }

    #[test]
    fn create_unshuffled_tiles_works_with_three_akadora() {
        let expected = [
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::PinzuRed5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::SouzuRed5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ];

        assert_eq!(
            create_unshuffled_tiles_4p(&NumberOfAkadora::Three),
            expected
        );
    }

    #[test]
    fn create_unshuffled_tiles_works_with_four_akadora() {
        let expected = [
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::PinzuRed5,
            Tile::PinzuRed5,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::SouzuRed5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ];

        assert_eq!(create_unshuffled_tiles_4p(&NumberOfAkadora::Four), expected);
    }
}

#[cfg(test)]
mod wall_tests {
    use core::error::Error;

    use super::*;

    fn create_wall() -> Wall {
        // Tiles shuffled with `rand` crate separately.
        // This ordering will remain the same for all tests to make the tests predictable and consistent.
        Wall::new(Box::new([
            Tile::Souzu2,
            Tile::Souzu4,
            Tile::Manzu5,
            Tile::WindSouth,
            Tile::Manzu8,
            Tile::Souzu9,
            Tile::DragonWhite,
            Tile::Manzu1,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::Souzu1,
            Tile::Souzu5,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Manzu4,
            Tile::WindSouth,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu5,
            Tile::Souzu9,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Manzu4,
            Tile::Souzu6,
            Tile::Manzu1,
            Tile::Souzu2,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu7,
            Tile::WindWest,
            Tile::DragonWhite,
            Tile::Pinzu2,
            Tile::DragonRed,
            Tile::WindEast,
            Tile::Pinzu2,
            Tile::DragonWhite,
            Tile::WindSouth,
            Tile::WindEast,
            Tile::Pinzu9,
            Tile::Manzu6,
            Tile::Souzu5,
            Tile::DragonRed,
            Tile::Manzu2,
            Tile::WindNorth,
            Tile::Souzu3,
            Tile::Pinzu6,
            Tile::DragonGreen,
            Tile::Manzu8,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Pinzu6,
            Tile::Souzu3,
            Tile::Pinzu2,
            Tile::Pinzu9,
            Tile::WindWest,
            Tile::DragonRed,
            Tile::Manzu3,
            Tile::WindEast,
            Tile::Pinzu8,
            Tile::Pinzu6,
            Tile::Pinzu8,
            Tile::Pinzu7,
            Tile::Manzu9,
            Tile::Souzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Pinzu8,
            Tile::Souzu4,
            Tile::Souzu8,
            Tile::Pinzu6,
            Tile::WindSouth,
            Tile::Manzu1,
            Tile::Souzu6,
            Tile::Manzu2,
            Tile::DragonGreen,
            Tile::Souzu6,
            Tile::Manzu3,
            Tile::Souzu3,
            Tile::Pinzu8,
            Tile::Manzu7,
            Tile::DragonWhite,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu4,
            Tile::Manzu6,
            Tile::Pinzu4,
            Tile::Manzu7,
            Tile::Pinzu3,
            Tile::Pinzu1,
            Tile::Pinzu5,
            Tile::Pinzu7,
            Tile::WindNorth,
            Tile::Souzu4,
            Tile::WindEast,
            Tile::WindWest,
            Tile::Souzu5,
            Tile::Manzu5,
            Tile::Pinzu5,
            Tile::Manzu7,
            Tile::WindNorth,
            Tile::Pinzu3,
            Tile::Pinzu9,
            Tile::Manzu6,
            Tile::Souzu8,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Pinzu1,
            Tile::Manzu9,
            Tile::Souzu1,
            Tile::WindWest,
            Tile::Souzu7,
            Tile::Manzu4,
            Tile::Pinzu9,
            Tile::Souzu8,
            Tile::Souzu5,
            Tile::Pinzu1,
            Tile::Manzu1,
            Tile::Souzu3,
            Tile::Manzu4,
            Tile::Pinzu5,
            Tile::Manzu9,
            Tile::DragonGreen,
            Tile::Pinzu1,
            Tile::Pinzu4,
            Tile::Souzu7,
            Tile::Pinzu4,
            Tile::Manzu9,
            Tile::Manzu3,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::WindNorth,
            Tile::Souzu1,
            Tile::Pinzu3,
            Tile::Manzu6,
            Tile::Manzu2,
            Tile::Souzu6,
        ]))
    }

    #[test]
    fn dead_wall_works() -> Result<(), Box<dyn Error>> {
        let mut wall = create_wall();

        let expected = [
            Tile::Pinzu1,
            Tile::Pinzu4,
            Tile::Souzu7,
            Tile::Pinzu4,
            Tile::Manzu9,
            Tile::Manzu3,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::WindNorth,
            Tile::Souzu1,
            Tile::Pinzu3,
            Tile::Manzu6,
            Tile::Manzu2,
            Tile::Souzu6,
        ];

        assert_eq!(wall.dead_wall(), expected);

        wall.increment_num_rinshanpai_drawn()?;
        wall.increment_num_revealed_dora()?;
        let expected = [
            Tile::DragonGreen,
            Tile::Pinzu1,
            Tile::Pinzu4,
            Tile::Souzu7,
            Tile::Pinzu4,
            Tile::Manzu9,
            Tile::Manzu3,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::WindNorth,
            Tile::Souzu1,
            Tile::Pinzu3,
            Tile::Manzu6,
            Tile::Manzu2,
            Tile::Souzu6,
        ];

        assert_eq!(wall.dead_wall(), expected);

        wall.increment_num_rinshanpai_drawn()?;
        wall.increment_num_revealed_dora()?;
        let expected = [
            Tile::Manzu9,
            Tile::DragonGreen,
            Tile::Pinzu1,
            Tile::Pinzu4,
            Tile::Souzu7,
            Tile::Pinzu4,
            Tile::Manzu9,
            Tile::Manzu3,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::WindNorth,
            Tile::Souzu1,
            Tile::Pinzu3,
            Tile::Manzu6,
            Tile::Manzu2,
            Tile::Souzu6,
        ];

        assert_eq!(wall.dead_wall(), expected);

        Ok(())
    }

    #[test]
    fn calculate_dora_indicator_tiles_returns_expected() -> Result<(), Box<dyn Error>> {
        let mut wall = create_wall();

        let expected = [(Tile::Souzu1, Tile::WindNorth)];
        // This checks that the `new()` initialization is working.
        assert_eq!(wall.dora_indicator_tiles(), expected);
        assert_eq!(*wall.calculate_dora_indicator_tiles(), expected);

        wall.increment_num_rinshanpai_drawn()?;
        wall.increment_num_revealed_dora()?;
        let expected = [
            (Tile::Souzu1, Tile::WindNorth),
            (Tile::Manzu5, Tile::Manzu5),
        ];
        // Calling `increment_num_revealed_dora()` should update the `dora_indicator_tiles` field.
        assert_eq!(wall.dora_indicator_tiles(), expected);
        assert_eq!(*wall.calculate_dora_indicator_tiles(), expected);

        wall.increment_num_rinshanpai_drawn()?;
        wall.increment_num_revealed_dora()?;
        let expected = [
            (Tile::Souzu1, Tile::WindNorth),
            (Tile::Manzu5, Tile::Manzu5),
            (Tile::Manzu3, Tile::Manzu9),
        ];
        assert_eq!(*wall.calculate_dora_indicator_tiles(), expected);

        wall.increment_num_rinshanpai_drawn()?;
        wall.increment_num_revealed_dora()?;
        let expected = [
            (Tile::Souzu1, Tile::WindNorth),
            (Tile::Manzu5, Tile::Manzu5),
            (Tile::Manzu3, Tile::Manzu9),
            (Tile::Pinzu4, Tile::Souzu7),
        ];
        assert_eq!(*wall.calculate_dora_indicator_tiles(), expected);

        Ok(())
    }

    #[test]
    fn increment_num_revealed_dora_is_ok_until_maximum_reached() -> Result<(), Box<dyn Error>> {
        let mut wall = create_wall();

        for _ in 0..(MAX_NUM_REVEALED_DORA - 1) {
            wall.increment_num_revealed_dora()?;
        }

        assert!(wall.increment_num_revealed_dora().is_err());

        Ok(())
    }

    #[test]
    fn increment_num_rinshanpai_drawn_is_ok_until_maximum_reached() -> Result<(), Box<dyn Error>> {
        let mut wall = create_wall();

        for _ in 0..MAX_NUM_RINSHANPAI {
            wall.increment_num_rinshanpai_drawn()?;
        }

        assert!(wall.increment_num_rinshanpai_drawn().is_err());

        Ok(())
    }

    #[test]
    fn increment_tile_is_ok_until_maximum_reached() -> Result<(), Box<dyn Error>> {
        let mut wall = create_wall();

        for _ in 0..(wall.last_live_tile_idx()) {
            wall.increment_tile_idx()?;
        }

        assert!(wall.increment_tile_idx().is_err());

        Ok(())
    }

    #[test]
    fn rinshanpai_returns_expected() -> Result<(), Box<dyn Error>> {
        let mut wall = create_wall();

        assert_eq!(*wall.rinshanpai(), Tile::Souzu6);

        wall.increment_num_rinshanpai_drawn()?;
        wall.increment_num_revealed_dora()?;
        assert_eq!(*wall.rinshanpai(), Tile::Manzu2);

        wall.increment_num_rinshanpai_drawn()?;
        wall.increment_num_revealed_dora()?;
        assert_eq!(*wall.rinshanpai(), Tile::Manzu6);

        wall.increment_num_rinshanpai_drawn()?;
        wall.increment_num_revealed_dora()?;
        assert_eq!(*wall.rinshanpai(), Tile::Pinzu3);

        wall.increment_num_rinshanpai_drawn()?;
        wall.increment_num_revealed_dora()?;
        assert_eq!(*wall.rinshanpai(), Tile::Souzu1);

        Ok(())
    }
}
