/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module contains the `PlayerRelativePosition` enum to provide directional context about another player relative to oneself.

use core::fmt;

use crate::wind::Wind;

/// Player position of a player relative to oneself.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum PlayerRelativePosition {
    /// Player to the right.
    ///
    /// Kanji: 下家
    Shimocha,
    /// Player across.
    ///
    /// Kanji: 対面
    Toimen,
    /// Player to the left.
    ///
    /// Kanji: 上家
    Kamicha,
}

impl PlayerRelativePosition {
    /// Determine the relative position from the first wind to the second wind.
    ///
    /// # Errors
    ///
    /// Returns an error if the winds are the same.
    pub const fn position_from_winds(
        wind1: &Wind,
        wind2: &Wind,
    ) -> Result<Self, PlayerRelativePositionError> {
        match (wind1, wind2) {
            (Wind::East, Wind::South)
            | (Wind::South, Wind::West)
            | (Wind::West, Wind::North)
            | (Wind::North, Wind::East) => Ok(Self::Shimocha),
            (Wind::East, Wind::West)
            | (Wind::South, Wind::North)
            | (Wind::West, Wind::East)
            | (Wind::North, Wind::South) => Ok(Self::Toimen),
            (Wind::East, Wind::North)
            | (Wind::South, Wind::East)
            | (Wind::West, Wind::South)
            | (Wind::North, Wind::West) => Ok(Self::Kamicha),
            (Wind::East, Wind::East)
            | (Wind::South, Wind::South)
            | (Wind::West, Wind::West)
            | (Wind::North, Wind::North) => Err(PlayerRelativePositionError::new(*wind1, *wind2)),
        }
    }
}

/// Error that may occur while determining a player's position relative to oneself.
#[derive(Debug, Clone)]
pub struct PlayerRelativePositionError(Wind, Wind);

impl core::error::Error for PlayerRelativePositionError {
    fn source(&self) -> Option<&(dyn core::error::Error + 'static)> {
        None
    }
}

impl fmt::Display for PlayerRelativePositionError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "A relative position could not be determined from the following winds: {}, {}",
            self.0, self.1
        )
    }
}

impl PlayerRelativePositionError {
    /// Create a new instance.
    pub const fn new(wind1: Wind, wind2: Wind) -> Self {
        Self(wind1, wind2)
    }
}
