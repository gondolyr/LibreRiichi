/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module provides types to give context around a player within a round.

use crate::meld::Meld;
use crate::wind::Wind;

/// When a player's hand is in furiten 「振聴」, the player cannot declare a win on another player's discard (i.e. they can't ron).
/// A player is still able to win with their own draw (tsumo), assuming the hand is valid.
#[derive(Debug, Clone, PartialEq)]
pub enum Furiten {
    /// If a player's own winning tile is within their discard pile, their hand is in furiten.
    Discard,
    /// When a player has declared riichi, and the player skips the first winning tile, the furiten becomes permanent.
    Permanent,
    /// This occurs when a player has declined calling ron, for whatever reason, but has not declared riichi.
    ///
    /// This is removed on their next draw.
    ///
    /// A player may be forced to decline calling ron if a winning tile would result in having no yaku such as the following:
    ///
    /// - A shanpon (double pair) wait, with one tile completing a yakuhai triplet, and the other tile having no yaku.
    /// - A ryanmen (double-sided) wait on 1-4 or 6-9 on a hand that would otherwise complete [tanyao](crate::yaku::Yaku::Tanyao).
    Temporary,
}

/// Variations of declaring riichi.
#[derive(Debug, Clone, PartialEq)]
pub enum Riichi {
    /// Declared a ready concealed hand.
    Riichi,
    /// Called riichi on the first turn.
    DoubleRiichi,
}

/// Context around a player within a round.
#[derive(Debug, Clone, PartialEq)]
pub struct PlayerRoundState {
    /// Flag determining whether or not the player has drawn their first tile for the round.
    drew_first_tile: bool,
    /// Player qualifies for the [riichi](crate::yaku::Yaku::Riichi) or [double riichi](crate::yaku::Yaku::DoubleRiichi) yaku if it is set.
    riichi: Option<Riichi>,
    /// Flag determining whether or not the player qualifies for the [ippatsu](crate::yaku::Yaku::Ippatsu) yaku.
    ippatsu: bool,
    /// Player's seat wind for the round.
    wind: Wind,
    /// The last meld call the player made.
    ///
    /// This is useful for checking the kuikae rule (swap calling) where a player cannot discard a tile that connects to the meld.
    ///
    /// See the [kuikae rule](crate::game_rules::GameRules#structfield.kuikae) for more information.
    last_meld: Option<Meld>,
    /// Flag determining whether or not a tile was called from the player's discard pile. This is specifically used for the [nagashi mangan](crate::yaku::Yaku::NagashiMangan) yaku.
    ///
    /// If this is `true`, [nagashi mangan](crate::yaku::Yaku::NagashiMangan) is invalid and cannot be earned.
    tile_called_from_own_discard: bool,
    /// Player's furiten status.
    furiten: Option<Furiten>,
}

impl PlayerRoundState {
    /// Create a new `PlayerRoundState`.
    pub const fn new(wind: Wind) -> Self {
        Self {
            drew_first_tile: false,
            riichi: None,
            ippatsu: false,
            wind,
            last_meld: None,
            tile_called_from_own_discard: false,
            furiten: None,
        }
    }

    /// Get the state of whether the player drew their first tile for the round.
    ///
    /// This is used for determining if a hand is valid for [renhou](crate::yaku::Yaku::Renhou).
    pub const fn drew_first_tile(&self) -> bool {
        self.drew_first_tile
    }

    /// Get a mutable reference to the `drew_first_tile` field.
    pub fn drew_first_tile_mut(&mut self) -> &mut bool {
        &mut self.drew_first_tile
    }

    /// Get the state of whether the player is in riichi.
    pub const fn riichi(&self) -> &Option<Riichi> {
        &self.riichi
    }

    /// Get a mutable reference to the `riichi` field.
    pub fn riichi_mut(&mut self) -> &mut Option<Riichi> {
        &mut self.riichi
    }

    /// Get an immutable reference to the state of whether the player is qualifies for the [`ippatsu`](crate::yaku::Yaku::Ippatsu) yaku.
    pub const fn ippatsu(&self) -> bool {
        self.ippatsu
    }

    /// Get a mutable reference to the state of whether the player is qualifies for the [`ippatsu`](crate::yaku::Yaku::Ippatsu) yaku.
    pub fn ippatsu_mut(&mut self) -> &mut bool {
        &mut self.ippatsu
    }

    /// Get an immutable reference to the player's seat wind.
    pub const fn wind(&self) -> &Wind {
        &self.wind
    }

    /// Get the state of whether or not the player is the dealer (East seat).
    pub const fn is_dealer(&self) -> bool {
        matches!(self.wind, Wind::East)
    }

    /// Get the last meld the player made.
    ///
    /// This should be set after making a call and set to `None` after discarding a tile.
    pub const fn last_meld(&self) -> &Option<Meld> {
        &self.last_meld
    }

    /// Get a mutable reference to the last meld the player made.
    ///
    /// This should be set after making a call and set to `None` after discarding a tile.
    pub fn last_meld_mut(&mut self) -> &mut Option<Meld> {
        &mut self.last_meld
    }

    /// Get the state of whether or not the player had a discarded tile called by another player.
    pub const fn tile_called_from_own_discard(&self) -> bool {
        self.tile_called_from_own_discard
    }

    /// Get a mutable reference to the `tile_called_from_own_discard` field.
    ///
    /// If `true`, the possibility of earning the [nagashi mangan](crate::yaku::Yaku::NagashiMangan) yaku is no longer possible.
    pub fn tile_called_from_own_discard_mut(&mut self) -> &mut bool {
        &mut self.tile_called_from_own_discard
    }

    /// Get the player's furiten state.
    pub const fn furiten(&self) -> &Option<Furiten> {
        &self.furiten
    }

    /// Get a mutable reference to the player's furiten state.
    ///
    /// This can be especially useful to remove the temporary furiten state.
    pub fn furiten_mut(&mut self) -> &mut Option<Furiten> {
        &mut self.furiten
    }
}
