/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Handle scoring for a hand.

use alloc::boxed::Box;

use crate::dora::Dora;
use crate::fu::Fu;
use crate::yaku::Yaku;

/// Struct to hold scoring information.
///
/// # Examples
///
/// ## Without Aotenjou
///
/// ```rust
/// use jikaze_riichi_core::dora::Dora;
/// use jikaze_riichi_core::fu::{Fu, FuMentsuTileClass};
/// use jikaze_riichi_core::score::HandScore;
/// use jikaze_riichi_core::yaku::Yaku;
///
/// // 2 han.
/// let yaku = [Yaku::Riichi, Yaku::Ippatsu];
/// let dora = Dora::default();
/// // 32 -> 40 fu.
/// let fu = [
///     // 20 fu.
///     Fu::Winning,
///     // 4 fu.
///     Fu::Ankou(FuMentsuTileClass::Tanyaohai),
///     // 8 fu.
///     Fu::Ankou(FuMentsuTileClass::RoutouhaiJihai)
/// ];
/// let aotenjou = false;
/// let score = HandScore::new(yaku.into(), &dora, fu.into(), aotenjou);
///
/// assert_eq!(score.base_value(), 640);
/// ```
///
/// ### Mangan with enough minipoints.
///
/// ```rust
/// use jikaze_riichi_core::dora::Dora;
/// use jikaze_riichi_core::fu::{Fu, FuMentsuTileClass};
/// use jikaze_riichi_core::score::HandScore;
/// use jikaze_riichi_core::yaku::Yaku;
///
/// // 3 han.
/// let yaku = [Yaku::Riichi, Yaku::Ippatsu];
/// let dora = Dora::new(1, 0, 0, 0);
/// // 62 -> 70 fu.
/// let fu = [
///     // 20 fu.
///     Fu::Winning,
///     // 10 fu.
///     Fu::ConcealedRon,
///     // 16 fu.
///     Fu::Ankan(FuMentsuTileClass::Tanyaohai),
///     // 16 fu.
///     Fu::Ankan(FuMentsuTileClass::Tanyaohai)
/// ];
/// let aotenjou = false;
/// let score = HandScore::new(yaku.into(), &dora, fu.into(), aotenjou);
///
/// assert_eq!(score.base_value(), 2000);
/// ```
///
/// ## With Aotenjou
///
/// ```rust
/// use jikaze_riichi_core::dora::Dora;
/// use jikaze_riichi_core::fu::{Fu, FuMentsuTileClass};
/// use jikaze_riichi_core::score::HandScore;
/// use jikaze_riichi_core::yaku::Yaku;
///
/// // 31 han.
/// let yaku = [
///     // 1 han.
///     Yaku::Riichi,
///     // 2 han.
///     Yaku::SanshokuDoukou,
///     // 2 han.
///     Yaku::Sankantsu,
///     // 2 han.
///     Yaku::Toitoi,
/// ];
/// let dora = Dora::new(12, 12, 0, 0);
/// // 50 fu. The fu defined below does not align with the yaku awarded for this example.
/// let fu = [
///     // 20 fu.
///     Fu::Winning,
///     // 10 fu.
///     Fu::ConcealedRon,
///     // 16 fu.
///     Fu::Ankan(FuMentsuTileClass::Tanyaohai),
///     // 2 fu.
///     Fu::Yakuhai,
///     // 2 fu.
///     Fu::Tanki,
/// ];
/// let aotenjou = true;
/// let score = HandScore::new(yaku.into(), &dora, fu.into(), aotenjou);
///
/// assert_eq!(score.base_value(), 429_496_729_600);
/// ```
#[derive(Debug, PartialEq, Eq, Clone)]
pub struct HandScore {
    /// # 「青天井」
    ///
    /// If true, all scoring caps and limits are removed. The basic points have the freedom to be
    /// valued as high as possible. The exponential nature of the scoring equation can produce
    /// astronomically large point values. Points are even much higher in conjunction with the
    /// multipliers applied for the dealer, non-dealer, and ron vs. tsumo wins. All yaku retain
    /// their original values. In additionally, yakuman patterns are defaulted at 13 han per yakuman
    /// count; and they are treated like normal yaku when determining the overall han and fu values.
    /// Furthermore, any han applied by dora counts as well to further increase the point values.
    ///
    /// # Base value forumla
    ///
    /// The formula for calculating the base value without caps or limits applied (aotenjou)
    /// or han < 5 (no aotenjou) is:
    ///
    /// ```text
    /// fu * 2 ^ (2 + han)
    /// ```
    ///
    /// # Examples
    ///
    /// ## Kokushi Musou
    ///
    /// ```text
    /// 一九 ①⑨ 19 東南西北 白發發中 +①
    /// ```
    ///
    /// This hand is then scored as 13 han 30 or 40 fu. By dealer, it is worth 7,864,400 (ron) or
    /// 1,966,080 (tsumo).
    ///
    /// ## Shousuushii + Tsuuiisou
    ///
    /// ```text
    /// 東東東 南南 西西 中中中 ⮦北北北
    /// ```
    ///
    /// Waiting for: `南` or `西`
    ///
    /// Every aspect of this hand is considered, not just the yakuman patterns. This hand also
    /// includes the regular yaku of [toitoi] and [sanankou] as well as two or three [yakuhai]
    /// when applicable. This hand stands at 31 han and 50 fu via tsumo:
    ///
    ///   - 13 han from [shousuushii](crate::yaku::Yaku::Shousuushii)
    ///   - 13 han from [tsuuiisou](crate::yaku::Yaku::Tsuuiisou)
    ///   - 2 han from [sanankou]
    ///   - 3 han from the maximum number of [yakuhai] possible
    ///   - Basic points (approx.): 4.29 x 10^11
    ///
    /// Any dora to these examples increases the hand even further.
    ///
    /// [sanankou]: crate::yaku::Yaku::Sanankou
    /// [toitoi]: crate::yaku::Yaku::Toitoi
    /// [yakuhai]: crate::yaku::Yaku::Yakuhai
    aotenjou: bool,
    /// Cache of the calculated base value.
    base_value: u64,
    /// Yaku that were awarded.
    yaku: Box<[Yaku]>,
    /// Dora that were awarded.
    dora: Dora,
    /// # 「飜」
    ///
    /// The main portion of scoring. Each yaku and dora is assigned a value in terms of han.
    ///
    /// This is the total han value of the hand from all sources including dora.
    han: u32,
    /// # 「符」
    ///
    /// List of fu (minipoints) that were awarded.
    ///
    /// Used for determining the hand base value and is calculated by using the hand composition,
    /// and/or win method into consideration.
    ///
    /// At 5 han and above, the hand value is only dependent on the han count and the fu count is
    /// ignored. When playing with the "aotenjou" rule (a scoring system by which all caps and
    /// limits are removed), the fu count is used for hands of any han value.
    fu: Box<[Fu]>,
    /// # 「符」
    ///
    /// This is the point value of the awarded fu.
    ///
    /// Used for determining the hand base value and is calculated by using the hand composition,
    /// and/or win method into consideration.
    ///
    /// At 5 han and above, the hand value is only dependent on the han count and the fu count is
    /// ignored. When playing with the "aotenjou" rule (a scoring system by which all caps and
    /// limits are removed), the fu count is used for hands of any han value.
    ///
    /// This value should contain the raw fu value as the rounding occurs when the base value is
    /// calculated.
    fu_value: u64,
}

impl HandScore {
    /// Create a new `HandScore` instance.
    pub fn new(yaku: Box<[Yaku]>, dora: &Dora, fu: Box<[Fu]>, aotenjou: bool) -> Self {
        let mut score = Self {
            aotenjou,
            base_value: 0,
            yaku,
            dora: dora.clone(),
            han: 0,
            fu,
            fu_value: 0,
        };
        score.han = score.calculate_han();
        score.fu_value = score.calculate_fu_value_from_fu();
        score.base_value = score.calculate_base_value();

        score
    }

    /// Create a new `HandScore` instance from the han and fu values.
    ///
    /// This can be handy to use when only the base value is needed and the han and fu values are known.
    /// The yaku and fu may be added later if needed, but the han and fu values will be automatically updated upon doing so.
    pub fn new_from_han_and_fu(han: u32, fu_value: u64, aotenjou: bool) -> Self {
        let mut score = Self {
            aotenjou,
            base_value: 0,
            yaku: Box::default(),
            dora: Dora::default(),
            han,
            fu: Box::default(),
            fu_value,
        };
        score.base_value = score.calculate_base_value();

        score
    }

    /// Get the state of whether or not the aotenjou rule (no caps or limits) is in effect.
    ///
    /// A value of `true` means that there is no points cap or limit.
    pub const fn aotenjou(&self) -> bool {
        self.aotenjou
    }

    /// Get a mutable reference to the aotenjou rule (no caps or limits) state.
    ///
    /// A value of `true` means that there is no points cap or limit.
    pub fn aotenjou_mut(&mut self) -> &mut bool {
        &mut self.aotenjou
    }

    /// Define whether or not the aotenjou rules (no caps or limits) are being used.
    ///
    /// A value of `true` means that there is no points cap or limit.
    ///
    /// This function provides additional convenience over [`Self::aotenjou_mut`] by recalculating the score.
    pub fn aotenjou_set(&mut self, aotenjou: bool) {
        self.aotenjou = aotenjou;
        self.base_value = self.calculate_base_value();
    }

    /// Get the base value of the hand.
    ///
    /// # Payment
    ///
    /// Multiply the basic point value by the amount listed in the table below.
    ///
    /// |                | Tsumo                                           | Ron                           |
    /// |----------------|-------------------------------------------------|-------------------------------|
    /// | **Non-dealer** | - 1x by other non-dealers<br>- 2x by the dealer | - 4x by the discarding player |
    /// | **Dealer**     | - 2x from all other players                     | - 6x by the discarding player |
    ///
    /// Each payment is rounded up to the nearest 100.
    ///
    /// In addition to the payment, the winner is paid an additional amount of points based on the number of honba counters on the table.
    pub const fn base_value(&self) -> u64 {
        self.base_value
    }

    /// Calculate the base hand value after calculating the fu (minipoints) and han.
    ///
    /// **NOTE: This does not calculate the final payment amount.**
    ///
    /// The formula for calculating the base value without caps or limits applied (aotenjou)
    /// or han < 5 is:
    ///
    /// <pre>
    /// fu * 2<sup>(2 + han)</sup>
    /// </pre>
    ///
    ///
    const fn calculate_base_value(&self) -> u64 {
        // Round up to the nearest 10, taking advantage of integer math.
        let fu_value = self.fu_value_rounded();

        if self.aotenjou
            || self.han < 3
            || (self.han == 3 && fu_value < 70)
            || (self.han == 4 && fu_value < 40)
        {
            fu_value * (2_u64.pow(2 + self.han))
        } else if self.han > 12 {
            // 13+ han.
            8_000
        } else if self.han > 10 {
            // 11-12 han.
            6_000
        } else if self.han > 7 {
            // 8-10 han.
            4_000
        } else if self.han > 5 {
            // 6-7 han.
            3_000
        } else {
            // - 5 han or enough minipoints.
            // - han == 3 && fu >= 70
            // - han == 4 && fu >= 40
            2_000
        }
    }

    /// Add up the han from the list of yaku and dora.
    fn calculate_han(&self) -> u32 {
        self.calculate_han_from_yaku() + self.calculate_han_from_dora()
    }

    /// Add up the han from the dora.
    fn calculate_han_from_dora(&self) -> u32 {
        self.dora.calculate_total_han()
    }

    /// Add up the han from the list of yaku.
    fn calculate_han_from_yaku(&self) -> u32 {
        self.yaku.iter().map(|yaku| yaku.han()).sum()
    }

    /// Add up the fu value from the list of fu.
    fn calculate_fu_value_from_fu(&self) -> u64 {
        self.fu.iter().map(|fu| fu.fu()).sum()
    }

    /// Get the dora.
    pub const fn dora(&self) -> &Dora {
        &self.dora
    }

    /// Get a mutable reference for the dora.
    pub const fn dora_mut(&mut self) -> &mut Dora {
        &mut self.dora
    }

    /// Get the list of fu awarded.
    pub const fn fu(&self) -> &[Fu] {
        &self.fu
    }

    /// Get a mutable reference to the list of fu awarded.
    pub fn fu_mut(&mut self) -> &mut [Fu] {
        &mut self.fu
    }

    /// Set the fu.
    pub fn fu_set(&mut self, fu: Box<[Fu]>) {
        self.fu = fu;
        self.fu_value = self.calculate_fu_value_from_fu();
        self.base_value = self.calculate_base_value();
    }

    /// Get the minipoints value.
    ///
    /// This does not account for any necessary rounding.
    /// For the rounded value, use [`Self::fu_value_rounded()`].
    ///
    /// # Rounding Rules
    ///
    /// - Must be rounded up to the nearest 10 before calculating the base value.
    /// - If the fu value is exactly 25 from chiitoitsu (seven pairs), no rounding is done.
    pub const fn fu_value(&self) -> u64 {
        self.fu_value
    }

    /// Get the minipoints value, rounded up to the nearest 10.
    ///
    /// For the raw value, use [`Self::fu_value()`].
    pub const fn fu_value_rounded(&self) -> u64 {
        // Fu for chiitoitsu (seven pairs) isn't rounded).
        if self.fu_value() == 25 {
            self.fu_value()
        } else {
            (self.fu_value() + 9) / 10 * 10
        }
    }

    /// Get a mutable reference to the minipoints value.
    pub fn fu_value_mut(&mut self) -> &mut u64 {
        &mut self.fu_value
    }

    /// Set the minipoints value.
    ///
    /// This function provides convenience by recalculating the score.
    pub fn fu_value_set(&mut self, fu_value: u64) {
        self.fu_value = fu_value;
        self.base_value = self.calculate_base_value();
    }

    /// Get the han value.
    pub const fn han(&self) -> u32 {
        self.han
    }

    /// Get a mutable reference to the han value.
    pub fn han_mut(&mut self) -> &mut u32 {
        &mut self.han
    }

    /// Set the han value.
    pub fn han_set(&mut self, han: u32) {
        self.han = han;
        self.base_value = self.calculate_base_value();
    }

    /// Get the yaku awarded.
    pub const fn yaku(&self) -> &[Yaku] {
        &self.yaku
    }

    /// Get a mutable reference to the yaku awarded.
    pub fn yaku_mut(&mut self) -> &mut [Yaku] {
        &mut self.yaku
    }

    /// Set the yaku.
    pub fn yaku_set(&mut self, yaku: Box<[Yaku]>) {
        self.yaku = yaku;
        self.han = self.calculate_han();
        self.base_value = self.calculate_base_value();
    }
}

#[cfg(test)]
mod tests {
    use crate::fu::FuMentsuTileClass;

    use super::*;

    #[test]
    fn calculate_base_hand_value_works() {
        let mut score = HandScore::new(
            [Yaku::Riichi].into(),
            &Dora::default(),
            [Fu::Winning, Fu::Ankan(FuMentsuTileClass::Tanyaohai)].into(),
            false,
        );

        // 1 han, 40 fu.
        assert_eq!(score.base_value(), 320);
        // 1 han, 32 fu.
        {
            score.fu_value_set(32);
            assert_eq!(score.base_value(), 320);
        }
        // 1 han, 80 fu.
        {
            score.fu_value_set(80);
            assert_eq!(score.base_value(), 640);
        }
        // 2 han, 20 fu.
        {
            score.han_set(2);
            score.fu_value_set(20);
            assert_eq!(score.base_value(), 320);
        }
        // 2 han, 32 fu.
        {
            score.fu_value_set(32);
            assert_eq!(score.base_value(), 640);
        }
        // 2 han, 40 fu.
        {
            score.fu_value_set(40);
            assert_eq!(score.base_value(), 640);
        }
        // 3 han, 40 fu.
        {
            score.han_set(3);
            assert_eq!(score.base_value(), 1280);
        }
        // 4 han, 40 fu.
        {
            score.han_set(4);
            assert_eq!(score.base_value(), 2000);
        }

        // Chiitoitsu without riichi.
        // 2 han, 25 fu.
        {
            score.han_set(2);
            score.fu_value_set(25);
            assert_eq!(score.base_value(), 400);
        }
        // Chiitoitsu with riichi.
        // 3 han, 25 fu.
        {
            score.han_set(3);
            assert_eq!(score.base_value(), 800);
        }

        // Mangan.
        // 5 han, 20 fu.
        {
            score.han_set(5);
            score.fu_value_set(20);
            assert_eq!(score.base_value(), 2_000);
        }
        // 5 han, 80 fu.
        {
            score.fu_value_set(80);
            assert_eq!(score.base_value(), 2_000);
        }

        // Haneman.
        // 6 han, 20 fu.
        {
            score.han_set(6);
            score.fu_value_set(20);
            assert_eq!(score.base_value(), 3_000);
        }
        // 7 han, 20 fu.
        {
            score.han_set(7);
            assert_eq!(score.base_value(), 3_000);
        }

        // Baiman.
        // 8 han, 20 fu.
        {
            score.han_set(8);
            assert_eq!(score.base_value(), 4_000);
        }
        // 9 han, 20 fu.
        {
            score.han_set(9);
            assert_eq!(score.base_value(), 4_000);
        }
        // 10 han, 20 fu.
        {
            score.han_set(10);
            assert_eq!(score.base_value(), 4_000);
        }

        // Sanbaiman.
        // 11 han, 20 fu.
        {
            score.han_set(11);
            assert_eq!(score.base_value(), 6_000);
        }
        // 12 han, 20 fu.
        {
            score.han_set(12);
            assert_eq!(score.base_value(), 6_000);
        }

        // Counted Yakuman.
        // 13 han, 20 fu.
        {
            score.han_set(13);
            assert_eq!(score.base_value(), 8_000);
        }
        // 14 han, 30 fu.
        {
            score.han_set(14);
            score.fu_value_set(30);
            assert_eq!(score.base_value(), 8_000);
        }
        // 15 han, 40 fu.
        {
            score.han_set(15);
            score.fu_value_set(40);
            assert_eq!(score.base_value(), 8_000);
        }

        // Yakuman.
        // 13 han, 30 fu, 1 yakuman.
        {
            score.han_set(13);
            score.fu_value_set(30);
            assert_eq!(score.base_value(), 8_000);
        }

        // Aotenjou enabled.
        {
            score.aotenjou_set(true);
            score.han_set(31);
            score.fu_value_set(50);
            assert_eq!(score.base_value(), 429_496_729_600);
        }
    }
}
