/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module contains the `Wind` enum to provide state for the round and player's seat winds.

use core::fmt;

use crate::{player::relative_position::PlayerRelativePosition, tile::Tile};

/// Used to provide context around the round wind and a player's seat wind.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
#[repr(u8)]
pub enum Wind {
    /// East wind.
    East,
    /// South wind.
    South,
    /// West wind.
    West,
    /// North wind.
    North,
}

impl TryFrom<u8> for Wind {
    type Error = &'static str;

    fn try_from(value: u8) -> Result<Self, Self::Error> {
        let wind = match value {
            0 => Self::East,
            1 => Self::South,
            2 => Self::West,
            3 => Self::North,
            _ => return Err("Invalid value for Wind."),
        };

        Ok(wind)
    }
}

impl fmt::Display for Wind {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::East => "East",
                Self::South => "South",
                Self::West => "West",
                Self::North => "North",
            }
        )
    }
}

impl Wind {
    /// Get the equivalent wind tile.
    pub const fn as_tile(&self) -> Tile {
        match self {
            Self::East => Tile::WindEast,
            Self::South => Tile::WindSouth,
            Self::West => Tile::WindWest,
            Self::North => Tile::WindNorth,
        }
    }

    /// Get the (player) wind to the left of this wind.
    pub const fn kamicha(&self) -> Self {
        self.previous_wind()
    }

    /// Get the next wind in the sequence.
    ///
    /// The order is as follows:
    ///
    /// East -> South -> West -> North -> East
    pub const fn next_wind(&self) -> Self {
        match self {
            Self::East => Self::South,
            Self::South => Self::West,
            Self::West => Self::North,
            Self::North => Self::East,
        }
    }

    /// Get the previous wind in the sequence.
    ///
    /// The order is as follows:
    ///
    /// East -> North -> West -> South -> East
    pub const fn previous_wind(&self) -> Self {
        match self {
            Self::East => Self::North,
            Self::South => Self::East,
            Self::West => Self::South,
            Self::North => Self::West,
        }
    }

    /// Get the (player) wind to the right of this wind.
    pub const fn shimocha(&self) -> Self {
        self.next_wind()
    }

    /// Get the (player) wind across from this wind.
    pub const fn toimen(&self) -> Self {
        match self {
            Self::East => Self::West,
            Self::South => Self::North,
            Self::West => Self::East,
            Self::North => Self::South,
        }
    }

    /// Get the wind from the relative position.
    pub const fn from_relative_position(&self, relative_position: &PlayerRelativePosition) -> Self {
        match relative_position {
            PlayerRelativePosition::Shimocha => self.shimocha(),
            PlayerRelativePosition::Toimen => self.toimen(),
            PlayerRelativePosition::Kamicha => self.kamicha(),
        }
    }
}
