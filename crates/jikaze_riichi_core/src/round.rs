/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module provides structs to give context to a round.

use crate::meld::KanType;
use crate::tile::Tile;
use crate::wall::Wall;
use crate::wind::Wind;

/// Context around the current round.
#[derive(Debug, Clone)]
pub struct RoundContext {
    /// Flag determining if it is the first turn and not interrupted by any call such as pon or kan.
    first_uninterrupted_turn: bool,
    /// The last discarded tile from a player.
    // While this may be annoying to have as an `Option` since most rounds will have this as `Some<T>`,
    // the East player's first draw will have this set as `None` since there is no last discard made.
    last_discard: Option<LastDiscard>,
    /// The last called kan from a player.
    ///
    /// After the player that called the kan completes the kan process by discarding a tile, this should
    /// be set to `None`.
    last_kan: Option<LastKan>,
    /// Pseudo-random number generator seed.
    ///
    /// This is the seed that is used to shuffle the tiles in the wall.
    ///
    /// **WARNING**: This should not be revealed during a game, or perhaps ever, even post-game.
    ///     This is tracked for the purposes of auditing games and should not be revealed to users.
    ///     A one-way hash of this value should also be kept in the records for users to cross-reference with the server records.
    seed: [u8; 32],
    /// Tiles the players draw from, read dora indicators, and draw rinshanpai (replacement tile).
    wall: Wall,
    /// Prevalent wind.
    wind: Wind,
}

impl RoundContext {
    /// Create a new `RoundContext`.
    pub const fn new(wall: Wall, wind: Wind, seed: [u8; 32]) -> Self {
        Self {
            first_uninterrupted_turn: true,
            last_discard: None,
            last_kan: None,
            seed,
            wall,
            wind,
        }
    }

    /// Get the state of whether or not it is the first uninterrupted turn.
    ///
    /// Useful for checking [tenhou](crate::yaku::Yaku::Tenhou), [chiihou](crate::yaku::Yaku::Chiihou), or [renhou](crate::yaku::Yaku::Renhou).
    pub const fn first_uninterrupted_turn(&self) -> bool {
        self.first_uninterrupted_turn
    }

    /// Get a mutable reference to the `first_uninterrupted_turn` field.
    pub fn first_uninterrupted_turn_mut(&mut self) -> &mut bool {
        &mut self.first_uninterrupted_turn
    }

    /// Get an immutable reference to the last discard.
    pub const fn last_discard(&self) -> &Option<LastDiscard> {
        &self.last_discard
    }

    /// Get a mutable reference to the last discard made by a player.
    pub fn last_discard_mut(&mut self) -> &mut Option<LastDiscard> {
        &mut self.last_discard
    }

    /// Get an immutable reference to the last kan called.
    pub const fn last_kan(&self) -> &Option<LastKan> {
        &self.last_kan
    }

    /// Get a mutable reference to the last kan called by a player.
    pub fn last_kan_mut(&mut self) -> &mut Option<LastKan> {
        &mut self.last_kan
    }

    /// Get an immutable reference to the psuedo-random number generator seed.
    pub const fn seed(&self) -> &[u8; 32] {
        &self.seed
    }

    /// Get an immutable reference to the wall for the round.
    pub const fn wall(&self) -> &Wall {
        &self.wall
    }

    /// Get a mutable reference to the wall for the round.
    pub fn wall_mut(&mut self) -> &mut Wall {
        &mut self.wall
    }

    /// Get an immutable reference to the round (prevalent) wind.
    pub const fn wind(&self) -> &Wind {
        &self.wind
    }
}

/// Metadata for the last discarded tile from a player.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct LastDiscard {
    /// Player that made the discard.
    player: Wind,
    /// Tile that was discarded.
    tile: Tile,
}

impl LastDiscard {
    /// Create a new `LastDiscard`.
    pub const fn new(player: Wind, tile: Tile) -> Self {
        Self { player, tile }
    }

    /// Get the player that made the discard.
    pub const fn player(&self) -> &Wind {
        &self.player
    }

    /// Get an immutable reference of the discarded tile.
    pub const fn tile(&self) -> &Tile {
        &self.tile
    }
}

/// Metadata for the last kan called by a player.
///
/// Useful for checking the [chankan](crate::yaku::Yaku::Chankan) yaku.
#[derive(Debug, Clone, Copy, PartialEq)]
pub struct LastKan {
    /// Metadata about the kan.
    kan: KanType,
    /// Player that called the kan.
    player: Wind,
}

impl LastKan {
    /// Create a new `LastKan`.
    pub const fn new(player: Wind, kan: KanType) -> Self {
        Self { kan, player }
    }

    /// Get an immutable reference of the kan that was called.
    pub const fn kan(&self) -> &KanType {
        &self.kan
    }

    /// Get the player that called the kan.
    pub const fn player(&self) -> &Wind {
        &self.player
    }
}
