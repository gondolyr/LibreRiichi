/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Hand patterns/conditions type.

use core::fmt;

use crate::hand::HandVisibility;

/// Hand patterns/conditions type.
///
/// At least one of these are needed to score a hand.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Yaku {
    // 1 han.
    ///
    /// |             |                        |
    /// |-------------|------------------------|
    /// | **Kanji**   | リーチ<br>立直         |
    /// | **Romaji**  | riichi                 |
    /// | **English** | Riichi                 |
    /// | **Value**   | 1 han (concealed only) |
    ///
    /// This may be declared upon achieving tenpai with a **concealed** hand.
    ///
    /// Players may opt not to declare riichi and retain the secrecy of a hand's tenpai status,
    /// using a tactic known as damaten.
    Riichi,
    ///
    /// |             |                        |
    /// |-------------|------------------------|
    /// | **Kanji**   | オープン立直           |
    /// | **Romaji**  | oopun riichi           |
    /// | **English** | Open Riichi            |
    /// | **Value**   | 1 han (concealed only) |
    ///
    /// This is an optional yaku, which is a modified version to [riichi](Self::Riichi).
    ///
    /// The principle works exactly in the same way as the original riichi. However, there is an
    /// added bonus of 1-han attached, which is earned by revealing either the hand or the tile
    /// waits. In general, the intent involves winning by self-draw. As a further optional rule,
    /// yakuman may be awarded if any player actually plays into the open riichi. This is more or
    /// less a harsh penalty for playing into a revealed hand.
    ///
    /// Many official organizations and tournaments do not implement open riichi. It is more or less
    /// reserved for casual or gambling game settings.
    OpenRiichi,
    /// |             |                        |
    /// |-------------|------------------------|
    /// | **Kanji**   | 一発                   |
    /// | **Romaji**  | ippatsu                |
    /// | **English** | One Shot               |
    /// | **Value**   | 1 han (concealed only) |
    ///
    /// A yaku completely dependent on riichi. By definition, ippatsu requires a riichi declaration
    /// to be in effect, for an additional han. Therefore, ippatsu cannot function as a stand-alone
    /// yaku.
    Ippatsu,
    /// |             |                                              |
    /// |-------------|----------------------------------------------|
    /// | **Kanji**   | 門前清自摸和<br>ツモ                         |
    /// | **Romaji**  | menzenchin tsumohou<br>menzen tsumo<br>tsumo |
    /// | **English** | Fully Concealed Hand                         |
    /// | **Value**   | 1 han (concealed only)                       |
    ///
    /// Winning on a self-draw on a **concealed** hand.
    MenzenTsumo,
    /// |             |                   |
    /// |-------------|-------------------|
    /// | **Kanji**   | 断幺九<br>断么九  |
    /// | **Romaji**  | tanyao            |
    /// | **English** | All Simples       |
    /// | **Value**   | 1 han             |
    ///
    /// Tile groups using numbered tiles 2-8 from any of the three main suits. Any tile numbered 2-8 are classed as _chunchanhai_ 「中張牌」, or simples.
    ///
    /// # Kuitan
    ///
    /// Kuitan 「喰い断」 is a rule allowing open tanyao. If kuitan is disallowed, then it would be specified as "kuitan nashi", and tanyao would be restricted as a closed-only yaku.
    ///
    /// # Examples
    ///
    /// ```text
    /// 三三三 四四 ②②② ⑤⑥⑦ ⑧⑧
    /// ```
    ///
    /// Waiting for: `⑧` or `四`
    Tanyao,
    /// |             |                        |
    /// |-------------|------------------------|
    /// | **Kanji**   | 平和                   |
    /// | **Romaji**  | pinfu                  |
    /// | **English** | Pinfu<br>All Sequences |
    /// | **Value**   | 1 han                  |
    ///
    /// **Concealed** all sequences hand with a valueless pair. The winning tile must be a
    /// two-sided wait (ryanmen, open-wait) to finish a sequence.
    ///
    /// # Examples
    ///
    /// ```text
    /// ニ三四 ④⑤ ⑦⑧⑨ 456 88
    /// ```
    ///
    /// Waiting for: `③` or `⑥`
    Pinfu,
    /// |             |                                                                 |
    /// |-------------|-----------------------------------------------------------------|
    /// | **Kanji**   | 一盃口                                                          |
    /// | **Romaji**  | iipeikou                                                        |
    /// | **English** | Pure Double Sequence<br>Identical Sequences<br>Double Sequences |
    /// | **Value**   | 1 han (concealed only)                                          |
    ///
    /// **Concealed** with two completely identical sequences, i.e. the same values in the
    /// same suit.
    ///
    /// # Examples
    ///
    /// ```text
    /// ②②③③④④
    /// ```
    Iipeikou,
    /// |             |                   |
    /// |-------------|-------------------|
    /// | **Kanji**   | 役牌<br>飜牌      |
    /// | **Romaji**  | yakuhai<br>fanpai |
    /// | **English** | Value Tiles       |
    /// | **Value**   | 1 han per triplet |
    ///
    /// A triplet of honor tiles.
    ///
    /// Yakuhai awarded for wind tiles may only apply to the player's wind assignment relative to
    /// the dealer position or to the round wind.
    ///
    /// Dragon tile triplets are universally applicable throughout the game.
    ///
    /// # Examples
    ///
    /// ```text
    /// ①②③ 55 發發 ⑨⮦⑨⑨ 東東⮦東
    /// ```
    ///
    /// Waiting for: `5` or `發`
    Yakuhai(Yakuhai),
    /// |             |                             |
    /// |-------------|-----------------------------|
    /// | **Kanji**   | 嶺上開花                    |
    /// | **Romaji**  | rinshan kaihou              |
    /// | **English** | Dead Wall Draw, After a Kan |
    /// | **Value**   | 1 han                       |
    ///
    /// A yaku where a player wins with the rinshanpai, or the replacement tile after forming a
    /// kantsu (kan call).
    RinshanKaihou,
    /// |             |               |
    /// |-------------|---------------|
    /// | **Kanji**   | 搶槓          |
    /// | **Romaji**  | chankan       |
    /// | **English** | Robbing a Kan |
    /// | **Value**   | 1 han         |
    ///
    /// This yaku is awarded when a player declares ron while a player calls to upgrade a
    /// minkou (triplet called via pon) to a shominkan (added kan). In other words, if a player is
    /// in tenpai for a tile used for that specific added kan, then the player may declare a win on
    /// that tile.
    ///
    /// In almost all cases, players are not allowed to call ron on an ankan (closed kan).
    /// The notable exception involves a kokushi tenpai hand, where the last tile needed for the
    /// yakuman is called for an ankan.
    /// The kokushi exception varies on the rules. In some rules, it is disallowed outright.
    ///
    /// A player is never allowed to call ron on daiminkan because they would be in furiten by not
    /// claiming the winning tile when it is discarded by another player.
    ///
    /// ### Ippatsu
    ///
    /// The ippatsu yaku is normally interrupted by a kan call.
    /// However, ippatsu is still not interrupted because the whole kan process has not been completed.
    /// The entire kan process includes the kan call itself, the rinshan draw, and the discard afterwards.
    /// The chankan win occurs in the middle of that process. So, it is allowable to score both ippatsu and chankan at the same time.
    Chankan,
    /// |             |                                     |
    /// |-------------|-------------------------------------|
    /// | **Kanji**   | 海底撈月                            |
    /// | **Romaji**  | haitei raoyue<br>haitei             |
    /// | **English** | Win by Last Draw<br>Under the River |
    /// | **Value**   | 1 han                               |
    ///
    /// A yaku that is awarded to the player that wins with the self-draw on the haiteihai, the last
    /// drawable tile from the live wall.
    /// The dead wall must retain 14 tiles.
    HaiteiRaoyue,
    /// |             |                                      |
    /// |-------------|--------------------------------------|
    /// | **Kanji**   | 河底撈魚                             |
    /// | **Romaji**  | houtei raoyui<br>houtei              |
    /// | **English** | Win by Last Discard<br>Under the Sea |
    /// | **Value**   | 1 han                                |
    ///
    /// A yaku that is awarded to the player that wins on the last discarded tile of the round.
    /// The dead wall must retain 14 tiles.
    HouteiRaoyui,

    // 2 han.
    /// |             |                        |
    /// |-------------|------------------------|
    /// | **Kanji**   | ダブルリーチ<br>両立直 |
    /// | **Romaji**  | daburu riichi          |
    /// | **English** | Double Riichi          |
    /// | **Value**   | 2 han (concealed only) |
    ///
    /// Same as [Riichi](Self::Riichi) except it is awarded for declaring riichi in the first
    /// go-around, i.e. in the players' first very first turn. The first go-around must be
    /// uninterrupted, i.e. if any claims for kan, pon, or chi, including concealed kans have
    /// occurred before the riichi declaration, this yaku is not possible.
    DoubleRiichi,
    /// |             |                                   |
    /// |-------------|-----------------------------------|
    /// | **Kanji**   | 三色同順<br>さんしき              |
    /// | **Romaji**  | sanshoku doujun<br>sanshiki       |
    /// | **English** | Three Colored Sequence            |
    /// | **Value**   | 2 han (concealed)<br>1 han (open) |
    ///
    /// Hand with three sequences of the same numerical sequence, one in each suit.
    ///
    /// # Examples
    ///
    /// ```text
    /// 一ニ三 ①②③ 123
    /// ```
    SanshokuDoujun(HandVisibility),
    /// |             |                                   |
    /// |-------------|-----------------------------------|
    /// | **Kanji**   | 一気通貫<br>一通                  |
    /// | **Romaji**  | ikkitsuukan<br>ittsuu             |
    /// | **English** | Pure Straight<br>Straight         |
    /// | **Value**   | 2 han (concealed)<br>1 han (open) |
    ///
    /// Hand with three distinct tile groups containing 123, 456, 789 in the same suit. Collectively
    /// the three groups form a complete single suit straight of 1 through 9.
    ///
    /// # Examples
    ///
    /// ```text
    /// 123 456 789
    /// ```
    Ittsuu(HandVisibility),
    /// |             |                                                             |
    /// |-------------|-------------------------------------------------------------|
    /// | **Kanji**   | 全帯<br>混全帯么九<br>全帯么<br>全帯么九                    |
    /// | **Romaji**  | chanta<br>honchantaiyaochuu<br>chantaiyao<br>chantaiyaochuu |
    /// | **English** | Outside Hand<br>Terminal or Honor in Each Group             |
    /// | **Value**   | 2 han (concealed)<br>1 han (open)                           |
    ///
    /// All groups contain at least 1 terminal or honor.
    /// The hand must contain at least one honor and one non-terminal tile, otherwise the hand will
    /// score [junchan](Self::Junchan), [honroutou](Self::Honroutou), or
    /// [chinroutou](Self::Chinroutou) instead.
    ///
    /// # Examples
    ///
    /// ```text
    /// 一一 ⑦⑧⑨ 123 南南 ⮦②①③
    /// ```
    ///
    /// Waiting for: `一` or `南`
    Chanta(HandVisibility),
    /// |             |                                   |
    /// |-------------|-----------------------------------|
    /// | **Kanji**   | 七対子                            |
    /// | **Romaji**  | chiitoitsu<br>chiitoi<br>niconico |
    /// | **English** | Seven Pairs                       |
    /// | **Value**   | 2 han (concealed only)            |
    ///
    /// **Concealed** hand with seven different pairs. Two identical pairs are not allowed.
    ///
    /// # Examples
    ///
    /// ```text
    /// 伍伍 ①① ⑦⑦ 44 南 西西 白白
    /// ```
    ///
    /// Waiting for: `南`
    Chiitoitsu,
    /// |             |                                          |
    /// |-------------|------------------------------------------|
    /// | **Kanji**   | 三色同刻                                 |
    /// | **Romaji**  | sanshoku doukou                         |
    /// | **English** | Mixed Triplets<br>Three Colored Triplets |
    /// | **Value**   | 2 han                                    |
    ///
    /// Koutsu (triplets) or kantsu (quads) of the same numbered tiles across the three main suits.
    ///
    /// # Examples
    ///
    /// ```text
    /// 三三三 ③③③ 333
    /// ```
    SanshokuDoukou,
    /// |             |                          |
    /// |-------------|--------------------------|
    /// | **Kanji**   | 三暗刻                   |
    /// | **Romaji**  | sanankou                 |
    /// | **English** | Three Concealed Triplets |
    /// | **Value**   | 2 han                    |
    ///
    /// Hand with three concealed triplets or quads. The fourth group may be an open triplet or
    /// sequence.
    ///
    /// # Examples
    ///
    /// ```text
    /// 六六六 ②③ ⑧⑧ 444 北北北
    /// ```
    ///
    /// Waiting for: `①` or `④`
    Sanankou,
    /// |             |                           |
    /// |-------------|---------------------------|
    /// | **Kanji**   | 三槓子                    |
    /// | **Romaji**  | sankantsu                 |
    /// | **English** | Three kans<br>Three quads |
    /// | **Value**   | 2 han                     |
    ///
    /// Hand with three kans (quads). The fourth group may be a triplet or sequence.
    ///
    /// # Examples
    ///
    /// ```text
    /// ②③ 44 一一一一 南南南南 ⑧⮦⑧⑧⑧
    /// ```
    ///
    /// Waiting for: `①` or `④`
    Sankantsu,
    /// |             |                     |
    /// |-------------|---------------------|
    /// | **Kanji**   | 対々<br>対々和      |
    /// | **Romaji**  | toitoi<br>toitoihou |
    /// | **English** | All Triplets        |
    /// | **Value**   | 2 han               |
    ///
    /// Every tile group is composed of triplets.
    ///
    /// # Examples
    ///
    /// ```text
    /// 一一一 ⑦⑦ 44 南南南 ⑧⮦⑧⑧
    /// ```
    ///
    /// Waiting for: `⑦` or `4`
    Toitoi,
    /// |             |                      |
    /// |-------------|----------------------|
    /// | **Kanji**   | 小三元               |
    /// | **Romaji**  | shousangen           |
    /// | **English** | Little Three Dragons |
    /// | **Value**   | 2 han                |
    ///
    /// Two sets of 3 dragon tiles and a pair of the third dragon tiles.
    ///
    /// Consider as 4 han for the individual [dragon triplets (yakuhai)](Self::Yakuhai).
    ///
    /// # Examples
    ///
    /// ```text
    /// 678 發發 中中 ⮦ニ三四
    /// ```
    ///
    /// Waiting for: `發` or `中`
    Shousangen,
    /// |             |                          |
    /// |-------------|--------------------------|
    /// | **Kanji**   | 混老頭                   |
    /// | **Romaji**  | honroutou                |
    /// | **English** | All Terminals and Honors |
    /// | **Value**   | 2 han                    |
    ///
    /// Hand containing only terminals and honors.
    ///
    /// Consider as han for [All Triplets](Self::Toitoi).
    ///
    /// # Examples
    ///
    /// ```text
    /// 一一一 九九 南南 99⮦9 北⮦北北
    /// ```
    ///
    /// Waiting for: `九` or `南`
    Honroutou,

    // 3 han.
    /// |             |                                   |
    /// |-------------|-----------------------------------|
    /// | **Kanji**   | 混一色                            |
    /// | **Romaji**  | honitsu<br>honiisou<br>honichi    |
    /// | **English** | Half Flush                        |
    /// | **Value**   | 3 han (concealed)<br>2 han (open) |
    ///
    /// A single suit hand mixed with some honor tiles.
    ///
    /// # Examples
    ///
    /// ```text
    /// ⑥⑥ ⑦⑧⑨ 南南 北北北 ⮦②①③
    /// ```
    ///
    /// Waiting for: `⑥` or `南`
    Honitsu(HandVisibility),
    /// |             |                                                |
    /// |-------------|------------------------------------------------|
    /// | **Kanji**   | 純全<br>純全帯么<br>純全帯么九                 |
    /// | **Romaji**  | junchan<br>junchan taiyo<br>junchan taiyo chuu |
    /// | **English** | Terminal in All Sets                           |
    /// | **Value**   | 3 han (concealed)<br>2 han (open)              |
    ///
    /// All sets contain at least one terminal. Additionally, at least one tile group must contain
    /// a non-terminal tile, or else [chinroutou](Self::Chinroutou) will be scored instead.
    ///
    /// Worth 1 less han if open.
    ///
    /// # Examples
    ///
    /// ```text
    /// 一ニ三 九九九 ⑦⑧⑨ 11 78
    /// ```
    ///
    /// Waiting for: `9`
    ///
    /// _Note_: Also waiting for `6`, but the hand would not be counted as junchan.
    Junchan(HandVisibility),
    /// |             |                                                                |
    /// |-------------|----------------------------------------------------------------|
    /// | **Kanji**   | 二盃口                                                         |
    /// | **Romaji**  | ryanpeikou                                                     |
    /// | **English** | Twice Pure Double Sequences<br>Two Sets of Identical Sequences |
    /// | **Value**   | 3 han (concealed only)                                         |
    ///
    /// **Concealed** hand with two sets of identical sequences ([iipeikou](Self::Iipeikou)).
    /// This hand does not combine with [chiitoitsu](Self::Chiitoitsu), even though the hand can be
    /// interpreted as one. No additional yaku for [Pure Double Sequence (iipeikou)](Self::Iipeikou)
    /// are counted.
    ///
    /// # Examples
    ///
    /// ```text
    /// 四四伍伍六六 ⑥⑥⑦⑧⑧ 22
    /// ```
    ///
    /// Waiting for: `⑦`
    Ryanpeikou,

    // 6 han.
    /// # 「清一色」 Full Flush
    ///
    /// |             |                                   |
    /// |-------------|-----------------------------------|
    /// | **Kanji**   | 清一色                            |
    /// | **Romaji**  | chinitsu<br>chiniisou<br>chinichi |
    /// | **English** | Full Flush<br>Flush               |
    /// | **Value**   | 6 han (concealed)<br>5 han (open) |
    ///
    /// Hand composed entirely of tiles from only one of the three suits.
    /// No honors allowed.
    ///
    /// Worth 1 less han if open.
    ///
    /// # Examples
    ///
    /// ## Using manzu:
    ///
    /// ```text
    /// ニ三四伍伍六六六七七八九九
    /// ```
    ///
    /// Waiting for: `一`, `四`, `七`, or `八`
    ///
    /// ## Using pinzu:
    ///
    /// ```text
    /// ①②③④④⑤⑤⑦⑦⑧⑧⑨⑨
    /// ```
    ///
    /// Waiting for: `④` or `⑤`
    ///
    /// ## Using souzu:
    ///
    /// ```text
    /// 1233456667788
    /// ```
    ///
    /// Waiting for: `3`, `6`, `7`, `8`, or `9`
    Chinitsu(HandVisibility),
    /// |             |                                                    |
    /// |-------------|----------------------------------------------------|
    /// | **Kanji**   | 流し満貫                                           |
    /// | **Romaji**  | nagashi mangan                                     |
    /// | **English** | All Terminals and Honors Discard<br>Nagashi Mangan |
    /// | **Value**   | Mangan (concealed only)                            |
    ///
    /// **Note**: This special hand cannot be combined with any other hand.
    ///
    /// After an exhaustive draw, a player can claim this if:
    ///
    ///   - The hand has been played all the way to ryuukyoku, which means no one must win a hand
    ///     before all the tiles are drawn.
    ///   - The hand is **concealed**.
    ///   - The player has discarded only terminal and honor tiles.
    ///   - None of the player's discards have been claimed.
    ///
    /// The player does not need to be in tenpai.
    ///
    /// The player receives payment equivalent to a self-drawn **mangan**, plus counters and riichi
    /// bets.
    NagashiMangan,

    // Yakuman.
    /// |             |                                       |
    /// |-------------|---------------------------------------|
    /// | **Kanji**   | 国士無双<br>国士無双１３面待ち        |
    /// | **Romaji**  | kokushi musou<br>kokushi              |
    /// | **English** | Thirteen Oprhans                      |
    /// | **Value**   | Yakuman (concealed only)              |
    ///
    /// **Concealed** hand with one of each of the 13 different terminal and honor tiles plus one
    /// extra terminal or honor.
    ///
    /// If a player draws the thirteen different tile types before pairing any of them,
    /// then the hand is called **kokushi musou juusan menmachi** 「国士無双１３面待ち」,
    /// or the kokushi musou 13-closed wait. This may be worth two yakuman in some rules.
    ///
    /// If the hand wins from a 13-sided wait, the winner wins with a value of two yakuman, which
    /// is set to `true`. If the game rules disallows double yakuman, the inner `bool` value is
    /// ignored.
    ///
    /// # Examples
    ///
    /// ## Single Wait
    ///
    /// ```text
    /// 一 ①⑨ 19 東南西北 白發發中
    /// ```
    ///
    /// Waiting for: `九`
    ///
    /// ## 13-sided Wait
    ///
    /// Winning from this wait may be worth two yakuman in some rules.
    ///
    /// ```text
    ///  一九 ①⑨ 19 東南西北 白發中
    /// ```
    ///
    /// Waiting for: Any of the tiles shown
    KokushiMusou(bool),
    /// |             |                          |
    /// |-------------|--------------------------|
    /// | **Kanji**   | 九連宝燈<br>純正九蓮宝燈 |
    /// | **Romaji**  | chuuren poutou           |
    /// | **English** | Nine Gates               |
    /// | **Value**   | Yakuman (concealed only) |
    ///
    /// **Concealed** hand consisting of the tiles 1112345678999 in the same suit plus any one extra
    /// tile in the same suit.
    ///
    /// In case of winning with a nine-sided wait, a double yakuman may be awarded depending on the
    /// rules.
    ///
    /// If the hand wins from a 9-sided wait, the winner wins with a value of two yakuman, which
    /// is set to `true`. If the game rules disallows double yakuman, the inner `bool` value is
    /// ignored.
    ///
    ///
    /// # Examples
    ///
    /// ## Regular
    ///
    /// ```text
    /// ①①①②③④⑤⑤⑦⑧⑨⑨⑨
    /// ```
    ///
    /// Waiting for: `⑥`
    ///
    /// Yasume: `⑤` or `⑨`
    ///
    /// If this hand wins with either of the yasume tiles, the hand will only score a
    /// [chinitsu](Self::Chinitsu) rather than the yakuman.
    ///
    /// ## Nine Tile Wait
    ///
    /// ```text
    /// ①①①②③④⑤⑥⑦⑧⑨⑨⑨
    /// ```
    ///
    /// Waiting for: `①`, `②`, `③`, `④`, `⑤`, `⑥`, `⑦`, `⑧`, `⑨`
    ChuurenPoutou(bool),
    /// |             |                    |
    /// |-------------|--------------------|
    /// | **Kanji**   | 天和               |
    /// | **Romaji**  | tenhou             |
    /// | **English** | Blessing of Heaven |
    /// | **Value**   | Yakuman            |
    ///
    /// East (dealer) winning on their initial deal. Concealed kan is not allowed. No extra yakuman
    /// can be obtained for a thirteen-sided, nine-sided, or single wait.
    Tenhou,
    /// |             |                   |
    /// |-------------|-------------------|
    /// | **Kanji**   | 地和              |
    /// | **Romaji**  | chiihou           |
    /// | **English** | Blessing of Earth |
    /// | **Value**   | Yakuman           |
    ///
    /// Winning on self-draw in the first un-interrupted go-around, before any called tile.
    /// Concealed kan is not allowed. The initial hand may use any pattern, as long as 13 tiles
    /// constitute a tenpai hand, and the 14th tile is the winning tile.
    Chiihou,
    /// |             |                 |
    /// |-------------|-----------------|
    /// | **Kanji**   | 人和            |
    /// | **Romaji**  | renhou          |
    /// | **English** | Blessing of Man |
    /// | **Value**   | Variable        |
    ///
    /// Winning on a discard in the first un-interrupted go-around, before any called tile
    /// and before drawing their first tile.
    /// Concealed kan is not allowed. The initial hand may use any pattern, as long as 13 tiles
    /// constitute a tenpai hand, and the 14th tile is the winning tile.
    ///
    /// # Value Variations
    ///
    /// The value of this yaku will depend on the rules.
    ///
    /// - **Mangan**
    ///
    ///     By default, renhou can be defaulted as a mangan hand. In this case, if the hand could
    ///     score more using normal counting of yaku and dora, it will be scored for the alternate
    ///     value (since the highest-scoring hand must always be chosen). The hand must have at
    ///     least one yaku for this, however; dora alone cannot allow the hand to be scored
    ///     separately.
    ///
    /// - **Yakuman**
    ///
    ///     Likewise, it can be set as a yakuman.
    ///
    /// - **Baiman**
    ///
    ///     A few rulesets set renhou as a baiman, though this is relatively rare.
    ///
    /// - **Yaku**
    ///
    ///     Renhou may be considered as a yaku, to be cumulative with other yaku, with a variable
    ///     han value. It is frequently set at 4 han, so that the hand is always worth mangan
    ///     minimum if it has any other yaku.
    ///
    /// - **Nashi**
    ///
    ///     Renhou may not be used at all.
    Renhou(RenhouValue),
    /// |             |                          |
    /// |-------------|--------------------------|
    /// | **Kanji**   | 四暗刻<br>四暗刻単騎     |
    /// | **Romaji**  | suuankou                 |
    /// | **English** | Four Concealed Triplets  |
    /// | **Value**   | Yakuman (concealed only) |
    ///
    /// Consists of four concealed triplets and a pair.
    ///
    /// If the hand is on a shanpon wait (2-sided wait with pairs), the winning tile must be
    /// self-drawn and not as a discard of another player. A win by discard may render the fourth
    /// triplet as an "open" triplet. Thus, the condition for the yakuman would not be met.
    ///
    /// If the hand wins from a single tile wait, the winner wins with a value of two yakuman, which
    /// is set to `true`. If the game rules disallows double yakuman, the inner `bool` value is
    /// ignored.
    ///
    /// # Examples
    ///
    /// ## 2-sided Wait
    ///
    /// ```text
    /// ⑤⑤ ⑥⑥⑥ 111 88 發發發
    /// ```
    ///
    /// Waiting for: `⑤` or `8`
    ///
    /// Either tile must be won by self-draw. Otherwise this hand is [toitoi](Self::Toitoi) and
    /// [sanankou](Self::Sanankou).
    ///
    /// ## Tanki Wait
    ///
    /// The tanki wait pattern is waiting to finish the pair. This is also referred to as the
    /// "pair wait".
    ///
    /// ### Example 1
    ///
    /// ```text
    /// 九 ⑧⑧⑧ 333 444 南南南
    /// ```
    ///
    /// Waiting for: `九`
    ///
    /// This wait is sometimes considered to be worth a double yakuman, if won by self-draw.
    /// Either way, yakuman is likely guaranteed with the winning tile. If the tanki wait is
    /// combined with additional patterns, then the yakuman may not be guaranteed.
    ///
    /// ### Example 2
    ///
    /// ```text
    /// ③③③ 222 3 777 █11█
    /// ```
    ///
    /// Waiting for: `1` (unavailable), `3`, and `4`
    ///
    /// Even with multiple waits allowing the hand to win, only the `3` produces the yakuman.
    /// The `4` would create a `234` sequence with a `22` pair.
    Suuankou(bool),
    /// |             |             |
    /// |-------------|-------------|
    /// | **Kanji**   | 四槓子      |
    /// | **Romaji**  | suukantsu   |
    /// | **English** | Four Kans   |
    /// | **Value**   | Yakuman     |
    ///
    /// Hand with four kans.
    ///
    /// A player in tenpai for suukantsu cannot claim chankan if a fifth shominkan (or any kan for that matter) is involved.
    /// Because there is no rinshanpai able to be drawn with the fifth kan, the round ends immediately after the call with no
    /// opportunity for anyone to declare a win.
    ///
    /// # Viability
    ///
    /// This yakuman requires four kan calls. As a result, the hand in tenpai always uses hadaka
    /// tanki ([See below](#hanada-tanki)). In order to call a single kan, a player
    /// must draw at least 3 out of 4 of a single tile type under any of these three scenarios:
    ///
    ///   - A player has a pair and calls pon. Then draws the fourth to call kan.
    ///   - A player has a closed triplet and calls kan on a discarded fourth.
    ///   - A player draws all four of a tile type and calls kan.
    ///
    /// For this yakuman, a player must repeat any of those kan calls four times.
    /// This yakuman is the most difficult yakuman to attain tenpai, let alone score.
    /// If any of those tiles are rendered unavailable for kan, it forces the hand to seek other
    /// possible tiles to call kan with; or the hand is made virtually impossible.
    ///
    /// # Hanada Tanki
    ///
    /// Hadaka tanki 「裸単騎」 is a special type of pair wait pattern.
    /// A hand in this state has called for discarded tiles four times.
    /// Upon doing so, only one tile is left closed in the hand.
    /// The yakuman suu kantsu always ends up as hadaka tanki at tenpai.
    ///
    /// ## Examples
    ///
    /// ```text
    /// ④ ▄██ ▄██ ▄██ ▄██
    /// ```
    ///
    /// Waiting for: `④`
    Suukantsu,
    /// |             |            |
    /// |-------------|------------|
    /// | **Kanji**   | 緑一色     |
    /// | **Romaji**  | ryuuiisou  |
    /// | **English** | All Green  |
    /// | **Value**   | Yakuman    |
    ///
    /// Hand composed entirely of green tiles. This hand may be open or concealed.
    ///
    /// Green tiles are: green dragons, and 2, 3, 4, 6, and 8 of bamboo.
    ///
    /// # Examples
    ///
    /// ## Concealed
    ///
    /// ```text
    /// 222 234 66 88 發發發
    /// ```
    ///
    /// Waiting for: `6` or `8`
    ///
    /// ## Open
    ///
    /// ```text
    /// 222 33 444 88 ⮦666
    /// ```
    ///
    /// Waiting for: `3` or `8`
    Ryuuiisou,
    /// |             |                        |
    /// |-------------|------------------------|
    /// | **Kanji**   | 清老頭                 |
    /// | **Romaji**  | chinroutou<br>chinro   |
    /// | **English** | All Terminals          |
    /// | **Value**   | Yakuman                |
    ///
    /// Hand composed entirely of terminal tiles. Simples and honors cannot be used.
    ///
    /// # Examples
    ///
    /// ```text
    /// 一一一 九九 ①① ⑨⑨⑨ 111
    /// ```
    ///
    /// Waiting for: `九` or `①`
    Chinroutou,
    /// |             |            |
    /// |-------------|------------|
    /// | **Kanji**   | 字一色     |
    /// | **Romaji**  | tsuuiisou  |
    /// | **English** | All Honors |
    /// | **Value**   | Yakuman    |
    ///
    /// Hand composed entirely of honor tiles.
    ///
    /// # Daichiishin (Big Seven Stars)
    ///
    /// See [Self::Daichiishin].
    ///
    /// # Examples
    ///
    /// ## Regular
    ///
    /// ```text
    /// 東東東 南南南 北北 發發 ⮦中中中
    /// ```
    ///
    /// Waiting for: `北` or `發`
    Tsuuiisou,
    /// |             |                              |
    /// |-------------|------------------------------|
    /// | **Kanji**   | 大七星                       |
    /// | **Romaji**  | daichiishin                  |
    /// | **English** | Big Seven Stars (All Honors) |
    /// | **Value**   | Yakuman                      |
    ///
    /// Hand composed entirely seven pairs of honor tiles.
    ///
    /// A specific variant to tsuuiisou is possible and can be considered as a separate yakuman,
    /// daichiishin 「大七星」], which means "Big Seven Stars".
    ///
    /// It is a variant of chiitoitsu (seven pairs). There are exactly seven types of honor tiles,
    /// which makes this difficult pattern possible but more difficult than the regular tsuuiisou.
    /// As a consequence, this variation remains as a closed only hand. This is optionally worth
    /// double yakuman.
    ///
    /// # Examples
    ///
    /// ```text
    /// 東東 南南 西西 北北 白白 發發 中
    /// ```
    ///
    /// Waiting for: `中`
    Daichiishin,
    /// |             |                   |
    /// |-------------|-------------------|
    /// | **Kanji**   | 大三元            |
    /// | **Romaji**  | daisangen         |
    /// | **English** | Big Three Dragons |
    /// | **Value**   | Yakuman           |
    ///
    /// Hand with three sets of dragons.
    ///
    /// # Sekinin Barai
    ///
    /// The rule of sekinin barai applies to daisangen in the form of yakuman pao.
    /// This occurs when a player seeking to develop this yakuman already had made open calls (pon)
    /// with two out of the three possible dragon types. The rule is then invoked when the third
    /// type is discarded and called to complete the yakuman portion of the hand.
    /// So, the call upgrades the hand into a very obviously open daisangen.
    /// That third discarder becomes partially liable or fully liable for that play.
    ///
    /// # Examples
    ///
    /// ```text
    /// 四伍六 ③③ 白白 發⮦發發 ⮦中中中
    /// ```
    ///
    /// Waiting for: `白`
    ///
    /// _Note_: This hand is also winnable with `③`, but this hand would be
    /// [shousangen](Self::Shousangen) instead.
    Daisangen,
    /// |             |                   |
    /// |-------------|-------------------|
    /// | **Kanji**   | 小四喜            |
    /// | **Romaji**  | shousuushii       |
    /// | **English** | Little Four Winds |
    /// | **Value**   | Yakuman           |
    ///
    /// Hand with three triplets/quads of winds and a pair of winds.
    ///
    /// # Examples
    ///
    /// ## Shousuushii
    ///
    /// ```text
    /// ④⑤⑥ 東東東 南南 西西 北⮦北北
    /// ```
    ///
    /// Waiting for: `南` or `西`
    ///
    /// ## Either Shousuushii or Daisuushii
    ///
    /// ```text
    /// 77 南南 北北北 東東⮦東 ⮦西西西西
    /// ```
    ///
    /// Waiting for: `南`
    ///
    /// Yasume: `7`
    ///
    /// The takame tile `南` will give daisuushii, while the yasume `7` will give shousuushii.
    Shousuushii,
    /// |             |                |
    /// |-------------|----------------|
    /// | **Kanji**   | 大四喜         |
    /// | **Romaji**  | daisuushii     |
    /// | **English** | Big Four Winds |
    /// | **Value**   | Yakuman        |
    ///
    /// Hand with four triplets/quads of winds.
    ///
    /// Some rules allow this to be counted as a double yakuman.
    ///
    /// # Examples
    ///
    /// ## Daisuushii
    ///
    /// ```text
    /// ニ 東東東 西西西 北北北 ⮦南南南
    /// ```
    ///
    /// Waiting for: `ニ`
    ///
    /// ## Either Shousuushii or Daisuushii
    ///
    /// ```text
    /// 77 南南 北北北 東東⮦東 ⮦西西西西
    /// ```
    ///
    /// Waiting for: `南`
    ///
    /// Yasume: `7`
    ///
    /// The takame tile `南` will give daisuushii, while the yasume `7` will give shousuushii.
    Daisuushii,
}

impl Yaku {
    /// Get the English name for the yaku.
    pub const fn english(&self) -> &str {
        match self {
            // 1 han.
            Self::Riichi => "Riichi",
            Self::OpenRiichi => "Open Riichi",
            Self::Ippatsu => "One Shot",
            Self::MenzenTsumo => "Fully Concealed Hand",
            Self::Tanyao => "All Simples",
            Self::Pinfu => "Pinfu",
            Self::Iipeikou => "Pure Double Sequence",
            Self::Yakuhai(yakuhai) => yakuhai.english(),
            Self::RinshanKaihou => "Dead Wall Draw",
            Self::Chankan => "Robbing a Kan",
            Self::HaiteiRaoyue => "Win by Last Draw",
            Self::HouteiRaoyui => "Win by Last Discard",

            // 2 han.
            Self::DoubleRiichi => "Double Riichi",
            Self::SanshokuDoujun(..) => "Three Colored Sequence",
            Self::Ittsuu(..) => "Pure Straight",
            Self::Chanta(..) => "Outside Hand",
            Self::Chiitoitsu => "Seven Pairs",
            Self::SanshokuDoukou => "Three Colored Triplets",
            Self::Sanankou => "Three Concealed Triplets",
            Self::Sankantsu => "Three Quads",
            Self::Toitoi => "All Triplets",
            Self::Shousangen => "Little Three Dragons",
            Self::Honroutou => "All Terminals and Honors",

            // 3 han.
            Self::Honitsu(..) => "Half Flush",
            Self::Junchan(..) => "Terminals in All Sets",
            Self::Ryanpeikou => "Twice Pure Double Sequences",

            // 6 han.
            Self::Chinitsu(..) => "Full Flush",
            Self::NagashiMangan => "All Terminals and Honors Discard",

            // Yakuman.
            Self::KokushiMusou(..) => "Thirteen Orphans",
            Self::ChuurenPoutou(..) => "Nine Gates",
            Self::Tenhou => "Blessing of Heaven",
            Self::Chiihou => "Blessing of Earth",
            Self::Renhou(..) => "Blessing of Man",
            Self::Suuankou(..) => "Four Concealed Triplets",
            Self::Suukantsu => "Four Kans",
            Self::Ryuuiisou => "All Green",
            Self::Chinroutou => "All Terminals",
            Self::Tsuuiisou => "All Honors",
            Self::Daichiishin => "Big Seven Stars",
            Self::Daisangen => "Big Three Dragons",
            Self::Shousuushii => "Little Four Winds",
            Self::Daisuushii => "Big Four Winds",
        }
    }

    /// Get the han value for the yaku.
    pub fn han(&self) -> u32 {
        match self {
            // 1 han.
            Self::Riichi => 1,
            Self::OpenRiichi => 1,
            Self::Ippatsu => 1,
            Self::MenzenTsumo => 1,
            Self::Tanyao => 1,
            Self::Pinfu => 1,
            Self::Iipeikou => 1,
            Self::Yakuhai(_) => 1, // All of the yakuhai are worth the same.
            Self::RinshanKaihou => 1,
            Self::Chankan => 1,
            Self::HaiteiRaoyue => 1,
            Self::HouteiRaoyui => 1,

            // 2 han.
            Self::DoubleRiichi => 2,
            Self::SanshokuDoujun(hand_visibility) => match hand_visibility {
                HandVisibility::Concealed => 2,
                HandVisibility::Open => 1,
            },
            Self::Ittsuu(hand_visibility) => match hand_visibility {
                HandVisibility::Concealed => 2,
                HandVisibility::Open => 1,
            },
            Self::Chanta(hand_visibility) => match hand_visibility {
                HandVisibility::Concealed => 2,
                HandVisibility::Open => 1,
            },
            Self::Chiitoitsu => 2,
            Self::SanshokuDoukou => 2,
            Self::Sanankou => 2,
            Self::Sankantsu => 2,
            Self::Toitoi => 2,
            Self::Shousangen => 2,
            Self::Honroutou => 2,

            // 3 han.
            Self::Honitsu(hand_visibility) => match hand_visibility {
                HandVisibility::Concealed => 3,
                HandVisibility::Open => 2,
            },
            Self::Junchan(hand_visibility) => match hand_visibility {
                HandVisibility::Concealed => 3,
                HandVisibility::Open => 2,
            },
            Self::Ryanpeikou => 3,

            // 5-6 han.
            Self::Chinitsu(hand_visibility) => match hand_visibility {
                HandVisibility::Concealed => 6,
                HandVisibility::Open => 5,
            },
            Self::NagashiMangan => 5, // Technically should be Mangan, also counting other points on the board.
            Self::Renhou(renhou_value) => renhou_value.into(),

            // Yakuman.
            //
            // With aotenjou rules, these are defaulted to 13 han per yakuman count, with dora
            // included in addition.
            // Under aotenjou rules, yaku that are implied by the completion of another yaku are not
            // counted as additional han for the purposes of scoring. For example, suuankou's 13 han
            // is not combined with sanankou's 2 han because sanankou is a prerequisite for
            // suuankou.
            Self::KokushiMusou(thirteen_wait) => {
                if *thirteen_wait {
                    26
                } else {
                    13
                }
            }
            Self::ChuurenPoutou(nine_wait) => {
                if *nine_wait {
                    26
                } else {
                    13
                }
            }
            Self::Tenhou => 13,
            Self::Chiihou => 13,
            Self::Suuankou(tanki_wait) => {
                if *tanki_wait {
                    26
                } else {
                    13
                }
            }
            Self::Suukantsu => 13,
            Self::Ryuuiisou => 13,
            Self::Chinroutou => 13,
            Self::Tsuuiisou => 13,
            // This is sometimes worth double yakuman depending on the rules.
            Self::Daichiishin => 13,
            Self::Daisangen => 13,
            Self::Shousuushii => 13,
            // This is sometimes worth double yakuman depending on the rules.
            Self::Daisuushii => 13,
        }
    }

    /// Check if the yaku is considered a yakuman.
    pub const fn is_yakuman(&self) -> bool {
        matches!(
            self,
            Self::Renhou(RenhouValue::Yakuman)
                | Self::KokushiMusou(..)
                | Self::ChuurenPoutou(..)
                | Self::Tenhou
                | Self::Chiihou
                | Self::Suuankou(..)
                | Self::Suukantsu
                | Self::Ryuuiisou
                | Self::Chinroutou
                | Self::Tsuuiisou
                | Self::Daichiishin
                | Self::Daisangen
                | Self::Shousuushii
                | Self::Daisuushii
        )
    }

    /// Get the Japanese representation for the yaku.
    pub const fn japanese(&self) -> &str {
        match self {
            // 1 han.
            Self::Riichi => "リーチ",
            Self::OpenRiichi => "オープン立直",
            Self::Ippatsu => "一発",
            Self::MenzenTsumo => "門前清自摸和",
            Self::Tanyao => "断幺九",
            Self::Pinfu => "平和",
            Self::Iipeikou => "一盃口",
            Self::Yakuhai(yakuhai) => match yakuhai {
                Yakuhai::Haku => "役牌 「役牌 白」",
                Yakuhai::Hatsu => "役牌 「役牌 發」",
                Yakuhai::Chun => "役牌 「役牌 中」",
                Yakuhai::SeatWind => "役牌 「自風」",
                Yakuhai::PrevalentWind => "役牌 「場風」",
            },
            Self::RinshanKaihou => "嶺上開花",
            Self::Chankan => "搶槓",
            Self::HaiteiRaoyue => "海底撈月",
            Self::HouteiRaoyui => "河底撈魚",

            // 2 han.
            Self::DoubleRiichi => "ダブルリーチ",
            Self::SanshokuDoujun(..) => "三色同順",
            Self::Ittsuu(..) => "一通",
            Self::Chanta(..) => "全帯",
            Self::Chiitoitsu => "七対子",
            Self::SanshokuDoukou => "三色同刻",
            Self::Sanankou => "三暗刻",
            Self::Sankantsu => "三槓子",
            Self::Toitoi => "対々",
            Self::Shousangen => "小三元",
            Self::Honroutou => "混老頭",

            // 3 han.
            Self::Honitsu(..) => "混一色",
            Self::Junchan(..) => "純全",
            Self::Ryanpeikou => "二盃口",

            // 6 han.
            Self::Chinitsu(..) => "清一色",
            Self::NagashiMangan => "流し満貫",

            // Yakuman.
            Self::KokushiMusou(..) => "国士無双",
            Self::ChuurenPoutou(..) => "九連宝燈",
            Self::Tenhou => "天和",
            Self::Chiihou => "地和",
            Self::Renhou(..) => "人和",
            Self::Suuankou(..) => "四暗刻",
            Self::Suukantsu => "四槓子",
            Self::Ryuuiisou => "緑一色",
            Self::Chinroutou => "清老頭",
            Self::Tsuuiisou => "字一色",
            Self::Daichiishin => "大七星",
            Self::Daisangen => "大三元",
            Self::Shousuushii => "小四喜",
            Self::Daisuushii => "大四喜",
        }
    }

    /// Get the number of yakuman the yaku is worth.
    pub fn yakuman(&self) -> u32 {
        // This is a hack since yakuman aren't always worth 13 han each.
        self.han() / 13
    }
}

impl fmt::Display for Yaku {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                // 1 han.
                Self::Riichi => "Riichi",
                Self::OpenRiichi => "Open Riichi",
                Self::Ippatsu => "Ippatsu",
                Self::MenzenTsumo => "Menzen Tsumo",
                Self::Tanyao => "Tanyao Chuu",
                Self::Pinfu => "Pinfu",
                Self::Iipeikou => "Iipeikou",
                Self::Yakuhai(yakuhai) => match yakuhai {
                    Yakuhai::Haku => "Yakuhai (White Dragon)",
                    Yakuhai::Hatsu => "Yakuhai (Green Dragon)",
                    Yakuhai::Chun => "Yakuhai (Red Dragon)",
                    Yakuhai::SeatWind => "Yakuhai (Seat Wind)",
                    Yakuhai::PrevalentWind => "Yakuhai (Prevalent Wind)",
                },
                Self::RinshanKaihou => "Rinshan Kaihou",
                Self::Chankan => "Chan Kan",
                Self::HaiteiRaoyue => "Haitei Raoyue",
                Self::HouteiRaoyui => "Houtei Raoyui",

                // 2 han.
                Self::DoubleRiichi => "Double Riichi",
                Self::SanshokuDoujun(..) => "SanshokuDoujun",
                Self::Ittsuu(..) => "Ittsuu",
                Self::Chanta(..) => "Chanta",
                Self::Chiitoitsu => "Chiitoitsu",
                Self::SanshokuDoukou => "Sanshokudokou",
                Self::Sanankou => "Sanankou",
                Self::Sankantsu => "Sankantsu",
                Self::Toitoi => "Toitoi",
                Self::Shousangen => "Shousangen",
                Self::Honroutou => "Honroutou",

                // 3 han.
                Self::Honitsu(..) => "Honitsu",
                Self::Junchan(..) => "Junchan",
                Self::Ryanpeikou => "Ryanpeikou",

                // 6 han.
                Self::Chinitsu(..) => "Chinitsu",
                Self::NagashiMangan => "Nagashi Mangan",

                // Yakuman.
                Self::KokushiMusou(..) => "Kokushi Musou",
                Self::ChuurenPoutou(..) => "Chuuren Poutou",
                Self::Tenhou => "Tenhou",
                Self::Chiihou => "Chiihou",
                Self::Renhou(..) => "Renhou",
                Self::Suuankou(..) => "Suuankou",
                Self::Suukantsu => "Suukantsu",
                Self::Ryuuiisou => "Ryuuiisou",
                Self::Chinroutou => "Chinroutou",
                Self::Tsuuiisou => "Tsuuiisou",
                Self::Daichiishin => "Daichiishin",
                Self::Daisangen => "Daisangen",
                Self::Shousuushii => "Shousuushii",
                Self::Daisuushii => "Daisuushii",
            }
        )
    }
}

/// Value for [Yaku::Renhou] depending on the game rules.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum RenhouValue {
    /// By default, renhou can be defaulted as a mangan hand. In this case, if the hand could
    /// score more using normal counting of yaku and dora, it will be scored for the alternate
    /// value (since the highest-scoring hand must always be chosen). The hand must have at
    /// least one yaku for this, however; dora alone cannot allow the hand to be scored
    /// separately.
    #[default]
    Mangan,
    /// The yaku is counted as a yakuman.
    Yakuman,
    /// A few rulesets set renhou as a baiman, though this is relatively rare.
    Baiman,
    /// Renhou may be considered as a yaku, to be cumulative with other yaku, with a variable
    /// han value. It is frequently set at 4 han, so that the hand is always worth mangan
    /// minimum if it has any other yaku.
    Yaku(u32),
    /// Renhou may not be used at all.
    Nashi,
}

impl From<u32> for RenhouValue {
    fn from(value: u32) -> Self {
        match value {
            0 => Self::Nashi,
            5 => Self::Mangan,
            8 => Self::Baiman,
            13.. => Self::Yakuman,
            other => Self::Yaku(other),
        }
    }
}

impl From<RenhouValue> for u32 {
    fn from(value: RenhouValue) -> Self {
        match value {
            RenhouValue::Nashi => 0,
            RenhouValue::Mangan => 5,
            RenhouValue::Baiman => 8,
            RenhouValue::Yakuman => 13,
            RenhouValue::Yaku(han_value) => han_value,
        }
    }
}

impl From<&RenhouValue> for u32 {
    fn from(value: &RenhouValue) -> Self {
        match value {
            RenhouValue::Nashi => 0,
            RenhouValue::Mangan => 5,
            RenhouValue::Baiman => 8,
            RenhouValue::Yakuman => 13,
            RenhouValue::Yaku(han_value) => *han_value,
        }
    }
}

/// Yaku containing a group of honor tiles.
///
/// | Yakupai      | Kanji   |
/// |:------------:|:-------:|
/// | East seat    | 自風 東 |
/// | South seat   | 自風 南 |
/// | West seat    | 自風 西 |
/// | North seat   | 自風 北 |
/// |              |         |
/// | East round   | 場風 東 |
/// | South round  | 場風 南 |
/// | West round   | 場風 西 |
/// |              |         |
/// | White dragon | 役牌 白 |
/// | Green dragon | 役牌 發 |
/// | Red dragon   | 役牌 中 |
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Yakuhai {
    /// A triplet of white dragons.
    Haku,
    /// A triplet of green dragons.
    Hatsu,
    /// A triplet of red dragons.
    Chun,
    /// Tile group applies to the player's wind assignment.
    SeatWind,
    /// Tile group applies to the round wind.
    PrevalentWind,
}

impl Yakuhai {
    /// Get an English-equivalent term.
    pub const fn english(&self) -> &str {
        match self {
            Self::Haku => "White Dragon",
            Self::Hatsu => "Green Dragon",
            Self::Chun => "Red Dragon",
            Self::SeatWind => "Seat Wind",
            Self::PrevalentWind => "Prevalent Wind",
        }
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn yaku_han_works() {
        assert_eq!(Yaku::Renhou(RenhouValue::Nashi).han(), 0);

        // No open penalty yaku.
        assert_eq!(Yaku::Riichi.han(), 1);
        assert_eq!(Yaku::Ippatsu.han(), 1);
        assert_eq!(Yaku::MenzenTsumo.han(), 1);
        assert_eq!(Yaku::Tanyao.han(), 1);
        assert_eq!(Yaku::Pinfu.han(), 1);
        assert_eq!(Yaku::Yakuhai(Yakuhai::Haku).han(), 1);
        assert_eq!(Yaku::Yakuhai(Yakuhai::Hatsu).han(), 1);
        assert_eq!(Yaku::Yakuhai(Yakuhai::Chun).han(), 1);
        assert_eq!(Yaku::Yakuhai(Yakuhai::SeatWind).han(), 1);
        assert_eq!(Yaku::Yakuhai(Yakuhai::PrevalentWind).han(), 1);
        assert_eq!(Yaku::Iipeikou.han(), 1);
        assert_eq!(Yaku::RinshanKaihou.han(), 1);
        assert_eq!(Yaku::Chankan.han(), 1);
        assert_eq!(Yaku::HaiteiRaoyue.han(), 1);
        assert_eq!(Yaku::HouteiRaoyui.han(), 1);

        assert_eq!(Yaku::DoubleRiichi.han(), 2);
        assert_eq!(Yaku::Chiitoitsu.han(), 2);
        assert_eq!(Yaku::SanshokuDoukou.han(), 2);
        assert_eq!(Yaku::Sanankou.han(), 2);
        assert_eq!(Yaku::Toitoi.han(), 2);
        assert_eq!(Yaku::Shousangen.han(), 2);
        assert_eq!(Yaku::Honroutou.han(), 2);

        assert_eq!(Yaku::Ryanpeikou.han(), 3);

        assert_eq!(Yaku::Renhou(RenhouValue::Yaku(4)).han(), 4);

        assert_eq!(Yaku::NagashiMangan.han(), 5);
        assert_eq!(Yaku::Renhou(RenhouValue::Mangan).han(), 5);

        assert_eq!(Yaku::Renhou(RenhouValue::Baiman).han(), 8);

        assert_eq!(Yaku::KokushiMusou(false).han(), 13);
        assert_eq!(Yaku::ChuurenPoutou(false).han(), 13);
        assert_eq!(Yaku::Tenhou.han(), 13);
        assert_eq!(Yaku::Chiihou.han(), 13);
        assert_eq!(Yaku::Renhou(RenhouValue::Yakuman).han(), 13);
        assert_eq!(Yaku::Suuankou(false).han(), 13);
        assert_eq!(Yaku::Suukantsu.han(), 13);
        assert_eq!(Yaku::Ryuuiisou.han(), 13);
        assert_eq!(Yaku::Chinroutou.han(), 13);
        assert_eq!(Yaku::Tsuuiisou.han(), 13);
        assert_eq!(Yaku::Daichiishin.han(), 13);
        assert_eq!(Yaku::Daisangen.han(), 13);
        assert_eq!(Yaku::Shousuushii.han(), 13);
        assert_eq!(Yaku::Daisuushii.han(), 13);

        // Yaku that have han penalties if opened.
        for hand_visibility in [HandVisibility::Concealed, HandVisibility::Open] {
            let han_deduction = match hand_visibility {
                HandVisibility::Concealed => 0,
                HandVisibility::Open => 1,
            };

            assert_eq!(
                Yaku::SanshokuDoujun(hand_visibility).han(),
                2 - han_deduction
            );
            assert_eq!(Yaku::Ittsuu(hand_visibility).han(), 2 - han_deduction);
            assert_eq!(Yaku::Chanta(hand_visibility).han(), 2 - han_deduction);

            assert_eq!(Yaku::Honitsu(hand_visibility).han(), 3 - han_deduction);
            assert_eq!(Yaku::Junchan(hand_visibility).han(), 3 - han_deduction);

            assert_eq!(Yaku::Chinitsu(hand_visibility).han(), 6 - han_deduction);
        }
    }
}
