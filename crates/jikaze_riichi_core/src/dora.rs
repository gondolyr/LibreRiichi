/*
 * SPDX-FileCopyrightText: 2025 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2025 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Special tiles that provide bonus han.

use core::fmt;

/// Value type for dora tiles.
///
/// Each one is worth 1 han, or more if there are overlapping dora indicators.
pub type DoraValue = u32;

/// Special tiles that each grant an additional 1 han.
///
/// Hands are still required to have one yaku in order to score. Dora do not count toward the
/// yaku requirement.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord, Default)]
pub struct Dora {
    /// |             |                |
    /// |-------------|----------------|
    /// | **Kanji**   | ドラ           |
    /// | **Romaji**  | dora           |
    /// | **English** | Dora           |
    /// | **Value**   | 1 han for each |
    ///
    /// Hand with specific tiles that grant 1 han per tile.
    ///
    /// In the event a dora indicator is repeated, either as a kandora or uradora, then the dora tile is worth the number of han as the dora indicator shows.
    /// For example, if the dora indicators are 🀔🀔 (2 five of bamboo), then each 🀕 (six of bamboo) is worth 2 han.
    ///
    /// Hands still require at least one non-dora yaku in order to score. It is best to consider
    /// this as a bonus.
    ///
    /// The dora is marked by the tile after the flipped tile in the dead wall.
    /// For example, if the revealed tile is 🀚 (two of circles), the dora is 🀛 (three of circles).
    /// The dora tile wraps around so 🀏 (nine of characters) as the indicator would mean the dora tile is 🀇 (one of characters).
    ///
    /// # Dora Order
    ///
    /// ## Suit Order
    ///
    /// 1 -> 2 -> 3 -> 4 -> 5 -> 6 -> 7 -> 8 -> 9 -> 1
    ///
    /// 🀇🀈🀉🀊🀋🀌🀍🀎🀏 -> 🀇
    ///
    /// 🀙🀚🀛🀜🀝🀞🀟🀠🀡 -> 🀙
    ///
    /// 🀐🀑🀒🀓🀔🀕🀖🀗🀘 -> 🀐
    ///
    /// ## Wind Order
    ///
    /// East -> South -> West -> North -> East
    ///
    /// 🀀🀁🀂🀃 -> 🀀
    ///
    /// ## Dragon Order
    ///
    /// White -> Green -> Red
    ///
    /// 🀆🀅🀄 -> 🀆
    ///
    /// It may also be easier to remember this order alphabetically (English):
    ///
    /// Green -> Red -> White
    dora: DoraValue,
    /// |             |                            |
    /// |-------------|----------------------------|
    /// | **Kanji**   | 裏ドラ                     |
    /// | **Romaji**  | uradora                    |
    /// | **English** | Uradora<br>Underneath dora |
    /// | **Value**   | 1 han for each             |
    ///
    /// This only applies to hands won using [riichi](crate::yaku::Yaku::Riichi). Thus, no open hand may ever have access to the uradora.
    ///
    /// When a riichi-declared hand has won, the tiles underneath the revealed dora indicators are revealed and serve as additional dora indicators.
    ///
    /// See [Self::dora] for additional information about dora in general.
    uradora: DoraValue,
    /// |             |                            |
    /// |-------------|----------------------------|
    /// | **Kanji**   | 赤ドラ<br>赤牌             |
    /// | **Romaji**  | akadora<br>akapai          |
    /// | **English** | Akadora<br>Red five        |
    /// | **Value**   | 1 han for each             |
    ///
    /// Akadora are specifically marked red dora tiles. If included in the game, they automatically count as dora, regardless of the dora indicator.
    /// If the dora indicator is a four, the red five of the same suit counts as 2 dora.
    ///
    /// Typically, one red five of each suit is used in actual play.
    ///
    /// See [Self::dora] for additional information about dora in general.
    akadora: DoraValue,
    /// |             |                |
    /// |-------------|----------------|
    /// | **Kanji**   | 抜きドラ       |
    /// | **Romaji**  | nukidora       |
    /// | **English** | Nukidora       |
    /// | **Value**   | 1 han for each |
    ///
    /// In some rulesets, especially in 3-player variants (sanma), the North, flower, or other
    /// tiles may be used as a special dora.
    ///
    /// When a nukidora is obtained, the player may call it, set the tile aside,
    /// and the hand gains 1 dora; their value may stack with the visible dora indicators,
    /// if applicable.
    /// Then, the player gets a dead wall draw (rinshanpai) and shifts the live wall.
    /// The same limits to calling kan apply and cannot be called on the
    /// last draw (exhausted wall).
    ///
    /// Tiles declared as nukidora do not count toward the overall hand shape,
    /// and thus may still achieve yaku such as [tanyao](crate::yaku::Yaku::Tanyao).
    ///
    /// Tiles not declared as nukidora are not dora and do not add additional han when scoring.
    nukidora: DoraValue,
}

impl fmt::Display for Dora {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            concat!(
                "Dora: {dora}",
                "Uradora: {uradora}",
                "Akadora: {akadora}",
                "Nukidora: {nukidora}",
            ),
            dora = self.dora,
            uradora = self.uradora,
            akadora = self.akadora,
            nukidora = self.nukidora,
        )
    }
}

impl Dora {
    /// Create a new instance.
    pub fn new(
        dora: DoraValue,
        uradora: DoraValue,
        akadora: DoraValue,
        nukidora: DoraValue,
    ) -> Self {
        Self {
            dora,
            uradora,
            akadora,
            nukidora,
        }
    }

    /// Get the number of akadora.
    pub const fn akadora(&self) -> DoraValue {
        self.akadora
    }

    /// Set the number of akadora.
    pub const fn akadora_mut(&mut self) -> &mut DoraValue {
        &mut self.akadora
    }

    /// Get the number of dora.
    pub const fn dora(&self) -> DoraValue {
        self.dora
    }

    /// Set the number of dora.
    pub const fn dora_mut(&mut self) -> &mut DoraValue {
        &mut self.dora
    }

    /// Get the number of nukidora.
    pub const fn nukidora(&self) -> &DoraValue {
        &self.nukidora
    }

    /// Get mutable access to the number of nukidora.
    pub const fn nukidora_mut(&mut self) -> &mut DoraValue {
        &mut self.nukidora
    }

    /// Get the number of uradora.
    pub const fn uradora(&self) -> DoraValue {
        self.uradora
    }

    /// Get mutable access to the number of uradora.
    pub const fn uradora_mut(&mut self) -> &mut DoraValue {
        &mut self.uradora
    }

    /// Calculate the total han from all of the dora.
    pub fn calculate_total_han(&self) -> DoraValue {
        self.akadora + self.dora + self.nukidora + self.uradora
    }

    /// Get the Japanese kanji for "akadora".
    pub const fn akadora_display_jp(&self) -> &str {
        "赤ドラ"
    }

    /// Get the Japanese kanji for "dora".
    pub const fn dora_display_jp(&self) -> &str {
        "ドラ"
    }

    /// Get the Japanese kanji for "nukidora".
    pub const fn nukidora_display_jp(&self) -> &str {
        "抜きドラ"
    }

    /// Get the Japanese kanji for "uradora".
    pub const fn uradora_display_jp(&self) -> &str {
        "裏ドラ"
    }
}
