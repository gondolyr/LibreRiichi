/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module provides an impl for the `TileSet` struct, which is used for holding sets of tiles such as for a player's hand or their discards.

use alloc::format;
use alloc::string::{String, ToString};
use alloc::vec::Vec;
use core::slice::SliceIndex;

use crate::tile::{Tile, TileSuit};

/// Newtype of a collection of tiles.
#[derive(Debug, Default, Clone, PartialEq, Eq, PartialOrd)]
pub struct TileSet(Vec<Tile>);

impl AsMut<Vec<Tile>> for TileSet {
    fn as_mut(&mut self) -> &mut Vec<Tile> {
        &mut self.0
    }
}

impl AsRef<Vec<Tile>> for TileSet {
    fn as_ref(&self) -> &Vec<Tile> {
        &self.0
    }
}

impl TileSet {
    /// Create a new instance of a `TileSet`.
    pub const fn new(tiles: Vec<Tile>) -> Self {
        Self(tiles)
    }

    /// Binary searches this slice for a given element. If the slice is not sorted, the returned
    /// result is unspecified and meaningless.
    ///
    /// This is a wrapper for the [`slice::binary_search()`] method.
    ///
    /// # Errors
    ///
    /// Note: Error description is from the standard library description of [`slice::binary_search()`].
    ///
    /// > If the value is not found then `Result::Err` is returned, containing the index where a matching element could be inserted while maintaining sorted order.
    pub fn binary_search(&self, tile: &Tile) -> Result<usize, usize> {
        self.0.binary_search(tile)
    }

    /// Return a collection of unique tiles in the set.
    pub fn distinct(&self) -> Vec<Tile> {
        let mut tiles = self.0.clone();
        tiles.sort();
        tiles.dedup();

        tiles
    }

    /// Filter the tiles with the given tile.
    ///
    /// Red 5s are kept if the tile is a regular 5 and vice-versa.
    pub fn filter_by_tile(&self, tile: &Tile) -> Vec<&Tile> {
        self.0.iter().filter(|t| *t == tile).collect()
    }

    /// Get the count of a given tile.
    pub fn find_count(&self, tile: &Tile) -> usize {
        self.filter_by_tile(tile).len()
    }

    /// Returns the first element of the slice, or `None` if it is empty.
    ///
    /// This is a wrapper for the [`slice::first()`](slice::first()) method.
    pub fn first(&self) -> Option<&Tile> {
        self.0.first()
    }

    /// Returns a reference to the tile or subslice depending on the type of index.
    ///
    /// If given a position, returns a reference to the element at that position or `None` if out of bounds.
    /// If given a range, returns the subslice corresponding to that range, or `None` if out of bounds.
    ///
    /// This is a wrapper for the [`slice::get()`](slice::get()) method.
    pub fn get<I>(&self, index: I) -> Option<&I::Output>
    where
        I: SliceIndex<[Tile]>,
    {
        self.0.get(index)
    }

    /// Returns a mutable reference to an element or subslice depending on the type of index (see get) or None if the index is out of bounds.
    ///
    /// This is a wrapper for the [`slice::get_mut()`](slice::get_mut()) method.
    pub fn get_mut<I>(&mut self, index: I) -> Option<&mut I::Output>
    where
        I: SliceIndex<[Tile]>,
    {
        self.0.get_mut(index)
    }

    /// Returns `true` if the vector contains no elements.
    ///
    /// This is a wrapper for the [`slice::is_empty()`](slice::is_empty()) method.
    #[must_use]
    pub fn is_empty(&self) -> bool {
        self.0.is_empty()
    }

    /// Returns the last element of the slice, or `None` if it is empty.
    ///
    /// This is a wrapper for the [`slice::last()`](slice::last()) method.
    pub fn last(&self) -> Option<&Tile> {
        self.0.last()
    }

    /// Returns the number of elements in the slice.
    ///
    /// This is a wrapper for the [`slice::len()`](slice::len()) method.
    pub fn len(&self) -> usize {
        self.0.len()
    }

    /// Removes the last element from a vector and returns it, or None if it is empty.
    ///
    /// This is a wrapper for the [`Vec::pop()`](alloc::vec::Vec::pop()) method.
    pub fn pop(&mut self) -> Option<Tile> {
        self.0.pop()
    }

    /// Appends a tile to the back of a tile set.
    ///
    /// This is a wrapper for the [`Vec::push()`](alloc::vec::Vec::push()) method.
    pub fn push(&mut self, tile: &Tile) {
        self.0.push(*tile);
    }

    /// Removes and returns the tile at position index within the tile set, shifting all tiles after it to the left.
    ///
    /// This is a wrapper for the [`Vec::remove()`](alloc::vec::Vec::remove()) method.
    pub fn remove(&mut self, index: usize) -> Tile {
        self.0.remove(index)
    }

    /// Serialize the tiles into the common shorthand format (e.g "1m", "0p", "7z").
    ///
    /// # Examples
    ///
    /// ```rust
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    ///
    /// let tiles = TileSet::new(vec![
    ///     Tile::DragonGreen,
    ///     Tile::WindNorth,
    ///     Tile::WindSouth,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Souzu9,
    ///     Tile::DragonWhite,
    ///     Tile::Manzu1,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu4,
    ///     Tile::Pinzu4,
    ///     Tile::Pinzu2,
    ///     Tile::Souzu8,
    /// ]);
    ///
    /// assert_eq!(tiles.serialize(), "123m234p489s2456z");
    /// ```
    pub fn serialize(&self) -> String {
        let mut manzu = Vec::new();
        let mut pinzu = Vec::new();
        let mut souzu = Vec::new();
        let mut honors = Vec::new();

        for tile in &self.0 {
            match tile.suit() {
                TileSuit::Manzu => manzu.push(tile),
                TileSuit::Pinzu => pinzu.push(tile),
                TileSuit::Souzu => souzu.push(tile),
                TileSuit::Wind | TileSuit::Dragon => honors.push(tile),
            }
        }

        manzu.sort();
        pinzu.sort();
        souzu.sort();
        honors.sort();

        format!(
            "{manzu}{manzu_identifier}{pinzu}{pinzu_identifier}{souzu}{souzu_identifier}{honors}{honors_identifier}",
            manzu = manzu
                .iter()
                .map(|t| t.shorthand_num().to_string())
                .collect::<Vec<String>>()
                .join(""),
            manzu_identifier = if manzu.is_empty() {
                "".to_string()
            } else {
                TileSuit::Manzu.shorthand().to_string()
            },
            pinzu = pinzu
                .iter()
                .map(|t| t.shorthand_num().to_string())
                .collect::<Vec<String>>()
                .join(""),
            pinzu_identifier = if pinzu.is_empty() {
                "".to_string()
            } else {
                TileSuit::Pinzu.shorthand().to_string()
            },
            souzu = souzu
                .iter()
                .map(|t| t.shorthand_num().to_string())
                .collect::<Vec<String>>()
                .join(""),
            souzu_identifier = if souzu.is_empty() {
                "".to_string()
            } else {
                TileSuit::Souzu.shorthand().to_string()
            },
            honors = honors
                .iter()
                .map(|t| t.shorthand_num().to_string())
                .collect::<Vec<String>>()
                .join(""),
            honors_identifier = if honors.is_empty() {
                "".to_string()
            } else {
                TileSuit::Wind.shorthand().to_string()
            }
        )
    }

    /// Sort the tile set.
    ///
    /// This is a wrapper for the [`slice::sort()`] method.
    pub fn sort(&mut self) {
        self.0.sort();
    }
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn find_count_works() {
        let tiles = TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::WindEast,
            Tile::Manzu1,
            Tile::WindEast,
            Tile::DragonRed,
        ]));

        assert_eq!(tiles.find_count(&Tile::Manzu1), 3);
        assert_eq!(tiles.find_count(&Tile::WindEast), 2);
        assert_eq!(tiles.find_count(&Tile::DragonRed), 1);
    }

    #[test]
    fn find_count_works_with_red_five() {
        let tile_set = TileSet::new(Vec::from([
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::PinzuRed5,
        ]));
        assert_eq!(tile_set.find_count(&Tile::Manzu5), 3);
        assert_eq!(tile_set.find_count(&Tile::ManzuRed5), 3);
    }
}
