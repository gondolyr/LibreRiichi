/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Hand patterns/conditions type.

use alloc::format;
use alloc::string::{String, ToString};
use core::fmt;

/// Minipoints that are awarded based on certain conditions that are fulfilled.
///
/// Typically the sources of fu are added up and then rounded up to the nearest 10, except in the case when the [chiitoitsu](crate::yaku::Yaku::Chiitoitsu) yaku is awarded.
///
/// # Winning Points
///
/// |                                                                  | Points |
/// |------------------------------------------------------------------|:------:|
/// | Winning                                                          | 20     |
/// | Tsumo (Except when [pinfu](crate::yaku::Yaku::Pinfu) is awarded) | 2      |
/// | Concealed Ron                                                    | 10     |
/// | Open Pinfu                                                       | 2      |
/// | [Chiitoitsu](crate::yaku::Yaku::Chiitoitsu) (Fixed value)        | 25     |
///
/// # Group Points
///
/// | English                   | Romaji | Simples | Terminal/Honor |
/// |---------------------------|--------|:-------:|:--------------:|
/// | Open Sequence (shuntsu)   | Minjun | 0       | 0              |
/// | Closed Sequence (shuntsu) | Anjun  | 0       | 0              |
/// | Open Triplet              | Minkou | 2       | 4              |
/// | Closed Triplet            | Ankou  | 4       | 8              |
/// | Open Quad                 | Minkan | 8       | 16             |
/// | Closed Quad               | Ankan  | 16      | 32             |
///
/// - A group can still be considered closed, even if the hand itself is open, depending on how the group is formed.
///
/// # Pair Points
///
/// |                              | Points |
/// |------------------------------|:------:|
/// | Value Honor                  | 2      |
/// | Double Wind (Seat and Round) | 2/4    |
///
/// # Wait Points
///
/// | English     | Romaji  | Points |
/// |-------------|---------|:------:|
/// | Open Wait   | Ryanmen | 0      |
/// | Edge Wait   | Penchan | 2      |
/// | Middle Wait | Kanchan | 2      |
/// | Pair Wait   | Tanki   | 2      |
///
/// - **Ryanmen**: Open wait involving two consecutively numbered tiles, waiting on the outside number.
///     - Example: `23m`, waiting on the `1-4m`.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum Fu {
    /// Points that are awarded for winning.
    Winning,
    /// When the fully concealed hand (tsumo) yaku is awarded, except when [pinfu](crate::yaku::Yaku::Pinfu) is also awarded.
    Tsumo,
    /// Winning a closed hand by ron.
    ConcealedRon,
    /// While the [pinfu](crate::yaku::Yaku::Pinfu) yaku isn't awarded for open hands, if the tiles in the hand and the wait resemble that of pinfu, the scored hand is granted 2 fu.
    ///
    /// # Examples
    ///
    /// Waiting on `3-6p`.
    ///
    /// ```text
    /// 123m45p22s 2'34p6'57s
    /// ```
    OpenPinfu,
    /// If the [chiitoitsu](crate::yaku::Yaku::Chiitoitsu) yaku is awarded, the fu is fixed at 25 points and no other points are awarded.
    Chiitoitsu,

    // Group Points.

    // NOTE: Open and closed sequences (minjun and anjun, respectively) intentionally omitted.
    /// Open triplet.
    Minkou(FuMentsuTileClass),
    /// Closed triplet.
    Ankou(FuMentsuTileClass),
    /// Open quad.
    Minkan(FuMentsuTileClass),
    /// Closed quad.
    Ankan(FuMentsuTileClass),

    // Pair Points.
    /// Value honor.
    Yakuhai,
    /// Double Wind (Seat and Round): Depending on rules, though typically 4 and is simply the yakuhai counted twice.
    BakazeJikaze,

    // Wait Points.

    // NOTE: Open wait (ryanmen) intentionally omitted.
    /// Wait pattern containing the tiles 1 and 2 of the same suit, or 8 and 9 of the same suit.
    ///
    /// # Examples
    ///
    /// Waiting on the `3p`.
    ///
    /// ```text
    /// 12p
    /// ```
    ///
    /// Waiting on the `7s`.
    ///
    /// ```text
    /// 89s
    /// ```
    Penchan,
    /// Wait pattern that completes with the middle number of a sequence.
    ///
    /// # Examples
    ///
    /// Waiting on the `7m`.
    ///
    /// ```text
    /// 68m
    /// ```
    Kanchan,
    /// Wait pattern needing a tile to finish the pair.
    ///
    /// # Examples
    ///
    /// ## Hadaka Tanki
    ///
    /// Waiting on the `2z`.
    ///
    /// ```text
    /// 2z
    /// ```
    ///
    /// ## Nakabukure
    ///
    /// Waiting on the `4m`.
    ///
    /// ```text
    /// 3445m
    /// ```
    ///
    /// ## Nobetan
    ///
    /// Waiting on the `6-9s`.
    ///
    /// ```text
    /// 6789s
    /// ```
    ///
    /// ## Sanmen Nobetan
    ///
    /// Waiting on the `3-6-9m`.
    ///
    /// ```text
    /// 3456789m
    /// ```
    Tanki,
}

impl Fu {
    /// Get the English name for the yaku.
    pub fn english(&self) -> String {
        match self {
            Self::Winning => "Winning".to_string(),
            Self::Tsumo => "Tsumo".to_string(),
            Self::ConcealedRon => "Concealed Ron".to_string(),
            Self::OpenPinfu => "Open Pinfu".to_string(),
            Self::Chiitoitsu => "Seven Pairs hand".to_string(),

            Self::Minkou(tile_classification) => {
                format!("Open Triplet ({})", tile_classification.english())
            }
            Self::Ankou(tile_classification) => {
                format!("Closed Triplet ({})", tile_classification.english())
            }
            Self::Minkan(tile_classification) => {
                format!("Open Quad ({})", tile_classification.english())
            }
            Self::Ankan(tile_classification) => {
                format!("Closed Quad ({})", tile_classification.english())
            }

            Self::Yakuhai => "Value Honor".to_string(),
            Self::BakazeJikaze => "Double Wind (seat & round)".to_string(),

            Self::Penchan => "Edge Wait".to_string(),
            Self::Kanchan => "Middle Wait".to_string(),
            Self::Tanki => "Pair Wait".to_string(),
        }
    }

    /// Get the point value for the fu.
    pub const fn fu(&self) -> u64 {
        match self {
            Self::Winning => 20,
            Self::Tsumo => 2,
            Self::ConcealedRon => 10,
            Self::OpenPinfu => 2,
            Self::Chiitoitsu => 25,

            Self::Minkou(tile_classification) => match tile_classification {
                FuMentsuTileClass::Tanyaohai => 2,
                FuMentsuTileClass::RoutouhaiJihai => 4,
            },
            Self::Ankou(tile_classification) => match tile_classification {
                FuMentsuTileClass::Tanyaohai => 4,
                FuMentsuTileClass::RoutouhaiJihai => 8,
            },
            Self::Minkan(tile_classification) => match tile_classification {
                FuMentsuTileClass::Tanyaohai => 8,
                FuMentsuTileClass::RoutouhaiJihai => 16,
            },
            Self::Ankan(tile_classification) => match tile_classification {
                FuMentsuTileClass::Tanyaohai => 16,
                FuMentsuTileClass::RoutouhaiJihai => 32,
            },

            Self::Yakuhai => 2,
            Self::BakazeJikaze => 4,

            Self::Penchan => 2,
            Self::Kanchan => 2,
            Self::Tanki => 2,
        }
    }

    /// Get the Japanese representation.
    pub fn japanese(&self) -> String {
        match self {
            Self::Winning => "和了".to_string(),
            Self::Tsumo => "ツモ".to_string(),
            Self::ConcealedRon => "暗ロン".to_string(),
            Self::OpenPinfu => "明平和".to_string(),
            Self::Chiitoitsu => "七対子".to_string(),

            Self::Minkou(tile_classification) => {
                format!("明刻 ({})", tile_classification.japanese())
            }
            Self::Ankou(tile_classification) => {
                format!("暗刻 ({})", tile_classification.japanese())
            }
            Self::Minkan(tile_classification) => {
                format!("明槓 ({})", tile_classification.japanese())
            }
            Self::Ankan(tile_classification) => {
                format!("暗槓 ({})", tile_classification.japanese())
            }

            Self::Yakuhai => "役牌".to_string(),
            Self::BakazeJikaze => "場風/自風".to_string(),

            Self::Penchan => "辺張".to_string(),
            Self::Kanchan => "嵌張".to_string(),
            Self::Tanki => "単騎".to_string(),
        }
    }
}

impl fmt::Display for Fu {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Winning => "Winning".to_string(),
                Self::Tsumo => "Tsumo".to_string(),
                Self::ConcealedRon => "Concealed Ron".to_string(),
                Self::OpenPinfu => "Open Pinfu".to_string(),
                Self::Chiitoitsu => "Seven Pairs hand".to_string(),

                Self::Minkou(tile_classification) => {
                    format!("Open Triplet ({})", tile_classification.english())
                }
                Self::Ankou(tile_classification) => {
                    format!("Closed Triplet ({})", tile_classification.english())
                }
                Self::Minkan(tile_classification) => {
                    format!("Open Quad ({})", tile_classification.english())
                }
                Self::Ankan(tile_classification) => {
                    format!("Closed Quad ({})", tile_classification.english())
                }

                Self::Yakuhai => "Value Honor".to_string(),
                Self::BakazeJikaze => "Double Wind (seat & round)".to_string(),

                Self::Penchan => "Edge Wait".to_string(),
                Self::Kanchan => "Middle Wait".to_string(),
                Self::Tanki => "Pair Wait".to_string(),
            }
        )
    }
}

/// Classification of group (mentsu) tile types into simples and terminal/honor.
#[derive(Debug, Clone, PartialEq, Eq, PartialOrd, Ord)]
pub enum FuMentsuTileClass {
    /// 「断幺牌」 Tiles numbered 2-8.
    Tanyaohai,
    /// Terminal (「老頭牌」 routouhai) and honor (「字牌」 jihai) tiles.
    RoutouhaiJihai,
}

impl fmt::Display for FuMentsuTileClass {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "{}",
            match self {
                Self::Tanyaohai => "Simples",
                Self::RoutouhaiJihai => "Terminals/Honors",
            }
        )
    }
}

impl FuMentsuTileClass {
    /// Get the English representation.
    pub const fn english(&self) -> &str {
        match self {
            Self::Tanyaohai => "Simples",
            Self::RoutouhaiJihai => "Terminals/Honors",
        }
    }

    /// Get the Japanese representation.
    pub const fn japanese(&self) -> &str {
        match self {
            FuMentsuTileClass::Tanyaohai => "断幺牌",
            FuMentsuTileClass::RoutouhaiJihai => "老頭牌/字牌",
        }
    }
}
