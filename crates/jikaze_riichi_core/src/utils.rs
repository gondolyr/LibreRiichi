/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! Utility functions that don't fit within anything specific to mahjong.

/// Round a given value up with interval 10 to a given precision.
///
/// # Parameters
///
/// - `value`: Number to round up.
/// - `scale`: Define the result's precision level. A scale value of `0` returns the same value that was provided.
///
/// # Examples
///
/// Round to the nearest ones digit:
///
/// ```
/// use jikaze_riichi_core::utils::ceilu64;
///
/// let rounded = ceilu64(3, 1);
/// assert_eq!(rounded, 10);
/// ```
///
/// Round to the nearest hundreds digit:
///
/// ```
/// use jikaze_riichi_core::utils::ceilu64;
///
/// let rounded = ceilu64(640, 2);
/// assert_eq!(rounded, 700);
/// ```
pub fn ceilu64(value: u64, scale: u32) -> u64 {
    if scale == 0 {
        return value;
    }

    let multiplier = {
        let mut multiplier = 1;
        for _ in 0..scale {
            multiplier *= 10;
        }

        multiplier
    };

    value.div_ceil(multiplier) * multiplier
}

#[cfg(test)]
mod tests {
    use super::*;

    #[test]
    fn ceil_works() {
        assert_eq!(ceilu64(3, 0), 3);
        assert_eq!(ceilu64(0, 1), 0);
        assert_eq!(ceilu64(1, 1), 10);
        assert_eq!(ceilu64(10, 1), 10);
        assert_eq!(ceilu64(11, 1), 20);
        assert_eq!(ceilu64(32, 1), 40);
        assert_eq!(ceilu64(100, 1), 100);
        assert_eq!(ceilu64(101, 1), 110);

        assert_eq!(ceilu64(0, 2), 0);
        assert_eq!(ceilu64(1, 2), 100);
        assert_eq!(ceilu64(99, 2), 100);
        assert_eq!(ceilu64(100, 2), 100);
        assert_eq!(ceilu64(101, 2), 200);
        assert_eq!(ceilu64(640, 2), 700);
    }
}
