/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module provides the `Meld` enum to show what kind of call a player makes on a mentsu (tile group).

use alloc::vec::Vec;

use crate::mentsu::Mentsu;
use crate::player::relative_position::PlayerRelativePosition;
use crate::tile::Tile;

/// Types of tile melds.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum Meld {
    /// 「チー」, Sequences.
    Chii {
        /// Tile claimed from another player's discard pile.
        claimed_tile: Tile,
        /// The player where this tile was taken from, relative to the player making this meld.
        relative_player: PlayerRelativePosition,
        /// Tiles that are used to form this sequence.
        ///
        /// This should be sorted before assigning the value.
        ///
        /// Tiles are explicitly stored here in case there are special tiles such as red 5s.
        tiles: [Tile; 3],
    },
    /// 「カン」, Four-of-a-kind.
    Kan(KanType),
    /// 「ポン」, Three-of-a-kind.
    Pon {
        /// Tile claimed from another player's discard pile.
        claimed_tile: Tile,
        /// The player where this tile was taken from, relative to the player making this meld.
        relative_player: PlayerRelativePosition,
        /// Tiles that are used to form this sequence.
        ///
        /// Tiles are explicitly stored here in case there are special tiles such as red 5s.
        tiles: [Tile; 3],
    },
}

impl PartialEq<Mentsu> for Meld {
    fn eq(&self, other: &Mentsu) -> bool {
        match self {
            Self::Chii {
                claimed_tile: _,
                relative_player: _,
                tiles,
            } => {
                match other {
                    Mentsu::Shuntsu(tile1, tile2, tile3) => {
                        // We sort the tiles because we cannot guarantee that they are
                        // sorted when they are added to the enum variant.
                        // This ensures we can compare the two groups accurately.
                        let mut group1 = Vec::from([
                            tiles.first().unwrap(),
                            tiles.get(1).unwrap(),
                            tiles.last().unwrap(),
                        ]);
                        group1.sort();
                        let group1 = group1.into_boxed_slice();

                        let mut group2 = Vec::from([tile1, tile2, tile3]);
                        group2.sort();
                        let group2 = group2.into_boxed_slice();

                        group1 == group2
                    }
                    _ => false,
                }
            }
            Self::Kan(_) => false,
            Self::Pon {
                claimed_tile,
                relative_player: _,
                tiles: _,
            } => match other {
                Mentsu::Koutsu(tile, ..) => claimed_tile == tile,
                _ => false,
            },
        }
    }
}

/// Types of kan.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum KanType {
    /// 「暗槓」, Closed kan.
    ///
    /// A type of kan where a player has drawn all four of a tile. Unlike the other types of kan,
    /// this keeps the hand closed, unless the hand has been opened previously. Thus, the player
    /// can still declare riichi and gain closed hand yaku and points.
    ///
    /// The kan dora indicator is revealed immediately after calling, rather than waiting for the
    /// player's discard or rinshan draw (dead wall draw).
    Ankan {
        /// Tiles that are used to form this sequence.
        ///
        /// Tiles are explicitly stored here in case there are special tiles such as red 5s.
        tiles: [Tile; 4],
    },
    /// 「大明槓」, Open kan.
    ///
    /// A type of kan where a player possesses three of a tile within the hand and then claims
    /// the discarded tile to form a four-of-a-kind. This opens up the hand if it was previously
    /// closed.
    ///
    /// The kan dora indicator is revealed after discarding or upon another kan declaration on the
    /// same turn.
    Daiminkan {
        /// Tile claimed from another player's discard pile.
        claimed_tile: Tile,
        /// The player where this tile was taken from, relative to the calling player.
        relative_player: PlayerRelativePosition,
        /// Tiles that are used to form this sequence.
        ///
        /// Tiles are explicitly stored here in case there are special tiles such as red 5s.
        tiles: [Tile; 4],
    },
    /// 「小明槓」, Added kan.
    ///
    /// A type of kan where a player had previously claimed pon to form a minkou, or open triplet.
    /// Upon drawing the fourth tile, the player has added that tile to the minkou and upgraded it
    /// to a shouminkan. If the fourth tile was discarded by another player, it cannot be claimed
    /// by kan.
    ///
    /// This is vulnerable to a specific yaku, chankan or robbing the kan.
    ///
    /// The kan dora indicator is revealed after discarding or upon another kan declaration on the
    /// same turn.
    Shominkan {
        /// Tile claimed from another player's discard pile.
        claimed_tile: Tile,
        /// The player where this tile was taken from, relative to the calling player.
        relative_player: PlayerRelativePosition,
        /// Tiles that are used to form this sequence.
        ///
        /// Tiles are explicitly stored here in case there are special tiles such as red 5s.
        tiles: [Tile; 4],
    },
}
