/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module provides the `Mentsu` enum and impls for it.
//!
//! Mentsu are tile shapes/groups.

use alloc::vec::Vec;

use crate::meld::Meld;
use crate::tile::Tile;

/// Shape or group within a hand that is not a meld (i.e. not a call).
#[derive(Debug, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum Mentsu {
    /// Two identical tiles.
    Jantou(Tile, Tile),
    /// Three sequential tiles.
    Shuntsu(Tile, Tile, Tile),
    /// Three identical tiles.
    Koutsu(Tile, Tile, Tile),
}

impl PartialEq<Meld> for Mentsu {
    fn eq(&self, other: &Meld) -> bool {
        match self {
            Self::Jantou(..) => false,
            Self::Shuntsu(tile1, tile2, tile3) => {
                match other {
                    Meld::Chii {
                        claimed_tile: _,
                        relative_player: _,
                        tiles,
                    } => {
                        // We sort the tiles because we cannot guarantee that they are
                        // sorted when they are added to the enum variant.
                        // This ensures we can compare the two groups accurately.
                        let mut group1 = Vec::from([tile1, tile2, tile3]);
                        group1.sort();
                        let group1 = group1.into_boxed_slice();

                        let mut group2 = Vec::from([
                            tiles.first().unwrap(),
                            tiles.get(1).unwrap(),
                            tiles.last().unwrap(),
                        ]);
                        group2.sort();
                        let group2 = group2.into_boxed_slice();

                        group1 == group2
                    }
                    _ => false,
                }
            }
            Self::Koutsu(tile, ..) => match other {
                Meld::Pon {
                    claimed_tile: _,
                    relative_player: _,
                    tiles,
                } => tile == tiles.first().unwrap(),
                _ => false,
            },
        }
    }
}
