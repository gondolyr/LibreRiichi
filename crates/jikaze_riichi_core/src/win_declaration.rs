/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module provides the `WinDeclaration` enum to denote how a player won a round.

use crate::tile::Tile;
use crate::wind::Wind;

/// The types of calls a player can make to declare a win.
#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub enum WinDeclaration {
    /// Winning call on a discard.
    ///
    /// The hand must have a valid yaku.
    /// When in furiten, a player cannot call ron.
    ///
    /// Calling ron can invalidate some yaku such as [`Yaku::Suuankou`](crate::yaku::Yaku::Suuankou) if waiting on a shanpon wait (double pair wait).
    Ron {
        /// Player that dealt-in.
        deal_in_player: Wind,
        /// Discarded tile that was called on.
        tile: Tile,
    },
    /// Winning by self-draw.
    ///
    /// This may apply to any hand, open or closed. With a closed hand, it also counts for the [`Yaku::MenzenTsumo`](crate::yaku::Yaku::MenzenTsumo) yaku.
    /// Even when in furiten, this is allowed to win the hand.
    Tsumo(Tile),
}
