/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This crate provides types necessary to run a game of Riichi Mahjong (Japanese Mahjong).

#![no_std]

extern crate alloc;

pub mod dora;
pub mod error;
pub mod fu;
pub mod game_rules;
pub mod hand;
pub mod meld;
pub mod mentsu;
pub mod player;
pub mod round;
pub mod score;
pub mod tile;
pub mod utils;
pub mod wall;
pub mod win_declaration;
pub mod wind;
pub mod yaku;
