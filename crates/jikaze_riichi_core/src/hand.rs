/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module provides types around a player's hand to store their hand state and provide some context.

use alloc::boxed::Box;
use alloc::format;
use alloc::vec::Vec;
use core::str::FromStr;

use crate::dora::{Dora, DoraValue};
use crate::error::{InvalidMeld, ParseMpszHandError};
use crate::fu::{Fu, FuMentsuTileClass};
use crate::game_rules::GameRules;
use crate::meld::{KanType, Meld};
use crate::mentsu::Mentsu;
use crate::player::player_round_state::{PlayerRoundState, Riichi};
use crate::player::relative_position::PlayerRelativePosition;
use crate::round::RoundContext;
use crate::score::HandScore;
use crate::tile::tile_suit::TileSuit;
use crate::tile::{Tile, TileSet, UNIQUE_TILES_4P};
use crate::win_declaration::WinDeclaration;
use crate::wind::Wind;
use crate::yaku::{RenhouValue, Yaku, Yakuhai};

/// State of a hand's visibility.
///
/// This depends on whether or not the player has made an open call to take another player's discarded tile.
#[derive(Debug, Default, Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
pub enum HandVisibility {
    /// No called melds.
    #[default]
    Concealed,
    /// Hand with called melds.
    Open,
}

/// Contains a player's hand state.
#[derive(Debug, Default, Clone, PartialEq)]
pub struct Hand {
    /// Tiles that the player has discarded.
    discards: TileSet,
    /// Current tile drawn from the wall. If it is `None`, then it is not the player's turn.
    drawn_tile: Option<Tile>,
    /// Parsed groups in the hand.
    groups: Box<[Mentsu]>,
    /// Tiles that are called, which makes the hand open, unless it only contains [ankan (closed kan)](KanType::Ankan).
    melds: Vec<Meld>,
    /// Tiles that are declared as nukidora. While this is typically [`Tile::WindNorth`]
    /// in 3-player variants, some rulesets allow for flowers or other tiles.
    ///
    /// The distinction between North and other tiles is important when counting the total amount
    /// of dora (i.e. West dora indicator(s) may stack on top of the North nukidora).
    nukidora: Vec<Tile>,
    /// Replacement tile after calling "kan".
    ///
    /// After the player has discarded a tile to complete the kan procedure,
    /// this should be set to `None`.
    rinshanpai: Option<Tile>,
    /// Tiles that part of the hand but are hidden, not called.
    ///
    /// This excludes the tile that is drawn.
    tiles: TileSet,
    /// Type of call a player has made to declare a win.
    win_declaration: Option<WinDeclaration>,
}

impl FromStr for Hand {
    type Err = Box<ParseMpszHandError>;

    /// Alias for [`Self::parse_mpsz()`].
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        Self::parse_mpsz(s)
    }
}

impl Hand {
    /// Create a new `Hand` with the given set of tiles.
    ///
    /// If an empty hand is desired, use the `default()` method instead.
    pub fn new(tiles: TileSet) -> Self {
        Self {
            tiles,
            ..Default::default()
        }
    }

    /// Get the number of red five tiles in the hand.
    ///
    /// See [`Dora::akadora`] for more information.
    pub fn akadora_count(&self) -> DoraValue {
        let mut akadora_count = 0;

        for tile in self.tiles.as_ref() {
            if tile.is_red_five() {
                akadora_count += 1;
            }
        }

        if let Some(tile) = self.drawn_tile {
            if tile.is_red_five() {
                akadora_count += 1;
            }
        } else if let Some(WinDeclaration::Ron { tile, .. }) = self.win_declaration {
            if tile.is_red_five() {
                akadora_count += 1;
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii { tiles, .. } | Meld::Pon { tiles, .. } => {
                    for tile in tiles {
                        if tile.is_red_five() {
                            akadora_count += 1;
                        }
                    }
                }
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        for tile in tiles {
                            if tile.is_red_five() {
                                akadora_count += 1;
                            }
                        }
                    }
                },
            }
        }

        akadora_count
    }

    /// Add a meld to the hand and remove the melded tiles from the hand.
    ///
    /// # Errors
    ///
    /// Will return `Err` if the given `Meld` is not valid for the hand and state of the game.
    ///
    /// # Panics
    ///
    /// This can panic if there is a bug with how the function iterates over the tiles in the hand.
    ///
    /// # Examples
    ///
    /// ## Chii
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::Meld;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu4,
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu6));
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// let meld = Meld::Chii {
    ///    claimed_tile: Tile::Manzu6,
    ///    relative_player: PlayerRelativePosition::Kamicha,
    ///    tiles: [Tile::Manzu4, Tile::Manzu5, Tile::Manzu6],
    /// };
    /// let actual = hand.apply_meld(&round_context, &player_round_state, &meld);
    /// let expected_tiles = TileSet::new(Vec::from([
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ]));
    /// let expected_melds = Vec::from([
    ///     Meld::Chii {
    ///         claimed_tile: Tile::Manzu6,
    ///         relative_player: PlayerRelativePosition::Kamicha,
    ///         tiles: [Tile::Manzu4, Tile::Manzu5, Tile::Manzu6],
    ///     },
    /// ]);
    ///
    /// assert!(actual.is_ok());
    /// assert_eq!(hand.tiles(), &expected_tiles);
    /// assert_eq!(hand.melds(), &expected_melds);
    /// ```
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::Meld;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu3,
    ///     Tile::Manzu4,
    ///     Tile::Manzu5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu5));
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// let meld = Meld::Chii {
    ///    claimed_tile: Tile::Manzu5,
    ///    relative_player: PlayerRelativePosition::Kamicha,
    ///    tiles: [Tile::Manzu3, Tile::Manzu4, Tile::Manzu5],
    /// };
    /// let actual = hand.apply_meld(&round_context, &player_round_state, &meld);
    /// let expected_tiles = TileSet::new(Vec::from([
    ///     Tile::Manzu5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ]));
    /// let expected_melds = Vec::from([
    ///     Meld::Chii {
    ///         claimed_tile: Tile::Manzu5,
    ///         relative_player: PlayerRelativePosition::Kamicha,
    ///         tiles: [Tile::Manzu3, Tile::Manzu4, Tile::Manzu5],
    ///     },
    /// ]);
    ///
    /// assert!(actual.is_ok());
    /// assert_eq!(hand.tiles(), &expected_tiles);
    /// assert_eq!(hand.melds(), &expected_melds);
    /// ```
    ///
    /// ## Kan
    ///
    /// ### Ankan (Concealed kan)
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::{KanType, Meld};
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let mut hand = {
    ///     let mut hand = Hand::new(TileSet::new(Vec::from([
    ///         Tile::Manzu5,
    ///         Tile::ManzuRed5,
    ///         Tile::Manzu5,
    ///         Tile::Manzu7,
    ///         Tile::Manzu8,
    ///         Tile::Pinzu2,
    ///         Tile::Pinzu2,
    ///         Tile::Pinzu3,
    ///         Tile::Pinzu3,
    ///         Tile::Souzu2,
    ///         Tile::Souzu3,
    ///         Tile::Souzu4,
    ///         Tile::WindEast,
    ///         Tile::WindEast,
    ///     ])));
    ///     *hand.drawn_tile_mut() = Some(Tile::Manzu5);
    ///
    ///     hand
    /// };
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::WindSouth));
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// let meld = Meld::Kan(KanType::Ankan {
    ///    tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
    /// });
    /// let actual = hand.apply_meld(&round_context, &player_round_state, &meld);
    /// let expected_tiles = TileSet::new(Vec::from([
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ]));
    /// let expected_melds = Vec::from([
    ///     Meld::Kan(KanType::Ankan {
    ///         tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
    ///     }),
    /// ]);
    ///
    /// assert!(actual.is_ok());
    /// assert_eq!(hand.tiles(), &expected_tiles);
    /// assert_eq!(hand.melds(), &expected_melds);
    /// ```
    ///
    /// ### Daiminkan (Open kan)
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::{KanType, Meld};
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu5,
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu5));
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// let meld = Meld::Kan(KanType::Daiminkan {
    ///    claimed_tile: Tile::Manzu5,
    ///    relative_player: PlayerRelativePosition::Kamicha,
    ///    tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
    /// });
    /// let actual = hand.apply_meld(&round_context, &player_round_state, &meld);
    /// let expected_tiles = TileSet::new(Vec::from([
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ]));
    /// let expected_melds = Vec::from([
    ///     Meld::Kan(KanType::Daiminkan {
    ///         claimed_tile: Tile::Manzu5,
    ///         relative_player: PlayerRelativePosition::Kamicha,
    ///         tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
    ///     }),
    /// ]);
    ///
    /// assert!(actual.is_ok());
    /// assert_eq!(hand.tiles(), &expected_tiles);
    /// assert_eq!(hand.melds(), &expected_melds);
    /// ```
    ///
    /// ### Shominkan (Added kan)
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::{KanType, Meld};
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let mut hand = {
    ///     let mut hand = Hand::new(TileSet::new(Vec::from([
    ///         Tile::Manzu7,
    ///         Tile::Manzu8,
    ///         Tile::Pinzu2,
    ///         Tile::Pinzu2,
    ///         Tile::Pinzu3,
    ///         Tile::Pinzu3,
    ///         Tile::Souzu2,
    ///         Tile::Souzu3,
    ///         Tile::Souzu4,
    ///         Tile::WindEast,
    ///         Tile::WindEast,
    ///     ])));
    ///     hand.melds_mut().push(Meld::Pon {
    ///         claimed_tile: Tile::Manzu5,
    ///         relative_player: PlayerRelativePosition::Kamicha,
    ///         tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5],
    ///     });
    ///     *hand.drawn_tile_mut() = Some(Tile::Manzu5);
    ///
    ///     hand
    /// };
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::WindWest));
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// let meld = Meld::Kan(KanType::Shominkan {
    ///    claimed_tile: Tile::Manzu5,
    ///    relative_player: PlayerRelativePosition::Kamicha,
    ///    tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
    /// });
    /// let actual = hand.apply_meld(&round_context, &player_round_state, &meld);
    /// let expected_tiles = TileSet::new(Vec::from([
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ]));
    /// let expected_melds = Vec::from([
    ///     Meld::Kan(KanType::Shominkan {
    ///         claimed_tile: Tile::Manzu5,
    ///         relative_player: PlayerRelativePosition::Kamicha,
    ///         tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
    ///     }),
    /// ]);
    ///
    /// assert!(actual.is_ok());
    /// assert_eq!(hand.tiles(), &expected_tiles);
    /// assert_eq!(hand.melds(), &expected_melds);
    /// ```
    ///
    /// ## Pon
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::Meld;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu5,
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu5));
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// let meld = Meld::Pon {
    ///    claimed_tile: Tile::Manzu5,
    ///    relative_player: PlayerRelativePosition::Kamicha,
    ///    tiles: [Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
    /// };
    /// let actual = hand.apply_meld(&round_context, &player_round_state, &meld);
    /// let expected_tiles = TileSet::new(Vec::from([
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ]));
    /// let expected_melds = Vec::from([
    ///     Meld::Pon {
    ///         claimed_tile: Tile::Manzu5,
    ///         relative_player: PlayerRelativePosition::Kamicha,
    ///         tiles: [Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
    ///     },
    /// ]);
    ///
    /// assert!(actual.is_ok());
    /// assert_eq!(hand.tiles(), &expected_tiles);
    /// assert_eq!(hand.melds(), &expected_melds);
    /// ```
    pub fn apply_meld(
        &mut self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
        meld: &Meld,
    ) -> Result<(), InvalidMeld> {
        match meld {
            Meld::Chii {
                claimed_tile,
                relative_player: _,
                tiles,
            }
            | Meld::Pon {
                claimed_tile,
                relative_player: _,
                tiles,
            } => {
                let meld_options = match meld {
                    Meld::Chii { .. } => self.chii_options(round_context, player_round_state),
                    Meld::Pon { .. } => self.pon_options(round_context, player_round_state),
                    Meld::Kan(..) => {
                        unreachable!("meld matched to Chii and Pon in outside match statement")
                    }
                };
                match meld_options {
                    Some(options) => {
                        // Check that the provided meld is valid given the hand's state.
                        if options.iter().any(|meld_option| meld == meld_option) {
                            // Remove the melded tiles from the hand.
                            {
                                // Since the claimed tile wasn't directly added to the hand, its removal can be skipped.
                                let mut claimed_tile_skipped = false;

                                let mut current_idx = 0;
                                for meld_tile in tiles {
                                    if !claimed_tile_skipped && meld_tile == claimed_tile {
                                        claimed_tile_skipped = true;

                                        continue;
                                    }

                                    // Starting the loop at the index marker saves a bit of time from having to look at tiles already looked at because they will be sorted.
                                    while current_idx < self.tiles().as_ref().len() {
                                        // Unwrapping here should be safe because we're within the bounds between 0 and the length of the set of tiles.
                                        let tile = self.tiles().get(current_idx).unwrap();
                                        if meld_tile == tile {
                                            if meld_tile.is_regular_five()
                                                || meld_tile.is_red_five()
                                            {
                                                if (meld_tile.is_regular_five()
                                                    && tile.is_regular_five())
                                                    || (meld_tile.is_red_five()
                                                        && tile.is_red_five())
                                                {
                                                    self.tiles_mut().remove(current_idx);

                                                    // The tile was found in the hand and removed. We can move to the next meld tile.
                                                    // Removing the tile shifts the tiles so the current index must not be incremented.
                                                    break;
                                                }
                                            } else {
                                                self.tiles_mut().remove(current_idx);

                                                // The tile was found in the hand and removed. We can move to the next meld tile.
                                                // Removing the tile shifts the tiles so the current index must not be incremented.
                                                break;
                                            }
                                        }

                                        current_idx += 1;
                                    }
                                }
                            }

                            self.melds_mut().push(*meld);
                        } else {
                            // The provided meld is not possible with the hand.
                            return Err(InvalidMeld::new(meld));
                        }
                    }
                    None => return Err(InvalidMeld::new(meld)),
                }
            }
            Meld::Kan(kan_type) => {
                let kan_options = self.kan_option(round_context, player_round_state);
                match kan_options {
                    Some(meld_option) => {
                        // Check that the provided meld is valid given the hand's state.
                        if meld == &meld_option {
                            self.melds_mut().push(*meld);

                            match kan_type {
                                KanType::Ankan { tiles }
                                | KanType::Daiminkan {
                                    claimed_tile: _,
                                    relative_player: _,
                                    tiles,
                                } => {
                                    let mut current_idx = 0;
                                    for meld_tile in tiles.get(0..3).unwrap() {
                                        // Starting the loop at the index marker saves a bit of time from having to look at tiles already looked at because they will be sorted.
                                        while current_idx < self.tiles().as_ref().len() {
                                            // Unwrapping here should be safe because we're within the bounds between 0 and the length of the set of tiles.
                                            let tile = self.tiles().get(current_idx).unwrap();
                                            if meld_tile == tile {
                                                self.tiles_mut().remove(current_idx);

                                                // The tile was found in the hand and removed. We can move to the next meld tile.
                                                // Removing the tile shifts the tiles so the current index must not be incremented.
                                                break;
                                            }

                                            current_idx += 1;
                                        }
                                    }

                                    if let KanType::Ankan { .. } = kan_type {
                                        *self.drawn_tile_mut() = None;
                                    }
                                }
                                KanType::Shominkan {
                                    claimed_tile,
                                    relative_player: _,
                                    tiles: _,
                                } => {
                                    let pon_idx = match self.melds().iter().position(|hand_meld| {
                                        if let Meld::Pon {
                                            claimed_tile: claimed_tile_pon,
                                            relative_player: _,
                                            tiles: _,
                                        } = hand_meld
                                        {
                                            claimed_tile == claimed_tile_pon
                                        } else {
                                            false
                                        }
                                    }) {
                                        Some(idx) => idx,
                                        None => {
                                            // This should be unreachable but for now, an error is returned instead.
                                            return Err(InvalidMeld::new(meld));
                                        }
                                    };
                                    // The kan meld is the last element so replace the pon meld with it.
                                    self.melds_mut().swap_remove(pon_idx);
                                }
                            }
                        } else {
                            // The provided meld is not possible with the hand.
                            return Err(InvalidMeld::new(meld));
                        }
                    }
                    None => return Err(InvalidMeld::new(meld)),
                }
            }
        }

        Ok(())
    }

    /// Calculate the number of identical sequences in the hand.
    ///
    /// An identical sequence is the same set of tiles in a sequence within the same suit.
    ///
    /// This is useful for checking [iipeikou](Yaku::Iipeikou) or [ryanpeikou](Yaku::Ryanpeikou).
    fn calculate_number_of_identical_sequences(&self) -> u8 {
        let mut num_matching_sequences = 0;
        'outer: for (outer_idx, group1) in self.groups.iter().enumerate() {
            match group1 {
                Mentsu::Shuntsu(..) => {
                    for group2 in self.groups.get((outer_idx + 1)..).unwrap().iter() {
                        match group2 {
                            Mentsu::Shuntsu(..) => {
                                if group1 == group2 {
                                    num_matching_sequences += 1;
                                    continue 'outer;
                                }
                            }
                            Mentsu::Jantou(..) | Mentsu::Koutsu(..) => continue,
                        }
                    }
                }
                Mentsu::Jantou(..) | Mentsu::Koutsu(..) => continue,
            }
        }

        num_matching_sequences
    }

    /// Calculate the yaku from the hand.
    ///
    /// # Examples
    ///
    /// ## Two Cumulative Single Yakuman
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::mentsu::Mentsu;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::yaku::Yaku;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Souzu2,
    ///     Tile::Souzu2,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu3,
    ///     Tile::Souzu3,
    ///     Tile::Souzu6,
    ///     Tile::Souzu6,
    ///     Tile::Souzu6,
    ///     Tile::Souzu8,
    ///     Tile::Souzu8,
    ///     Tile::Souzu8,
    ///     Tile::DragonGreen,
    ///     Tile::DragonGreen,
    /// ])));
    /// // It is expected to run `groups_mut()` with `find_groups()`.
    /// *hand.groups_mut() = Box::new([
    ///     Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
    ///     Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
    ///     Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
    ///     Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
    ///     Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
    /// ]);
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     *hand.calculate_yaku(&game_rules, &round_context, &player_round_state),
    ///     [Yaku::Suuankou(false), Yaku::Ryuuiisou]
    /// );
    /// ```
    ///
    /// # Three Cumulative Single Yakuman
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::mentsu::Mentsu;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::yaku::Yaku;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::DragonWhite,
    ///     Tile::DragonWhite,
    ///     Tile::DragonWhite,
    ///     Tile::DragonGreen,
    ///     Tile::DragonGreen,
    ///     Tile::DragonGreen,
    ///     Tile::DragonRed,
    ///     Tile::DragonRed,
    ///     Tile::DragonRed,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    /// ])));
    /// // It is expected to run `groups_mut()` with `find_groups()`.
    /// *hand.groups_mut() = Box::new([
    ///     Mentsu::Koutsu(Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite),
    ///     Mentsu::Koutsu(Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen),
    ///     Mentsu::Koutsu(Tile::DragonRed, Tile::DragonRed, Tile::DragonRed),
    ///     Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
    ///     Mentsu::Jantou(Tile::WindWest, Tile::WindWest),
    /// ]);
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     *hand.calculate_yaku(&game_rules, &round_context, &player_round_state),
    ///     [Yaku::Suuankou(false), Yaku::Tsuuiisou, Yaku::Daisangen]
    /// );
    /// ```
    ///
    /// ## Riichi, Tsumo Yaku
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::mentsu::Mentsu;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::{PlayerRoundState, Riichi};
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::yaku::Yaku;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Pinzu8,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    /// ])));
    /// *hand.drawn_tile_mut() = Some(Tile::Souzu4);
    /// // It is expected to run `groups_mut()` with `find_groups()`.
    /// *hand.groups_mut() = Box::new([
    ///     Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
    ///     Mentsu::Koutsu(Tile::Manzu6, Tile::Manzu6, Tile::Manzu6),
    ///     Mentsu::Shuntsu(Tile::Pinzu6, Tile::Pinzu7, Tile::Pinzu8),
    ///     Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
    ///     Mentsu::Jantou(Tile::WindWest, Tile::WindWest),
    /// ]);
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// let mut player_round_state = PlayerRoundState::new(Wind::East);
    /// *player_round_state.riichi_mut() = Some(Riichi::Riichi);
    ///
    /// assert_eq!(
    ///     *hand.calculate_yaku(&game_rules, &round_context, &player_round_state),
    ///     [Yaku::Riichi, Yaku::MenzenTsumo]
    /// );
    /// ```
    ///
    /// ## Sanshoku Doujun Yaku
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::mentsu::Mentsu;
    /// use jikaze_riichi_core::hand::{Hand, HandVisibility};
    /// use jikaze_riichi_core::meld::Meld;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::win_declaration::WinDeclaration;
    /// use jikaze_riichi_core::yaku::Yaku;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Pinzu1,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu8,
    /// ])));
    /// // It is expected to run `groups_mut()` with `find_groups()`.
    /// *hand.groups_mut() = Box::new([
    ///     Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
    ///     Mentsu::Koutsu(Tile::Manzu6, Tile::Manzu6, Tile::Manzu6),
    ///     Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
    ///     Mentsu::Jantou(Tile::Souzu8, Tile::Souzu8),
    /// ]);
    /// let mut melds = hand.melds_mut();
    /// melds.push(Meld::Chii {
    ///     claimed_tile: Tile::Souzu1,
    ///     relative_player: PlayerRelativePosition::Kamicha,
    ///     tiles: [Tile::Souzu1, Tile::Souzu2, Tile::Souzu3],
    /// });
    /// *hand.win_declaration_mut() = Some(WinDeclaration::Ron {
    ///     deal_in_player: Wind::South,
    ///     tile: Tile::Souzu8,
    /// });
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// let mut player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     *hand.calculate_yaku(&game_rules, &round_context, &player_round_state),
    ///     [Yaku::SanshokuDoujun(HandVisibility::Open)]
    /// );
    /// ```
    ///
    /// ## Kokushi Musou
    ///
    /// ### Single Wait
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::yaku::Yaku;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu9,
    ///     Tile::Pinzu1,
    ///     Tile::Pinzu9,
    ///     Tile::Souzu1,
    ///     Tile::Souzu9,
    ///     Tile::WindEast,
    ///     Tile::WindSouth,
    ///     Tile::WindWest,
    ///     Tile::WindNorth,
    ///     Tile::DragonWhite,
    ///     Tile::DragonGreen,
    ///     Tile::DragonGreen,
    /// ])));
    /// *hand.drawn_tile_mut() = Some(Tile::DragonRed);
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     *hand.calculate_yaku(&game_rules, &round_context, &player_round_state),
    ///     [Yaku::KokushiMusou(false)]
    /// );
    /// ```
    ///
    /// ### Thirteen-Way Wait
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::yaku::Yaku;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu9,
    ///     Tile::Pinzu1,
    ///     Tile::Pinzu9,
    ///     Tile::Souzu1,
    ///     Tile::Souzu9,
    ///     Tile::WindEast,
    ///     Tile::WindSouth,
    ///     Tile::WindWest,
    ///     Tile::WindNorth,
    ///     Tile::DragonWhite,
    ///     Tile::DragonGreen,
    ///     Tile::DragonRed,
    /// ])));
    /// *hand.drawn_tile_mut() = Some(Tile::DragonRed);
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     *hand.calculate_yaku(&game_rules, &round_context, &player_round_state),
    ///     [Yaku::KokushiMusou(true)]
    /// );
    /// ```
    ///
    /// ## Chuuren Poutou
    ///
    /// ### Single Wait
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::yaku::Yaku;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu1,
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu2,
    ///     Tile::Manzu4,
    ///     Tile::Manzu5,
    ///     Tile::Manzu6,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Manzu9,
    ///     Tile::Manzu9,
    ///     Tile::Manzu9,
    /// ])));
    /// *hand.drawn_tile_mut() = Some(Tile::Manzu3);
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     *hand.calculate_yaku(&game_rules, &round_context, &player_round_state),
    ///     [Yaku::ChuurenPoutou(false)]
    /// );
    /// ```
    ///
    /// ### Kyuumen Machi
    ///
    /// Nine-way wait.
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::yaku::Yaku;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu1,
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Manzu4,
    ///     Tile::Manzu5,
    ///     Tile::Manzu6,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Manzu9,
    ///     Tile::Manzu9,
    ///     Tile::Manzu9,
    /// ])));
    /// *hand.drawn_tile_mut() = Some(Tile::Manzu2);
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     *hand.calculate_yaku(&game_rules, &round_context, &player_round_state),
    ///     [Yaku::ChuurenPoutou(true)]
    /// );
    /// ```
    pub fn calculate_yaku(
        &self,
        game_rules: &GameRules,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> Box<[Yaku]> {
        let mut yaku = Vec::new();

        if self.has_tenhou(round_context, player_round_state) {
            yaku.push(Yaku::Tenhou);
        } else if self.has_chiihou(round_context, player_round_state) {
            yaku.push(Yaku::Chiihou);
        }

        if self.has_kokushi_musou(round_context) {
            let thirteen_sided_wait = self.get_waits(round_context).len() == 13;
            yaku.push(Yaku::KokushiMusou(thirteen_sided_wait));

            return yaku.into();
        }

        if self.has_chuuren_poutou() {
            let nine_sided_wait = self.get_waits(round_context).len() == 9;
            yaku.push(Yaku::ChuurenPoutou(nine_sided_wait));

            return yaku.into();
        }

        if self.has_suukantsu() {
            yaku.push(Yaku::Suukantsu);
        } else if self.has_suuankou(round_context) {
            let single_tile_wait = self.get_waits(round_context).len() == 1;
            yaku.push(Yaku::Suuankou(single_tile_wait));
        }

        if self.has_ryuuiisou() {
            yaku.push(Yaku::Ryuuiisou);
        }

        if self.has_chinroutou() {
            yaku.push(Yaku::Chinroutou);
        }

        if self.has_tsuuiisou() {
            yaku.push(Yaku::Tsuuiisou);
        }

        if self.has_daisangen() {
            yaku.push(Yaku::Daisangen);
        }

        if self.has_daisuushii() {
            yaku.push(Yaku::Daisuushii);
        } else if self.has_shousuushii() {
            yaku.push(Yaku::Shousuushii);
        }

        // While renhou isn't a yakuman, if it is granted, it is the only yaku that is given.
        // Renhou is only granted if it provides the most amount of points compared to combining other yaku, this is the only yaku that should be returned.
        if self.has_renhou(game_rules, round_context, player_round_state) {
            yaku.push(Yaku::Renhou(RenhouValue::Mangan));
        }

        // Now that all of the yakuman have been checked, we need to return early if there are any
        // as yakuman do not gain any han from other shapes.
        if !yaku.is_empty() {
            return yaku.into();
        }

        let hand_visibility = self.hand_visibility();

        if let Some(riichi) = player_round_state.riichi() {
            match riichi {
                Riichi::Riichi => yaku.push(Yaku::Riichi),
                Riichi::DoubleRiichi => yaku.push(Yaku::DoubleRiichi),
            }

            if player_round_state.ippatsu() {
                yaku.push(Yaku::Ippatsu);
            }
        }

        if self.has_menzen_tsumo() {
            yaku.push(Yaku::MenzenTsumo);
        } else if self.has_chankan(round_context, player_round_state) {
            yaku.push(Yaku::Chankan);
        }

        if self.has_haitei_raoyue(round_context) {
            yaku.push(Yaku::HaiteiRaoyue);
        } else if self.has_houtei_raoyui(round_context) {
            yaku.push(Yaku::HouteiRaoyui);
        } else if self.has_rinshan_kaihou() {
            yaku.push(Yaku::RinshanKaihou);
        }

        if self.has_pinfu(round_context, player_round_state) {
            yaku.push(Yaku::Pinfu);
        }

        if self.has_tanyao(game_rules.kuitan()) {
            yaku.push(Yaku::Tanyao);
        }

        if self.has_ryanpeikou() {
            yaku.push(Yaku::Ryanpeikou);
        } else if self.has_chiitoitsu() {
            yaku.push(Yaku::Chiitoitsu);
        } else if self.has_iipeikou() {
            yaku.push(Yaku::Iipeikou);
        }

        if self.has_yakuhai_haku() {
            yaku.push(Yaku::Yakuhai(Yakuhai::Haku));
        }
        if self.has_yakuhai_hatsu() {
            yaku.push(Yaku::Yakuhai(Yakuhai::Hatsu));
        }
        if self.has_yakuhai_chun() {
            yaku.push(Yaku::Yakuhai(Yakuhai::Chun));
        }

        if self.has_yakuhai_wind(player_round_state.wind()) {
            yaku.push(Yaku::Yakuhai(Yakuhai::SeatWind));
        }
        if self.has_yakuhai_wind(round_context.wind()) {
            yaku.push(Yaku::Yakuhai(Yakuhai::PrevalentWind));
        }

        if self.has_ittsuu() {
            yaku.push(Yaku::Ittsuu(hand_visibility));
        }

        if self.has_sanshoku_doujun() {
            yaku.push(Yaku::SanshokuDoujun(hand_visibility));
        }

        if self.has_junchan() {
            yaku.push(Yaku::Junchan(hand_visibility))
        } else if self.has_honroutou() {
            yaku.push(Yaku::Honroutou);
        } else if self.has_chanta() {
            yaku.push(Yaku::Chanta(hand_visibility));
        }

        if self.has_sanshoku_doukou() {
            yaku.push(Yaku::SanshokuDoukou);
        }

        if self.has_sanankou() {
            yaku.push(Yaku::Sanankou);
        }

        if self.has_sankantsu() {
            yaku.push(Yaku::Sankantsu);
        }

        if self.has_toitoi() {
            yaku.push(Yaku::Toitoi);
        }

        if self.has_shousangen() {
            yaku.push(Yaku::Shousangen);
        }

        if self.has_chinitsu() {
            yaku.push(Yaku::Chinitsu(hand_visibility));
        } else if self.has_honitsu() {
            yaku.push(Yaku::Honitsu(hand_visibility));
        }

        yaku.into()
    }

    /// Calculate the dora in the hand.
    ///
    /// ## Riichi, Tsumo
    ///
    /// ```rust
    /// use jikaze_riichi_core::dora::Dora;
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::mentsu::Mentsu;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::{PlayerRoundState, Riichi};
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::yaku::Yaku;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu5,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Pinzu8,
    ///     Tile::Souzu1,
    ///     Tile::Souzu2,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    /// *hand.drawn_tile_mut() = Some(Tile::Souzu3);
    /// // It is expected to run `groups_mut()` with `find_groups()`.
    /// *hand.groups_mut() = Box::new([
    ///     Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
    ///     Mentsu::Koutsu(Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5),
    ///     Mentsu::Shuntsu(Tile::Pinzu6, Tile::Pinzu7, Tile::Pinzu8),
    ///     Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
    ///     Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
    /// ]);
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// let mut player_round_state = PlayerRoundState::new(Wind::East);
    /// *player_round_state.riichi_mut() = Some(Riichi::Riichi);
    ///
    /// let expected = Dora::new(1, 2, 1, 0);
    ///
    /// assert_eq!(
    ///     hand.calculate_dora(&round_context, &player_round_state),
    ///     expected
    /// );
    /// ```
    pub fn calculate_dora(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> Dora {
        let mut dora = Dora::default();

        *dora.dora_mut() = self.dora_count(round_context);

        if player_round_state.riichi().is_some() {
            *dora.uradora_mut() = self.uradora_count(round_context, player_round_state);
        }

        *dora.akadora_mut() = self.akadora_count();

        *dora.nukidora_mut() = self.nukidora_count();

        dora
    }

    /// Check if a [kan](crate::meld::Meld::Kan) call can be made,
    /// and return the type of kan call ([Ankan](crate::meld::KanType::Ankan), [Daiminkan](crate::meld::KanType::Daiminkan), [Shominkan](crate::meld::KanType::Shominkan)) if such a call is possible.
    ///
    /// If it is `None`, a kan call is not possible for varying reasons.
    ///
    /// # Panics
    ///
    /// This should not panic unless there is a bug in the function to convert a `Vec<Tile>` to a slice.
    ///
    /// # Examples
    ///
    /// ## Ankan (Closed kan)
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::{KanType, Meld};
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    /// *hand.drawn_tile_mut() = Some(Tile::Manzu5);
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     hand.kan_option(&round_context, &player_round_state),
    ///     Some(Meld::Kan(KanType::Ankan {
    ///         tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5, Tile::Manzu5]
    ///     }))
    /// );
    /// ```
    ///
    /// ## Daiminkan (Open kan)
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::{KanType, Meld};
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu5));
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     hand.kan_option(&round_context, &player_round_state),
    ///     Some(Meld::Kan(KanType::Daiminkan {
    ///         claimed_tile: Tile::Manzu5,
    ///         relative_player: PlayerRelativePosition::Kamicha,
    ///         tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5, Tile::Manzu5]
    ///     }))
    /// );
    /// ```
    ///
    /// ## Shominkan (Added kan)
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::{KanType, Meld};
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    /// hand.melds_mut().push(Meld::Pon {
    ///     claimed_tile: Tile::Manzu5,
    ///     relative_player: PlayerRelativePosition::Kamicha,
    ///     tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5],
    /// });
    /// *hand.drawn_tile_mut() = Some(Tile::Manzu5);
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32]
    /// );
    ///
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     hand.kan_option(&round_context, &player_round_state),
    ///     Some(Meld::Kan(KanType::Shominkan {
    ///         claimed_tile: Tile::Manzu5,
    ///         relative_player: PlayerRelativePosition::Kamicha,
    ///         tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5, Tile::Manzu5]
    ///     }))
    /// );
    /// ```
    pub fn kan_option(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> Option<Meld> {
        if round_context.wall().is_wall_exhausted() {
            return None;
        }

        match self.drawn_tile() {
            Some(drawn_tile) => {
                if self.tiles().find_count(drawn_tile) == 3 {
                    let mut kan_tiles = self
                        .tiles()
                        .as_ref()
                        .iter()
                        .filter_map(|tile| {
                            if tile == drawn_tile {
                                Some(*tile)
                            } else {
                                None
                            }
                        })
                        .collect::<Vec<Tile>>();
                    kan_tiles.push(*drawn_tile);
                    // Ensure that any red 5s are sorted first.
                    kan_tiles.sort();

                    Some(Meld::Kan(KanType::Ankan {
                        tiles: kan_tiles.as_slice().try_into().unwrap(),
                    }))
                } else if let Some(Meld::Pon {
                    claimed_tile,
                    relative_player: player,
                    tiles,
                }) = self
                    .melds()
                    .iter()
                    .filter_map(|meld| match meld {
                        Meld::Pon { claimed_tile, .. } => {
                            if drawn_tile == claimed_tile {
                                Some(*meld)
                            } else {
                                None
                            }
                        }
                        _ => None,
                    })
                    .next()
                {
                    let mut tiles = Vec::from(tiles);
                    tiles.push(*drawn_tile);
                    // Ensure that any red 5s are sorted first.
                    tiles.sort();

                    Some(Meld::Kan(KanType::Shominkan {
                        claimed_tile,
                        relative_player: player,
                        tiles: tiles.as_slice().try_into().unwrap(),
                    }))
                } else {
                    None
                }
            }
            None => match round_context.last_discard() {
                Some(last_discard) => {
                    if self.tiles().find_count(last_discard.tile()) == 3 {
                        let mut kan_tiles = self
                            .tiles()
                            .as_ref()
                            .iter()
                            .filter_map(|tile| {
                                if tile == last_discard.tile() {
                                    Some(*tile)
                                } else {
                                    None
                                }
                            })
                            .collect::<Vec<Tile>>();
                        kan_tiles.push(*last_discard.tile());
                        // Ensure that any red 5s are sorted first.
                        kan_tiles.sort();

                        Some(Meld::Kan(KanType::Daiminkan {
                            claimed_tile: *last_discard.tile(),
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: kan_tiles.as_slice().try_into().unwrap(),
                        }))
                    } else {
                        None
                    }
                }
                None => None,
            },
        }
    }

    /// Get the tile sets that can be used for a [chii](crate::meld::Meld::Chii) call.
    ///
    /// When the returned value is `None`, that means that there are not tiles within the player's hand that can make a valid chii call,
    /// or the discard is not from the player to their left.
    ///
    /// In a 3-player game, this function should **not** be called.
    ///
    /// # Panics
    ///
    /// This could panic if there is a bug in determining the relative player position for the last player to discard a tile.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::Meld;
    /// use jikaze_riichi_core::player::player_round_state::{PlayerRoundState, Riichi};
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu4,
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu6));
    ///
    /// let mut player_round_state = PlayerRoundState::new(Wind::East);
    /// *player_round_state.riichi_mut() = Some(Riichi::Riichi);
    ///
    /// assert_eq!(
    ///     hand.chii_options(&round_context, &player_round_state),
    ///     Some(
    ///         [
    ///              Meld::Chii {
    ///                 claimed_tile: Tile::Manzu6,
    ///                 relative_player: PlayerRelativePosition::Kamicha,
    ///                 tiles: [Tile::Manzu4, Tile::ManzuRed5, Tile::Manzu6],
    ///              },
    ///              Meld::Chii {
    ///                 claimed_tile: Tile::Manzu6,
    ///                 relative_player: PlayerRelativePosition::Kamicha,
    ///                 tiles: [Tile::Manzu4, Tile::Manzu5, Tile::Manzu6],
    ///              },
    ///              Meld::Chii {
    ///                 claimed_tile: Tile::Manzu6,
    ///                 relative_player: PlayerRelativePosition::Kamicha,
    ///                 tiles: [Tile::ManzuRed5, Tile::Manzu6, Tile::Manzu7],
    ///              },
    ///              Meld::Chii {
    ///                 claimed_tile: Tile::Manzu6,
    ///                 relative_player: PlayerRelativePosition::Kamicha,
    ///                 tiles: [Tile::Manzu5, Tile::Manzu6, Tile::Manzu7],
    ///              },
    ///              Meld::Chii {
    ///                 claimed_tile: Tile::Manzu6,
    ///                 relative_player: PlayerRelativePosition::Kamicha,
    ///                 tiles: [Tile::Manzu6, Tile::Manzu7, Tile::Manzu8],
    ///              },
    ///         ]
    ///         .into(),
    ///     )
    /// );
    /// ```
    pub fn chii_options(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> Option<Box<[Meld]>> {
        if round_context.wall().is_wall_exhausted() {
            // The round has ended so no more calls to steal tiles can be made (except to win).
            return None;
        }

        let last_discard = match round_context.last_discard() {
            Some(last_discard) => last_discard,
            // Can't call chii from no discard, so terminate early.
            None => return None,
        };

        // Terminate early if the last discard wasn't made by the player to their left.
        if player_round_state.wind().previous_wind() != *last_discard.player() {
            return None;
        }

        let last_discarded_tile = last_discard.tile();
        match last_discarded_tile {
            // "2" and "3" exist.
            Tile::Manzu1 | Tile::Pinzu1 | Tile::Souzu1 => {
                let two = last_discarded_tile.next_tile();
                let three = two.next_tile();

                if self.tiles().find_count(&two) > 0 && self.tiles().find_count(&three) > 0 {
                    Some(
                        [Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [*last_discarded_tile, two, three],
                        }]
                        .into(),
                    )
                } else {
                    None
                }
            }

            Tile::Manzu2 | Tile::Pinzu2 | Tile::Souzu2 => {
                let mut options = Vec::new();

                // "1" and "3" exist.
                {
                    let one = last_discarded_tile.previous_tile();
                    let three = last_discarded_tile.next_tile();

                    if self.tiles().find_count(&one) > 0 && self.tiles.find_count(&three) > 0 {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [one, *last_discarded_tile, three],
                        });
                    }
                }

                // "3" and "4" exist.
                {
                    let three = last_discarded_tile.next_tile();
                    let four = three.next_tile();

                    if self.tiles().find_count(&three) > 0 && self.tiles.find_count(&four) > 0 {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [*last_discarded_tile, three, four],
                        });
                    }
                }

                if options.is_empty() {
                    None
                } else {
                    Some(options.into())
                }
            }

            Tile::Manzu3
            | Tile::Manzu4
            | Tile::ManzuRed5
            | Tile::Manzu5
            | Tile::Manzu6
            | Tile::Pinzu3
            | Tile::Pinzu4
            | Tile::PinzuRed5
            | Tile::Pinzu5
            | Tile::Pinzu6
            | Tile::Souzu3
            | Tile::Souzu4
            | Tile::SouzuRed5
            | Tile::Souzu5
            | Tile::Souzu6
            | Tile::Manzu7
            | Tile::Pinzu7
            | Tile::Souzu7 => {
                let mut options = Vec::new();

                let red_five = match last_discarded_tile.suit() {
                    TileSuit::Manzu => Tile::ManzuRed5,
                    TileSuit::Pinzu => Tile::PinzuRed5,
                    TileSuit::Souzu => Tile::SouzuRed5,
                    TileSuit::Wind | TileSuit::Dragon => unreachable!(),
                };
                let has_akadora = self
                    .tiles()
                    // This gets the correct suit.
                    .filter_by_tile(&red_five)
                    .iter()
                    .filter(|tile| tile.is_red_five())
                    .count()
                    // `> 0` because if the game is being played with 4 akadora, there may be 2 pinzu akadora.
                    > 0;

                // (Tile - 2) and (Tile - 1) exist.
                {
                    let previous_tile = last_discarded_tile.previous_tile();
                    // No akadora.
                    let has_previous_tile = if previous_tile.is_regular_five() {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == previous_tile && tile.is_regular_five())
                            .count()
                            > 0
                    } else {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == previous_tile)
                            .count()
                            > 0
                    };
                    let two_previous_tile = previous_tile.previous_tile();
                    // No akadora.
                    let has_two_previous_tile = if two_previous_tile.is_regular_five() {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == two_previous_tile && tile.is_regular_five())
                            .count()
                            > 0
                    } else {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == two_previous_tile)
                            .count()
                            > 0
                    };

                    // Account for possible akadora in hand.
                    if two_previous_tile.is_regular_five() && has_akadora && has_previous_tile {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [red_five, previous_tile, *last_discarded_tile],
                        });
                    } else if previous_tile.is_regular_five()
                        && has_akadora
                        && has_two_previous_tile
                    {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [two_previous_tile, red_five, *last_discarded_tile],
                        });
                    }

                    if has_two_previous_tile && has_previous_tile {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [two_previous_tile, previous_tile, *last_discarded_tile],
                        });
                    }
                }

                // (Tile - 1) and (Tile + 1) exist.
                {
                    let previous_tile = last_discarded_tile.previous_tile();
                    let has_previous_tile = if previous_tile.is_regular_five() {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == previous_tile && tile.is_regular_five())
                            .count()
                            > 0
                    } else {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == previous_tile)
                            .count()
                            > 0
                    };
                    let next_tile = last_discarded_tile.next_tile();
                    let has_next_tile = if next_tile.is_regular_five() {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == next_tile && tile.is_regular_five())
                            .count()
                            > 0
                    } else {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == next_tile)
                            .count()
                            > 0
                    };

                    // Account for possible akadora in hand.
                    if previous_tile.is_regular_five() && has_akadora && has_next_tile {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [red_five, *last_discarded_tile, next_tile],
                        });
                    } else if next_tile.is_regular_five() && has_akadora && has_previous_tile {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [previous_tile, *last_discarded_tile, red_five],
                        });
                    }

                    if has_previous_tile && has_next_tile {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [previous_tile, *last_discarded_tile, next_tile],
                        });
                    }
                }

                // (Tile + 1) and (Tile + 2) exist.
                {
                    let next_tile = last_discarded_tile.next_tile();
                    // No akadora.
                    let has_next_tile = if next_tile.is_regular_five() {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == next_tile && tile.is_regular_five())
                            .count()
                            > 0
                    } else {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == next_tile)
                            .count()
                            > 0
                    };
                    let two_next_tile = next_tile.next_tile();
                    // No akadora.
                    let has_two_next_tile = if two_next_tile.is_regular_five() {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == two_next_tile && tile.is_regular_five())
                            .count()
                            > 0
                    } else {
                        self.tiles()
                            .as_ref()
                            .iter()
                            .filter(|tile| **tile == two_next_tile)
                            .count()
                            > 0
                    };

                    // Account for possible akadora in hand.
                    if next_tile.is_regular_five() && has_akadora && has_two_next_tile {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [*last_discarded_tile, red_five, two_next_tile],
                        });
                    } else if two_next_tile.is_regular_five() && has_akadora && has_next_tile {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [*last_discarded_tile, next_tile, red_five],
                        });
                    }

                    if has_next_tile && has_two_next_tile {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [*last_discarded_tile, next_tile, two_next_tile],
                        });
                    }
                }

                if options.is_empty() {
                    None
                } else {
                    Some(options.into())
                }
            }

            Tile::Manzu8 | Tile::Pinzu8 | Tile::Souzu8 => {
                let mut options = Vec::new();

                // "6" and "7" exist.
                {
                    let seven = last_discarded_tile.previous_tile();
                    let six = seven.previous_tile();

                    if self.tiles().find_count(&six) > 0 && self.tiles.find_count(&seven) > 0 {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [six, seven, *last_discarded_tile],
                        });
                    }
                }

                // "7" and "9" exist.
                {
                    let seven = last_discarded_tile.previous_tile();
                    let nine = last_discarded_tile.next_tile();

                    if self.tiles().find_count(&seven) > 0 && self.tiles.find_count(&nine) > 0 {
                        options.push(Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [seven, *last_discarded_tile, nine],
                        });
                    }
                }

                if options.is_empty() {
                    None
                } else {
                    Some(options.into())
                }
            }

            // "7" and "8" exist.
            Tile::Manzu9 | Tile::Pinzu9 | Tile::Souzu9 => {
                let eight = last_discarded_tile.previous_tile();
                let seven = eight.previous_tile();

                if self.tiles().find_count(&eight) > 0 && self.tiles().find_count(&seven) > 0 {
                    Some(
                        [Meld::Chii {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [seven, eight, *last_discarded_tile],
                        }]
                        .into(),
                    )
                } else {
                    None
                }
            }

            Tile::WindEast
            | Tile::WindSouth
            | Tile::WindWest
            | Tile::WindNorth
            | Tile::DragonWhite
            | Tile::DragonGreen
            | Tile::DragonRed => None,
        }
    }

    /// Get the player's discards.
    pub const fn discards(&self) -> &TileSet {
        &self.discards
    }

    /// Get a mutable reference to the player's discards.
    pub fn discards_mut(&mut self) -> &mut TileSet {
        &mut self.discards
    }

    /// Get the number of dora tiles in the hand.
    ///
    /// See [`Dora::dora`] for more information.
    ///
    /// # Panics
    ///
    /// Will panic if the number of North tiles as nukidora exceeds the capacity of [`DoraValue`].
    pub fn dora_count(&self, round_context: &RoundContext) -> DoraValue {
        let mut dora_count = 0;

        for (dora_indicator, _uradora_indicator) in round_context.wall().dora_indicator_tiles() {
            let dora_tile = dora_indicator.next_tile();
            dora_count += self.tile_count(&dora_tile);

            if matches!(dora_indicator, Tile::WindWest) {
                dora_count += DoraValue::try_from(
                    self.nukidora
                        .iter()
                        .filter(|tile| matches!(tile, Tile::WindNorth))
                        .count(),
                )
                .unwrap();
            }
        }

        dora_count
    }

    /// Get the currently drawn tile, if it exists.
    pub const fn drawn_tile(&self) -> Option<&Tile> {
        match &self.drawn_tile {
            Some(tile) => Some(tile),
            None => None,
        }
    }

    /// Get a mutable reference to the current drawn tile.
    pub fn drawn_tile_mut(&mut self) -> &mut Option<Tile> {
        &mut self.drawn_tile
    }

    /// Find the groups of 3 (sequences or triplets) and the pair.
    ///
    /// # Panics
    ///
    /// This should not panic unless there is a bug with how a red 5 is searched for in the hand.
    ///
    /// # Examples
    ///
    /// ## Four copies of `3m` creates sequence and a triplet.
    ///
    /// ```text
    /// 12333456m678999s
    /// ```
    ///
    /// ```rust
    /// use jikaze_riichi_core::mentsu::Mentsu;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Manzu3,
    ///     Tile::Manzu3,
    ///     Tile::Pinzu4,
    ///     Tile::Pinzu5,
    ///     Tile::Pinzu6,
    ///     Tile::Souzu6,
    ///     Tile::Souzu7,
    ///     Tile::Souzu8,
    ///     Tile::Souzu9,
    ///     Tile::Souzu9,
    ///     Tile::Souzu9,
    /// ])));
    ///
    /// let expected = [
    ///     Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
    ///     Mentsu::Jantou(Tile::Manzu3, Tile::Manzu3),
    ///     Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
    ///     Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
    ///     Mentsu::Koutsu(Tile::Souzu9, Tile::Souzu9, Tile::Souzu9),
    /// ];
    ///
    /// assert_eq!(*hand.find_groups(), expected);
    /// ```
    ///
    /// ## Chiitoitsu
    ///
    /// ```text
    /// 224405m334455p11z
    /// ```
    ///
    /// ```rust
    /// use jikaze_riichi_core::mentsu::Mentsu;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu2,
    ///     Tile::Manzu2,
    ///     Tile::Manzu4,
    ///     Tile::Manzu4,
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu4,
    ///     Tile::Pinzu4,
    ///     Tile::Pinzu5,
    ///     Tile::Pinzu5,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    ///
    /// let expected = [
    ///     Mentsu::Jantou(Tile::Manzu2, Tile::Manzu2),
    ///     Mentsu::Jantou(Tile::Manzu4, Tile::Manzu4),
    ///     Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
    ///     Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
    ///     Mentsu::Jantou(Tile::Pinzu4, Tile::Pinzu4),
    ///     Mentsu::Jantou(Tile::Pinzu5, Tile::Pinzu5),
    ///     Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
    /// ];
    ///
    /// assert_eq!(*hand.find_groups(), expected);
    /// ```
    pub fn find_groups(&self) -> Box<[Mentsu]> {
        let mut result = Vec::new();

        let mut tile_set = self.tiles.clone();
        if let Some(tile) = &self.drawn_tile {
            // If a tile has been drawn from the wall, we need to add it to the tile set so that the
            // hand can be accurately parsed.
            // A "tsumo" win declaration is assumed to be the same here.
            tile_set.push(tile);
        } else if let Some(tile) = &self.rinshanpai {
            // If a tile has been drawn from the dead wall, we need to add it to the tile set so that
            // the hand can be accurately parsed.
            // A "tsumo" win declaration is assumed to be the same here.
            tile_set.push(tile);
        } else if let Some(WinDeclaration::Ron {
            deal_in_player: _,
            tile,
        }) = &self.win_declaration
        {
            tile_set.push(tile);
        }
        // Ensure the tiles are sorted for optimal binary searching.
        tile_set.sort();

        // We are mutating the tile set and shifting the elements left so this index doesn't need to change.
        let mut idx = 0;
        while idx < tile_set.as_ref().len() {
            let mut group_found = false;
            let tile = *tile_set.get(idx).unwrap();

            if tile_set.find_count(&tile) == 4 {
                // This shape is unusual and we have to check if this is a sequence and a triplet OR two sequences and a pair.

                // Ensure that the next tile isn't part of a sequence for something like `234444567m` where we don't want to count `456m` as the sequence.
                if tile.next_tile().has_next_tile() && tile.next_tile().next_tile().has_next_tile()
                {
                    // This is not part of a sequence that starts with this tile; the sequence doesn't connect with a different sequence like `234444567m` where the sequences are `234m` and `567m`.
                    let mut triplet_tiles = Vec::new();

                    // Remove the tiles from the `TileSet`.
                    for _ in 0..3 {
                        // While `swap_remove` would be faster, we need to preserve the order to make the binary search optimal for each iteration.
                        triplet_tiles.push(tile_set.remove(idx));
                    }

                    result.push(Mentsu::Koutsu(
                        *triplet_tiles.first().unwrap(),
                        *triplet_tiles.get(1).unwrap(),
                        *triplet_tiles.last().unwrap(),
                    ));

                    group_found = true;
                }
            }

            // Check for a triplet.
            if tile_set.find_count(&tile) == 3 {
                // Ensure that the next tile isn't part of a sequence for something like `44456m` where we don't want to count `444m` a triplet. We do this by checking if the next tile is part of a sequence like with `444567m`.
                // We need to also check to see that there aren't 3 triplets squentially such as `444555666m` where we _do_ want to count each as a triplet.
                if tile.has_next_tile()
                    && tile_set.find_count(&tile.next_tile()) >= 1
                    && tile_set.find_count(&tile.next_tile()) < 3
                    // Check if the tile after the next one exists.
                    && tile.next_tile().has_next_tile()
                    && tile_set.find_count(&tile.next_tile().next_tile()) >= 1
                    && tile_set.find_count(&tile.next_tile().next_tile()) < 3
                {
                    // The next two tiles exist and potentially connect with this tile to form a pair and a sequence.
                    // If the final tile in the next tile's potential sequence exists, this is a triplet and a sequence.
                    if tile.next_tile().next_tile().has_next_tile()
                        && tile_set.find_count(&tile.next_tile().next_tile().next_tile()) >= 1
                        && tile_set.find_count(&tile.next_tile().next_tile().next_tile()) < 3
                    {
                        // The current tile isn't a pair and a sequence.
                        let mut triplet_tiles = Vec::new();
                        for _ in 0..3 {
                            // While `swap_remove` would be faster, we need to preserve the order to make the binary search optimal for each iteration.
                            let tile_idx = match tile_set.binary_search(&tile) {
                                Ok(tile_idx) => tile_idx,
                                Err(_) => {
                                    // This tile must be a red 5.
                                    let tile = match tile {
                                        Tile::Manzu5 => Tile::ManzuRed5,
                                        Tile::Pinzu5 => Tile::PinzuRed5,
                                        Tile::Souzu5 => Tile::SouzuRed5,
                                        _ => tile,
                                    };
                                    tile_set.binary_search(&tile).unwrap()
                                }
                            };
                            triplet_tiles.push(tile_set.remove(tile_idx));
                        }

                        result.push(Mentsu::Koutsu(
                            *triplet_tiles.first().unwrap(),
                            *triplet_tiles.get(1).unwrap(),
                            *triplet_tiles.last().unwrap(),
                        ));
                        group_found = true;
                    }
                } else {
                    // Remove the tiles from the `TileSet`.
                    let mut triplet_tiles = Vec::new();
                    for _ in 0..3 {
                        // While `swap_remove` would be faster, we need to preserve the order to make the binary search optimal for each iteration.
                        let tile_idx = match tile_set.binary_search(&tile) {
                            Ok(tile_idx) => tile_idx,
                            Err(_) => {
                                // This tile must be a red 5.
                                let tile = match tile {
                                    Tile::Manzu5 => Tile::ManzuRed5,
                                    Tile::Pinzu5 => Tile::PinzuRed5,
                                    Tile::Souzu5 => Tile::SouzuRed5,
                                    _ => tile,
                                };
                                tile_set.binary_search(&tile).unwrap()
                            }
                        };
                        triplet_tiles.push(tile_set.remove(tile_idx));
                    }

                    result.push(Mentsu::Koutsu(
                        *triplet_tiles.first().unwrap(),
                        *triplet_tiles.get(1).unwrap(),
                        *triplet_tiles.last().unwrap(),
                    ));
                    group_found = true;
                }
            }

            // Check for the pair.
            if tile_set.find_count(&tile) == 2 {
                if tile.has_next_tile()
                    && tile_set.find_count(&tile.next_tile()) >= 1
                    && tile_set.find_count(&tile.next_tile()) < 3
                    // Check if the tile after the next one exists.
                    && tile.next_tile().has_next_tile()
                    && tile_set.find_count(&tile.next_tile().next_tile()) >= 1
                    && tile_set.find_count(&tile.next_tile().next_tile()) < 3
                {
                    // The next two tiles exist and potentially connect with this tile to form a pair and a sequence.
                    // If the final tile in the next tile's potential sequence exists, this is a triplet and a sequence.
                    if tile.next_tile().next_tile().has_next_tile()
                        && tile_set.find_count(&tile.next_tile().next_tile().next_tile()) >= 1
                        && tile_set.find_count(&tile.next_tile().next_tile().next_tile()) < 3
                    {
                        // We have a choice between two different sequences.

                        // Check for a mixed triple sequence and favor it if it's possible.
                        // `self.tiles` is used instead of `tile_set` because sequences may have been removed after finding matches.
                        match tile.suit() {
                            TileSuit::Manzu => {
                                let circle_tile = tile.pinzu_tile_equivalent().unwrap();
                                let has_no_matching_circle_sequence = !((self
                                    .tiles
                                    .find_count(&circle_tile)
                                    >= 1
                                    && self.tiles.find_count(&circle_tile) < 3)
                                    && (circle_tile.has_next_tile()
                                        && self.tiles.find_count(&circle_tile.next_tile()) >= 1
                                        && self.tiles.find_count(&circle_tile.next_tile()) < 3)
                                    && (circle_tile.next_tile().has_next_tile()
                                        && self
                                            .tiles
                                            .find_count(&circle_tile.next_tile().next_tile())
                                            >= 1
                                        && self
                                            .tiles
                                            .find_count(&circle_tile.next_tile().next_tile())
                                            < 3));
                                let bamboo_tile = tile.souzu_tile_equivalent().unwrap();
                                let has_no_matching_bamboo_sequence = !((self
                                    .tiles
                                    .find_count(&bamboo_tile)
                                    >= 1
                                    && self.tiles.find_count(&bamboo_tile) < 3)
                                    && (bamboo_tile.has_next_tile()
                                        && self.tiles.find_count(&bamboo_tile.next_tile()) >= 1
                                        && self.tiles.find_count(&bamboo_tile.next_tile()) < 3)
                                    && (bamboo_tile.next_tile().has_next_tile()
                                        && self
                                            .tiles
                                            .find_count(&bamboo_tile.next_tile().next_tile())
                                            >= 1
                                        && self
                                            .tiles
                                            .find_count(&bamboo_tile.next_tile().next_tile())
                                            < 3));

                                if has_no_matching_circle_sequence
                                    || has_no_matching_bamboo_sequence
                                {
                                    // This can be counted as a pair.
                                    let mut pair_tiles = Vec::new();
                                    for _ in 0..2 {
                                        let tile_idx = match tile_set.binary_search(&tile) {
                                            Ok(tile_idx) => tile_idx,
                                            Err(_) => {
                                                // This tile must be a red 5.
                                                let tile = match tile {
                                                    Tile::Manzu5 => Tile::ManzuRed5,
                                                    _ => tile,
                                                };
                                                tile_set.binary_search(&tile).unwrap()
                                            }
                                        };
                                        pair_tiles.push(tile_set.remove(tile_idx));
                                    }

                                    result.push(Mentsu::Jantou(
                                        *pair_tiles.first().unwrap(),
                                        *pair_tiles.last().unwrap(),
                                    ));
                                    group_found = true;
                                }
                            }
                            TileSuit::Pinzu => {
                                let character_tile = tile.manzu_tile_equivalent().unwrap();
                                let has_no_matching_character_sequence =
                                    !((self.tiles.find_count(&character_tile) >= 1
                                        && self.tiles.find_count(&character_tile) < 3)
                                        && (character_tile.has_next_tile()
                                            && self.tiles.find_count(&character_tile.next_tile())
                                                >= 1
                                            && self.tiles.find_count(&character_tile.next_tile())
                                                < 3)
                                        && (character_tile.next_tile().has_next_tile()
                                            && self.tiles.find_count(
                                                &character_tile.next_tile().next_tile(),
                                            ) >= 1
                                            && self.tiles.find_count(
                                                &character_tile.next_tile().next_tile(),
                                            ) < 3));

                                let bamboo_tile = tile.souzu_tile_equivalent().unwrap();
                                let has_no_matching_bamboo_sequence = !((self
                                    .tiles
                                    .find_count(&bamboo_tile)
                                    >= 1
                                    && self.tiles.find_count(&bamboo_tile) < 3)
                                    && (bamboo_tile.has_next_tile()
                                        && self.tiles.find_count(&bamboo_tile.next_tile()) >= 1
                                        && self.tiles.find_count(&bamboo_tile.next_tile()) < 3)
                                    && (bamboo_tile.next_tile().has_next_tile()
                                        && self
                                            .tiles
                                            .find_count(&bamboo_tile.next_tile().next_tile())
                                            >= 1
                                        && self
                                            .tiles
                                            .find_count(&bamboo_tile.next_tile().next_tile())
                                            < 3));

                                if has_no_matching_character_sequence
                                    || has_no_matching_bamboo_sequence
                                {
                                    // This can be counted as a pair.
                                    let mut pair_tiles = Vec::new();
                                    for _ in 0..2 {
                                        let tile_idx = match tile_set.binary_search(&tile) {
                                            Ok(tile_idx) => tile_idx,
                                            Err(_) => {
                                                // This tile must be a red 5.
                                                let tile = match tile {
                                                    Tile::Pinzu5 => Tile::PinzuRed5,
                                                    _ => tile,
                                                };
                                                tile_set.binary_search(&tile).unwrap()
                                            }
                                        };
                                        pair_tiles.push(tile_set.remove(tile_idx));
                                    }

                                    result.push(Mentsu::Jantou(
                                        *pair_tiles.first().unwrap(),
                                        *pair_tiles.last().unwrap(),
                                    ));
                                    group_found = true;
                                }
                            }
                            TileSuit::Souzu => {
                                let character_tile = tile.manzu_tile_equivalent().unwrap();
                                let has_no_matching_character_sequence =
                                    !((self.tiles.find_count(&character_tile) >= 1
                                        && self.tiles.find_count(&character_tile) < 3)
                                        && (character_tile.has_next_tile()
                                            && self.tiles.find_count(&character_tile.next_tile())
                                                >= 1
                                            && self.tiles.find_count(&character_tile.next_tile())
                                                < 3)
                                        && (character_tile.next_tile().has_next_tile()
                                            && self.tiles.find_count(
                                                &character_tile.next_tile().next_tile(),
                                            ) >= 1
                                            && self.tiles.find_count(
                                                &character_tile.next_tile().next_tile(),
                                            ) < 3));
                                let circle_tile = tile.pinzu_tile_equivalent().unwrap();
                                let has_no_matching_circle_sequence = !((self
                                    .tiles
                                    .find_count(&circle_tile)
                                    >= 1
                                    && self.tiles.find_count(&circle_tile) < 3)
                                    && (circle_tile.has_next_tile()
                                        && self.tiles.find_count(&circle_tile.next_tile()) >= 1
                                        && self.tiles.find_count(&circle_tile.next_tile()) < 3)
                                    && (circle_tile.next_tile().has_next_tile()
                                        && self
                                            .tiles
                                            .find_count(&circle_tile.next_tile().next_tile())
                                            >= 1
                                        && self
                                            .tiles
                                            .find_count(&circle_tile.next_tile().next_tile())
                                            < 3));

                                if has_no_matching_character_sequence
                                    || has_no_matching_circle_sequence
                                {
                                    // This can be counted as a pair.
                                    let mut pair_tiles = Vec::new();
                                    for _ in 0..2 {
                                        let tile_idx = match tile_set.binary_search(&tile) {
                                            Ok(tile_idx) => tile_idx,
                                            Err(_) => {
                                                // This tile must be a red 5.
                                                let tile = match tile {
                                                    Tile::Souzu5 => Tile::SouzuRed5,
                                                    _ => tile,
                                                };
                                                tile_set.binary_search(&tile).unwrap()
                                            }
                                        };
                                        pair_tiles.push(tile_set.remove(tile_idx));
                                    }

                                    result.push(Mentsu::Jantou(
                                        *pair_tiles.first().unwrap(),
                                        *pair_tiles.last().unwrap(),
                                    ));
                                    group_found = true;
                                }
                            }
                            // Honor tiles cannot be part of a sequence.
                            _ => {
                                let mut pair_tiles = Vec::new();
                                for _ in 0..2 {
                                    pair_tiles.push(tile_set.remove(idx));
                                }

                                result.push(Mentsu::Jantou(
                                    *pair_tiles.first().unwrap(),
                                    *pair_tiles.last().unwrap(),
                                ));
                                group_found = true;
                            }
                        }
                    }
                } else {
                    // This tile cannot be part of a sequence.
                    let mut pair_tiles = Vec::new();
                    for _ in 0..2 {
                        pair_tiles.push(tile_set.remove(idx));
                    }

                    result.push(Mentsu::Jantou(
                        *pair_tiles.first().unwrap(),
                        *pair_tiles.last().unwrap(),
                    ));
                    group_found = true;
                }
            }

            // Check for a sequence.
            if tile.has_next_tile()
                && tile_set.find_count(&tile.next_tile()) > 0
                && tile.next_tile().has_next_tile()
            {
                let t2 = tile.next_tile();
                let t3 = t2.next_tile();

                let mut sequence_tiles = Vec::new();

                if tile_set.find_count(&tile) >= 1
                    && tile_set.find_count(&t2) >= 1
                    && tile_set.find_count(&t3) >= 1
                {
                    // Remove the tiles from the `TileSet`.
                    // We know that the tile set has this tile so we can unwrap safely.
                    // While `swap_remove` would be faster, we need to preserve the order to make the binary search optimal for each iteration.
                    sequence_tiles.push(tile_set.remove(idx));

                    let tile_idx = match tile_set.binary_search(&t2) {
                        Ok(tile_idx) => tile_idx,
                        Err(_) => {
                            // This tile must be a red 5.
                            let t2 = match t2 {
                                Tile::Manzu5 => Tile::ManzuRed5,
                                Tile::Pinzu5 => Tile::PinzuRed5,
                                Tile::Souzu5 => Tile::SouzuRed5,
                                _ => t2,
                            };
                            tile_set.binary_search(&t2).unwrap()
                        }
                    };
                    sequence_tiles.push(tile_set.remove(tile_idx));

                    let tile_idx = match tile_set.binary_search(&t3) {
                        Ok(tile_idx) => tile_idx,
                        Err(_) => {
                            // This tile must be a red 5.
                            let t3 = match t3 {
                                Tile::Manzu5 => Tile::ManzuRed5,
                                Tile::Pinzu5 => Tile::PinzuRed5,
                                Tile::Souzu5 => Tile::SouzuRed5,
                                _ => t3,
                            };
                            tile_set.binary_search(&t3).unwrap()
                        }
                    };
                    sequence_tiles.push(tile_set.remove(tile_idx));

                    result.push(Mentsu::Shuntsu(
                        *sequence_tiles.first().unwrap(),
                        *sequence_tiles.get(1).unwrap(),
                        *sequence_tiles.last().unwrap(),
                    ));
                    group_found = true;
                }
            }

            if !group_found {
                // This tile does not belong to any group -- move onto the next tile.
                idx += 1;
            }
        }

        // Check if there is a chiitoitsu potential when iipeikou is present.
        {
            let sequences: Vec<Mentsu> = result
                .iter()
                .filter_map(|group| {
                    if matches!(group, Mentsu::Shuntsu(..)) {
                        Some(*group)
                    } else {
                        None
                    }
                })
                .collect();
            if result
                .iter()
                .filter(|group| matches!(group, Mentsu::Jantou(..)))
                .count()
                == 4
                && sequences.len() == 2
            {
                let first_sequence = sequences.first().unwrap();
                let second_sequence = sequences.last().unwrap();
                if first_sequence == second_sequence {
                    let mut new_result: Vec<Mentsu> = result
                        .iter()
                        .filter_map(|group| {
                            if !matches!(group, Mentsu::Shuntsu(..)) {
                                Some(*group)
                            } else {
                                None
                            }
                        })
                        .collect();

                    // These if-statements are just boilerplate but are necessary to deconstruct the items within.
                    if let Mentsu::Shuntsu(s1t1, s1t2, s1t3) = first_sequence {
                        if let Mentsu::Shuntsu(s2t1, s2t2, s2t3) = second_sequence {
                            new_result.push(Mentsu::Jantou(*s1t1, *s2t1));
                            new_result.push(Mentsu::Jantou(*s1t2, *s2t2));
                            new_result.push(Mentsu::Jantou(*s1t3, *s2t3));

                            result = new_result;
                            result.sort();
                        }
                    }
                }
            }
        }

        result.into()
    }

    /// Get a list of fu (minipoints).
    ///
    /// # Panics
    ///
    /// This could panic if there is a bug with how the number of melds are added to the number of groups.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use jikaze_riichi_core::fu::{Fu,FuMentsuTileClass};
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::mentsu::Mentsu;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::{PlayerRoundState, Riichi};
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::score::HandScore;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::win_declaration::WinDeclaration;
    /// use jikaze_riichi_core::yaku::{Yaku, Yakuhai};
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Pinzu1,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu4,
    ///     Tile::Pinzu5,
    ///     Tile::Pinzu6,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindSouth,
    ///     Tile::WindSouth,
    ///     Tile::WindSouth,
    ///     Tile::DragonGreen,
    /// ])));
    /// *hand.groups_mut() = Box::new([
    ///     // Sequence: 0 fu
    ///     Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
    ///     // Sequence: 0 fu
    ///     Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
    ///     // Sequence: 0 fu
    ///     Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
    ///     // Triplet, honor, concealed: +8 fu
    ///     Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
    ///     // Pair, value honor: +2 fu
    ///     // Pair wait: +2 fu
    ///     Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
    /// ]);
    /// *hand.win_declaration_mut() = Some(WinDeclaration::Ron {
    ///     deal_in_player: Wind::South,
    ///     tile: Tile::DragonGreen,
    /// });
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    /// let mut round_context = RoundContext::new(wall, Wind::East, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    /// let mut player_round_state = PlayerRoundState::new(Wind::South);
    /// *player_round_state.riichi_mut() = Some(Riichi::Riichi);
    /// *player_round_state.ippatsu_mut() = true;
    ///
    /// let yaku = [
    ///     Yaku::Riichi,
    ///     Yaku::Ippatsu,
    ///     Yaku::Yakuhai(Yakuhai::SeatWind),
    /// ];
    ///
    /// // The fu comes from (42->50):
    /// //   - Winning: +20
    /// //   - Concealed ron: +10
    /// //   - Triplet, honor, concealed: +8
    /// //   - Pair, value honor: +2
    /// //   - Pair wait: +2
    /// let expected = [
    ///     Fu::Winning,
    ///     Fu::ConcealedRon,
    ///     Fu::Ankou(FuMentsuTileClass::RoutouhaiJihai),
    ///     Fu::Yakuhai,
    ///     Fu::Tanki,
    /// ];
    ///
    /// assert_eq!(
    ///     *hand.fu(&round_context, &player_round_state, &yaku),
    ///     expected
    /// );
    /// ```
    pub fn fu(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
        yaku: &[Yaku],
    ) -> Box<[Fu]> {
        let mut fu = Vec::new();

        if yaku.contains(&Yaku::Chiitoitsu) {
            // No more fu is given for chiitoitsu.
            fu.push(Fu::Chiitoitsu);
        } else if let Some(win_declaration) = self.win_declaration {
            // Winning provides a base of 20 fu.
            fu.push(Fu::Winning);

            // Winning Points.
            {
                match win_declaration {
                    WinDeclaration::Ron { tile, .. } => {
                        match self.hand_visibility() {
                            HandVisibility::Concealed => fu.push(Fu::ConcealedRon),
                            HandVisibility::Open => {
                                // Create a temporary `Hand` to check for pinfu.
                                let mut temp_hand = self.clone();
                                *temp_hand.win_declaration_mut() =
                                    Some(WinDeclaration::Tsumo(tile));
                                if temp_hand.has_pinfu(round_context, player_round_state) {
                                    fu.push(Fu::OpenPinfu);
                                }
                            }
                        }
                    }
                    WinDeclaration::Tsumo(_) => {
                        // The fu is provided only if pinfu was _not_ awarded, in which case 1 han is given instead.
                        if !yaku.contains(&Yaku::Pinfu) {
                            fu.push(Fu::Tsumo);
                        }
                    }
                }
            }

            // Group Points.
            {
                for group in self.groups.iter() {
                    match group {
                        Mentsu::Jantou(tile, ..) => {
                            if *tile == round_context.wind().as_tile()
                                && *tile == player_round_state.wind().as_tile()
                            {
                                fu.push(Fu::BakazeJikaze);
                            } else if matches!(tile.suit(), TileSuit::Dragon)
                                || (*tile == round_context.wind().as_tile()
                                    || *tile == player_round_state.wind().as_tile())
                            {
                                fu.push(Fu::Yakuhai);
                            }
                        }
                        Mentsu::Shuntsu(..) => {}
                        Mentsu::Koutsu(tile, ..) => {
                            if tile.is_terminal() || tile.is_honor() {
                                match &win_declaration {
                                    WinDeclaration::Ron {
                                        tile: claimed_tile, ..
                                    } => {
                                        if claimed_tile == tile {
                                            fu.push(Fu::Minkou(FuMentsuTileClass::RoutouhaiJihai));
                                        } else {
                                            fu.push(Fu::Ankou(FuMentsuTileClass::RoutouhaiJihai));
                                        }
                                    }
                                    WinDeclaration::Tsumo(_) => {
                                        fu.push(Fu::Ankou(FuMentsuTileClass::RoutouhaiJihai))
                                    }
                                }
                            } else {
                                // 2-8 tile.
                                match &win_declaration {
                                    WinDeclaration::Ron {
                                        tile: claimed_tile, ..
                                    } => {
                                        if claimed_tile == tile {
                                            fu.push(Fu::Minkou(FuMentsuTileClass::Tanyaohai));
                                        } else {
                                            fu.push(Fu::Ankou(FuMentsuTileClass::Tanyaohai));
                                        }
                                    }
                                    WinDeclaration::Tsumo(_) => {
                                        fu.push(Fu::Ankou(FuMentsuTileClass::Tanyaohai))
                                    }
                                }
                            }
                        }
                    }
                }

                for meld in self.melds.iter() {
                    match meld {
                        Meld::Chii { .. } => {}
                        Meld::Pon { claimed_tile, .. } => {
                            fu.push(Fu::Minkou(
                                if claimed_tile.is_terminal() || claimed_tile.is_honor() {
                                    FuMentsuTileClass::RoutouhaiJihai
                                } else {
                                    FuMentsuTileClass::Tanyaohai
                                },
                            ));
                        }
                        Meld::Kan(KanType::Ankan { tiles }) => {
                            let tile = tiles.first().unwrap();
                            fu.push(Fu::Ankan(if tile.is_terminal() || tile.is_honor() {
                                FuMentsuTileClass::RoutouhaiJihai
                            } else {
                                FuMentsuTileClass::Tanyaohai
                            }));
                        }
                        Meld::Kan(KanType::Daiminkan { claimed_tile, .. })
                        | Meld::Kan(KanType::Shominkan { claimed_tile, .. }) => {
                            fu.push(Fu::Minkan(
                                if claimed_tile.is_terminal() || claimed_tile.is_honor() {
                                    FuMentsuTileClass::RoutouhaiJihai
                                } else {
                                    FuMentsuTileClass::Tanyaohai
                                },
                            ));
                        }
                    }
                }
            }

            // Wait Points.
            {
                // Create a temporary `Hand` to check for the number of shapes.
                let mut temp_hand = self.clone();
                *temp_hand.win_declaration_mut() = None;
                *temp_hand.groups_mut() = temp_hand.find_groups();

                let mut num_groups: u8 = 0;
                for group in temp_hand.groups() {
                    match group {
                        Mentsu::Jantou(..) => {}
                        Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => num_groups += 1,
                    }
                }
                num_groups +=
                    core::convert::TryInto::<u8>::try_into(temp_hand.melds().len()).unwrap();

                let waits = self.get_waits(round_context);
                if num_groups == 4 || num_groups == 3 {
                    fu.push(Fu::Tanki);
                } else if waits.len() == 1 {
                    if matches!(
                        waits.first().unwrap(),
                        Tile::Manzu3
                            | Tile::Manzu7
                            | Tile::Pinzu3
                            | Tile::Pinzu7
                            | Tile::Souzu3
                            | Tile::Souzu7
                    ) {
                        // This isn't always "penchan" because `24` and `68` also need these tiles.
                        fu.push(Fu::Penchan);
                    } else {
                        fu.push(Fu::Kanchan);
                    }
                }
            }
        }

        fu.into()
    }

    /// Get the tiles needed to complete the hand, assuming tenpai.
    ///
    /// This does not check to see if each tile is able to win the round (i.e. no valid yaku).
    ///
    /// Also, this does not count the waits (ukeire 「受け入れ」) so winning may be impossible if one is
    /// not careful due to all potential winning tiles having already been used in the player's
    /// hand, a discard pool, a called meld, or visible as dora indicators.
    /// This is known as "Karaten" 「受け入れ」.
    ///
    /// # Basic Wait Patterns
    ///
    /// - [Kanchan 「嵌張」][kanchan]: Closed wait, middle wait.
    /// - [Ryanmen 「両面」][ryanmen]: Open wait.
    /// - [Penchan Machi 「辺張待ち」][penchan]: Edge wait.
    /// - [Shanpon 「双ポン」][shanpon]: Dual paired.
    /// - [Tanki 「単騎」][tanki]: Pair wait.
    ///
    /// # Named Combinations
    ///
    /// There are additional complex patterns that do not have specific names and are combined forms of other machi (wait patterns).
    ///
    /// - Aryanmen 「亜両面」: Variation of the [ryanmen wait][ryanmen] where two tiles completing the sequence are next to a pair of one of the waiting tiles.
    ///     - Example: `5567m`, waiting for `5m` or `8m`.
    /// - [Enotsu 「エントツ」](#entotsu-エントツ): Combines [ryanmen] and [shanpon].
    /// - Happoubijin 「八方美人」: A wait on 8 consecutive tiles.
    ///     - Example: `3334567888p`, waiting for `2p`, `3p`, `4p`, `5p`, `6p`, `7p`, `8p`, `9p`.
    /// - Kantan 「嵌単」: Combination of [kanchan] and [tanki].
    ///     - Example: `5557s`, waiting for `6s` or `7s`.
    /// - KantanKan 「嵌単嵌」: Extension of kantan with a 3-sided wait.
    ///     - Example: `3335777p`, waiting for `4p`, `5p`, or `6p`.
    /// - [Nobetan 「ノベタン」](#nobetan-ノベタン--nobetanki-延べ単騎): Double pair.
    /// - Pentan 「ペンタン」: Combination of [penchan] and [tanki].
    ///     - Example: `8889p`, waiting for `7p` or `9p`.
    /// - [Ryantan 「リャンタン」](#ryantan-リャンタン): Combination of [ryanmen wait][ryanmen] and [tanki wait][tanki].
    ///     - Example: `2333m`, waiting for `1m`, `2m`, or `4m`.
    /// - [Sanmenchan 「三面張」](#sanmenchan-三面張): 3-sided wait.
    /// - Tatsumaki 「竜巻」: Combination of [ryanmen], [shanpon], and [tanki], waiting on 5 consecutive tiles in the same suit.
    ///     - Example: `4445666m`, waiting for `3m`, `4m`, `5m`, `6m`, `7m`.
    ///
    /// # Examples
    ///
    /// This is a non-exhaustive list of examples.
    ///
    /// ## Chuuren Poutou
    ///
    /// ### Kyuumen Machi
    ///
    /// Nine-way wait.
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu1,
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Manzu4,
    ///     Tile::Manzu5,
    ///     Tile::Manzu6,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Manzu9,
    ///     Tile::Manzu9,
    ///     Tile::Manzu9,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(
    ///     *hand.get_waits(&round_context),
    ///     [
    ///         Tile::Manzu1,
    ///         Tile::Manzu2,
    ///         Tile::Manzu3,
    ///         Tile::Manzu4,
    ///         Tile::Manzu5,
    ///         Tile::Manzu6,
    ///         Tile::Manzu7,
    ///         Tile::Manzu8,
    ///         Tile::Manzu9,
    ///     ]
    /// );
    /// ```
    ///
    /// ## Kanchan Wait
    ///
    /// Wait pattern that completes with the middle number of a sequence.
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu3,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Pinzu8,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Manzu2]);
    /// ```
    ///
    /// ## Kokushi Musou
    ///
    /// ### Single Wait
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu9,
    ///     Tile::Pinzu1,
    ///     Tile::Pinzu9,
    ///     Tile::Souzu1,
    ///     Tile::Souzu9,
    ///     Tile::WindEast,
    ///     Tile::WindSouth,
    ///     Tile::WindWest,
    ///     Tile::WindNorth,
    ///     Tile::DragonWhite,
    ///     Tile::DragonGreen,
    ///     Tile::DragonGreen,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::DragonRed]);
    /// ```
    ///
    /// ### Thirteen-Way Wait
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu9,
    ///     Tile::Pinzu1,
    ///     Tile::Pinzu9,
    ///     Tile::Souzu1,
    ///     Tile::Souzu9,
    ///     Tile::WindEast,
    ///     Tile::WindSouth,
    ///     Tile::WindWest,
    ///     Tile::WindNorth,
    ///     Tile::DragonWhite,
    ///     Tile::DragonGreen,
    ///     Tile::DragonRed,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(
    ///     *hand.get_waits(&round_context),
    ///     [
    ///         Tile::Manzu1,
    ///         Tile::Manzu9,
    ///         Tile::Pinzu1,
    ///         Tile::Pinzu9,
    ///         Tile::Souzu1,
    ///         Tile::Souzu9,
    ///         Tile::WindEast,
    ///         Tile::WindSouth,
    ///         Tile::WindWest,
    ///         Tile::WindNorth,
    ///         Tile::DragonWhite,
    ///         Tile::DragonGreen,
    ///         Tile::DragonRed,
    ///     ]
    /// );
    /// ```
    ///
    /// ## Penchan Wait
    ///
    /// Wait pattern containing the tiles 1 and 2 of the same suit, or 8 and 9 of the same suit.
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Pinzu8,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Manzu3]);
    /// ```
    ///
    /// ## Ryanmen Wait
    ///
    /// Open wait involving two consecutively numbered tiles, waiting on the outside number.
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Pinzu8,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Manzu1, Tile::Manzu4]);
    /// ```
    ///
    /// ### Shanpon Wait
    ///
    /// Wait pattern involving two of pairs, where either is looking to complete a triplet.
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Pinzu8,
    ///     Tile::Pinzu9,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Manzu6, Tile::WindWest]);
    /// ```
    ///
    /// ## Sanmenchan 「三面張」
    ///
    /// ### Standard Sanmen
    ///
    /// Three-sided wait involving a combination of two [ryanmen (open wait)][ryanmen].
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Pinzu4,
    ///     Tile::Pinzu5,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Pinzu8,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Pinzu3, Tile::Pinzu6, Tile::Pinzu9]);
    /// ```
    ///
    /// ### Entotsu 「エントツ」
    ///
    /// Wait pattern that combines the [shanpon wait][shanpon] and a [ryanmen (open wait)][ryanmen].
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Pinzu4,
    ///     Tile::Pinzu5,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Pinzu3, Tile::Pinzu6, Tile::WindWest]);
    /// ```
    ///
    /// ### Ryantan 「リャンタン」
    ///
    /// A contraction of "[ryanmen]" and "[tanki]".
    /// This is composed of a triplet of a middle-numbered tile with one more tile adjacent to it.
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Pinzu5,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    ///     Tile::WindWest,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu7]);
    /// ```
    ///
    /// ### Sanmentan 「三面ノベタン」
    ///
    /// See [Sanmen Nobetan 「三面ノベタン」](#sanmen-nobetan-三面ノベタン).
    ///
    /// ## Tanki Wait
    ///
    /// Wait pattern needing a tile to finish the pair, commonly referred to as the "pair wait".
    ///
    /// ### Common Pattern
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Manzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Pinzu8,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::WindWest,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::WindWest]);
    /// ```
    ///
    /// ### Hadaka Tanki 「裸単騎」
    ///
    /// A hand in this state has called for four open melds. Only one tile is left closed in the hand.
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::meld::Meld;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([Tile::WindWest])));
    /// let mut melds = hand.melds_mut();
    /// melds.push(Meld::Pon {
    ///     claimed_tile: Tile::Manzu1,
    ///     relative_player: PlayerRelativePosition::Toimen,
    ///     tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
    /// });
    /// melds.push(Meld::Pon {
    ///     claimed_tile: Tile::Manzu6,
    ///     relative_player: PlayerRelativePosition::Shimocha,
    ///     tiles: [Tile::Manzu6, Tile::Manzu6, Tile::Manzu6],
    /// });
    /// melds.push(Meld::Pon {
    ///     claimed_tile: Tile::Pinzu8,
    ///     relative_player: PlayerRelativePosition::Kamicha,
    ///     tiles: [Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8],
    /// });
    /// melds.push(Meld::Pon {
    ///     claimed_tile: Tile::Souzu5,
    ///     relative_player: PlayerRelativePosition::Toimen,
    ///     tiles: [Tile::Souzu5, Tile::Souzu5, Tile::Souzu5],
    /// });
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::WindWest]);
    /// ```
    ///
    /// ### Nakabukure 「中膨れ」
    ///
    /// Wait pattern where the middle tile is duplicated in a sequence and an additional copy of it is needed.
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Pinzu5,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::Souzu8,
    ///     Tile::Souzu8,
    ///     Tile::Souzu8,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Pinzu6]);
    /// ```
    ///
    /// ### Nobetan 「ノベタン」 / Nobetanki 「延べ単騎」
    ///
    /// Wait pattern that has two separate [tanki waits (pair waits)][tanki].
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu1,
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Pinzu5,
    ///     Tile::Pinzu6,
    ///     Tile::Pinzu7,
    ///     Tile::Pinzu8,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::Souzu8,
    ///     Tile::Souzu8,
    ///     Tile::Souzu8,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Pinzu5, Tile::Pinzu8]);
    /// ```
    ///
    /// ### Sanmen Nobetan 「三面ノベタン」
    ///
    /// Wait pattern that has three separate [tanki waits (pair waits)][tanki].
    /// This is an extension to the [nobetan pattern](#nobetan-ノベタン--nobetanki-延べ単騎).
    ///
    /// ```rust
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Manzu2,
    ///     Tile::Manzu3,
    ///     Tile::Manzu4,
    ///     Tile::Manzu5,
    ///     Tile::Manzu6,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Souzu4,
    ///     Tile::Souzu5,
    ///     Tile::Souzu6,
    ///     Tile::Souzu8,
    ///     Tile::Souzu8,
    ///     Tile::Souzu8,
    /// ])));
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let mut round_context = RoundContext::new(wall, Wind::South, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    ///
    /// assert_eq!(*hand.get_waits(&round_context), [Tile::Manzu2, Tile::Manzu5, Tile::Manzu8]);
    /// ```
    ///
    /// [kanchan]: #kanchan-wait
    /// [penchan]: #penchan-wait
    /// [ryanmen]: #ryanmen-wait
    /// [shanpon]: #shanpon-wait
    /// [tanki]: #tanki-wait
    pub fn get_waits(&self, round_context: &RoundContext) -> Box<[Tile]> {
        let mut waits = Vec::new();

        let mut temp_hand = Hand::new(self.tiles.clone());
        temp_hand.melds.clone_from(&self.melds);
        // While this has a time complexity of O(n), there are only 34 tiles to iterate through.
        for tile in UNIQUE_TILES_4P {
            temp_hand.drawn_tile = Some(tile);

            if temp_hand.has_chiitoitsu()
                || temp_hand.has_chuuren_poutou()
                || temp_hand.has_kokushi_musou(round_context)
            {
                waits.push(tile);
            } else {
                let groups = temp_hand.find_groups();
                let mut num_completed_shapes = 0;
                let mut num_pairs = 0;

                for group in groups.iter() {
                    match group {
                        Mentsu::Jantou(..) => num_pairs += 1,
                        _ => num_completed_shapes += 1,
                    }
                }
                for _ in &temp_hand.melds {
                    num_completed_shapes += 1;
                }

                if num_completed_shapes == 4 && num_pairs == 1 {
                    waits.push(tile);
                }
            }
        }

        waits.into()
    }

    /// Get the groups/sets in the hand.
    pub const fn groups(&self) -> &[Mentsu] {
        &self.groups
    }

    /// Get a mutable reference to the groups/sets in the hand.
    pub fn groups_mut(&mut self) -> &mut Box<[Mentsu]> {
        &mut self.groups
    }

    /// Get the hand's visibility state.
    pub fn hand_visibility(&self) -> HandVisibility {
        for meld in &self.melds {
            match meld {
                Meld::Chii { .. }
                | Meld::Pon { .. }
                | Meld::Kan(KanType::Daiminkan { .. })
                | Meld::Kan(KanType::Shominkan { .. }) => return HandVisibility::Open,
                Meld::Kan(KanType::Ankan { .. }) => continue,
            }
        }

        HandVisibility::Concealed
    }

    /// Calculate if the hand has the chankan, or robbing a kan, yaku.
    ///
    /// See [Yaku::Chankan] for more information about this yaku.
    pub fn has_chankan(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> bool {
        match round_context.last_kan() {
            Some(last_kan) => {
                // The current player cannot claim chankan from themself.
                if last_kan.player() == player_round_state.wind() {
                    return false;
                }

                match last_kan.kan() {
                    KanType::Shominkan { claimed_tile, .. } => {
                        let mut temp_hand = Hand::new(self.tiles.clone());
                        temp_hand.tiles.push(claimed_tile);
                        temp_hand.groups = temp_hand.find_groups();

                        let mut num_groups = 0;
                        let mut num_pairs = 0;

                        for group in temp_hand.groups.iter() {
                            match group {
                                Mentsu::Jantou(..) => num_pairs += 1,
                                Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => num_groups += 1,
                            }
                        }

                        for _ in &self.melds {
                            num_groups += 1;
                        }

                        num_groups == 4 && num_pairs == 1
                    }
                    // A player cannot win from a daiminkan because they would be in furiten
                    // by not claiming a win on the last discard.
                    KanType::Ankan { .. } | KanType::Daiminkan { .. } => false,
                }
            }
            None => false,
        }
    }

    /// Calculate if the hand has a chanta, or half outside hand, yaku.
    ///
    /// See [Yaku::Chanta] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This can panic when checking the meld tiles.
    /// This should not happen unless there is a bug in this function.
    pub fn has_chanta(&self) -> bool {
        let mut has_sequence = false;
        let mut has_honor = false;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(tile, _) | Mentsu::Koutsu(tile, _, _) => {
                    if !tile.is_honor() && !tile.is_terminal() {
                        return false;
                    }

                    if tile.is_honor() {
                        has_honor = true;
                    }
                }
                Mentsu::Shuntsu(tile1, _, tile3) => {
                    has_sequence = true;

                    if !tile1.is_terminal() && !tile3.is_terminal() {
                        return false;
                    }
                }
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii { tiles, .. } => {
                    has_sequence = true;

                    if !tiles.first().unwrap().is_terminal()
                        && !tiles.get(1).unwrap().is_terminal()
                        && !tiles.last().unwrap().is_terminal()
                    {
                        return false;
                    }
                }
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        let tile1 = tiles.first().unwrap();
                        if !tile1.is_honor() && !tile1.is_terminal() {
                            return false;
                        }

                        if tile1.is_honor() {
                            has_honor = true;
                        }
                    }
                },
                Meld::Pon { claimed_tile, .. } => {
                    if !claimed_tile.is_honor() && !claimed_tile.is_terminal() {
                        return false;
                    }

                    if claimed_tile.is_honor() {
                        has_honor = true;
                    }
                }
            }
        }

        has_sequence && has_honor
    }

    /// Calculate if the hand has a chiihou, or blessing of earth, yaku.
    ///
    /// See [Yaku::Chiihou] for more information about this yaku.
    pub fn has_chiihou(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> bool {
        if !round_context.first_uninterrupted_turn()
            || matches!(player_round_state.wind(), Wind::East)
            || self.drawn_tile.is_none()
        {
            return false;
        }

        if self.has_kokushi_musou(round_context) {
            return true;
        }

        let mut num_groups = 0;
        let mut num_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(..) => num_pairs += 1,
                Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => num_groups += 1,
            }
        }

        (num_groups == 4 && num_pairs == 1) || num_pairs == 7
    }

    /// Calculate if the winning hand has a chiitoitsu, or seven pairs, yaku.
    ///
    /// See [Yaku::Chiitoitsu] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// Will panic if there is a bug with how the tiles are compared with each other in the hand (out of index bounds).
    pub fn has_chiitoitsu(&self) -> bool {
        let mut pair_types = Vec::new();

        let mut tiles = self.tiles.clone();
        if let Some(drawn_tile) = self.drawn_tile {
            tiles.push(&drawn_tile);
        } else if let Some(win_declaration) = &self.win_declaration {
            match win_declaration {
                WinDeclaration::Ron { tile, .. } | WinDeclaration::Tsumo(tile) => tiles.push(tile),
            }
        }

        if !self.melds.is_empty() || tiles.len() < 14 {
            return false;
        }
        // We need to ensure the hand is sorted for the logic below.
        tiles.sort();
        let tiles = tiles;

        // This is assuming that the hand is sorted.
        for i in (0..(tiles.as_ref().len())).step_by(2) {
            if tiles.as_ref().get(i).unwrap() == tiles.as_ref().get(i + 1).unwrap() {
                // Check that all of the pairs are unique.
                if pair_types.contains(tiles.get(i).unwrap()) {
                    return false;
                }

                pair_types.push(*tiles.get(i).unwrap())
            } else {
                return false;
            }
        }

        true
    }

    /// Calculate if the hand has the chinitsu, or full flush, yaku
    ///
    /// See [Yaku::Chinitsu] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This can panic while checking the tiles in any kan meld.
    /// This should not happen unless there is a bug in this function.
    pub fn has_chinitsu(&self) -> bool {
        let tile_suit = self.tiles.first().unwrap().suit();
        let mut num_groups = 0;
        let mut num_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(tile, _) => {
                    if tile.suit() != tile_suit {
                        return false;
                    }
                    num_pairs += 1;
                }
                Mentsu::Shuntsu(tile, ..) | Mentsu::Koutsu(tile, ..) => {
                    if tile.suit() != tile_suit {
                        return false;
                    }

                    num_groups += 1;
                }
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii { claimed_tile, .. } | Meld::Pon { claimed_tile, .. } => {
                    if claimed_tile.suit() != tile_suit {
                        return false;
                    }

                    num_groups += 1;
                }
                Meld::Kan(KanType::Ankan { tiles })
                | Meld::Kan(KanType::Daiminkan { tiles, .. })
                | Meld::Kan(KanType::Shominkan { tiles, .. }) => {
                    if tiles.first().unwrap().suit() != tile_suit {
                        return false;
                    }

                    num_groups += 1;
                }
            }
        }

        (num_groups == 4 && num_pairs == 1) || (num_pairs == 7)
    }

    /// Check if the hand has the chinroutou, or perfect terminals, yaku.
    ///
    /// See [Yaku::Chinroutou] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This can panic while checking any kan melds.
    /// This should not happen unless there is a bug in this function.
    pub fn has_chinroutou(&self) -> bool {
        let mut num_completed_shapes = 0;
        let mut has_pair = false;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(tile, ..) => {
                    if tile.is_terminal() {
                        has_pair = true
                    } else {
                        return false;
                    }
                }
                Mentsu::Shuntsu(..) => return false,
                Mentsu::Koutsu(tile, ..) => {
                    if tile.is_terminal() {
                        num_completed_shapes += 1;
                    } else {
                        return false;
                    }
                }
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii { .. } => return false,
                Meld::Pon { claimed_tile, .. } => {
                    if claimed_tile.is_terminal() {
                        num_completed_shapes += 1
                    } else {
                        return false;
                    }
                }
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        let tile = tiles.first().unwrap();
                        if tile.is_terminal() {
                            num_completed_shapes += 1
                        } else {
                            return false;
                        }
                    }
                },
            }
        }

        num_completed_shapes == 4 && has_pair
    }

    /// Calculate if the winning hand has a chuuren poutou, or nine gates, yaku.
    ///
    /// See [Yaku::ChuurenPoutou] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This should not panic unless the tiles in the hand is empty for whatever reason. This would be a bug.
    pub fn has_chuuren_poutou(&self) -> bool {
        // The hand must be closed.
        // TODO: An ankan (closed kan) should be valid, I think, and may need to be accounted for.
        if !self.melds.is_empty() {
            return false;
        }

        let mut tiles = self.tiles.clone();
        if let Some(drawn_tile) = self.drawn_tile {
            tiles.push(&drawn_tile);
        }
        let tiles = tiles;

        // Check if all of the tiles are the same suit.
        let tile_suit = tiles.as_ref().first().unwrap().suit();
        if matches!(tile_suit, TileSuit::Wind | TileSuit::Dragon)
            && !tiles.as_ref().iter().all(|t| t.suit() == tile_suit)
        {
            return false;
        }

        // This section assumes a properly formed hand.
        let mut current_tile = match tile_suit {
            TileSuit::Manzu => Tile::Manzu1,
            TileSuit::Pinzu => Tile::Pinzu1,
            TileSuit::Souzu => Tile::Souzu1,
            _ => return false,
        };
        // Tracker for the additional tile in the suit.
        let mut final_tile_found = false;
        // Go in sequential order to see if all 1-9 tiles are present.
        // NOTE: `1..=9` is used here to be consistent with the 1-9 tiles instead of using `0..9` to make this more readable.
        for _ in 1..=9 {
            let tile_count = tiles.find_count(&current_tile);

            if current_tile.is_terminal() {
                if tile_count < 3 {
                    return false;
                }

                if tile_count == 4 {
                    final_tile_found = true;
                }
            } else {
                if tile_count == 0 {
                    return false;
                }

                if tile_count == 2 {
                    final_tile_found = true;
                }
            }

            current_tile = current_tile.next_tile();
        }

        final_tile_found
    }

    /// Check if the hand has the daisangen, or big dragons, yaku.
    ///
    /// This does not check if the hand is valid, only that the hand has the yaku.
    ///
    /// See [Yaku::Daisangen] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This can panic while checking any kan melds.
    /// This should not happen unless there is a bug in the function.
    pub fn has_daisangen(&self) -> bool {
        let mut has_green_dragon_meld = false;
        let mut has_red_dragon_meld = false;
        let mut has_white_dragon_meld = false;

        for group in self.groups.iter() {
            match group {
                Mentsu::Koutsu(Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite) => {
                    has_white_dragon_meld = true
                }
                Mentsu::Koutsu(Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen) => {
                    has_green_dragon_meld = true
                }
                Mentsu::Koutsu(Tile::DragonRed, Tile::DragonRed, Tile::DragonRed) => {
                    has_red_dragon_meld = true
                }
                _ => continue,
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Pon { claimed_tile, .. } => match claimed_tile {
                    Tile::DragonWhite => has_white_dragon_meld = true,
                    Tile::DragonGreen => has_green_dragon_meld = true,
                    Tile::DragonRed => has_red_dragon_meld = true,
                    _ => continue,
                },
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        let tile = tiles.first().unwrap();
                        match tile {
                            Tile::DragonWhite => has_white_dragon_meld = true,
                            Tile::DragonGreen => has_green_dragon_meld = true,
                            Tile::DragonRed => has_red_dragon_meld = true,
                            _ => continue,
                        }
                    }
                },
                _ => continue,
            }
        }

        has_green_dragon_meld && has_red_dragon_meld && has_white_dragon_meld
    }

    /// Check if the hand has the daisuushii, or big winds, yaku.
    ///
    /// This does not check if the hand is valid, only that the hand has the yaku.
    ///
    /// See [Yaku::Daisuushii] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This can panic while checking any kan melds.
    /// This should not happen unless there is a bug in the function.
    pub fn has_daisuushii(&self) -> bool {
        let mut has_east_wind_meld = false;
        let mut has_south_wind_meld = false;
        let mut has_west_wind_meld = false;
        let mut has_north_wind_meld = false;

        for group in self.groups.iter() {
            match group {
                Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast) => {
                    has_east_wind_meld = true
                }
                Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth) => {
                    has_south_wind_meld = true
                }
                Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest) => {
                    has_west_wind_meld = true
                }
                Mentsu::Koutsu(Tile::WindNorth, Tile::WindNorth, Tile::WindNorth) => {
                    has_north_wind_meld = true
                }
                _ => continue,
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Pon {
                    claimed_tile,
                    relative_player: _,
                    tiles: _,
                } => match claimed_tile {
                    Tile::WindEast => has_east_wind_meld = true,
                    Tile::WindSouth => has_south_wind_meld = true,
                    Tile::WindWest => has_west_wind_meld = true,
                    Tile::WindNorth => has_north_wind_meld = true,
                    _ => continue,
                },
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        let tile = tiles.first().unwrap();
                        match tile {
                            Tile::WindEast => has_east_wind_meld = true,
                            Tile::WindSouth => has_south_wind_meld = true,
                            Tile::WindWest => has_west_wind_meld = true,
                            Tile::WindNorth => has_north_wind_meld = true,
                            _ => continue,
                        }
                    }
                },
                _ => continue,
            }
        }

        has_east_wind_meld && has_south_wind_meld && has_west_wind_meld && has_north_wind_meld
    }

    /// Calculate if the hand has the haitei raoyue, or under the sea, yaku.
    ///
    /// See [Yaku::HaiteiRaoyue] for more information about this yaku.
    pub fn has_haitei_raoyue(&self, round_context: &RoundContext) -> bool {
        if self.drawn_tile.is_some() && round_context.wall().is_wall_exhausted() {
            let mut num_groups = 0;
            let mut num_pairs = 0;

            for group in self.groups.iter() {
                match group {
                    Mentsu::Jantou(..) => num_pairs += 1,
                    Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => num_groups += 1,
                }
            }

            num_groups += self.melds.len();

            (num_groups == 4 && num_pairs == 1) || num_pairs == 7
        } else {
            false
        }
    }

    /// Calculate if the hand has the honitsu, or half flush, yaku.
    ///
    /// See [Yaku::Honitsu] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This can panic while checking any kan melds.
    /// This should not happen unless there is a bug in the function.
    pub fn has_honitsu(&self) -> bool {
        // `tile_suit` will only be set with `TileSuit::Manzu`, `TileSuit::Pinzu`, or `TileSuit::Souzu`.
        let mut tile_suit = None;
        let mut has_honor_group = false;
        let mut num_groups = 0;
        let mut num_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(tile, _) => {
                    if tile.is_honor() {
                        has_honor_group = true;
                    } else if tile_suit.is_none() {
                        tile_suit = Some(tile.suit());
                    } else if let Some(suit) = &tile_suit {
                        if &tile.suit() != suit {
                            return false;
                        }
                    }

                    num_pairs += 1;
                }
                Mentsu::Shuntsu(tile, ..) | Mentsu::Koutsu(tile, ..) => {
                    if tile.is_honor() {
                        has_honor_group = true;
                    } else if tile_suit.is_none() {
                        tile_suit = Some(tile.suit());
                    } else if let Some(suit) = &tile_suit {
                        if &tile.suit() != suit {
                            return false;
                        }
                    }

                    num_groups += 1;
                }
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii {
                    claimed_tile: tile, ..
                }
                | Meld::Pon {
                    claimed_tile: tile, ..
                } => {
                    if tile.is_honor() {
                        has_honor_group = true;
                    } else if tile_suit.is_none() {
                        tile_suit = Some(tile.suit());
                    } else if let Some(suit) = &tile_suit {
                        if &tile.suit() != suit {
                            return false;
                        }
                    }

                    num_groups += 1;
                }
                Meld::Kan(KanType::Ankan { tiles })
                | Meld::Kan(KanType::Daiminkan { tiles, .. })
                | Meld::Kan(KanType::Shominkan { tiles, .. }) => {
                    let tile = tiles.first().unwrap();
                    if tile.is_honor() {
                        has_honor_group = true;
                    } else if tile_suit.is_none() {
                        tile_suit = Some(tile.suit());
                    } else if let Some(suit) = &tile_suit {
                        if &tile.suit() != suit {
                            return false;
                        }
                    }

                    num_groups += 1;
                }
            }
        }

        ((num_groups == 4 && num_pairs == 1) || (num_pairs == 7))
            && tile_suit.is_some()
            && has_honor_group
    }

    /// Calculate if the hand has the honroutou, or all terminals and honors, yaku.
    ///
    /// See [Yaku::Honroutou] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This can panic while checking any kan melds.
    /// This should not happen unless there is a bug in the function.
    pub fn has_honroutou(&self) -> bool {
        let mut has_honor = false;
        let mut has_terminal = false;
        let mut num_groups = 0;
        let mut num_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(tile, _) | Mentsu::Koutsu(tile, ..) => {
                    if matches!(group, Mentsu::Jantou(..)) {
                        num_pairs += 1;
                    } else if matches!(group, Mentsu::Koutsu(..)) {
                        num_groups += 1;
                    }

                    if tile.is_honor() {
                        has_honor = true;
                    } else if tile.is_terminal() {
                        has_terminal = true;
                    } else {
                        return false;
                    }
                }
                Mentsu::Shuntsu(..) => return false,
            }
        }

        for meld in &self.melds {
            let tile = match meld {
                Meld::Chii { .. } => return false,
                Meld::Pon { tiles, .. } => tiles.first().unwrap(),
                Meld::Kan(KanType::Ankan { tiles })
                | Meld::Kan(KanType::Daiminkan { tiles, .. })
                | Meld::Kan(KanType::Shominkan { tiles, .. }) => tiles.first().unwrap(),
            };

            num_groups += 1;

            if tile.is_honor() {
                has_honor = true;
            } else if tile.is_terminal() {
                has_terminal = true;
            } else {
                return false;
            }
        }

        // This yaku can't exclusively have only terminals or only honors, otherwise, a different yaku is granted.
        ((num_groups == 4 && num_pairs == 1) || num_pairs == 7) && has_terminal && has_honor
    }

    /// Calculate if the hand has the houtei raoyui, or under the river, yaku.
    ///
    /// See [Yaku::HouteiRaoyui] for more information about this yaku.
    pub fn has_houtei_raoyui(&self, round_context: &RoundContext) -> bool {
        if self.drawn_tile.is_none() && round_context.wall().is_wall_exhausted() {
            let mut num_groups = 0;
            let mut num_pairs = 0;

            let mut temp_hand = Hand::new(self.tiles.clone());
            if let Some(last_discard) = round_context.last_discard() {
                temp_hand.tiles.as_mut().push(*last_discard.tile());
            } else {
                // It should never reach this state but in case it does, the hand does not have this yaku.
                return false;
            }

            for group in temp_hand.find_groups().iter() {
                match group {
                    Mentsu::Jantou(..) => num_pairs += 1,
                    Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => num_groups += 1,
                }
            }

            num_groups += self.melds.len();

            (num_groups == 4 && num_pairs == 1) || num_pairs == 7
        } else {
            false
        }
    }

    /// Calculate if the winning hand has an iipeikou, or pure double sequences, yaku.
    ///
    /// See [Yaku::Iipeikou] for more information about this yaku.
    pub fn has_iipeikou(&self) -> bool {
        if self.hand_visibility() == HandVisibility::Open {
            return false;
        }

        let num_identical_sequences = self.calculate_number_of_identical_sequences();
        // Counting the pairs ensures this isn't a chiitoitsu hand.
        let num_pairs = self
            .groups
            .iter()
            .filter(|group| matches!(group, Mentsu::Jantou(..)))
            .count();

        num_identical_sequences == 1 && num_pairs == 1
    }

    /// Calculate if the winning hand has an ittsuu, or full straight, yaku.
    ///
    /// See [Yaku::Ittsuu] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This should never panic.
    pub fn has_ittsuu(&self) -> bool {
        let mut has_sequence_123 = false;
        let mut has_sequence_456 = false;
        let mut has_sequence_789 = false;

        let mut suit: Option<TileSuit> = None;

        // We need to find out what suit has a potential full straight.
        // Just because all 9 tiles are present, that doesn't mean that the straight is actually
        // there. For example, `111234567789m` has the groups `111 234 567 789`, which wouldn't
        // count for the yaku.
        'outer: for starting_tile in [Tile::Manzu1, Tile::Pinzu1, Tile::Souzu1] {
            let mut tile = starting_tile;
            for _ in 1..=9 {
                let mut tile_in_sequence_meld = false;
                for meld in &self.melds {
                    match meld {
                        Meld::Chii {
                            claimed_tile: _,
                            relative_player: _,
                            tiles,
                        } => {
                            if tiles.contains(&tile) {
                                tile_in_sequence_meld = true;
                                break;
                            }
                        }
                        _ => continue,
                    }
                }
                let tile_in_sequence_meld = tile_in_sequence_meld;

                if self.tiles.find_count(&tile) == 0 && !tile_in_sequence_meld {
                    continue 'outer;
                }

                tile = tile.next_tile();
            }
            suit = Some(tile.suit());
        }

        let suit = if let Some(suit) = suit {
            suit
        } else {
            return false;
        };

        // Now we need to verify that there are three sequences forming 1-2-3, 4-5-6, and 7-8-9
        // in the hand or in the melds.

        for group in self.groups.iter() {
            match group {
                Mentsu::Shuntsu(tile1, tile2, tile3) => {
                    if tile1.suit() != suit {
                        continue;
                    }

                    let mut tiles = Vec::from([tile1, tile2, tile3]);
                    tiles.sort();
                    let tiles = tiles;

                    match suit {
                        TileSuit::Manzu => {
                            if tiles == [&Tile::Manzu1, &Tile::Manzu2, &Tile::Manzu3] {
                                has_sequence_123 = true;
                            } else if tiles == [&Tile::Manzu4, &Tile::Manzu5, &Tile::Manzu6] {
                                has_sequence_456 = true;
                            } else if tiles == [&Tile::Manzu7, &Tile::Manzu8, &Tile::Manzu9] {
                                has_sequence_789 = true;
                            }
                        }
                        TileSuit::Pinzu => {
                            if tiles == [&Tile::Pinzu1, &Tile::Pinzu2, &Tile::Pinzu3] {
                                has_sequence_123 = true;
                            } else if tiles == [&Tile::Pinzu4, &Tile::Pinzu5, &Tile::Pinzu6] {
                                has_sequence_456 = true;
                            } else if tiles == [&Tile::Pinzu7, &Tile::Pinzu8, &Tile::Pinzu9] {
                                has_sequence_789 = true;
                            }
                        }
                        TileSuit::Souzu => {
                            if tiles == [&Tile::Souzu1, &Tile::Souzu2, &Tile::Souzu3] {
                                has_sequence_123 = true;
                            } else if tiles == [&Tile::Souzu4, &Tile::Souzu5, &Tile::Souzu6] {
                                has_sequence_456 = true;
                            } else if tiles == [&Tile::Souzu7, &Tile::Souzu8, &Tile::Souzu9] {
                                has_sequence_789 = true;
                            }
                        }
                        _ => continue,
                    }
                }
                _ => continue,
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii {
                    claimed_tile: _,
                    relative_player: _,
                    tiles,
                } => {
                    if tiles.first().unwrap().suit() != suit {
                        continue;
                    }

                    match suit {
                        TileSuit::Manzu => {
                            if *meld == Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3) {
                                has_sequence_123 = true;
                            } else if *meld
                                == Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6)
                            {
                                has_sequence_456 = true;
                            } else if *meld
                                == Mentsu::Shuntsu(Tile::Manzu7, Tile::Manzu8, Tile::Manzu9)
                            {
                                has_sequence_789 = true;
                            }
                        }
                        TileSuit::Pinzu => {
                            if *meld == Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3) {
                                has_sequence_123 = true;
                            } else if *meld
                                == Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6)
                            {
                                has_sequence_456 = true;
                            } else if *meld
                                == Mentsu::Shuntsu(Tile::Pinzu7, Tile::Pinzu8, Tile::Pinzu9)
                            {
                                has_sequence_789 = true;
                            }
                        }
                        TileSuit::Souzu => {
                            if *meld == Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3) {
                                has_sequence_123 = true;
                            } else if *meld
                                == Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6)
                            {
                                has_sequence_456 = true;
                            } else if *meld
                                == Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9)
                            {
                                has_sequence_789 = true;
                            }
                        }
                        _ => continue,
                    }
                }
                _ => continue,
            }
        }

        has_sequence_123 && has_sequence_456 && has_sequence_789
    }

    /// Calculate if the hand has a junchan, or perfect ends, yaku
    ///
    /// See [Yaku::Junchan] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This should never panic.
    pub fn has_junchan(&self) -> bool {
        let mut has_sequence = false;
        let mut num_groups = 0;
        let mut num_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(tile, _) => {
                    if !tile.is_terminal() {
                        return false;
                    }

                    num_pairs += 1;
                }
                Mentsu::Koutsu(tile, _, _) => {
                    if !tile.is_terminal() {
                        return false;
                    }

                    num_groups += 1;
                }
                Mentsu::Shuntsu(tile1, _, tile3) => {
                    if !tile1.is_terminal() && !tile3.is_terminal() {
                        return false;
                    }

                    has_sequence = true;
                    num_groups += 1;
                }
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii { tiles, .. } => {
                    if !tiles.first().unwrap().is_terminal()
                        && !tiles.get(1).unwrap().is_terminal()
                        && !tiles.last().unwrap().is_terminal()
                    {
                        return false;
                    }

                    has_sequence = true;
                    num_groups += 1;
                }
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        if !tiles.first().unwrap().is_terminal() {
                            return false;
                        }

                        num_groups += 1;
                    }
                },
                Meld::Pon { claimed_tile, .. } => {
                    if !claimed_tile.is_terminal() {
                        return false;
                    }

                    num_groups += 1;
                }
            }
        }

        num_groups == 4 && num_pairs == 1 && has_sequence
    }

    /// Calculate if the winning hand has a kokushi musou, or thirteen orphans, yaku.
    ///
    /// See [Yaku::KokushiMusou] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This should never panic.
    pub fn has_kokushi_musou(&self, round_context: &RoundContext) -> bool {
        let mut kokushi_musou = [false; 13];

        let mut tiles = self.tiles.clone();
        if let Some(drawn_tile) = self.drawn_tile {
            if tiles.len() == 13 {
                tiles.push(&drawn_tile);
            }
        } else if let Some(last_discard) = round_context.last_discard() {
            if tiles.len() == 13 {
                tiles.push(last_discard.tile());
            }
        } else if let Some(last_kan) = round_context.last_kan() {
            match last_kan.kan() {
                KanType::Ankan { tiles: kan_tiles }
                | KanType::Shominkan {
                    tiles: kan_tiles, ..
                } => {
                    tiles.push(kan_tiles.first().unwrap());
                }
                KanType::Daiminkan { .. } => {}
            }
        }
        let tiles = tiles;

        // Check that each tile type is included.
        if tiles.len() == 13 || tiles.len() == 14 {
            for tile in tiles.as_ref() {
                // Unwrapping here is safe because the array is fixed to a size of 13.
                match tile {
                    Tile::Manzu1 => *kokushi_musou.get_mut(0).unwrap() = true,
                    Tile::Manzu9 => *kokushi_musou.get_mut(1).unwrap() = true,
                    Tile::Pinzu1 => *kokushi_musou.get_mut(2).unwrap() = true,
                    Tile::Pinzu9 => *kokushi_musou.get_mut(3).unwrap() = true,
                    Tile::Souzu1 => *kokushi_musou.get_mut(4).unwrap() = true,
                    Tile::Souzu9 => *kokushi_musou.get_mut(5).unwrap() = true,
                    Tile::WindEast => *kokushi_musou.get_mut(6).unwrap() = true,
                    Tile::WindSouth => *kokushi_musou.get_mut(7).unwrap() = true,
                    Tile::WindWest => *kokushi_musou.get_mut(8).unwrap() = true,
                    Tile::WindNorth => *kokushi_musou.get_mut(9).unwrap() = true,
                    Tile::DragonWhite => *kokushi_musou.get_mut(10).unwrap() = true,
                    Tile::DragonGreen => *kokushi_musou.get_mut(11).unwrap() = true,
                    Tile::DragonRed => *kokushi_musou.get_mut(12).unwrap() = true,
                    _ => return false,
                }
            }
        }

        kokushi_musou.iter().all(|&x| x)
    }

    /// Check if the hand has the menzen tsumo, or fully concealed hand, yaku.
    ///
    /// See [Yaku::MenzenTsumo] for more information about this yaku.
    pub fn has_menzen_tsumo(&self) -> bool {
        if self.drawn_tile.is_none() || self.hand_visibility() == HandVisibility::Open {
            return false;
        }

        if self.has_chiitoitsu() {
            return true;
        }

        // We don't need to check for other unusual hand shapes like Kokushi Musou or Chuuren Poutou
        // since the Menzen Tsumo yaku isn't usually cumulative with yakuman.

        let groups = self.find_groups();
        let mut num_completed_shapes = 0;
        let mut num_pairs = 0;

        for group in groups.iter() {
            match group {
                Mentsu::Jantou(..) => num_pairs += 1,
                Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => num_completed_shapes += 1,
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii {
                    claimed_tile: _,
                    relative_player: _,
                    tiles: _,
                }
                | Meld::Pon {
                    claimed_tile: _,
                    relative_player: _,
                    tiles: _,
                } => return false,
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles: _ } => num_completed_shapes += 1,
                    KanType::Daiminkan {
                        claimed_tile: _,
                        relative_player: _,
                        tiles: _,
                    }
                    | KanType::Shominkan {
                        claimed_tile: _,
                        relative_player: _,
                        tiles: _,
                    } => return false,
                },
            }
        }

        num_completed_shapes == 4 && num_pairs == 1
    }

    /// Calculate if the hand has the nagashi mangan, or mangan at draw, yaku.
    ///
    /// See [Yaku::NagashiMangan] for more information about this yaku.
    pub fn has_nagashi_mangan(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> bool {
        // Must be at exhaustive draw.
        round_context.wall().is_wall_exhausted()
            // Player's discards have not been called.
            && !player_round_state.tile_called_from_own_discard()
            // Player has not made any calls, including ankan.
            && self.melds.is_empty()
            // All of the discarded tiles are honors and terminals.
            && !self.discards.as_ref().iter().any(|tile| !tile.is_honor() && !tile.is_terminal())
    }

    /// Calculate if the hand has the pinfu yaku.
    ///
    /// See [Yaku::Pinfu] for more information about this yaku.
    pub fn has_pinfu(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> bool {
        if !self.melds.is_empty()
            || self.groups.iter().any(|group| {
                matches!(
                    group,
                    Mentsu::Jantou(Tile::DragonWhite, Tile::DragonWhite)
                        | Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen)
                        | Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed)
                        | Mentsu::Koutsu(..)
                ) || {
                    if let Mentsu::Jantou(tile, _) = group {
                        *tile == round_context.wind().as_tile()
                            || *tile == player_round_state.wind().as_tile()
                    } else {
                        false
                    }
                }
            })
        {
            return false;
        }

        if let Some(drawn_tile) = self.drawn_tile {
            let waits = self.get_waits(round_context);
            if waits.len() < 2 || !waits.contains(&drawn_tile) {
                return false;
            }

            // Check that the drawn tile completes the last sequence.
            let temp_hand = Hand::new(self.tiles.clone());
            let num_sequences_post_draw = self
                .groups
                .iter()
                .filter(|group| matches!(group, Mentsu::Shuntsu(..)))
                .count();
            let mut num_sequences_pre_draw = 0;
            let mut num_pairs_pre_draw = 0;

            for group in temp_hand.find_groups().iter() {
                match group {
                    Mentsu::Jantou(..) => num_pairs_pre_draw += 1,
                    Mentsu::Shuntsu(..) => num_sequences_pre_draw += 1,
                    Mentsu::Koutsu(..) => {
                        unreachable!("The hand has already been checked for triplets")
                    }
                }
            }

            num_sequences_pre_draw == 3 && num_sequences_post_draw == 4 && num_pairs_pre_draw == 1
        } else {
            false
        }
    }

    /// Calculate if the hand has the renhou, or blessing of man, yaku.
    ///
    /// If the hand would score more points than a mangan by combining the other yaku, this yaku is not awarded.
    ///
    /// See [Yaku::Renhou] for more information about this yaku.
    pub fn has_renhou(
        &self,
        game_rules: &GameRules,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> bool {
        if !round_context.first_uninterrupted_turn()
            || player_round_state.drew_first_tile()
            || matches!(game_rules.renhou_value(), RenhouValue::Nashi)
            || self.drawn_tile.is_some()
            || matches!(player_round_state.wind(), Wind::East)
        {
            return false;
        }

        if matches!(game_rules.renhou_value(), RenhouValue::Yakuman)
            && self.has_kokushi_musou(round_context)
        {
            return true;
        }

        // Check if the hand would score more points by adding the other yaku and if so, return `false`.
        let mut temp_round_context = round_context.clone();
        *temp_round_context.first_uninterrupted_turn_mut() = false;
        // NOTE: Because the temporary `RoundContext` has the `first_uninterrupted_turn` set to `false`, this shouldn't get into an infinite recursion state.
        let yaku = self.calculate_yaku(game_rules, &temp_round_context, player_round_state);
        let han: u32 = yaku.iter().map(|yaku| yaku.han()).sum();
        // If the resulting hand with other yaku has the same value as renhou, award renhou because it is rarer.
        if !matches!(game_rules.renhou_value(), RenhouValue::Yakuman)
            && han > game_rules.renhou_value().into()
        {
            return false;
        }

        let mut num_groups = 0;
        let mut num_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(..) => num_pairs += 1,
                Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => num_groups += 1,
            }
        }

        (num_groups == 4 && num_pairs == 1) || num_pairs == 7
    }

    /// Calculate if the hand has the rinshan kaihou, or after a kan, yaku.
    ///
    /// See [Yaku::RinshanKaihou] for more information about this yaku.
    pub fn has_rinshan_kaihou(&self) -> bool {
        match self.rinshanpai {
            Some(_) => {
                let mut num_groups = 0;
                let mut num_pairs = 0;

                for group in self.groups.iter() {
                    match group {
                        Mentsu::Jantou(..) => {
                            num_pairs += 1;
                        }
                        Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => {
                            num_groups += 1;
                        }
                    }
                }

                for _ in &self.melds {
                    num_groups += 1;
                }

                num_groups == 4 && num_pairs == 1
            }
            None => false,
        }
    }

    /// Calculate if the winning hand has the ryanpeikou, or twice pure sequences, yaku.
    ///
    /// See [Yaku::Ryanpeikou] for more information about this yaku.
    pub fn has_ryanpeikou(&self) -> bool {
        if self.hand_visibility() == HandVisibility::Open {
            return false;
        }

        let num_identical_sequences = self.calculate_number_of_identical_sequences();
        // Counting the pairs ensures this isn't a chii toitsu hand.
        let num_pairs = self
            .groups
            .iter()
            .filter(|group| matches!(group, Mentsu::Jantou(..)))
            .count();

        num_identical_sequences == 2 && num_pairs == 1
    }

    /// Calculate if the winning hand has the ryuuiisou, or all greens, yaku.
    ///
    /// See [Yaku::Ryuuiisou] for more information about this yaku.
    pub fn has_ryuuiisou(&self) -> bool {
        if !self.is_all_greens() {
            return false;
        }

        let mut num_completed_shapes = 0;
        let mut has_pair = false;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(..) => has_pair = true,
                Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => num_completed_shapes += 1,
            }
        }

        for _ in &self.melds {
            num_completed_shapes += 1;
        }

        num_completed_shapes == 4 && has_pair
    }

    /// Calculate if the hand has the sanankou, or three concealed triplets, yaku.
    ///
    /// See [Yaku::Sanankou] for more information about this yaku.
    pub fn has_sanankou(&self) -> bool {
        if let Some(WinDeclaration::Ron {
            deal_in_player: _,
            tile: _,
        }) = &self.win_declaration
        {
            let mut temp_hand = Hand::new(self.tiles.clone());
            temp_hand.win_declaration = None;
            temp_hand
                .find_groups()
                .iter()
                .filter(|group| matches!(group, Mentsu::Koutsu(..)))
                .count()
                == 3
        } else {
            self.groups
                .iter()
                .filter(|group| matches!(group, Mentsu::Koutsu(..)))
                .count()
                == 3
        }
    }

    /// Calculate if the hand has the sankantsu, or three quads, yaku.
    ///
    /// See [Yaku::Sankantsu] for more information about this yaku.
    pub fn has_sankantsu(&self) -> bool {
        self.melds
            .iter()
            .filter(|meld| matches!(meld, Meld::Kan(..)))
            .count()
            == 3
    }

    /// Calculate if the hand has the sanshoku doujun, or mixed triple sequence, yaku.
    ///
    /// See [Yaku::SanshokuDoujun] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This should not panic unless there is a bug.
    pub fn has_sanshoku_doujun(&self) -> bool {
        let mut has_manzu_sequence = false;
        let mut has_pinzu_sequence = false;
        let mut has_souzu_sequence = false;

        for (group1_idx, group1) in self.groups.iter().enumerate() {
            match group1 {
                Mentsu::Shuntsu(group1_tile1, group1_tile2, group1_tile3) => {
                    match group1_tile1.suit() {
                        TileSuit::Manzu => has_manzu_sequence = true,
                        TileSuit::Pinzu => has_pinzu_sequence = true,
                        TileSuit::Souzu => has_souzu_sequence = true,
                        _ => unreachable!(),
                    }

                    // We can't have the index go out of bounds, though we can still check the melds on the last group.
                    if group1_idx < self.groups.len() - 1 {
                        // Check all of the groups first.
                        for group2 in self.groups.get((group1_idx + 1)..).unwrap().iter() {
                            match group2 {
                                Mentsu::Shuntsu(..) => {
                                    if !has_manzu_sequence {
                                        // Check if there's an equivalent manzu sequence.
                                        let group1_tiles = Mentsu::Shuntsu(
                                            group1_tile1.manzu_tile_equivalent().unwrap(),
                                            group1_tile2.manzu_tile_equivalent().unwrap(),
                                            group1_tile3.manzu_tile_equivalent().unwrap(),
                                        );

                                        if &group1_tiles == group2 {
                                            has_manzu_sequence = true;
                                        }
                                    }

                                    if !has_pinzu_sequence {
                                        // Check if there's an equivalent pinzu sequence.
                                        let group1_tiles = Mentsu::Shuntsu(
                                            group1_tile1.pinzu_tile_equivalent().unwrap(),
                                            group1_tile2.pinzu_tile_equivalent().unwrap(),
                                            group1_tile3.pinzu_tile_equivalent().unwrap(),
                                        );

                                        if &group1_tiles == group2 {
                                            has_pinzu_sequence = true;
                                        }
                                    }

                                    if !has_souzu_sequence {
                                        // Check if there's an equivalent souzu sequence.
                                        let group1_tiles = Mentsu::Shuntsu(
                                            group1_tile1.souzu_tile_equivalent().unwrap(),
                                            group1_tile2.souzu_tile_equivalent().unwrap(),
                                            group1_tile3.souzu_tile_equivalent().unwrap(),
                                        );

                                        if &group1_tiles == group2 {
                                            has_souzu_sequence = true;
                                        }
                                    }
                                }
                                _ => continue,
                            }

                            // We can terminate early if we've found the same sequence in all suits.
                            if has_manzu_sequence && has_pinzu_sequence && has_souzu_sequence {
                                return true;
                            }
                        }
                    }

                    // Check all of the melds.
                    for meld in &self.melds {
                        match meld {
                            Meld::Chii {
                                claimed_tile: _,
                                relative_player: _,
                                tiles: _,
                            } => {
                                if !has_manzu_sequence {
                                    // Check if there's an equivalent manzu sequence.
                                    let group1_tiles = Mentsu::Shuntsu(
                                        group1_tile1.manzu_tile_equivalent().unwrap(),
                                        group1_tile2.manzu_tile_equivalent().unwrap(),
                                        group1_tile3.manzu_tile_equivalent().unwrap(),
                                    );

                                    if &group1_tiles == meld {
                                        has_manzu_sequence = true;
                                    }
                                }

                                if !has_pinzu_sequence {
                                    // Check if there's an equivalent pinzu sequence.
                                    let group1_tiles = Mentsu::Shuntsu(
                                        group1_tile1.pinzu_tile_equivalent().unwrap(),
                                        group1_tile2.pinzu_tile_equivalent().unwrap(),
                                        group1_tile3.pinzu_tile_equivalent().unwrap(),
                                    );

                                    if &group1_tiles == meld {
                                        has_pinzu_sequence = true;
                                    }
                                }

                                if !has_souzu_sequence {
                                    // Check if there's an equivalent souzu sequence.
                                    let group1_tiles = Mentsu::Shuntsu(
                                        group1_tile1.souzu_tile_equivalent().unwrap(),
                                        group1_tile2.souzu_tile_equivalent().unwrap(),
                                        group1_tile3.souzu_tile_equivalent().unwrap(),
                                    );

                                    if &group1_tiles == meld {
                                        has_souzu_sequence = true;
                                    }
                                }
                            }
                            _ => continue,
                        }

                        // We can terminate early if we've found the same sequence in all suits.
                        if has_manzu_sequence && has_pinzu_sequence && has_souzu_sequence {
                            return true;
                        }
                    }
                }
                _ => continue,
            }

            // Reset the states of matching suit sequences because the hand didn't have all 3 of this sequence.
            has_manzu_sequence = false;
            has_pinzu_sequence = false;
            has_souzu_sequence = false;
        }

        // All of the sequences may be melds.
        for (meld1_idx, meld1) in self.melds.iter().enumerate() {
            match meld1 {
                Meld::Chii {
                    claimed_tile,

                    relative_player: _,
                    tiles,
                } => {
                    match claimed_tile.suit() {
                        TileSuit::Manzu => has_manzu_sequence = true,
                        TileSuit::Pinzu => has_pinzu_sequence = true,
                        TileSuit::Souzu => has_souzu_sequence = true,
                        _ => unreachable!(),
                    }

                    for meld2 in self.melds.get((meld1_idx + 1)..).unwrap().iter() {
                        match meld2 {
                            Meld::Chii {
                                claimed_tile: _,
                                relative_player: _,
                                tiles: _,
                            } => {
                                if !has_manzu_sequence {
                                    // Check if there's an equivalent manzu sequence.
                                    let group = Mentsu::Shuntsu(
                                        tiles.first().unwrap().manzu_tile_equivalent().unwrap(),
                                        tiles.get(1).unwrap().manzu_tile_equivalent().unwrap(),
                                        tiles.last().unwrap().manzu_tile_equivalent().unwrap(),
                                    );

                                    if &group == meld2 {
                                        has_manzu_sequence = true;
                                    }
                                }

                                if !has_pinzu_sequence {
                                    // Check if there's an equivalent pinzu sequence.
                                    let group = Mentsu::Shuntsu(
                                        tiles.first().unwrap().pinzu_tile_equivalent().unwrap(),
                                        tiles.get(1).unwrap().pinzu_tile_equivalent().unwrap(),
                                        tiles.last().unwrap().pinzu_tile_equivalent().unwrap(),
                                    );

                                    if &group == meld2 {
                                        has_pinzu_sequence = true;
                                    }
                                }

                                if !has_souzu_sequence {
                                    // Check if there's an equivalent souzu sequence.
                                    let group = Mentsu::Shuntsu(
                                        tiles.first().unwrap().souzu_tile_equivalent().unwrap(),
                                        tiles.get(1).unwrap().souzu_tile_equivalent().unwrap(),
                                        tiles.last().unwrap().souzu_tile_equivalent().unwrap(),
                                    );

                                    if &group == meld2 {
                                        has_souzu_sequence = true;
                                    }
                                }
                            }
                            _ => continue,
                        }

                        // We can terminate early if we've found the same sequence in all suits.
                        if has_manzu_sequence && has_pinzu_sequence && has_souzu_sequence {
                            return true;
                        }
                    }
                }
                _ => continue,
            }

            // Reset the states of matching suit sequences because the hand didn't have all 3 of this sequence.
            has_manzu_sequence = false;
            has_pinzu_sequence = false;
            has_souzu_sequence = false;
        }

        has_manzu_sequence && has_pinzu_sequence && has_souzu_sequence
    }

    /// Check if the hand has the sanshoku dokou, or mixed triplets (or quads), yaku.
    ///
    /// See [Yaku::SanshokuDoukou] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This should not panic unless there is a bug.
    pub fn has_sanshoku_doukou(&self) -> bool {
        // While the names used below say "triplet", they also include quads.
        let mut has_manzu_triplet = false;
        let mut has_pinzu_triplet = false;
        let mut has_souzu_triplet = false;

        for (group1_idx, group1) in self.groups.iter().enumerate() {
            match group1 {
                Mentsu::Koutsu(group1_tile1, _, _) => {
                    match group1_tile1.suit() {
                        TileSuit::Manzu => has_manzu_triplet = true,
                        TileSuit::Pinzu => has_pinzu_triplet = true,
                        TileSuit::Souzu => has_souzu_triplet = true,
                        _ => continue,
                    }

                    // We can't have the index go out of bounds, though we can still check the melds on the last group.
                    if group1_idx < self.groups.len() - 1 {
                        // Check all of the groups first.
                        for group2 in self.groups.get((group1_idx + 1)..).unwrap().iter() {
                            match group2 {
                                Mentsu::Koutsu(group2_tile1, ..) => {
                                    // Check if there's an equivalent manzu triplet.
                                    if !has_manzu_triplet
                                        && (group1_tile1.manzu_tile_equivalent().unwrap()
                                            == *group2_tile1)
                                    {
                                        has_manzu_triplet = true;
                                    }

                                    // Check if there's an equivalent pinzu triplet.
                                    if !has_pinzu_triplet
                                        && (group1_tile1.pinzu_tile_equivalent().unwrap()
                                            == *group2_tile1)
                                    {
                                        has_pinzu_triplet = true;
                                    }

                                    // Check if there's an equivalent souzu triplet.
                                    if !has_souzu_triplet
                                        && (group1_tile1.souzu_tile_equivalent().unwrap()
                                            == *group2_tile1)
                                    {
                                        has_souzu_triplet = true;
                                    }
                                }
                                _ => continue,
                            }

                            // We can terminate early if we've found the same triplet in all suits.
                            if has_manzu_triplet && has_pinzu_triplet && has_souzu_triplet {
                                return true;
                            }
                        }
                    }

                    // Check all of the melds.
                    for meld in &self.melds {
                        match meld {
                            Meld::Pon { claimed_tile, .. } => {
                                // Check if there's an equivalent manzu triplet.
                                if !has_manzu_triplet
                                    && (group1_tile1.manzu_tile_equivalent().unwrap()
                                        == *claimed_tile)
                                {
                                    has_manzu_triplet = true;
                                }

                                // Check if there's an equivalent pinzu triplet.
                                if !has_pinzu_triplet
                                    && (group1_tile1.pinzu_tile_equivalent().unwrap()
                                        == *claimed_tile)
                                {
                                    has_pinzu_triplet = true;
                                }

                                // Check if there's an equivalent souzu triplet.
                                if !has_souzu_triplet
                                    && (group1_tile1.souzu_tile_equivalent().unwrap()
                                        == *claimed_tile)
                                {
                                    has_souzu_triplet = true;
                                }
                            }
                            Meld::Kan(kan_type) => match kan_type {
                                KanType::Ankan { tiles }
                                | KanType::Daiminkan { tiles, .. }
                                | KanType::Shominkan { tiles, .. } => {
                                    // Check if there's an equivalent manzu triplet.
                                    if !has_manzu_triplet
                                        && (group1_tile1.manzu_tile_equivalent().unwrap()
                                            == *tiles.first().unwrap())
                                    {
                                        has_manzu_triplet = true;
                                    }

                                    // Check if there's an equivalent pinzu triplet.
                                    if !has_pinzu_triplet
                                        && (group1_tile1.pinzu_tile_equivalent().unwrap()
                                            == *tiles.first().unwrap())
                                    {
                                        has_pinzu_triplet = true;
                                    }

                                    // Check if there's an equivalent souzu triplet.
                                    if !has_souzu_triplet
                                        && (group1_tile1.souzu_tile_equivalent().unwrap()
                                            == *tiles.first().unwrap())
                                    {
                                        has_souzu_triplet = true;
                                    }
                                }
                            },
                            _ => continue,
                        }

                        // We can terminate early if we've found the same triplet in all suits.
                        if has_manzu_triplet && has_pinzu_triplet && has_souzu_triplet {
                            return true;
                        }
                    }
                }
                _ => continue,
            }

            // Reset the states of matching suit triplets because the hand didn't have all 3 of this triplet.
            has_manzu_triplet = false;
            has_pinzu_triplet = false;
            has_souzu_triplet = false;
        }

        // All of the triplets may be melds.
        for (meld1_idx, meld1) in self.melds.iter().enumerate() {
            match meld1 {
                Meld::Pon {
                    claimed_tile: group1_claimed_tile,
                    ..
                } => {
                    match group1_claimed_tile.suit() {
                        TileSuit::Manzu => has_manzu_triplet = true,
                        TileSuit::Pinzu => has_pinzu_triplet = true,
                        TileSuit::Souzu => has_souzu_triplet = true,
                        _ => continue,
                    }

                    for meld2 in self.melds.get((meld1_idx + 1)..).unwrap().iter() {
                        match meld2 {
                            Meld::Pon {
                                claimed_tile: group2_claimed_tile,
                                relative_player: _,
                                tiles: _,
                            } => {
                                // Check if there's an equivalent manzu triplet.
                                if !has_manzu_triplet
                                    && (group1_claimed_tile.manzu_tile_equivalent().unwrap()
                                        == *group2_claimed_tile)
                                {
                                    has_manzu_triplet = true;
                                }

                                // Check if there's an equivalent pinzu triplet.
                                if !has_pinzu_triplet
                                    && (group1_claimed_tile.pinzu_tile_equivalent().unwrap()
                                        == *group2_claimed_tile)
                                {
                                    has_pinzu_triplet = true;
                                }

                                // Check if there's an equivalent souzu triplet.
                                if !has_souzu_triplet
                                    && (group1_claimed_tile.souzu_tile_equivalent().unwrap()
                                        == *group2_claimed_tile)
                                {
                                    has_souzu_triplet = true;
                                }
                            }
                            Meld::Kan(kan_type) => match kan_type {
                                KanType::Ankan { tiles }
                                | KanType::Daiminkan { tiles, .. }
                                | KanType::Shominkan { tiles, .. } => {
                                    // Check if there's an equivalent manzu triplet.
                                    if !has_manzu_triplet
                                        && (group1_claimed_tile.manzu_tile_equivalent().unwrap()
                                            == *tiles.first().unwrap())
                                    {
                                        has_manzu_triplet = true;
                                    }

                                    // Check if there's an equivalent pinzu triplet.
                                    if !has_pinzu_triplet
                                        && (group1_claimed_tile.pinzu_tile_equivalent().unwrap()
                                            == *tiles.first().unwrap())
                                    {
                                        has_pinzu_triplet = true;
                                    }

                                    // Check if there's an equivalent souzu triplet.
                                    if !has_souzu_triplet
                                        && (group1_claimed_tile.souzu_tile_equivalent().unwrap()
                                            == *tiles.first().unwrap())
                                    {
                                        has_souzu_triplet = true;
                                    }
                                }
                            },
                            _ => continue,
                        }

                        // We can terminate early if we've found the same triplet in all suits.
                        if has_manzu_triplet && has_pinzu_triplet && has_souzu_triplet {
                            return true;
                        }
                    }
                }
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        let group1_tile = tiles.first().unwrap();
                        match group1_tile.suit() {
                            TileSuit::Manzu => has_manzu_triplet = true,
                            TileSuit::Pinzu => has_pinzu_triplet = true,
                            TileSuit::Souzu => has_souzu_triplet = true,
                            _ => continue,
                        }

                        for meld2 in self.melds.get((meld1_idx + 1)..).unwrap().iter() {
                            match meld2 {
                                Meld::Pon {
                                    claimed_tile: group2_claimed_tile,
                                    ..
                                } => {
                                    // Check if there's an equivalent manzu triplet.
                                    if !has_manzu_triplet
                                        && (group1_tile.manzu_tile_equivalent().unwrap()
                                            == *group2_claimed_tile)
                                    {
                                        has_manzu_triplet = true;
                                    }

                                    // Check if there's an equivalent pinzu triplet.
                                    if !has_pinzu_triplet
                                        && (group1_tile.pinzu_tile_equivalent().unwrap()
                                            == *group2_claimed_tile)
                                    {
                                        has_pinzu_triplet = true;
                                    }

                                    // Check if there's an equivalent souzu triplet.
                                    if !has_souzu_triplet
                                        && (group1_tile.souzu_tile_equivalent().unwrap()
                                            == *group2_claimed_tile)
                                    {
                                        has_souzu_triplet = true;
                                    }
                                }
                                Meld::Kan(kan_type) => match kan_type {
                                    KanType::Ankan { tiles }
                                    | KanType::Daiminkan { tiles, .. }
                                    | KanType::Shominkan { tiles, .. } => {
                                        // Check if there's an equivalent manzu triplet.
                                        if !has_manzu_triplet
                                            && (group1_tile.manzu_tile_equivalent().unwrap()
                                                == *tiles.first().unwrap())
                                        {
                                            has_manzu_triplet = true;
                                        }

                                        // Check if there's an equivalent pinzu triplet.
                                        if !has_pinzu_triplet
                                            && (group1_tile.pinzu_tile_equivalent().unwrap()
                                                == *tiles.first().unwrap())
                                        {
                                            has_pinzu_triplet = true;
                                        }

                                        // Check if there's an equivalent souzu triplet.
                                        if !has_souzu_triplet
                                            && (group1_tile.souzu_tile_equivalent().unwrap()
                                                == *tiles.first().unwrap())
                                        {
                                            has_souzu_triplet = true;
                                        }
                                    }
                                },
                                _ => continue,
                            }

                            // We can terminate early if we've found the same triplet in all suits.
                            if has_manzu_triplet && has_pinzu_triplet && has_souzu_triplet {
                                return true;
                            }
                        }
                    }
                },
                _ => continue,
            }

            // Reset the states of matching suit triplets because the hand didn't have all 3 of this triplet.
            has_manzu_triplet = false;
            has_pinzu_triplet = false;
            has_souzu_triplet = false;
        }

        has_manzu_triplet && has_pinzu_triplet && has_souzu_triplet
    }

    /// Check if the hand has the shousangen, or little dragons, yaku.
    ///
    /// See [Yaku::Shousangen] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This should never panic.
    pub fn has_shousangen(&self) -> bool {
        let mut num_dragon_triplets = 0;
        let mut num_dragon_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(tile, _) => {
                    if matches!(tile.suit(), TileSuit::Dragon) {
                        num_dragon_pairs += 1;
                    }
                }
                Mentsu::Shuntsu(..) => continue,
                Mentsu::Koutsu(tile, ..) => {
                    if matches!(tile.suit(), TileSuit::Dragon) {
                        num_dragon_triplets += 1;
                    }
                }
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii { .. } => continue,
                Meld::Kan(KanType::Ankan { tiles })
                | Meld::Kan(KanType::Daiminkan { tiles, .. })
                | Meld::Kan(KanType::Shominkan { tiles, .. }) => {
                    if matches!(tiles.first().unwrap().suit(), TileSuit::Dragon) {
                        num_dragon_triplets += 1;
                    }
                }
                Meld::Pon { claimed_tile, .. } => {
                    if matches!(claimed_tile.suit(), TileSuit::Dragon) {
                        num_dragon_triplets += 1;
                    }
                }
            }
        }

        num_dragon_triplets == 2 && num_dragon_pairs == 1
    }

    /// Check if the hand has the shousuushii, or little winds, yaku.
    ///
    /// This does not check if the hand is valid, only that the hand has the yaku.
    ///
    /// See [Yaku::Shousuushii] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This should never panic.
    pub fn has_shousuushii(&self) -> bool {
        let mut num_wind_melds = 0;
        let mut num_wind_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(tile, _) => {
                    if matches!(tile.suit(), TileSuit::Wind) {
                        num_wind_pairs += 1;
                    }
                }
                Mentsu::Koutsu(tile, _, _) => {
                    if matches!(tile.suit(), TileSuit::Wind) {
                        num_wind_melds += 1;
                    }
                }
                _ => continue,
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Pon { claimed_tile, .. } => {
                    if matches!(claimed_tile.suit(), TileSuit::Wind) {
                        num_wind_melds += 1
                    }
                }
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        if matches!(tiles.first().unwrap().suit(), TileSuit::Wind) {
                            num_wind_melds += 1
                        }
                    }
                },
                _ => continue,
            }
        }

        num_wind_melds == 3 && num_wind_pairs == 1
    }

    /// Calculate if the winning hand has the suuankou, or four concealed triplets, yaku.
    ///
    /// See [Yaku::Suuankou] for more information about this yaku.
    pub fn has_suuankou(&self, round_context: &RoundContext) -> bool {
        let mut num_completed_shapes = 0;
        let mut num_pairs = 0;

        for meld in &self.melds {
            match meld {
                Meld::Kan(KanType::Ankan { tiles: _ }) => num_completed_shapes += 1,
                _ => return false,
            }
        }

        if matches!(self.win_declaration, Some(WinDeclaration::Ron { .. })) {
            let mut temp_hand = Hand::new(self.tiles.clone());
            temp_hand.win_declaration = None;

            // Check to see that this wasn't a shanpon (double-pair) wait.
            num_completed_shapes += temp_hand
                .find_groups()
                .iter()
                .filter(|group| matches!(group, Mentsu::Koutsu(..)))
                .count();
            num_pairs = self
                .groups
                .iter()
                .filter(|group| matches!(group, Mentsu::Jantou(..)))
                .count();
        } else {
            for group in self.groups.iter() {
                match group {
                    Mentsu::Jantou(..) => num_pairs += 1,
                    Mentsu::Koutsu(..) => num_completed_shapes += 1,
                    Mentsu::Shuntsu(..) => return false,
                }
            }

            if self.drawn_tile.is_none() {
                if let Some(last_discard) = round_context.last_discard() {
                    let mut temp_hand = Hand::new(self.tiles.clone());
                    temp_hand.tiles.push(last_discard.tile());

                    let mut num_groups_after_including_discard = 0;
                    let mut num_pairs_after_including_discard = 0;

                    for group in temp_hand.find_groups().iter() {
                        match group {
                            Mentsu::Jantou(..) => num_pairs_after_including_discard += 1,
                            Mentsu::Koutsu(..) => num_groups_after_including_discard += 1,
                            Mentsu::Shuntsu(..) => return false,
                        }
                    }

                    // Check if the hand is on a shanpon (double-pair) wait.
                    if (num_completed_shapes == 3 && num_pairs == 2)
                        && (num_groups_after_including_discard == 4
                            && num_pairs_after_including_discard == 1)
                    {
                        return false;
                    }

                    num_completed_shapes = num_groups_after_including_discard;
                    num_pairs = num_pairs_after_including_discard;
                }
            }
        }

        num_completed_shapes == 4 && num_pairs == 1
    }

    /// Calculate if the winning hand has the suukantsu, or four quads, yaku.
    ///
    /// A player in tenpai for suukantsu cannot claim chankan if a fifth shominkan (or any kan for that matter) is involved.
    ///
    /// See [Yaku::Suukantsu] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This can panic when checking if there is a valid pair.
    /// This should not happen unless there is a bug in this function.
    pub fn has_suukantsu(&self) -> bool {
        let mut num_completed_shapes = 0;
        // We don't need to make check for a fifth kan here because we're not temporarily adding the last kan'ed tile to the hand to check for completion.
        let has_pair = self.tiles.as_ref().len() == 2
            && self.tiles.first().unwrap() == self.tiles.get(1).unwrap();

        for meld in &self.melds {
            match meld {
                Meld::Kan(_) => num_completed_shapes += 1,
                _ => return false,
            }
        }

        num_completed_shapes == 4 && has_pair
    }

    /// Check if the hand has the tanyao, or all simples, yaku.
    ///
    /// This does not check if the hand is valid, only that the hand has the yaku.
    ///
    /// See [Yaku::Tanyao] for more information about this yaku.
    ///
    /// # Parameters
    ///
    /// - `kuitan`: A rule allowing open tanyao. If it is disallowed (`false`), then the tanyao
    ///     yaku would be restricted as a closed-only yaku.
    ///
    /// # Panics
    ///
    /// This should never panic.
    pub fn has_tanyao(&self, kuitan: bool) -> bool {
        let unmelded_tiles_are_simple = self
            .tiles
            .as_ref()
            .iter()
            .all(|tile| !tile.is_honor() && !tile.is_terminal());

        let melded_tiles_are_simple = if kuitan {
            self.melds.iter().all(|meld| match meld {
                Meld::Chii {
                    claimed_tile: _,
                    relative_player: _,
                    tiles,
                }
                | Meld::Pon { tiles, .. } => tiles
                    .iter()
                    .all(|tile| !tile.is_honor() && !tile.is_terminal()),
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        let tile = tiles.first().unwrap();
                        !tile.is_honor() && !tile.is_terminal()
                    }
                },
            })
        } else {
            // Open tanyao is not allowed.
            self.melds.iter().all(|meld| match meld {
                Meld::Chii { .. } | Meld::Pon { .. } => false,
                Meld::Kan(KanType::Ankan { tiles }) => {
                    let tile = tiles.first().unwrap();
                    !tile.is_honor() && !tile.is_terminal()
                }
                Meld::Kan(_) => false,
            })
        };

        unmelded_tiles_are_simple && melded_tiles_are_simple
    }

    /// Check if the hand has the tenhou, or blessing of heaven, yaku.
    ///
    /// See [Yaku::Tenhou] for more information about this yaku.
    pub fn has_tenhou(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> bool {
        if !round_context.first_uninterrupted_turn()
            || !matches!(player_round_state.wind(), Wind::East)
            || self.drawn_tile.is_none()
        {
            return false;
        }

        if self.has_kokushi_musou(round_context) {
            return true;
        }

        let mut num_groups = 0;
        let mut num_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(..) => num_pairs += 1,
                Mentsu::Shuntsu(..) | Mentsu::Koutsu(..) => num_groups += 1,
            }
        }

        (num_groups == 4 && num_pairs == 1) || num_pairs == 7
    }

    /// Check if the hand has the toitoi, or all triplets, yaku.
    ///
    /// See [Yaku::Toitoi] for more information about this yaku.
    pub fn has_toitoi(&self) -> bool {
        let mut num_shapes = 0;
        // Track the number of called kans because 4 kantsu is a different yaku.
        let mut num_kantsu = 0;
        // Track the number of pairs, which should just be 1, just in case the hand got into a weird state.
        let mut num_pairs = 0;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(..) => num_pairs += 1,
                Mentsu::Shuntsu(..) => return false,
                Mentsu::Koutsu(..) => num_shapes += 1,
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii { .. } => return false,
                Meld::Kan(_) => {
                    num_shapes += 1;
                    num_kantsu += 1;
                }
                Meld::Pon { .. } => num_shapes += 1,
            }
        }

        num_shapes == 4 && num_kantsu < 4 && num_pairs == 1
    }

    /// Check if there is a triplet, pon, or kan of the given tile.
    ///
    /// # Panics
    ///
    /// This should never panic.
    fn has_triplet_pon_or_kan_of_tile(&self, tile: &Tile) -> bool {
        let mut tiles = self.tiles.clone();
        if let Some(drawn_tile) = self.drawn_tile {
            tiles.push(&drawn_tile);
        }
        let tiles = tiles;
        if tiles.find_count(tile) == 3 {
            return true;
        }

        for meld in &self.melds {
            match meld {
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        if tiles.first().unwrap() == tile {
                            return true;
                        }
                    }
                },
                Meld::Pon { claimed_tile, .. } => {
                    if claimed_tile == tile {
                        return true;
                    }
                }
                Meld::Chii { .. } => continue,
            }
        }

        false
    }

    /// Check if the hand has the tsuuiisou, or all honors, yaku.
    ///
    /// See [Yaku::Tsuuiisou] for more information about this yaku.
    ///
    /// # Panics
    ///
    /// This should never panic.
    pub fn has_tsuuiisou(&self) -> bool {
        let mut num_completed_shapes = 0;
        let mut has_pair = false;

        for group in self.groups.iter() {
            match group {
                Mentsu::Jantou(tile, ..) => {
                    if tile.is_honor() {
                        has_pair = true;
                    } else {
                        return false;
                    }
                }
                Mentsu::Shuntsu(..) => return false,
                Mentsu::Koutsu(tile, ..) => {
                    if tile.is_honor() {
                        num_completed_shapes += 1;
                    } else {
                        return false;
                    }
                }
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii { .. } => return false,
                Meld::Pon { claimed_tile, .. } => {
                    if claimed_tile.is_honor() {
                        num_completed_shapes += 1
                    } else {
                        return false;
                    }
                }
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        let tile = tiles.first().unwrap();
                        if tile.is_honor() {
                            num_completed_shapes += 1
                        } else {
                            return false;
                        }
                    }
                },
            }
        }

        num_completed_shapes == 4 && has_pair
    }

    /// Check if the hand has the yakuhai chun, or red dragon, yaku.
    ///
    /// See [Yaku::Yakuhai] ([Yakuhai::Chun]) for more information about this yaku.
    pub fn has_yakuhai_chun(&self) -> bool {
        self.has_triplet_pon_or_kan_of_tile(&Tile::DragonRed)
    }

    /// Check if the hand has the yakuhai haku, or white dragon, yaku.
    ///
    /// See [Yaku::Yakuhai] ([Yakuhai::Haku]) for more information about this yaku.
    pub fn has_yakuhai_haku(&self) -> bool {
        self.has_triplet_pon_or_kan_of_tile(&Tile::DragonWhite)
    }

    /// Check if the hand has the yakuhai hatsu, or green dragon, yaku.
    ///
    /// See [Yaku::Yakuhai] ([Yakuhai::Hatsu]) for more information about this yaku.
    pub fn has_yakuhai_hatsu(&self) -> bool {
        self.has_triplet_pon_or_kan_of_tile(&Tile::DragonGreen)
    }

    /// Check if the hand has the yakuhai jikaze or bakaze, or seat wind or prevalent wind respectively, yaku.
    ///
    /// See [Yaku::Yakuhai] ([Yakuhai::SeatWind] or [Yakuhai::PrevalentWind]) for more information about this yaku.
    pub fn has_yakuhai_wind(&self, wind: &Wind) -> bool {
        let tile = match wind {
            Wind::East => Tile::WindEast,
            Wind::South => Tile::WindSouth,
            Wind::West => Tile::WindWest,
            Wind::North => Tile::WindNorth,
        };

        self.has_triplet_pon_or_kan_of_tile(&tile)
    }

    /// Check if the hand only contains all green tiles for the all greens yaku (ryuu iisou).
    ///
    /// This does not check if the hand is valid, only that the hand has this tile type.
    ///
    /// # Panics
    ///
    /// This should never panic.
    pub fn is_all_greens(&self) -> bool {
        if !self.tiles.as_ref().iter().all(|tile| tile.is_green()) {
            return false;
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii {
                    claimed_tile: _,
                    relative_player: _,
                    tiles,
                } => {
                    if !tiles.iter().all(|tile| tile.is_green()) {
                        return false;
                    }
                }

                Meld::Pon { claimed_tile, .. } => {
                    if !claimed_tile.is_green() {
                        return false;
                    }
                }

                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        if !tiles.first().unwrap().is_green() {
                            return false;
                        }
                    }
                },
            }
        }

        true
    }

    /// Get the player's called melds.
    pub const fn melds(&self) -> &Vec<Meld> {
        &self.melds
    }

    /// Get a mutable reference to the player's called melds.
    pub fn melds_mut(&mut self) -> &mut Vec<Meld> {
        &mut self.melds
    }

    /// Get the list of tiles claimed as nukidora.
    ///
    /// See [`Dora::nukidora`] for more information.
    pub const fn nukidora(&self) -> &Vec<Tile> {
        &self.nukidora
    }

    /// Get mutable access to the list of nukidora tiles.
    pub const fn nukidora_mut(&mut self) -> &mut Vec<Tile> {
        &mut self.nukidora
    }

    /// Get the total number of nukidora claimed for the hand.
    ///
    /// # Panics
    ///
    /// Will panic if the number of nukidora exceeds the capacity of [`DoraValue`].
    pub fn nukidora_count(&self) -> DoraValue {
        DoraValue::try_from(self.nukidora.len()).unwrap()
    }

    /// Parse a serialized hand in the MPSZ notation.
    ///
    /// **WARNING**: This function is not complete and should not be used.
    ///
    /// # MPSZ Notation Reference table
    ///
    /// Each tile is identified by its column number, followed by its row letter.
    ///
    /// <style>
    ///   .tile { font-size: 4.5em; }
    ///   .red { color: red; }
    /// </style>
    ///
    /// |   | 0                               | 1                           | 2                           | 3                           | 4                           | 5                           | 6                           | 7                                    | 8                           | 9                           |
    /// |:-:|:-------------------------------:|:---------------------------:|:---------------------------:|:---------------------------:|:---------------------------:|:---------------------------:|:---------------------------:|:------------------------------------:|:---------------------------:|:---------------------------:|
    /// | m | <span class="tile red">🀋</span> | <span class="tile">🀇</span> | <span class="tile">🀈</span> | <span class="tile">🀉</span> | <span class="tile">🀊</span> | <span class="tile">🀋</span> | <span class="tile">🀌</span> | <span class="tile">🀍</span>          | <span class="tile">🀎</span> | <span class="tile">🀏</span> |
    /// | p | <span class="tile red">🀝</span> | <span class="tile">🀙</span> | <span class="tile">🀚</span> | <span class="tile">🀛</span> | <span class="tile">🀜</span> | <span class="tile">🀝</span> | <span class="tile">🀞</span> | <span class="tile">🀟</span>          | <span class="tile">🀠</span> | <span class="tile">🀡</span> |
    /// | s | <span class="tile red">🀔</span> | <span class="tile">🀐</span> | <span class="tile">🀑</span> | <span class="tile">🀒</span> | <span class="tile">🀓</span> | <span class="tile">🀔</span> | <span class="tile">🀕</span> | <span class="tile">🀖</span>          | <span class="tile">🀗</span> | <span class="tile">🀘</span> |
    /// | z | <span class="tile">🀫</span>     | <span class="tile">🀀</span> | <span class="tile">🀁</span> | <span class="tile">🀂</span> | <span class="tile">🀃</span> | <span class="tile">🀆</span> | <span class="tile">🀅</span> | <span class="tile">🀄&#xFE0E;</span> |                             |                             |
    ///
    /// # Errors
    ///
    /// Will return `Err` if the hand is invalid or a character other than `1234567890mpsz` is in the input string.
    ///
    /// # Panics
    ///
    /// This should not panic unless there is a bug.
    ///
    /// It could panic converting a `char` to a `u8`.
    ///
    /// A panic could occur while trying to convert an integer into a `Tile`.
    ///
    /// # Examples
    ///
    /// ## All Suits, Sorted, No Melds
    ///
    /// ```rust
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    ///
    /// let hand = Hand::parse_mpsz("123m45666p345s111z")?;
    ///
    /// assert_eq!(
    ///     hand.tiles(),
    ///     &TileSet::new(Vec::from([
    ///         Tile::Manzu1,
    ///         Tile::Manzu2,
    ///         Tile::Manzu3,
    ///         Tile::Pinzu4,
    ///         Tile::Pinzu5,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Souzu3,
    ///         Tile::Souzu4,
    ///         Tile::Souzu5,
    ///         Tile::WindEast,
    ///         Tile::WindEast,
    ///         Tile::WindEast,
    ///     ])
    /// ));
    ///
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// ## Chii
    ///
    /// ```rust,should_panic
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::Meld;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::parse_mpsz("12'3m45666p345s111z")?;
    ///
    /// assert_eq!(
    ///     hand.tiles(),
    ///     &TileSet::new(Vec::from([
    ///         Tile::Pinzu4,
    ///         Tile::Pinzu5,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Souzu3,
    ///         Tile::Souzu4,
    ///         Tile::Souzu5,
    ///         Tile::WindEast,
    ///         Tile::WindEast,
    ///         Tile::WindEast,
    ///     ])
    /// ));
    ///
    /// assert_eq!(
    ///     hand.melds(),
    ///     &Vec::from([
    ///         Meld::Chii {
    ///             claimed_tile: Tile::Manzu2,
    ///             // This is a placeholder as we are unable to determine which player this is called from.
    ///             relative_player: PlayerRelativePosition::Toimen,
    ///             tiles: [Tile::Manzu1, Tile::Manzu2, Tile::Manzu3],
    ///         }
    ///     ]
    /// ));
    ///
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// ## Pon
    ///
    /// ```rust,should_panic
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::Meld;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::parse_mpsz("123m45666p345s11'1z")?;
    ///
    /// assert_eq!(
    ///     hand.tiles(),
    ///     &TileSet::new(Vec::from([
    ///         Tile::Manzu1,
    ///         Tile::Manzu2,
    ///         Tile::Manzu3,
    ///         Tile::Pinzu4,
    ///         Tile::Pinzu5,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Souzu3,
    ///         Tile::Souzu4,
    ///         Tile::Souzu5,
    ///     ])
    /// ));
    ///
    /// assert_eq!(
    ///     hand.melds(),
    ///     &Vec::from([
    ///         Meld::Pon {
    ///             claimed_tile: Tile::WindEast,
    ///             // This is a placeholder as we are unable to determine which player this is called from.
    ///             relative_player: PlayerRelativePosition::Toimen,
    ///             tiles: [Tile::WindEast, Tile::WindEast, Tile::WindEast],
    ///         }
    ///     ]
    /// ));
    ///
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// ## Daiminkan
    ///
    /// ```rust,should_panic
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::{KanType, Meld};
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::parse_mpsz("123m45666p345s11'11z")?;
    ///
    /// assert_eq!(
    ///     hand.tiles(),
    ///     &TileSet::new(Vec::from([
    ///         Tile::Manzu1,
    ///         Tile::Manzu2,
    ///         Tile::Manzu3,
    ///         Tile::Pinzu4,
    ///         Tile::Pinzu5,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Souzu3,
    ///         Tile::Souzu4,
    ///         Tile::Souzu5,
    ///     ])
    /// ));
    ///
    /// assert_eq!(
    ///     hand.melds(),
    ///     &Vec::from([
    ///         Meld::Kan(KanType::Daiminkan {
    ///             claimed_tile: Tile::WindEast,
    ///             // This is a placeholder as we are unable to determine which player this is called from.
    ///             relative_player: PlayerRelativePosition::Toimen,
    ///             tiles: [Tile::WindEast, Tile::WindEast, Tile::WindEast, Tile::WindEast],
    ///         })
    ///     ]
    /// ));
    ///
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// ## Shominkan
    ///
    /// ```rust,should_panic
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::{KanType, Meld};
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::parse_mpsz("123m45666p345s11\"1z")?;
    ///
    /// assert_eq!(
    ///     hand.tiles(),
    ///     &TileSet::new(Vec::from([
    ///         Tile::Manzu1,
    ///         Tile::Manzu2,
    ///         Tile::Manzu3,
    ///         Tile::Pinzu4,
    ///         Tile::Pinzu5,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Souzu3,
    ///         Tile::Souzu4,
    ///         Tile::Souzu5,
    ///     ])
    /// ));
    ///
    /// assert_eq!(
    ///     hand.melds(),
    ///     &Vec::from([
    ///         Meld::Kan(KanType::Shominkan {
    ///             claimed_tile: Tile::WindEast,
    ///             // This is a placeholder as we are unable to determine which player this is called from.
    ///             relative_player: PlayerRelativePosition::Toimen,
    ///             tiles: [Tile::WindEast, Tile::WindEast, Tile::WindEast, Tile::WindEast],
    ///         })
    ///     ]
    /// ));
    ///
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// ## Partially Parsed With Errors
    ///
    /// If there were problems parsing the hand, it will try to parse as much as possible and return a [`Hand`].
    ///
    /// ```rust
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    ///
    /// let hand = Hand::parse_mpsz("123mE45666p345s111z").unwrap_err();
    ///
    /// assert_eq!(
    ///     hand.hand().as_ref().unwrap().tiles(),
    ///     &TileSet::new(Vec::from([
    ///         Tile::Manzu1,
    ///         Tile::Manzu2,
    ///         Tile::Manzu3,
    ///         Tile::Pinzu4,
    ///         Tile::Pinzu5,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Pinzu6,
    ///         Tile::Souzu3,
    ///         Tile::Souzu4,
    ///         Tile::Souzu5,
    ///         Tile::WindEast,
    ///         Tile::WindEast,
    ///         Tile::WindEast,
    ///     ])
    /// ));
    ///
    /// assert_eq!(
    ///     hand.messages(),
    ///     &Vec::from([
    ///         "Unexpected character 'E'.".to_string(),
    ///     ]
    /// ));
    ///
    /// # Ok(())
    /// # }
    /// ```
    ///
    /// ## Errors With No Parsed Hand
    ///
    /// When the entire hand is invalid or unable to be parsed, the hand will be `None` in the error struct.
    ///
    /// ```rust
    /// # use std::error::Error;
    /// #
    /// # fn main() -> Result<(), Box<dyn Error>> {
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    ///
    /// let hand = Hand::parse_mpsz("123a456b999c").unwrap_err();
    ///
    /// assert!(hand.hand().is_none());
    ///
    /// assert_eq!(
    ///     hand.messages(),
    ///     &Vec::from([
    ///         "Unexpected character 'a'.".to_string(),
    ///         "Unexpected character 'b'.".to_string(),
    ///         "Unexpected character 'c'.".to_string(),
    ///     ]
    /// ));
    ///
    /// # Ok(())
    /// # }
    /// ```
    pub fn parse_mpsz(s: &str) -> Result<Self, Box<ParseMpszHandError>> {
        let mpsz = s.to_lowercase();

        let mut hand = Self::default();

        let mut errors = Vec::new();

        let mut values = Vec::new();
        for (idx, c) in mpsz.chars().enumerate() {
            match c {
                // Tile value.
                '0'..='9' => {
                    // Convert the character into the respective `u8` digit.
                    let value = u8::try_from(c).unwrap() - 48u8;
                    values.push(value);
                }
                // Tile suit.
                'm' | 'p' | 's' | 'z' => {
                    if values.is_empty() {
                        errors.push(format!("No tiles for suit '{c}'"));
                    } else {
                        for value in values {
                            let offset = match c {
                                'm' => {
                                    if value == 0 {
                                        35
                                    } else {
                                        0
                                    }
                                }
                                'p' => {
                                    if value == 0 {
                                        36
                                    } else {
                                        9
                                    }
                                }
                                's' => {
                                    if value == 0 {
                                        37
                                    } else {
                                        18
                                    }
                                }
                                'z' => {
                                    if value > 0 && value < 8 {
                                        27
                                    } else {
                                        errors
                                            .push(format!("Unexpected honor tile value '{value}'"));
                                        continue;
                                    }
                                }
                                _ => unreachable!(),
                            };

                            hand.tiles_mut()
                                .push(&Tile::try_from(value + offset - 1).unwrap());
                        }

                        // We've consumed the tiles for the parsed suit so reset the values.
                        values = Vec::new();
                    }
                }
                '-' | 'x' | '?' => {}
                '\'' => todo!("Need to handle ' to rotate preceeding tile (for meld)."),
                '"' => {
                    todo!("Need to handle \" to rotate and stack preceeding tile (for shominkan).")
                }
                // Unexpected character.
                _ => errors.push(format!(
                    "Unexpected character '{}'.",
                    &s.get(idx..idx + 1).unwrap()
                )),
            }
        }

        if errors.is_empty() {
            hand.tiles_mut().sort();
            *hand.groups_mut() = hand.find_groups();

            Ok(hand)
        } else {
            let hand = if hand.tiles().is_empty() {
                None
            } else {
                Some(hand)
            };

            Err(Box::new(ParseMpszHandError::new(hand, errors)))
        }
    }

    /// Get the tile sets that can be used for a [pon](crate::meld::Meld::Pon) call.
    ///
    /// When the returned value is `None`, that means that there are no tiles within the player's hand that can make a valid pon call.
    ///
    /// # Panics
    ///
    /// This should not panic unless the game context is in a weird state with the player that made the last discard.
    /// If this does happen, this would be a bug.
    ///
    /// # Examples
    ///
    /// ```rust
    /// use jikaze_riichi_core::game_rules::NumberOfAkadora;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::meld::Meld;
    /// use jikaze_riichi_core::player::player_round_state::PlayerRoundState;
    /// use jikaze_riichi_core::player::relative_position::PlayerRelativePosition;
    /// use jikaze_riichi_core::round::{LastDiscard, RoundContext};
    /// use jikaze_riichi_core::tile::Tile;
    /// use jikaze_riichi_core::tile::tile_set::TileSet;
    /// use jikaze_riichi_core::wall::{create_unshuffled_tiles_4p, Wall};
    /// use jikaze_riichi_core::wind::Wind;
    ///
    /// let hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::ManzuRed5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu5,
    ///     Tile::Manzu7,
    ///     Tile::Manzu8,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu3,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindEast,
    ///     Tile::WindEast,
    /// ])));
    ///
    /// let mut round_context = RoundContext::new(
    ///     Wall::new(Box::new(create_unshuffled_tiles_4p(
    ///         &NumberOfAkadora::Three,
    ///     ))),
    ///     Wind::East,
    ///     [0; 32],
    /// );
    /// *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu5));
    /// let player_round_state = PlayerRoundState::new(Wind::East);
    ///
    /// assert_eq!(
    ///     hand.pon_options(&round_context, &player_round_state),
    ///     Some(
    ///         [
    ///             Meld::Pon {
    ///                 claimed_tile: Tile::Manzu5,
    ///                 relative_player: PlayerRelativePosition::Kamicha,
    ///                 tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5],
    ///             },
    ///             Meld::Pon {
    ///                 claimed_tile: Tile::Manzu5,
    ///                 relative_player: PlayerRelativePosition::Kamicha,
    ///                 tiles: [Tile::Manzu5, Tile::Manzu5, Tile::Manzu5],
    ///             },
    ///         ]
    ///         .into(),
    ///     )
    /// );
    /// ```
    pub fn pon_options(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> Option<Box<[Meld]>> {
        if round_context.wall().is_wall_exhausted() {
            return None;
        }

        let last_discard = match round_context.last_discard() {
            Some(last_discard) => last_discard,
            // Can't call pon from no discard, so terminate early.
            None => return None,
        };

        let last_discarded_tile = last_discard.tile();
        if self.tiles().find_count(last_discarded_tile) >= 2 {
            let mut options = Vec::new();

            if last_discarded_tile.is_regular_five() || last_discarded_tile.is_red_five() {
                // Account for akadora in the hand.
                let regular_five_count = self
                    .tiles()
                    // Ensures that the correct tile suit is selected.
                    .filter_by_tile(last_discarded_tile)
                    .iter()
                    .filter(|tile| tile.is_regular_five())
                    .count();
                let akadora_count = self
                    .tiles()
                    // Ensures that the correct tile suit is selected.
                    .filter_by_tile(last_discarded_tile)
                    .iter()
                    .filter(|tile| tile.is_red_five())
                    .count();

                // `> 0` because if the game is being played with 4 akadora, there may be 2 pinzu akadora.
                if akadora_count > 0 {
                    let red_five = match last_discarded_tile.suit() {
                        TileSuit::Manzu => Tile::ManzuRed5,
                        TileSuit::Pinzu => Tile::PinzuRed5,
                        TileSuit::Souzu => Tile::SouzuRed5,
                        TileSuit::Wind | TileSuit::Dragon => unreachable!(),
                    };

                    // When there are 4 akadora in play, a player may have 2 akadora in the pinzu suit.
                    if akadora_count > 1 {
                        options.push(Meld::Pon {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [red_five, red_five, *last_discarded_tile],
                        });
                    }

                    if regular_five_count >= 1 {
                        options.push(Meld::Pon {
                            claimed_tile: *last_discarded_tile,
                            relative_player: PlayerRelativePosition::position_from_winds(
                                player_round_state.wind(),
                                last_discard.player(),
                            )
                            .unwrap(),
                            tiles: [
                                red_five,
                                *last_discarded_tile,
                                // Ensure that a regular 5 is inserted.
                                last_discarded_tile.previous_tile().next_tile(),
                            ],
                        });
                    }
                }

                if regular_five_count >= 2 {
                    options.push(Meld::Pon {
                        claimed_tile: *last_discarded_tile,
                        relative_player: PlayerRelativePosition::position_from_winds(
                            player_round_state.wind(),
                            last_discard.player(),
                        )
                        .unwrap(),
                        tiles: [
                            *last_discarded_tile,
                            *last_discarded_tile,
                            *last_discarded_tile,
                        ],
                    });
                }
            } else if self.tiles().find_count(last_discarded_tile) >= 2 {
                options.push(Meld::Pon {
                    claimed_tile: *last_discarded_tile,
                    relative_player: PlayerRelativePosition::position_from_winds(
                        player_round_state.wind(),
                        last_discard.player(),
                    )
                    .unwrap(),
                    tiles: [
                        *last_discarded_tile,
                        *last_discarded_tile,
                        *last_discarded_tile,
                    ],
                });
            }

            if options.is_empty() {
                None
            } else {
                Some(options.into())
            }
        } else {
            None
        }
    }

    /// Get the rinshanpai, or replacement tile after calling "kan".
    pub const fn rinshanpai(&self) -> &Option<Tile> {
        &self.rinshanpai
    }

    /// Get a mutable reference to the rinshanpai, or replacement tile after calling "kan".
    pub fn rinshanpai_mut(&mut self) -> &mut Option<Tile> {
        &mut self.rinshanpai
    }

    /// Get the hand's score.
    ///
    /// # Examples
    ///
    /// ## Fewer than 4 Han but Enough Fu for Mangan
    ///
    /// ```rust
    /// use jikaze_riichi_core::dora::Dora;
    /// use jikaze_riichi_core::fu::{Fu,FuMentsuTileClass};
    /// use jikaze_riichi_core::game_rules::GameRules;
    /// use jikaze_riichi_core::mentsu::Mentsu;
    /// use jikaze_riichi_core::hand::Hand;
    /// use jikaze_riichi_core::player::player_round_state::{PlayerRoundState, Riichi};
    /// use jikaze_riichi_core::round::RoundContext;
    /// use jikaze_riichi_core::score::HandScore;
    /// use jikaze_riichi_core::tile::{Tile, TileSet};
    /// use jikaze_riichi_core::wall::Wall;
    /// use jikaze_riichi_core::wind::Wind;
    /// use jikaze_riichi_core::win_declaration::WinDeclaration;
    /// use jikaze_riichi_core::yaku::{Yaku, Yakuhai};
    ///
    /// let mut hand = Hand::new(TileSet::new(Vec::from([
    ///     Tile::Pinzu1,
    ///     Tile::Pinzu2,
    ///     Tile::Pinzu3,
    ///     Tile::Pinzu4,
    ///     Tile::Pinzu5,
    ///     Tile::Pinzu6,
    ///     Tile::Souzu2,
    ///     Tile::Souzu3,
    ///     Tile::Souzu4,
    ///     Tile::WindSouth,
    ///     Tile::WindSouth,
    ///     Tile::WindSouth,
    ///     Tile::DragonGreen,
    /// ])));
    /// *hand.groups_mut() = Box::new([
    ///     // Sequence: 0 fu
    ///     Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
    ///     // Sequence: 0 fu
    ///     Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
    ///     // Sequence: 0 fu
    ///     Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
    ///     // Triplet, honor, concealed: +8 fu
    ///     Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
    ///     // Pair, value honor: +2 fu
    ///     // Pair wait: +2 fu
    ///     Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
    /// ]);
    /// *hand.win_declaration_mut() = Some(WinDeclaration::Ron {
    ///     deal_in_player: Wind::South,
    ///     tile: Tile::DragonGreen,
    /// });
    ///
    /// let wall = Wall::new(Box::new([
    ///     // ...
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu5,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu1,
    /// #     Tile::DragonGreen,
    /// #     Tile::DragonRed,
    /// #     Tile::Souzu1,
    /// #     Tile::Souzu5,
    /// #     Tile::Souzu7,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu4,
    /// #     Tile::WindSouth,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu7,
    /// #     Tile::Pinzu5,
    /// #     Tile::Souzu9,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Manzu4,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu3,
    /// #     Tile::Souzu2,
    /// #     Tile::Souzu7,
    /// #     Tile::WindWest,
    /// #     Tile::DragonWhite,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonRed,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu2,
    /// #     Tile::DragonWhite,
    /// #     Tile::WindSouth,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu5,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu2,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu6,
    /// #     Tile::DragonGreen,
    /// #     Tile::Manzu8,
    /// #     Tile::Souzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu6,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu2,
    /// #     Tile::Pinzu9,
    /// #     Tile::WindWest,
    /// #     Tile::DragonRed,
    /// #     Tile::Manzu3,
    /// #     Tile::WindEast,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::Pinzu8,
    /// #     Tile::Pinzu7,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu2,
    /// #     Tile::Pinzu2,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu8,
    /// #     Tile::Souzu4,
    /// #     Tile::Souzu8,
    /// #     Tile::Pinzu6,
    /// #     Tile::WindSouth,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::DragonGreen,
    /// #     Tile::Souzu6,
    /// #     Tile::Manzu3,
    /// #     Tile::Souzu3,
    /// #     Tile::Pinzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::DragonWhite,
    /// #     Tile::Manzu2,
    /// #     Tile::Manzu3,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu6,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu7,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu5,
    /// #     Tile::Pinzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu4,
    /// #     Tile::WindEast,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu7,
    /// #     Tile::WindNorth,
    /// #     Tile::Pinzu3,
    /// #     Tile::Pinzu9,
    /// #     Tile::Manzu6,
    /// #     Tile::Souzu8,
    /// #     Tile::Manzu7,
    /// #     Tile::Manzu8,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu9,
    /// #     Tile::Souzu1,
    /// #     Tile::WindWest,
    /// #     Tile::Souzu7,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu9,
    /// #     Tile::Souzu8,
    /// #     Tile::Souzu5,
    /// #     Tile::Pinzu1,
    /// #     Tile::Manzu1,
    /// #     Tile::Souzu3,
    /// #     Tile::Manzu4,
    /// #     Tile::Pinzu5,
    /// #     Tile::Manzu9,
    /// #     Tile::DragonGreen,
    /// #     Tile::Pinzu1,
    /// #     Tile::Pinzu4,
    /// #     Tile::Souzu7,
    /// #     Tile::Pinzu4,
    /// #     Tile::Manzu9,
    /// #     Tile::Manzu3,
    /// #     Tile::Manzu5,
    /// #     Tile::Manzu5,
    /// #     Tile::WindNorth,
    /// #     Tile::Souzu1,
    /// #     Tile::Pinzu3,
    /// #     Tile::Manzu6,
    /// #     Tile::Manzu2,
    /// #     Tile::Souzu6,
    /// ]));
    ///
    /// let game_rules = GameRules::new_4p_default();
    /// let mut round_context = RoundContext::new(wall, Wind::East, [0; 32]);
    /// *round_context.first_uninterrupted_turn_mut() = false;
    /// let mut player_round_state = PlayerRoundState::new(Wind::South);
    /// *player_round_state.riichi_mut() = Some(Riichi::Riichi);
    /// *player_round_state.ippatsu_mut() = true;
    ///
    /// // The fu comes from (42->50):
    /// //   - Winning: +20
    /// //   - Concealed ron: +10
    /// //   - Triplet, honor, concealed: +8
    /// //   - Pair, value honor: +2
    /// //   - Pair wait: +2
    /// let expected = HandScore::new(
    ///     [
    ///         Yaku::Riichi,
    ///         Yaku::Ippatsu,
    ///         Yaku::Yakuhai(Yakuhai::SeatWind),
    ///     ]
    ///     .into(),
    ///     &Dora::new(1, 0, 0, 0),
    ///     [
    ///         Fu::Winning,
    ///         Fu::ConcealedRon,
    ///         Fu::Ankou(FuMentsuTileClass::RoutouhaiJihai),
    ///         Fu::Yakuhai,
    ///         Fu::Tanki,
    ///     ]
    ///     .into(),
    ///     false,
    /// );
    ///
    /// assert_eq!(
    ///     hand.score(&game_rules, &round_context, &player_round_state),
    ///     expected
    /// );
    /// ```
    pub fn score(
        &self,
        game_rules: &GameRules,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> HandScore {
        let yaku = self.calculate_yaku(game_rules, round_context, player_round_state);
        let fu = self.fu(round_context, player_round_state, &yaku);
        let dora = self.calculate_dora(round_context, player_round_state);

        HandScore::new(yaku, &dora, fu, game_rules.aotenjou())
    }

    /// Get the number of tiles in the hand and melds.
    ///
    /// # Panics
    ///
    /// Will panic if the tile count (usize) cannot be converted into a `u32`.
    fn tile_count(&self, tile: &Tile) -> u32 {
        let mut tile_count = self.tiles.find_count(tile);

        if let Some(drawn_tile) = self.drawn_tile {
            if &drawn_tile == tile {
                tile_count += 1;
            }
        } else if let Some(WinDeclaration::Ron {
            tile: claimed_tile, ..
        }) = self.win_declaration
        {
            if &claimed_tile == tile {
                tile_count += 1;
            }
        }

        for meld in &self.melds {
            match meld {
                Meld::Chii { tiles, .. } => {
                    if tiles.contains(tile) {
                        tile_count += 1;
                    }
                }
                Meld::Pon { claimed_tile, .. } => {
                    if claimed_tile == tile {
                        tile_count += 3;
                    }
                }
                Meld::Kan(kan_type) => match kan_type {
                    KanType::Ankan { tiles }
                    | KanType::Daiminkan { tiles, .. }
                    | KanType::Shominkan { tiles, .. } => {
                        if tiles.first().unwrap() == tile {
                            tile_count += 4;
                        }
                    }
                },
            }
        }

        tile_count.try_into().unwrap()
    }

    /// Get the concealed tiles in the hand.
    pub const fn tiles(&self) -> &TileSet {
        &self.tiles
    }

    /// Get a mutable reference to the concealed tiles in the hand.
    pub fn tiles_mut(&mut self) -> &mut TileSet {
        &mut self.tiles
    }

    /// Get the number of uradora tiles in the hand.
    ///
    /// See [`Dora::uradora`] for more information.
    ///
    /// # Panics
    ///
    /// Will panic if the number of North tiles as nukidora exceeds the capacity of [`DoraValue`].
    pub fn uradora_count(
        &self,
        round_context: &RoundContext,
        player_round_state: &PlayerRoundState,
    ) -> DoraValue {
        if player_round_state.riichi().is_none() {
            return 0;
        }

        let mut uradora_count = 0;

        for (_dora_indicator, uradora_indicator) in round_context.wall().dora_indicator_tiles() {
            let uradora_tile = uradora_indicator.next_tile();
            uradora_count += self.tile_count(&uradora_tile);

            if matches!(uradora_indicator, Tile::WindWest) {
                uradora_count += DoraValue::try_from(
                    self.nukidora
                        .iter()
                        .filter(|tile| matches!(tile, Tile::WindNorth))
                        .count(),
                )
                .unwrap();
            }
        }

        uradora_count
    }

    /// Get the win declaration for the hand.
    pub const fn win_declaration(&self) -> Option<&WinDeclaration> {
        match &self.win_declaration {
            Some(win_declaration) => Some(win_declaration),
            None => None,
        }
    }

    /// Get a mutable reference to the win declaration for the hand.
    pub fn win_declaration_mut(&mut self) -> &mut Option<WinDeclaration> {
        &mut self.win_declaration
    }
}

#[cfg(test)]
mod tests {
    use core::error::Error;

    use super::*;
    use crate::error::IncrementWallIndexError;
    use crate::game_rules::{GameLength, LocalYaku, NumberOfAkadora, Renchan};
    use crate::round::{LastDiscard, LastKan, RoundContext};
    use crate::tile::{TOTAL_NUM_TILES_4P, Tile, TileSet};
    use crate::wall::{NUM_DEAD_WALL_TILES, Wall};
    use crate::wind::Wind;
    use crate::yaku::{Yaku, Yakuhai};

    fn create_wall() -> Wall {
        // Tiles shuffled with `rand` crate separately.
        // This ordering will remain the same for all tests to make the tests predictable and consistent.
        // The tiles here will not align with the hands in the tests.
        Wall::new(Box::new([
            Tile::Souzu2,
            Tile::Souzu4,
            Tile::Manzu5,
            Tile::WindSouth,
            Tile::Manzu8,
            Tile::Souzu9,
            Tile::DragonWhite,
            Tile::Manzu1,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::Souzu1,
            Tile::Souzu5,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Manzu4,
            Tile::WindSouth,
            Tile::Pinzu7,
            Tile::Pinzu7,
            Tile::Pinzu5,
            Tile::Souzu9,
            Tile::Manzu8,
            Tile::Souzu4,
            Tile::Manzu4,
            Tile::Souzu6,
            Tile::Manzu1,
            Tile::Souzu2,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu7,
            Tile::WindWest,
            Tile::DragonWhite,
            Tile::Pinzu2,
            Tile::DragonRed,
            Tile::WindEast,
            Tile::Pinzu2,
            Tile::DragonWhite,
            Tile::WindSouth,
            Tile::WindEast,
            Tile::Pinzu9,
            Tile::Manzu6,
            Tile::Souzu5,
            Tile::DragonRed,
            Tile::Manzu2,
            Tile::WindNorth,
            Tile::Souzu3,
            Tile::Pinzu6,
            Tile::DragonGreen,
            Tile::Manzu8,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Pinzu6,
            Tile::Souzu3,
            Tile::Pinzu2,
            Tile::Pinzu9,
            Tile::WindWest,
            Tile::DragonRed,
            Tile::Manzu3,
            Tile::WindEast,
            Tile::Pinzu8,
            Tile::Pinzu6,
            Tile::Pinzu8,
            Tile::Pinzu7,
            Tile::Manzu9,
            Tile::Souzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Pinzu8,
            Tile::Souzu4,
            Tile::Souzu8,
            Tile::Pinzu6,
            Tile::WindSouth,
            Tile::Manzu1,
            Tile::Souzu6,
            Tile::Manzu2,
            Tile::DragonGreen,
            Tile::Souzu6,
            Tile::Manzu3,
            Tile::Souzu3,
            Tile::Pinzu8,
            Tile::Manzu7,
            Tile::DragonWhite,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu4,
            Tile::Manzu6,
            Tile::Pinzu4,
            Tile::Manzu7,
            Tile::Pinzu3,
            Tile::Pinzu1,
            Tile::Pinzu5,
            Tile::Pinzu7,
            Tile::WindNorth,
            Tile::Souzu4,
            Tile::WindEast,
            Tile::WindWest,
            Tile::Souzu5,
            Tile::Manzu5,
            Tile::Pinzu5,
            Tile::Manzu7,
            Tile::Manzu5,
            Tile::Pinzu3,
            Tile::Pinzu9,
            Tile::Manzu6,
            Tile::Souzu8,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Pinzu1,
            Tile::Manzu9,
            Tile::Souzu1,
            Tile::WindWest,
            Tile::Souzu7,
            Tile::Manzu4,
            Tile::Pinzu9,
            Tile::Souzu8,
            Tile::Souzu5,
            Tile::Pinzu1,
            Tile::Manzu1,
            Tile::Souzu3,
            Tile::Manzu4,
            Tile::Pinzu5,
            Tile::Manzu9,
            Tile::DragonGreen,
            Tile::Pinzu1,
            Tile::Pinzu4,
            Tile::Souzu7,
            Tile::Pinzu4,
            Tile::Manzu9,
            Tile::Manzu3,
            Tile::WindNorth,
            Tile::Manzu5,
            Tile::WindNorth,
            Tile::Souzu1,
            Tile::Pinzu3,
            Tile::Manzu6,
            Tile::Manzu2,
            Tile::Souzu6,
        ]))
    }

    #[test]
    fn akadora_count_returns_zero_with_no_red_tiles() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu5);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu3, Tile::Pinzu4, Tile::Pinzu5),
            Mentsu::Shuntsu(Tile::Souzu3, Tile::Souzu4, Tile::Souzu5),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        assert_eq!(hand.akadora_count(), 0);
    }

    #[test]
    fn akadora_count_returns_correct_number_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::SouzuRed5,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::PinzuRed5);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu3, Tile::Pinzu4, Tile::PinzuRed5),
            Mentsu::Shuntsu(Tile::Souzu3, Tile::Souzu4, Tile::SouzuRed5),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        assert_eq!(hand.akadora_count(), 2);
    }

    #[test]
    fn akadora_count_returns_correct_number_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::PinzuRed5);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu3, Tile::Pinzu4, Tile::PinzuRed5),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);
        hand.melds = Vec::from([Meld::Chii {
            claimed_tile: Tile::Souzu4,
            relative_player: PlayerRelativePosition::Toimen,
            tiles: [Tile::Souzu3, Tile::Souzu4, Tile::SouzuRed5],
        }]);

        assert_eq!(hand.akadora_count(), 2);
    }

    #[test]
    fn calculate_yaku_returns_expected() {
        let game_rules = GameRules::new_4p_default();

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.first_uninterrupted_turn_mut() = false;

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);
        *player_round_state.ippatsu_mut() = true;

        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Souzu2,   // Dora tile.
            Tile::Souzu2,   // Dora tile.
            Tile::WindEast, // Uradora tile.
            Tile::WindEast, // Uradora tile.
        ])));
        hand.drawn_tile = Some(Tile::WindEast);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Shuntsu(Tile::Manzu7, Tile::Manzu8, Tile::Manzu9),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
        ]);

        let expected = [
            Yaku::Riichi,
            Yaku::Ippatsu,
            Yaku::MenzenTsumo,
            Yaku::Yakuhai(Yakuhai::SeatWind),
            Yaku::Yakuhai(Yakuhai::PrevalentWind),
            Yaku::Ittsuu(HandVisibility::Concealed),
        ];

        assert_eq!(
            *hand.calculate_yaku(&game_rules, &round_context, &player_round_state),
            expected
        );
    }

    #[test]
    fn chii_options_returns_none_when_no_last_discard() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(hand.chii_options(&round_context, &player_round_state), None);
    }

    #[test]
    fn chii_options_returns_none_when_last_discard_not_made_by_player_to_left() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::Manzu1));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(hand.chii_options(&round_context, &player_round_state), None);
    }

    #[test]
    fn chii_options_returns_none_when_wall_is_exhausted() -> Result<(), IncrementWallIndexError> {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::Manzu1));
        // Exhaust the wall to create the end of a round.
        for _ in 0..(round_context.wall().last_live_tile_idx()) {
            round_context.wall_mut().increment_tile_idx()?;
        }

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(hand.chii_options(&round_context, &player_round_state), None);

        Ok(())
    }

    #[test]
    fn chii_options_returns_none_with_honor_discard() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::WindEast));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(hand.chii_options(&round_context, &player_round_state), None);

        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::DragonWhite));

        assert_eq!(hand.chii_options(&round_context, &player_round_state), None);
    }

    #[test]
    fn chii_options_returns_none_with_no_tiles_to_make_sequence() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu1));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(hand.chii_options(&round_context, &player_round_state), None);
    }

    #[test]
    fn chii_options_returns_one_option_with_tile_value_one_discard() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu1));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.chii_options(&round_context, &player_round_state),
            Some(
                [Meld::Chii {
                    claimed_tile: Tile::Manzu1,
                    relative_player: PlayerRelativePosition::Kamicha,
                    tiles: [Tile::Manzu1, Tile::Manzu2, Tile::Manzu3]
                }]
                .into()
            )
        );
    }

    #[test]
    fn chii_options_returns_one_option_with_tile_value_two_discard() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu2));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.chii_options(&round_context, &player_round_state),
            Some(
                [
                    Meld::Chii {
                        claimed_tile: Tile::Manzu2,
                        relative_player: PlayerRelativePosition::Kamicha,
                        tiles: [Tile::Manzu1, Tile::Manzu2, Tile::Manzu3]
                    },
                    Meld::Chii {
                        claimed_tile: Tile::Manzu2,
                        relative_player: PlayerRelativePosition::Kamicha,
                        tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4]
                    }
                ]
                .into()
            )
        );
    }

    #[test]
    fn chii_options_returns_one_option_with_tile_value_three_through_seven_discard() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu5,
            Tile::Manzu7,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu4));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.chii_options(&round_context, &player_round_state),
            Some(
                [
                    Meld::Chii {
                        claimed_tile: Tile::Manzu4,
                        relative_player: PlayerRelativePosition::Kamicha,
                        tiles: [Tile::Manzu2, Tile::Manzu3, Tile::Manzu4]
                    },
                    Meld::Chii {
                        claimed_tile: Tile::Manzu4,
                        relative_player: PlayerRelativePosition::Kamicha,
                        tiles: [Tile::Manzu3, Tile::Manzu4, Tile::Manzu5]
                    }
                ]
                .into()
            )
        );
    }

    #[test]
    fn chii_options_returns_one_option_with_tile_value_three_through_seven_discard_with_red_five_in_hand()
     {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu3,
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu8,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu7));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.chii_options(&round_context, &player_round_state),
            Some(
                [
                    Meld::Chii {
                        claimed_tile: Tile::Manzu7,
                        relative_player: PlayerRelativePosition::Kamicha,
                        tiles: [Tile::ManzuRed5, Tile::Manzu6, Tile::Manzu7]
                    },
                    Meld::Chii {
                        claimed_tile: Tile::Manzu7,
                        relative_player: PlayerRelativePosition::Kamicha,
                        tiles: [Tile::Manzu5, Tile::Manzu6, Tile::Manzu7]
                    },
                    Meld::Chii {
                        claimed_tile: Tile::Manzu7,
                        relative_player: PlayerRelativePosition::Kamicha,
                        tiles: [Tile::Manzu6, Tile::Manzu7, Tile::Manzu8]
                    },
                ]
                .into()
            )
        );
    }

    #[test]
    fn chii_options_returns_one_option_with_tile_value_eight_discard() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu8));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.chii_options(&round_context, &player_round_state),
            Some(
                [
                    Meld::Chii {
                        claimed_tile: Tile::Manzu8,
                        relative_player: PlayerRelativePosition::Kamicha,
                        tiles: [Tile::Manzu6, Tile::Manzu7, Tile::Manzu8]
                    },
                    Meld::Chii {
                        claimed_tile: Tile::Manzu8,
                        relative_player: PlayerRelativePosition::Kamicha,
                        tiles: [Tile::Manzu7, Tile::Manzu8, Tile::Manzu9]
                    }
                ]
                .into()
            )
        );
    }

    #[test]
    fn chii_options_returns_one_option_with_tile_value_nine_discard() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Manzu9));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.chii_options(&round_context, &player_round_state),
            Some(
                [Meld::Chii {
                    claimed_tile: Tile::Manzu9,
                    relative_player: PlayerRelativePosition::Kamicha,
                    tiles: [Tile::Manzu7, Tile::Manzu8, Tile::Manzu9]
                }]
                .into()
            )
        );
    }

    #[test]
    fn dora_count_returns_correct_number_with_one_indicator_and_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2, // Dora tile.
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        assert_eq!(hand.dora_count(&round_context), 1);
    }

    #[test]
    fn dora_count_returns_correct_number_with_three_indicators_and_no_melds()
    -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu4, // Dora tile.
            Tile::Manzu4, // Dora tile.
            Tile::Manzu4, // Dora tile.
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Souzu2, // Dora tile.
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu4, Tile::Manzu4, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..2 {
            round_context.wall_mut().increment_num_kans_called()?;
            round_context.wall_mut().increment_num_rinshanpai_drawn()?;
            round_context.wall_mut().increment_num_revealed_dora()?;
        }

        // The `6m` tile is also a dora tile but it's not in the hand.
        assert_eq!(hand.dora_count(&round_context), 4);

        Ok(())
    }

    #[test]
    fn dora_count_returns_correct_number_with_three_indicators_and_melds()
    -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Manzu4,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Manzu4, Tile::Manzu4, Tile::Manzu4],
            },
            Meld::Chii {
                claimed_tile: Tile::Souzu3,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu2, Tile::Souzu3, Tile::Souzu4],
            },
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..3 {
            round_context.wall_mut().increment_num_kans_called()?;
            round_context.wall_mut().increment_num_rinshanpai_drawn()?;
            round_context.wall_mut().increment_num_revealed_dora()?;
        }

        // The `6m` tile is also a dora tile but it's not in the hand.
        assert_eq!(hand.dora_count(&round_context), 4);

        Ok(())
    }

    #[test]
    fn uradora_count_returns_correct_number_with_one_indicator_and_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast, // Uradora tile.
            Tile::WindEast, // Uradora tile.
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(hand.uradora_count(&round_context, &player_round_state), 2);
    }

    #[test]
    fn uradora_count_returns_correct_number_with_multiple_identical_indicators()
    -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast, // Uradora tile.
            Tile::WindEast, // Uradora tile.
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..2 {
            round_context.wall_mut().increment_num_kans_called()?;
            round_context.wall_mut().increment_num_rinshanpai_drawn()?;
            round_context.wall_mut().increment_num_revealed_dora()?;
        }

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        // Within the dead wall, the North wind is the indicator twice, so each East tile is worth 2.
        assert_eq!(hand.uradora_count(&round_context, &player_round_state), 4);

        Ok(())
    }

    #[test]
    fn uradora_count_returns_zero_when_not_in_riichi() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast, // Uradora tile.
            Tile::WindEast, // Uradora tile.
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert_eq!(hand.uradora_count(&round_context, &player_round_state), 0);
    }

    #[test]
    fn has_chankan_returns_true_with_regular_hand_shape() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu3, Tile::Souzu4, Tile::Souzu5),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_kan_mut() = Some(LastKan::new(
            Wind::West,
            KanType::Shominkan {
                claimed_tile: Tile::Souzu6,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
            },
        ));

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(hand.has_chankan(&round_context, &player_round_state));
    }

    #[test]
    fn has_chankan_returns_false_with_no_kan_made_by_another_player() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu3, Tile::Souzu4, Tile::Souzu5),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_chankan(&round_context, &player_round_state));
    }

    #[test]
    fn has_chankan_returns_false_with_invalid_hand() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindWest,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu3, Tile::Souzu4, Tile::Souzu5),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_kan_mut() = Some(LastKan::new(
            Wind::South,
            KanType::Shominkan {
                claimed_tile: Tile::Souzu6,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
            },
        ));

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_chankan(&round_context, &player_round_state));
    }

    #[test]
    fn has_chankan_returns_false_with_self_making_last_kan() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::WindEast,
            Tile::WindWest,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_kan_mut() = Some(LastKan::new(
            Wind::South,
            KanType::Shominkan {
                claimed_tile: Tile::Souzu6,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
            },
        ));

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_chankan(&round_context, &player_round_state));
    }

    #[test]
    fn has_chankan_returns_false_with_ankan_made() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu3, Tile::Souzu4, Tile::Souzu5),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_kan_mut() = Some(LastKan::new(
            Wind::North,
            KanType::Ankan {
                tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
            },
        ));

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_chankan(&round_context, &player_round_state));
    }

    #[test]
    fn has_chankan_returns_false_with_daiminkan_made() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu3, Tile::Souzu4, Tile::Souzu5),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_kan_mut() = Some(LastKan::new(
            Wind::North,
            KanType::Daiminkan {
                claimed_tile: Tile::Souzu6,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
            },
        ));

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_chankan(&round_context, &player_round_state));
    }

    #[test]
    fn has_chanta_returns_true_when_present_and_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
        ]);

        assert!(hand.has_chanta());
    }

    #[test]
    fn has_chanta_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9),
        ]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::WindEast,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::WindEast, Tile::WindEast, Tile::WindEast],
            },
            Meld::Chii {
                claimed_tile: Tile::Pinzu1,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3],
            },
        ]);

        assert!(hand.has_chanta());
    }

    #[test]
    fn has_chanta_returns_false_with_group_containing_no_terminal() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Pinzu2, Tile::Pinzu3, Tile::Pinzu4),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9),
        ]);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::WindEast,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::WindEast, Tile::WindEast, Tile::WindEast],
        }]);

        assert!(!hand.has_chanta());
    }

    #[test]
    fn has_chanta_returns_false_with_no_sequence() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Koutsu(Tile::Souzu9, Tile::Souzu9, Tile::Souzu9),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
        ]);

        assert!(!hand.has_chanta());
    }

    #[test]
    fn has_chanta_returns_false_with_no_honor_group() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9),
        ]);

        assert!(!hand.has_chanta());
    }

    #[test]
    fn has_chiihou_returns_true_with_valid_hand_and_first_draw() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.drawn_tile = Some(Tile::Souzu8);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(hand.has_chiihou(&round_context, &player_round_state));
    }

    #[test]
    fn has_chiihou_returns_true_with_kokushi_musou_and_first_draw() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu1);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(hand.has_chiihou(&round_context, &player_round_state));
    }

    #[test]
    fn has_chiihou_returns_true_with_chuuren_poutou() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));
        hand.drawn_tile = Some(Tile::ManzuRed5);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(hand.has_chiihou(&round_context, &player_round_state));
    }

    #[test]
    fn has_chiihou_returns_false_when_not_first_turn() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));
        hand.drawn_tile = Some(Tile::ManzuRed5);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.first_uninterrupted_turn_mut() = false;

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(!hand.has_chiihou(&round_context, &player_round_state));
    }

    #[test]
    fn has_chiihou_returns_false_when_in_east_seat() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));
        hand.drawn_tile = Some(Tile::ManzuRed5);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_chiihou(&round_context, &player_round_state));
    }

    #[test]
    fn has_chinitsu_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu7, Tile::Souzu7, Tile::Souzu7),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
        ]);

        assert!(hand.has_chinitsu());
    }

    #[test]
    fn has_chinitsu_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu9,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
        ]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Souzu7,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu7, Tile::Souzu7, Tile::Souzu7],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
            }),
        ]);

        assert!(hand.has_chinitsu());
    }

    #[test]
    fn has_chinitsu_returns_true_when_present_with_chiitoitsu() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
            Mentsu::Jantou(Tile::Souzu3, Tile::Souzu3),
            Mentsu::Jantou(Tile::Souzu4, Tile::Souzu4),
            Mentsu::Jantou(Tile::Souzu6, Tile::Souzu6),
            Mentsu::Jantou(Tile::Souzu7, Tile::Souzu7),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
        ]);

        assert!(hand.has_chinitsu());
    }

    #[test]
    fn has_chinitsu_returns_false_with_honor_group() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu7, Tile::Souzu7, Tile::Souzu7),
            Mentsu::Jantou(Tile::WindWest, Tile::WindWest),
        ]);

        assert!(!hand.has_chinitsu());
    }

    #[test]
    fn has_chinitsu_returns_false_with_mixed_suit() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu7, Tile::Souzu7, Tile::Souzu7),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
        ]);

        assert!(!hand.has_chinitsu());
    }

    #[test]
    fn has_chiitoitsu_returns_true_when_present() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));

        assert!(hand.has_chiitoitsu());
    }

    #[test]
    fn has_chiitoitsu_returns_false_when_not_enough_pairs() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::DragonRed,
            Tile::DragonGreen,
            Tile::DragonWhite,
            Tile::WindNorth,
            Tile::WindWest,
            Tile::WindSouth,
            Tile::WindEast,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Pinzu9,
            Tile::Pinzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::DragonRed,
        ])));

        assert!(!hand.has_chiitoitsu());
    }

    #[test]
    fn has_chiitoitsu_returns_false_when_not_unique_pairs() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));

        assert!(!hand.has_chiitoitsu());
    }

    #[test]
    fn has_chinroutou_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu9, Tile::Souzu9, Tile::Souzu9),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
        ]);

        assert!(hand.has_chinroutou());
    }

    #[test]
    fn has_chinroutou_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::Pinzu9, Tile::Pinzu9])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9)]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Manzu9, Tile::Manzu9, Tile::Manzu9, Tile::Manzu9],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Souzu9,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu9, Tile::Souzu9, Tile::Souzu9, Tile::Souzu9],
            }),
        ]);

        assert!(hand.has_chinroutou());
    }

    #[test]
    fn has_chinroutou_returns_false_with_chii() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::Pinzu9, Tile::Pinzu9])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9)]);
        hand.melds = Vec::from([
            Meld::Chii {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Manzu1, Tile::Manzu2, Tile::Manzu3],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Manzu9, Tile::Manzu9, Tile::Manzu9, Tile::Manzu9],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Souzu9,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu9, Tile::Souzu9, Tile::Souzu9, Tile::Souzu9],
            }),
        ]);

        assert!(!hand.has_chinroutou());
    }

    #[test]
    fn has_chinroutou_returns_false_with_honor_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::WindEast, Tile::WindEast])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::WindEast, Tile::WindEast)]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Manzu9, Tile::Manzu9, Tile::Manzu9, Tile::Manzu9],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Souzu9,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu9, Tile::Souzu9, Tile::Souzu9, Tile::Souzu9],
            }),
        ]);

        assert!(!hand.has_chinroutou());
    }

    #[test]
    fn has_chinroutou_returns_false_with_simple_tile_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::Manzu2, Tile::Manzu2])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::Manzu2, Tile::Manzu2)]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Manzu9, Tile::Manzu9, Tile::Manzu9, Tile::Manzu9],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Souzu9,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu9, Tile::Souzu9, Tile::Souzu9, Tile::Souzu9],
            }),
        ]);

        assert!(!hand.has_chinroutou());
    }

    #[test]
    fn has_chinroutou_returns_false_with_sequence() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Jantou(Tile::Manzu2, Tile::Manzu2),
        ]);
        hand.melds = Vec::from([
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Manzu9, Tile::Manzu9, Tile::Manzu9, Tile::Manzu9],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Souzu9,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu9, Tile::Souzu9, Tile::Souzu9, Tile::Souzu9],
            }),
        ]);

        assert!(!hand.has_chinroutou());
    }

    #[test]
    fn has_chuuren_poutou_returns_true_when_present() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::ManzuRed5,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
        ]);

        assert!(hand.has_chuuren_poutou());
    }

    #[test]
    fn has_chuuren_poutou_returns_false_with_mixed_suit() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu5,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));

        assert!(!hand.has_chuuren_poutou());
    }

    #[test]
    fn has_chuuren_poutou_returns_false_with_honor_tile() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::WindEast,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu5,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));

        assert!(!hand.has_chuuren_poutou());
    }

    #[test]
    fn has_chuuren_poutou_returns_false_when_fewer_than_3_terminals() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));

        assert!(!hand.has_chuuren_poutou());

        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));

        assert!(!hand.has_chuuren_poutou());
    }

    #[test]
    fn has_chuuren_poutou_returns_false_when_missing_middle_tile() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));

        assert!(!hand.has_chuuren_poutou());
    }

    #[test]
    fn has_daisangen_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite),
            Mentsu::Koutsu(Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen),
            Mentsu::Koutsu(Tile::DragonRed, Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(hand.has_daisangen());
    }

    #[test]
    fn has_daisangen_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::DragonRed, Tile::DragonRed, Tile::DragonRed),
        ]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::DragonGreen,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen],
            },
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::DragonWhite,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                ],
            }),
        ]);

        assert!(hand.has_daisangen());
    }

    #[test]
    fn has_daisangen_returns_false_with_dragon_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Koutsu(Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite),
            Mentsu::Koutsu(Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(!hand.has_daisangen());
    }

    #[test]
    fn has_daisuushii_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
            Mentsu::Koutsu(Tile::WindNorth, Tile::WindNorth, Tile::WindNorth),
        ]);

        assert!(hand.has_daisuushii());
    }

    #[test]
    fn has_daisuushii_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
        ]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::WindWest,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::WindWest, Tile::WindWest, Tile::WindWest],
            },
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::WindNorth,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [
                    Tile::WindNorth,
                    Tile::WindNorth,
                    Tile::WindNorth,
                    Tile::WindNorth,
                ],
            }),
        ]);

        assert!(hand.has_daisuushii());
    }

    #[test]
    fn has_daisuushii_returns_false_with_wind_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);

        assert!(!hand.has_daisuushii());
    }

    #[test]
    fn has_haitei_raoyue_returns_true_on_last_draw_with_standard_hand() -> Result<(), Box<dyn Error>>
    {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.drawn_tile = Some(Tile::Manzu9);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::Manzu9, Tile::Manzu9),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(hand.has_haitei_raoyue(&round_context));

        Ok(())
    }

    #[test]
    fn has_haitei_raoyue_returns_true_on_last_draw_with_chiitoitsu() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.drawn_tile = Some(Tile::Manzu9);
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Manzu2, Tile::Manzu2),
            Mentsu::Jantou(Tile::Manzu9, Tile::Manzu9),
            Mentsu::Jantou(Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Jantou(Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Jantou(Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindWest, Tile::WindWest),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(hand.has_haitei_raoyue(&round_context));

        Ok(())
    }

    #[test]
    fn has_haitei_raoyue_returns_false_with_invalid_hand() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.drawn_tile = Some(Tile::Manzu8);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(!hand.has_haitei_raoyue(&round_context));

        Ok(())
    }

    #[test]
    fn has_haitei_raoyue_returns_false_on_tsumo_but_not_last_draw() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.drawn_tile = Some(Tile::Manzu9);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::Manzu9, Tile::Manzu9),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        // Stop before the last tile in the wall.
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len())
            - 1)
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(!hand.has_haitei_raoyue(&round_context));

        Ok(())
    }

    #[test]
    fn has_haitei_raoyue_returns_false_with_ron_on_last_discard() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::Manzu9, Tile::Manzu9),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::West,
            tile: Tile::Manzu9,
        });

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        // Stop before the last tile in the wall.
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(!hand.has_haitei_raoyue(&round_context));

        Ok(())
    }

    #[test]
    fn has_honitsu_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu7, Tile::Souzu7, Tile::Souzu7),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(hand.has_honitsu());
    }

    #[test]
    fn has_honitsu_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Souzu7,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu7, Tile::Souzu7, Tile::Souzu7],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
            }),
        ]);

        assert!(hand.has_honitsu());
    }

    #[test]
    fn has_honitsu_returns_true_when_present_with_all_number_suits_in_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::DragonRed, Tile::DragonRed])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed)]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Souzu3,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu3, Tile::Souzu3, Tile::Souzu3],
            },
            Meld::Chii {
                claimed_tile: Tile::Souzu1,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu1, Tile::Souzu2, Tile::Souzu3],
            },
            Meld::Pon {
                claimed_tile: Tile::Souzu7,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu7, Tile::Souzu7, Tile::Souzu7],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
            }),
        ]);

        assert!(hand.has_honitsu());
    }

    #[test]
    fn has_honitsu_returns_true_when_present_with_chiitoitsu() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
            Mentsu::Jantou(Tile::Souzu3, Tile::Souzu3),
            Mentsu::Jantou(Tile::Souzu4, Tile::Souzu4),
            Mentsu::Jantou(Tile::Souzu6, Tile::Souzu6),
            Mentsu::Jantou(Tile::Souzu7, Tile::Souzu7),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(hand.has_honitsu());
    }

    #[test]
    fn has_honitsu_returns_false_with_no_honor_group() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu7, Tile::Souzu7, Tile::Souzu7),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
        ]);

        assert!(!hand.has_honitsu());
    }

    #[test]
    fn has_honitsu_returns_false_with_only_honor_groups() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen),
            Mentsu::Koutsu(Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);

        assert!(!hand.has_honitsu());
    }

    #[test]
    fn has_honitsu_returns_false_with_mixed_suit() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu7, Tile::Souzu7, Tile::Souzu7),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(!hand.has_honitsu());
    }

    #[test]
    fn has_honroutou_returns_true_with_typical_hand_shape() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(hand.has_honroutou());
    }

    #[test]
    fn has_honroutou_returns_true_with_chiitoitsu() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Manzu1, Tile::Manzu1),
            Mentsu::Jantou(Tile::Manzu9, Tile::Manzu9),
            Mentsu::Jantou(Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Jantou(Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(hand.has_honroutou());
    }

    #[test]
    fn has_honroutou_returns_true_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);
        hand.melds = Vec::from([
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Manzu9, Tile::Manzu9, Tile::Manzu9, Tile::Manzu9],
            }),
            Meld::Pon {
                claimed_tile: Tile::Souzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Souzu1, Tile::Souzu1, Tile::Souzu1],
            },
        ]);

        assert!(hand.has_honroutou());
    }

    #[test]
    fn has_honroutou_returns_false_with_non_honor_or_terminal() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(!hand.has_honroutou());
    }

    #[test]
    fn has_honroutou_returns_false_with_sequence() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(!hand.has_honroutou());
    }

    #[test]
    fn has_honroutou_returns_false_with_chii() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);
        hand.melds = Vec::from([Meld::Chii {
            claimed_tile: Tile::Manzu1,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu1, Tile::Manzu2, Tile::Manzu3],
        }]);

        assert!(!hand.has_honroutou());
    }

    #[test]
    fn has_honroutou_returns_false_with_only_honors() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::DragonWhite,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite],
        }]);

        assert!(!hand.has_honroutou());
    }

    #[test]
    fn has_honroutou_returns_false_with_only_terminals() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
        ]);

        assert!(!hand.has_honroutou());
    }

    #[test]
    fn has_houtei_raoyui_returns_true_on_last_discard_with_standard_hand()
    -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::Manzu9, Tile::Manzu9),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::West, Tile::Manzu9));
        // Stop before the last tile in the wall.
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(hand.has_houtei_raoyui(&round_context));

        Ok(())
    }

    #[test]
    fn has_houtei_raoyui_returns_true_on_last_discard_with_chiitoitsu() -> Result<(), Box<dyn Error>>
    {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Manzu2, Tile::Manzu2),
            Mentsu::Jantou(Tile::Manzu9, Tile::Manzu9),
            Mentsu::Jantou(Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Jantou(Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Jantou(Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindWest, Tile::WindWest),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::West, Tile::Manzu9));
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(hand.has_houtei_raoyui(&round_context));

        Ok(())
    }

    #[test]
    fn has_houtei_raoyui_returns_false_with_invalid_hand() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::West, Tile::Manzu8));
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(!hand.has_houtei_raoyui(&round_context));

        Ok(())
    }

    #[test]
    fn has_houtei_raoyui_returns_false_not_last_discard() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::Manzu9, Tile::Manzu9),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::West, Tile::Manzu9));
        // Stop before the last tile in the wall.
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len())
            - 1)
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(!hand.has_houtei_raoyui(&round_context));

        Ok(())
    }

    #[test]
    fn has_houtei_raoyui_returns_false_with_tsumo_on_draw() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.drawn_tile = Some(Tile::Manzu9);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::Manzu9, Tile::Manzu9),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert!(!hand.has_houtei_raoyui(&round_context));

        Ok(())
    }

    #[test]
    fn has_iipeikou_returns_true_with_one_identical_sequence() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Pinzu2,
            Tile::Pinzu2,
        ])));
        hand.drawn_tile = Some(Tile::Manzu1);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::Pinzu2, Tile::Pinzu2),
        ]);

        assert!(hand.has_iipeikou());
    }

    #[test]
    fn has_iipeikou_returns_true_with_one_identical_sequence_and_ankou() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Pinzu2,
            Tile::Pinzu2,
        ])));
        hand.drawn_tile = Some(Tile::Manzu1);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Jantou(Tile::Pinzu2, Tile::Pinzu2),
        ]);
        hand.melds = Vec::from([Meld::Kan(KanType::Ankan {
            tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
        })]);

        assert!(hand.has_iipeikou());
    }

    #[test]
    fn has_iipeikou_returns_false_with_chii() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Pinzu2,
            Tile::Pinzu2,
        ])));
        hand.drawn_tile = Some(Tile::Manzu1);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Jantou(Tile::Pinzu2, Tile::Pinzu2),
        ]);
        hand.melds = Vec::from([Meld::Chii {
            claimed_tile: Tile::Manzu1,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::Manzu1, Tile::Manzu2, Tile::Manzu3],
        }]);

        assert!(!hand.has_iipeikou());
    }

    #[test]
    fn has_iipeikou_returns_false_with_two_identical_sequence() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Pinzu2,
            Tile::Pinzu2,
        ])));
        hand.drawn_tile = Some(Tile::Manzu1);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Jantou(Tile::Pinzu2, Tile::Pinzu2),
        ]);

        assert!(!hand.has_iipeikou());
    }

    #[test]
    fn has_ittsuu_returns_true_when_present_and_closed() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::WindEast);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu1, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Shuntsu(Tile::Manzu7, Tile::Manzu8, Tile::Manzu9),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
        ]);

        assert!(hand.has_ittsuu());
    }

    #[test]
    fn has_ittsuu_returns_true_when_present_and_closed_with_red_5() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::WindEast);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu1, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::ManzuRed5, Tile::Manzu6),
            Mentsu::Shuntsu(Tile::Manzu7, Tile::Manzu8, Tile::Manzu9),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
        ]);

        assert!(hand.has_ittsuu());
    }

    #[test]
    fn has_ittsuu_returns_true_when_present_and_open() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::WindEast);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu1, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu7, Tile::Manzu8, Tile::Manzu9),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
        ]);
        hand.melds = Vec::from([Meld::Chii {
            claimed_tile: Tile::Manzu5,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu4, Tile::Manzu5, Tile::Manzu6],
        }]);

        assert!(hand.has_ittsuu());
    }

    #[test]
    fn has_ittsuu_returns_false_when_not_all_tiles_present() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Souzu2,
            Tile::Souzu2,
        ])));
        // Hand had a 6-9 wait and didn't get the 9 to complete this.
        hand.drawn_tile = Some(Tile::Manzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
        ]);

        assert!(!hand.has_ittsuu());
    }

    #[test]
    fn has_ittsuu_returns_false_with_all_tiles_present_but_not_all_sequences() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Souzu2,
            Tile::Souzu2,
        ])));
        hand.drawn_tile = Some(Tile::Manzu4);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Shuntsu(Tile::Manzu7, Tile::Manzu8, Tile::Manzu9),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
        ]);

        assert!(!hand.has_ittsuu());
    }

    #[test]
    fn has_ittsuu_returns_false_when_not_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Souzu8,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Koutsu(Tile::Manzu6, Tile::Manzu6, Tile::Manzu6),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Jantou(Tile::Souzu8, Tile::Souzu8),
        ]);
        hand.melds = Vec::from([Meld::Chii {
            claimed_tile: Tile::Souzu1,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::Souzu1, Tile::Souzu2, Tile::Souzu3],
        }]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::South,
            tile: Tile::Souzu8,
        });

        assert!(!hand.has_ittsuu());
    }

    #[test]
    fn has_junchan_returns_true_when_present_and_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9),
            Mentsu::Koutsu(Tile::Souzu9, Tile::Souzu9, Tile::Souzu9),
        ]);

        assert!(hand.has_junchan());
    }

    #[test]
    fn has_junchan_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9),
        ]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Souzu9,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu9, Tile::Souzu9, Tile::Souzu9],
            },
            Meld::Chii {
                claimed_tile: Tile::Pinzu1,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3],
            },
        ]);

        assert!(hand.has_junchan());
    }

    #[test]
    fn has_junchan_returns_false_with_group_containing_no_terminal() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Pinzu2, Tile::Pinzu3, Tile::Pinzu4),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9),
        ]);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Souzu9,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::Souzu9, Tile::Souzu9, Tile::Souzu9],
        }]);

        assert!(!hand.has_junchan());
    }

    #[test]
    fn has_junchan_returns_false_with_group_containing_honor() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Pinzu2, Tile::Pinzu3, Tile::Pinzu4),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9),
        ]);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::WindEast,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::WindEast, Tile::WindEast, Tile::WindEast],
        }]);

        assert!(!hand.has_junchan());
    }

    #[test]
    fn has_junchan_returns_false_with_no_sequence() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Koutsu(Tile::Souzu9, Tile::Souzu9, Tile::Souzu9),
        ]);

        assert!(!hand.has_junchan());
    }

    #[test]
    fn has_kokushi_musou_returns_true_when_present() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::DragonRed,
            Tile::DragonGreen,
            Tile::DragonWhite,
            Tile::WindNorth,
            Tile::WindWest,
            Tile::WindSouth,
            Tile::WindEast,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Pinzu9,
            Tile::Pinzu1,
            Tile::Manzu9,
            Tile::Manzu1,
            Tile::DragonRed,
        ])));

        let round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);

        assert!(hand.has_kokushi_musou(&round_context));
    }

    #[test]
    fn has_kokushi_musou_returns_true_when_ankan_called() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);
        *round_context.last_kan_mut() = Some(LastKan::new(
            Wind::East,
            KanType::Ankan {
                tiles: [Tile::Manzu9, Tile::Manzu9, Tile::Manzu9, Tile::Manzu9],
            },
        ));

        assert!(hand.has_kokushi_musou(&round_context));
    }

    #[test]
    fn has_kokushi_musou_returns_true_when_shominkan_called() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);
        *round_context.last_kan_mut() = Some(LastKan::new(
            Wind::East,
            KanType::Shominkan {
                claimed_tile: Tile::Manzu9,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Manzu9, Tile::Manzu9, Tile::Manzu9, Tile::Manzu9],
            },
        ));

        assert!(hand.has_kokushi_musou(&round_context));
    }

    #[test]
    fn has_kokushi_musou_returns_false_when_not_present() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));

        let round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);

        assert!(!hand.has_kokushi_musou(&round_context));
    }

    #[test]
    fn has_menzen_tsumo_returns_true_with_chiitoitsu_and_self_drawn_tile() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu4,
            Tile::Manzu4,
            Tile::Manzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.drawn_tile = Some(Tile::Manzu6);

        assert!(hand.has_menzen_tsumo());
    }

    #[test]
    fn has_menzen_tsumo_returns_true_with_standard_shapes_and_self_drawn_tile() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.drawn_tile = Some(Tile::Manzu6);

        assert!(hand.has_menzen_tsumo());
    }

    #[test]
    fn has_menzen_tsumo_returns_true_with_ankan_and_self_drawn_tile() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.drawn_tile = Some(Tile::Manzu6);
        hand.melds = Vec::from([Meld::Kan(KanType::Ankan {
            tiles: [Tile::Souzu7, Tile::Souzu7, Tile::Souzu7, Tile::Souzu7],
        })]);

        assert!(hand.has_menzen_tsumo());
    }

    #[test]
    fn has_menzen_tsumo_returns_false_with_standard_closed_shapes_and_win_from_ron() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.win_declaration = Some(WinDeclaration::Ron {
            tile: Tile::Manzu6,
            deal_in_player: Wind::West,
        });

        assert!(!hand.has_menzen_tsumo());
    }

    #[test]
    fn has_menzen_tsumo_returns_false_with_open_meld_and_self_drawn_tile() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.drawn_tile = Some(Tile::Manzu6);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Souzu7,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::Souzu7, Tile::Souzu7, Tile::Souzu7],
        }]);

        assert!(!hand.has_menzen_tsumo());
    }

    #[test]
    fn has_nagashi_mangan_returns_true_on_exhaustive_draw_and_only_discard_honors_and_terminals()
    -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.discards = TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonWhite,
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::DragonRed,
            Tile::Pinzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::DragonWhite,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::DragonGreen,
        ]));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(hand.has_nagashi_mangan(&round_context, &player_round_state));

        Ok(())
    }

    #[test]
    fn has_nagashi_mangan_returns_false_with_non_honor_or_terminal_in_discard()
    -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.discards = TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonWhite,
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::DragonRed,
            Tile::Pinzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::DragonWhite,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::Pinzu2,
        ]));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_nagashi_mangan(&round_context, &player_round_state));

        Ok(())
    }

    #[test]
    fn has_nagashi_mangan_returns_false_with_called_tile() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.discards = TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonWhite,
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::DragonRed,
            Tile::Pinzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::DragonWhite,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::Pinzu2,
        ]));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.tile_called_from_own_discard_mut() = true;

        assert!(!hand.has_nagashi_mangan(&round_context, &player_round_state));

        Ok(())
    }

    #[test]
    fn has_nagashi_mangan_returns_false_with_player_calling_ankan() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu8,
            Tile::Pinzu8,
        ])));
        hand.discards = TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonWhite,
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::DragonRed,
            Tile::Pinzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::DragonWhite,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::DragonGreen,
        ]));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
        ]);
        hand.melds = Vec::from([Meld::Kan(KanType::Ankan {
            tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
        })]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_nagashi_mangan(&round_context, &player_round_state));

        Ok(())
    }

    #[test]
    fn has_nagashi_mangan_returns_false_with_open_hand() -> Result<(), Box<dyn Error>> {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu8,
            Tile::Pinzu8,
        ])));
        hand.discards = TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonWhite,
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::DragonRed,
            Tile::Pinzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::DragonWhite,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::DragonGreen,
        ]));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
        ]);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Souzu2,
            relative_player: PlayerRelativePosition::Toimen,
            tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
        }]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        for _ in 0..(TOTAL_NUM_TILES_4P
            - (NUM_DEAD_WALL_TILES + round_context.wall().dora_indicator_tiles().len()))
        {
            round_context.wall_mut().increment_tile_idx()?;
        }

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_nagashi_mangan(&round_context, &player_round_state));

        Ok(())
    }

    #[test]
    fn has_nagashi_mangan_returns_false_when_not_at_exhaustive_draw() -> Result<(), Box<dyn Error>>
    {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.discards = TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Souzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindWest,
            Tile::WindWest,
            Tile::DragonWhite,
            Tile::Manzu1,
            Tile::Pinzu1,
            Tile::DragonRed,
            Tile::Pinzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::DragonWhite,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::DragonGreen,
        ]));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        round_context.wall_mut().increment_tile_idx()?;

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_nagashi_mangan(&round_context, &player_round_state));

        Ok(())
    }

    #[test]
    fn has_pinfu_returns_true_with_all_sequences_and_two_sided_ryanmen_wait() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(hand.has_pinfu(&round_context, &player_round_state));
    }

    /// Three-sided wait involving a combination of two [ryanmen (open wait)][ryanmen].
    #[test]
    fn has_pinfu_returns_true_with_all_sequences_and_sanmenchan_wait() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu3);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Pinzu3, Tile::Pinzu4, Tile::Pinzu5),
            Mentsu::Shuntsu(Tile::Pinzu6, Tile::Pinzu7, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::Souzu8, Tile::Souzu8),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(hand.has_pinfu(&round_context, &player_round_state));

        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Shuntsu(Tile::Pinzu6, Tile::Pinzu7, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::Souzu8, Tile::Souzu8),
        ]);

        assert!(hand.has_pinfu(&round_context, &player_round_state));

        hand.drawn_tile = Some(Tile::Pinzu9);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Shuntsu(Tile::Pinzu7, Tile::Pinzu8, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::Souzu8, Tile::Souzu8),
        ]);

        assert!(hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_pinfu_returns_true_with_non_seat_and_round_wind_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Jantou(Tile::WindWest, Tile::WindWest),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_pinfu_returns_true_with_tanki_wait_and_ryanmen_wait_but_draw_on_ryanmen() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_pinfu(&round_context, &player_round_state));

        hand.drawn_tile = Some(Tile::Pinzu9);

        assert!(!hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_pinfu_returns_false_with_ryanmen_wait_but_open() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
        ]);
        hand.melds = Vec::from([Meld::Chii {
            claimed_tile: Tile::Souzu1,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Souzu1, Tile::Souzu2, Tile::Souzu3],
        }]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_pinfu_returns_false_with_seat_wind_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Jantou(Tile::WindWest, Tile::WindWest),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::West);

        assert!(!hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_pinfu_returns_false_with_prevalent_wind_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::West);

        assert!(!hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_pinfu_returns_false_with_dragon_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::DragonWhite,
            Tile::DragonWhite,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Jantou(Tile::DragonWhite, Tile::DragonWhite),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::West);

        assert!(!hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_pinfu_returns_false_with_tanki_wait() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu8);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_pinfu_returns_false_with_kanchan_wait() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu6,
            Tile::Pinzu8,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu5);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_pinfu_returns_false_with_tanki_wait_and_ryanmen_wait_but_draw_on_tanki() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu3);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Jantou(Tile::Pinzu8, Tile::Pinzu8),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_pinfu(&round_context, &player_round_state));
    }

    #[test]
    fn has_renhou_returns_true_with_valid_hand_during_first_turn() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        let game_rules = GameRules::new_4p_default();

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::East, Tile::Souzu8));

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(hand.has_renhou(&game_rules, &round_context, &player_round_state));
    }

    #[test]
    fn has_renhou_returns_true_with_chiitoitsu_during_first_turn() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Manzu1, Tile::Manzu1),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
            Mentsu::Jantou(Tile::Souzu3, Tile::Souzu3),
            Mentsu::Jantou(Tile::Souzu6, Tile::Souzu6),
            Mentsu::Jantou(Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        let game_rules = GameRules::new_4p_default();

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::East, Tile::Souzu9));

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(hand.has_renhou(&game_rules, &round_context, &player_round_state));
    }

    /// When the renhou value is a yakuman, it should stack with other yakuman.
    #[test]
    fn has_renhou_returns_true_with_kokushi_musou_and_renhou_value_is_yakuman() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));

        let game_rules = GameRules::new_4p(
            NumberOfAkadora::Three,
            false,
            GameLength::Hanchan,
            true,
            LocalYaku::default(),
            30_000,
            Renchan::default(),
            RenhouValue::Yakuman,
            1_000,
            false,
            25_000,
            true,
            300,
        );

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::East, Tile::Pinzu1));

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(hand.has_renhou(&game_rules, &round_context, &player_round_state));
    }

    #[test]
    fn has_renhou_returns_false_with_kokushi_musou_and_renhou_value_is_mangan() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));

        let game_rules = GameRules::new_4p_default();

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::East, Tile::Pinzu1));

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(!hand.has_renhou(&game_rules, &round_context, &player_round_state));
    }

    #[test]
    fn has_renhou_returns_false_when_not_first_turn() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
        ]);

        let game_rules = GameRules::new_4p_default();

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.first_uninterrupted_turn_mut() = false;
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::East, Tile::ManzuRed5));

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(!hand.has_renhou(&game_rules, &round_context, &player_round_state));
    }

    /// This should be impossible since there wouldn't be any discards as the dealer (East) but this ensures that we return `false` just in case.
    #[test]
    fn has_renhou_returns_false_when_in_east_seat() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));
        hand.drawn_tile = Some(Tile::ManzuRed5);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
        ]);

        let game_rules = GameRules::new_4p_default();

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        // This SHOULD be an invalid state.
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::East, Tile::ManzuRed5));

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_renhou(&game_rules, &round_context, &player_round_state));
    }

    #[test]
    fn has_renhou_returns_false_with_other_yaku_more_valuable_than_renhou_value() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu8,
        ])));
        // This should be:
        //   - Pinfu (1)
        //   - Iipeikou (1)
        //   - Ittsuu (2)
        //   - Chinitsu (6)
        // for a total of 10 han (Baiman)
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Jantou(Tile::Souzu3, Tile::Souzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu7, Tile::Souzu8, Tile::Souzu9),
        ]);

        let game_rules = GameRules::new_4p_default();

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::East, Tile::Souzu9));

        let player_round_state = PlayerRoundState::new(Wind::North);

        assert!(!hand.has_renhou(&game_rules, &round_context, &player_round_state));
    }

    /// During the first turn, if the South or West seat declare ron after drawing their first tile,
    /// the renhou yaku is not awarded.
    #[test]
    fn has_renhou_returns_false_after_taking_turn_during_first_turn() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        let game_rules = GameRules::new_4p_default();

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::East, Tile::Souzu8));

        let mut player_round_state = PlayerRoundState::new(Wind::South);
        *player_round_state.drew_first_tile_mut() = true;

        assert!(!hand.has_renhou(&game_rules, &round_context, &player_round_state));
    }

    #[test]
    fn has_rinshan_kaihou_returns_true_with_regular_hand_shape() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.rinshanpai = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        assert!(hand.has_rinshan_kaihou());
    }

    #[test]
    fn has_rinshan_kaihou_returns_false_with_no_rinshanpai() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        assert!(!hand.has_rinshan_kaihou());
    }

    #[test]
    fn has_rinshan_kaihou_returns_false_with_invalid_hand() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::WindEast,
            Tile::WindWest,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.rinshanpai = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
        ]);

        assert!(!hand.has_rinshan_kaihou());
    }

    #[test]
    fn has_ryanpeikou_returns_true_when_present() {
        // `112233p 445566s 11z`.
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        assert!(hand.has_ryanpeikou());
    }

    #[test]
    fn has_ryanpeikou_returns_true_with_red_5() {
        // `112233p 044566s 11z`.
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::SouzuRed5,
            Tile::Souzu6,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::SouzuRed5, Tile::Souzu6),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ]);

        assert!(hand.has_ryanpeikou());
    }

    #[test]
    fn has_ryuuiisou_returns_true_when_present_and_closed() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        assert!(hand.has_ryuuiisou());
    }

    #[test]
    fn has_ryuuiisou_returns_false_when_non_green_bamboo_tile_present() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        assert!(!hand.has_ryuuiisou());

        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu3, Tile::Souzu4, Tile::Souzu5),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        assert!(!hand.has_ryuuiisou());

        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu3, Tile::Souzu4, Tile::Souzu5),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        assert!(!hand.has_ryuuiisou());
    }

    #[test]
    fn has_sanankou_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.win_declaration = Some(WinDeclaration::Tsumo(Tile::Souzu6));

        assert!(hand.has_sanankou());
    }

    #[test]
    fn has_sanankou_returns_true_when_present_with_chii_meld() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.drawn_tile = Some(Tile::Souzu1);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.melds = Vec::from([Meld::Chii {
            claimed_tile: Tile::Souzu6,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Souzu4, Tile::Souzu5, Tile::Souzu6],
        }]);
        hand.win_declaration = Some(WinDeclaration::Tsumo(Tile::Souzu1));

        assert!(hand.has_sanankou());
    }

    #[test]
    fn has_sanankou_returns_true_when_present_with_pon_meld_and_tsumo() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.drawn_tile = Some(Tile::Souzu1);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Souzu6,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
        }]);
        hand.win_declaration = Some(WinDeclaration::Tsumo(Tile::Souzu1));

        assert!(hand.has_sanankou());
    }

    #[test]
    fn has_sanankou_returns_true_when_present_with_4_triplets_but_win_with_ron_from_double_pair_wait()
     {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::West,
            tile: Tile::Souzu1,
        });

        assert!(hand.has_sanankou());
    }

    #[test]
    fn has_sanankou_returns_false_when_not_present() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.win_declaration = Some(WinDeclaration::Tsumo(Tile::Souzu6));

        assert!(!hand.has_sanankou());
    }

    #[test]
    fn has_sanankou_returns_false_when_ron_for_third_triplet() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu5, Tile::Souzu5, Tile::Souzu5),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::West,
            tile: Tile::Souzu5,
        });

        assert!(!hand.has_sanankou());
    }

    #[test]
    fn has_sanankou_returns_false_with_pon_meld_and_ron() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Souzu6,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
        }]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::West,
            tile: Tile::Souzu1,
        });

        assert!(!hand.has_sanankou());
    }

    #[test]
    fn has_sankantsu_returns_true_when_present() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::WindNorth,
        ])));
        hand.melds = Vec::from([
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu4,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu9,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Pinzu9, Tile::Pinzu9, Tile::Pinzu9, Tile::Pinzu9],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::WindEast,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                ],
            }),
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::South,
            tile: Tile::WindNorth,
        });

        assert!(hand.has_sankantsu());
    }

    #[test]
    fn has_sankantsu_returns_false_with_too_few_kans() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::WindNorth,
        ])));
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Pinzu4,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4],
            },
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu9,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Pinzu9, Tile::Pinzu9, Tile::Pinzu9, Tile::Pinzu9],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::WindEast,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                ],
            }),
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::South,
            tile: Tile::WindNorth,
        });

        assert!(!hand.has_sankantsu());
    }

    #[test]
    fn has_sankantsu_returns_false_with_too_four_kans() {
        // This would be the suu kantsu yaku.
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::WindNorth])));
        hand.melds = Vec::from([
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Manzu9, Tile::Manzu9, Tile::Manzu9, Tile::Manzu9],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu4,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu9,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Pinzu9, Tile::Pinzu9, Tile::Pinzu9, Tile::Pinzu9],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::WindEast,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                ],
            }),
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::South,
            tile: Tile::WindNorth,
        });

        assert!(!hand.has_sankantsu());
    }

    #[test]
    fn has_sanshoku_doujun_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.drawn_tile = Some(Tile::Souzu1);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);

        assert!(hand.has_sanshoku_doujun());
    }

    #[test]
    fn has_sanshoku_doujun_returns_true_when_present_with_some_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.drawn_tile = Some(Tile::Souzu1);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.melds = Vec::from([Meld::Chii {
            claimed_tile: Tile::Manzu1,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Manzu1, Tile::Manzu2, Tile::Manzu3],
        }]);

        assert!(hand.has_sanshoku_doujun());
    }

    #[test]
    fn has_sanshoku_doujun_returns_true_when_present_with_all_in_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::WindNorth,
        ])));
        hand.drawn_tile = Some(Tile::WindNorth);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.melds = Vec::from([
            Meld::Chii {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Manzu1, Tile::Manzu2, Tile::Manzu3],
            },
            Meld::Chii {
                claimed_tile: Tile::Pinzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3],
            },
            Meld::Chii {
                claimed_tile: Tile::Souzu1,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu1, Tile::Souzu2, Tile::Souzu3],
            },
        ]);

        assert!(hand.has_sanshoku_doujun());
    }

    #[test]
    fn has_sanshoku_doujun_returns_false_when_not_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.drawn_tile = Some(Tile::Souzu4);
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);

        assert!(!hand.has_sanshoku_doujun());
    }

    #[test]
    fn has_sanshoku_doukou_returns_true_when_present_closed_with_only_triplets() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Pinzu2, Tile::Pinzu2, Tile::Pinzu2),
            Mentsu::Jantou(Tile::Pinzu6, Tile::Pinzu6),
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
        ]);

        assert!(hand.has_sanshoku_doukou());
    }

    #[test]
    fn has_sanshoku_doukou_returns_true_when_present_closed_with_ankan() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Jantou(Tile::Pinzu6, Tile::Pinzu6),
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
        ]);
        hand.melds = Vec::from([Meld::Kan(KanType::Ankan {
            tiles: [Tile::Pinzu2, Tile::Pinzu2, Tile::Pinzu2, Tile::Pinzu2],
        })]);

        assert!(hand.has_sanshoku_doukou());
    }

    #[test]
    fn has_sanshoku_doukou_returns_true_when_present_open() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Jantou(Tile::Pinzu6, Tile::Pinzu6),
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
        ]);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Pinzu2,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::Pinzu2, Tile::Pinzu2, Tile::Pinzu2],
        }]);

        assert!(hand.has_sanshoku_doukou());
    }

    #[test]
    fn has_sanshoku_doukou_returns_true_with_all_melds_with_honor_meld() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::Pinzu6, Tile::Pinzu6])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::Pinzu6, Tile::Pinzu6)]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::DragonWhite,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite],
            },
            Meld::Pon {
                claimed_tile: Tile::Souzu2,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
            },
            Meld::Pon {
                claimed_tile: Tile::Manzu2,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Manzu2, Tile::Manzu2, Tile::Manzu2],
            },
            Meld::Pon {
                claimed_tile: Tile::Pinzu2,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Pinzu2, Tile::Pinzu2, Tile::Pinzu2],
            },
        ]);

        assert!(hand.has_sanshoku_doukou());
    }

    #[test]
    fn has_sanshoku_doukou_returns_false_when_not_present() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Jantou(Tile::Pinzu6, Tile::Pinzu6),
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
        ]);

        assert!(!hand.has_sanshoku_doukou());
    }

    #[test]
    fn has_shousangen_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Koutsu(Tile::Souzu9, Tile::Souzu9, Tile::Souzu9),
            Mentsu::Jantou(Tile::DragonWhite, Tile::DragonWhite),
            Mentsu::Koutsu(Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen),
            Mentsu::Koutsu(Tile::DragonRed, Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(hand.has_shousangen());
    }

    #[test]
    fn has_shousangen_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::DragonWhite,
            Tile::DragonWhite,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Koutsu(Tile::Souzu9, Tile::Souzu9, Tile::Souzu9),
            Mentsu::Jantou(Tile::DragonWhite, Tile::DragonWhite),
        ]);
        hand.melds = Vec::from([
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::DragonRed,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [
                    Tile::DragonRed,
                    Tile::DragonRed,
                    Tile::DragonRed,
                    Tile::DragonRed,
                ],
            }),
            Meld::Pon {
                claimed_tile: Tile::DragonGreen,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen],
            },
        ]);

        assert!(hand.has_shousangen());
    }

    #[test]
    fn has_shousangen_returns_false_when_all_dragons_are_triplets() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
            Mentsu::Koutsu(Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite),
            Mentsu::Koutsu(Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen),
            Mentsu::Koutsu(Tile::DragonRed, Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(!hand.has_shousangen());
    }

    #[test]
    fn has_shousangen_returns_false_with_chiitoitsu() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Manzu5, Tile::Manzu5),
            Mentsu::Jantou(Tile::Manzu6, Tile::Manzu6),
            Mentsu::Jantou(Tile::Manzu7, Tile::Manzu7),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
            Mentsu::Jantou(Tile::DragonWhite, Tile::DragonWhite),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
            Mentsu::Jantou(Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(!hand.has_shousangen());
    }

    #[test]
    fn has_shousangen_returns_false_when_not_all_dragons_present() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Koutsu(Tile::Souzu9, Tile::Souzu9, Tile::Souzu9),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
            Mentsu::Koutsu(Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen),
            Mentsu::Koutsu(Tile::DragonRed, Tile::DragonRed, Tile::DragonRed),
        ]);

        assert!(!hand.has_shousangen());
    }

    #[test]
    fn has_shousuushii_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);

        assert!(hand.has_shousuushii());
    }

    #[test]
    fn has_shousuushii_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Jantou(Tile::WindSouth, Tile::WindSouth),
        ]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::WindWest,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::WindWest, Tile::WindWest, Tile::WindWest],
            },
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::WindNorth,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [
                    Tile::WindNorth,
                    Tile::WindNorth,
                    Tile::WindNorth,
                    Tile::WindNorth,
                ],
            }),
        ]);

        assert!(hand.has_shousuushii());
    }

    #[test]
    fn has_shousuushii_returns_false_with_four_wind_triplets() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
            Mentsu::Koutsu(Tile::WindNorth, Tile::WindNorth, Tile::WindNorth),
        ]);

        assert!(!hand.has_shousuushii());
    }

    #[test]
    fn has_shousuushii_returns_false_with_four_wind_triplets_and_quad() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
        ])));
        hand.groups = Box::new([
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
        ]);
        hand.melds = Vec::from([Meld::Kan(KanType::Daiminkan {
            claimed_tile: Tile::WindNorth,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [
                Tile::WindNorth,
                Tile::WindNorth,
                Tile::WindNorth,
                Tile::WindNorth,
            ],
        })]);

        assert!(!hand.has_shousuushii());
    }

    #[test]
    fn has_suuankou_returns_true_when_present_and_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.drawn_tile = Some(Tile::Souzu8);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);

        assert!(hand.has_suuankou(&round_context));
    }

    #[test]
    fn has_suuankou_returns_true_when_present_with_ankan() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.drawn_tile = Some(Tile::Souzu8);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        hand.melds = Vec::from([Meld::Kan(KanType::Ankan {
            tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
        })]);

        let round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);

        assert!(hand.has_suuankou(&round_context));
    }

    #[test]
    fn has_suuankou_returns_true_with_ron_on_tanki_wait() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::West,
            tile: Tile::DragonGreen,
        });

        let round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);

        assert!(hand.has_suuankou(&round_context));
    }

    #[test]
    fn has_suuankou_returns_true_with_discard_on_tanki_wait() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::DragonGreen));

        assert!(hand.has_suuankou(&round_context));
    }

    #[test]
    fn has_suuankou_returns_false_with_pon() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Souzu2,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
        }]);

        let round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);

        assert!(!hand.has_suuankou(&round_context));
    }

    #[test]
    fn has_suuankou_returns_false_with_chii() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        hand.melds = Vec::from([Meld::Chii {
            claimed_tile: Tile::Souzu4,
            relative_player: PlayerRelativePosition::Kamicha,
            tiles: [Tile::Souzu2, Tile::Souzu3, Tile::Souzu4],
        }]);

        let round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);

        assert!(!hand.has_suuankou(&round_context));
    }

    #[test]
    fn has_suuankou_returns_false_with_sequence() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);

        assert!(!hand.has_suuankou(&round_context));
    }

    #[test]
    fn has_suuankou_returns_false_with_discard_on_double_pair_wait() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Jantou(Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::North, Tile::Souzu8));

        assert!(!hand.has_suuankou(&round_context));
    }

    #[test]
    fn has_suuankou_returns_false_with_ron_on_double_pair_wait() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::West,
            tile: Tile::Souzu8,
        });

        let round_context = RoundContext::new(create_wall(), Wind::South, [0; 32]);

        assert!(!hand.has_suuankou(&round_context));
    }

    #[test]
    fn has_suukantsu_returns_true_with_four_kan_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::Souzu3, Tile::Souzu3])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::Souzu3, Tile::Souzu3)]);
        hand.melds = Vec::from([
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Pinzu3,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3],
            }),
        ]);

        assert!(hand.has_suukantsu());
    }

    #[test]
    fn has_suukantsu_returns_false_without_four_kans() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
            Mentsu::Jantou(Tile::Souzu3, Tile::Souzu3),
        ]);
        hand.melds = Vec::from([
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Pinzu3,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3],
            }),
        ]);

        assert!(!hand.has_suukantsu());
    }

    #[test]
    fn has_suukantsu_returns_false_with_chii() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::Souzu3, Tile::Souzu3])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::Souzu3, Tile::Souzu3)]);
        hand.melds = Vec::from([
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Pinzu3,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3],
            }),
            Meld::Chii {
                claimed_tile: Tile::Souzu2,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Souzu2, Tile::Souzu3, Tile::Souzu4],
            },
        ]);

        assert!(!hand.has_suukantsu());
    }

    #[test]
    fn has_suukantsu_returns_false_with_pon() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::Souzu3, Tile::Souzu3])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::Souzu3, Tile::Souzu3)]);
        hand.melds = Vec::from([
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Manzu1, Tile::Manzu1, Tile::Manzu1, Tile::Manzu1],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::Pinzu8,
                relative_player: PlayerRelativePosition::Kamicha,
                tiles: [Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8, Tile::Pinzu8],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::Pinzu3,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3, Tile::Pinzu3],
            }),
            Meld::Pon {
                claimed_tile: Tile::Souzu2,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu2, Tile::Souzu2, Tile::Souzu2],
            },
        ]);

        assert!(!hand.has_suukantsu());
    }

    #[test]
    fn has_tenhou_returns_true_with_valid_hand_and_first_draw() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.drawn_tile = Some(Tile::Souzu8);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(hand.has_tenhou(&round_context, &player_round_state));
    }

    #[test]
    fn has_tenhou_returns_true_with_kokushi_musou_and_first_draw() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonGreen,
            Tile::DragonRed,
            Tile::DragonRed,
        ])));
        hand.drawn_tile = Some(Tile::Pinzu1);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(hand.has_tenhou(&round_context, &player_round_state));
    }

    #[test]
    fn has_tenhou_returns_true_with_chuuren_poutou() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));
        hand.drawn_tile = Some(Tile::ManzuRed5);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(hand.has_tenhou(&round_context, &player_round_state));
    }

    #[test]
    fn has_tenhou_returns_false_when_not_first_turn() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));
        hand.drawn_tile = Some(Tile::ManzuRed5);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
        ]);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.first_uninterrupted_turn_mut() = false;

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert!(!hand.has_tenhou(&round_context, &player_round_state));
    }

    #[test]
    fn has_tenhou_returns_false_when_not_in_east_seat() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
        ])));
        hand.drawn_tile = Some(Tile::ManzuRed5);
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
        ]);

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::South);

        assert!(!hand.has_tenhou(&round_context, &player_round_state));
    }

    #[test]
    fn has_toitoi_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu1, Tile::Souzu1, Tile::Souzu1),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        hand.win_declaration = Some(WinDeclaration::Tsumo(Tile::Souzu8));

        assert!(hand.has_toitoi());
    }

    #[test]
    fn has_toitoi_returns_true_when_present_with_some_open_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::Souzu1,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu1, Tile::Souzu1, Tile::Souzu1],
            },
            Meld::Pon {
                claimed_tile: Tile::Souzu3,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Souzu3, Tile::Souzu3, Tile::Souzu3],
            },
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::West,
            tile: Tile::Souzu8,
        });

        assert!(hand.has_toitoi());
    }

    #[test]
    fn has_toitoi_returns_false_with_sequence() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu3, Tile::Souzu3, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu6, Tile::Souzu6, Tile::Souzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        hand.win_declaration = Some(WinDeclaration::Tsumo(Tile::Souzu8));

        assert!(!hand.has_toitoi());
    }

    #[test]
    fn has_toitoi_returns_false_with_four_kantsu() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::DragonGreen])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen)]);
        hand.melds = Vec::from([
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu1, Tile::Souzu1, Tile::Souzu1, Tile::Souzu1],
            }),
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu3, Tile::Souzu3, Tile::Souzu3, Tile::Souzu3],
            }),
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu6, Tile::Souzu6, Tile::Souzu6, Tile::Souzu6],
            }),
            Meld::Kan(KanType::Ankan {
                tiles: [Tile::Souzu8, Tile::Souzu8, Tile::Souzu8, Tile::Souzu8],
            }),
        ]);
        hand.win_declaration = Some(WinDeclaration::Ron {
            deal_in_player: Wind::West,
            tile: Tile::DragonGreen,
        });

        assert!(!hand.has_toitoi());
    }

    #[test]
    fn has_tsuuiisou_returns_true_when_present_with_no_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Koutsu(Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen),
            Mentsu::Koutsu(Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
            Mentsu::Koutsu(Tile::WindWest, Tile::WindWest, Tile::WindWest),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);

        assert!(hand.has_tsuuiisou());
    }

    #[test]
    fn has_tsuuiisou_returns_true_when_present_with_melds() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::WindNorth, Tile::WindNorth])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth)]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::DragonGreen,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                ],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::WindEast,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                ],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::WindWest,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [
                    Tile::WindWest,
                    Tile::WindWest,
                    Tile::WindWest,
                    Tile::WindWest,
                ],
            }),
        ]);

        assert!(hand.has_tsuuiisou());
    }

    #[test]
    fn has_tsuuiisou_returns_false_with_chii() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::WindNorth, Tile::WindNorth])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth)]);
        hand.melds = Vec::from([
            Meld::Chii {
                claimed_tile: Tile::Manzu1,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::Manzu1, Tile::Manzu2, Tile::Manzu3],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [
                    Tile::DragonGreen,
                    Tile::DragonGreen,
                    Tile::DragonGreen,
                    Tile::DragonGreen,
                ],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::DragonWhite,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                ],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::WindEast,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                ],
            }),
        ]);

        assert!(!hand.has_tsuuiisou());
    }

    #[test]
    fn has_tsuuiisou_returns_false_with_terminal_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::Pinzu9, Tile::Pinzu9])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::Pinzu9, Tile::Pinzu9)]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::DragonGreen,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                ],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::WindEast,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                ],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::WindWest,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [
                    Tile::WindWest,
                    Tile::WindWest,
                    Tile::WindWest,
                    Tile::WindWest,
                ],
            }),
        ]);

        assert!(!hand.has_tsuuiisou());
    }

    #[test]
    fn has_tsuuiisou_returns_false_with_simple_tile_pair() {
        let mut hand = Hand::new(TileSet::new(Vec::from([Tile::Manzu2, Tile::Manzu2])));
        hand.groups = Box::new([Mentsu::Jantou(Tile::Manzu2, Tile::Manzu2)]);
        hand.melds = Vec::from([
            Meld::Pon {
                claimed_tile: Tile::DragonGreen,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [Tile::DragonGreen, Tile::DragonGreen, Tile::DragonGreen],
            },
            Meld::Kan(KanType::Ankan {
                tiles: [
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                ],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::WindEast,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                ],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::WindWest,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [
                    Tile::WindWest,
                    Tile::WindWest,
                    Tile::WindWest,
                    Tile::WindWest,
                ],
            }),
        ]);

        assert!(!hand.has_tsuuiisou());
    }

    #[test]
    fn has_tsuuiisou_returns_false_with_sequence() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::WindNorth,
            Tile::WindNorth,
        ])));
        hand.groups = Box::new([
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Jantou(Tile::WindNorth, Tile::WindNorth),
        ]);
        hand.melds = Vec::from([
            Meld::Kan(KanType::Ankan {
                tiles: [
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                    Tile::DragonWhite,
                ],
            }),
            Meld::Kan(KanType::Daiminkan {
                claimed_tile: Tile::WindEast,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                    Tile::WindEast,
                ],
            }),
            Meld::Kan(KanType::Shominkan {
                claimed_tile: Tile::WindWest,
                relative_player: PlayerRelativePosition::Shimocha,
                tiles: [
                    Tile::WindWest,
                    Tile::WindWest,
                    Tile::WindWest,
                    Tile::WindWest,
                ],
            }),
        ]);

        assert!(!hand.has_tsuuiisou());
    }

    #[test]
    fn is_all_greens_returns_true_with_all_green_tiles() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));

        assert!(hand.is_all_greens());
    }

    #[test]
    fn is_all_greens_returns_false_with_not_all_green_tiles() {
        for non_green_tile in [
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu8,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu5,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::WindEast,
            Tile::WindSouth,
            Tile::WindWest,
            Tile::WindNorth,
            Tile::DragonWhite,
            Tile::DragonRed,
        ] {
            let hand = Hand::new(TileSet::new(Vec::from([
                Tile::Souzu2,
                Tile::Souzu2,
                Tile::Souzu3,
                Tile::Souzu3,
                Tile::Souzu4,
                Tile::Souzu4,
                Tile::Souzu6,
                Tile::Souzu6,
                Tile::Souzu6,
                Tile::Souzu8,
                Tile::Souzu8,
                non_green_tile,
                Tile::DragonGreen,
                Tile::DragonGreen,
            ])));

            assert!(!hand.is_all_greens());
        }
    }

    #[test]
    fn has_tanyao_returns_true_with_no_honors_or_terminals() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
        ])));

        assert!(hand.has_tanyao(true));
    }

    #[test]
    fn has_tanyao_returns_true_with_one_honor_or_terminal() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::WindEast,
        ])));

        assert!(!hand.has_tanyao(true));

        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu5,
            Tile::Pinzu5,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::Pinzu6,
            Tile::Pinzu6,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu4,
        ])));

        assert!(!hand.has_tanyao(true));
    }

    #[test]
    fn has_tanyao_returns_true_when_kuitan_and_open_hand() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Pinzu5,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::Pinzu5, Tile::Pinzu5, Tile::PinzuRed5],
        }]);

        assert!(hand.has_tanyao(true));
    }

    #[test]
    fn has_tanyao_returns_false_when_kuitan_and_open_terminal_meld() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Pinzu1,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1],
        }]);

        assert!(!hand.has_tanyao(true));
    }

    #[test]
    fn has_tanyao_returns_true_when_kuitan_nashi_and_ankan() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));
        hand.melds = Vec::from([Meld::Kan(KanType::Ankan {
            tiles: [Tile::Pinzu5, Tile::Pinzu5, Tile::Pinzu5, Tile::PinzuRed5],
        })]);

        assert!(hand.has_tanyao(false));
    }

    #[test]
    fn has_tanyao_returns_false_when_kuitan_nashi_and_open_hand() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::Pinzu5,
            relative_player: PlayerRelativePosition::Shimocha,
            tiles: [Tile::Pinzu5, Tile::Pinzu5, Tile::PinzuRed5],
        }]);

        assert!(!hand.has_tanyao(false));
    }

    #[test]
    fn has_tanyao_returns_false_when_kuitan_nashi_and_terminal_ankan() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));
        hand.melds = Vec::from([Meld::Kan(KanType::Ankan {
            tiles: [Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1],
        })]);

        assert!(!hand.has_tanyao(false));
    }

    #[test]
    fn has_yakuhai_chun_returns_true_with_triplet() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::DragonRed,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));

        assert!(hand.has_yakuhai_chun());
    }

    #[test]
    fn has_yakuhai_haku_returns_true_with_triplet() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));

        assert!(hand.has_yakuhai_haku());
    }

    #[test]
    fn has_yakuhai_haku_returns_true_with_ankan() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));
        hand.melds = Vec::from([Meld::Kan(KanType::Ankan {
            tiles: [
                Tile::DragonWhite,
                Tile::DragonWhite,
                Tile::DragonWhite,
                Tile::DragonWhite,
            ],
        })]);

        assert!(hand.has_yakuhai_haku());
    }

    #[test]
    fn has_yakuhai_haku_returns_true_with_pon() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));
        hand.melds = Vec::from([Meld::Pon {
            claimed_tile: Tile::DragonWhite,
            tiles: [Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite],
            relative_player: PlayerRelativePosition::Toimen,
        }]);

        assert!(hand.has_yakuhai_haku());
    }

    #[test]
    fn has_yakuhai_haku_returns_false_with_pair() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::DragonWhite,
            Tile::DragonWhite,
        ])));

        assert!(!hand.has_yakuhai_haku());
    }

    #[test]
    fn has_yakuhai_hatsu_returns_true_with_triplet() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::DragonGreen,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));

        assert!(hand.has_yakuhai_hatsu());
    }

    #[test]
    fn has_yakuhai_wind_returns_true_with_triplet() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));
        // This can be either the seat wind or prevalent round wind.
        let wind = Wind::East;

        assert!(hand.has_yakuhai_wind(&wind));
    }

    #[test]
    fn has_yakuhai_wind_returns_false_with_different_wind() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
        ])));
        // This can be either the seat wind or prevalent round wind.
        let wind = Wind::South;

        assert!(!hand.has_yakuhai_wind(&wind));
    }

    #[test]
    fn kan_options_returns_none_when_wall_exhausted() -> Result<(), IncrementWallIndexError> {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let player_round_state = PlayerRoundState::new(Wind::East);

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        // Exhaust the wall to create the end of a round.
        for _ in 0..(round_context.wall().last_live_tile_idx()) {
            round_context.wall_mut().increment_tile_idx()?;
        }

        assert_eq!(hand.kan_option(&round_context, &player_round_state), None);

        Ok(())
    }

    #[test]
    fn kan_options_returns_none_when_no_draw_and_no_discard() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let player_round_state = PlayerRoundState::new(Wind::East);

        assert_eq!(hand.kan_option(&round_context, &player_round_state), None);
    }

    #[test]
    fn kan_options_returns_two_red_five_with_ankan() {
        {
            let mut hand = Hand::new(TileSet::new(Vec::from([
                Tile::PinzuRed5,
                Tile::Pinzu5,
                Tile::Pinzu5,
                Tile::Pinzu1,
                Tile::Pinzu1,
                Tile::Pinzu2,
                Tile::Souzu2,
                Tile::Souzu3,
                Tile::Souzu4,
                Tile::Souzu7,
                Tile::Souzu8,
                Tile::WindEast,
                Tile::WindEast,
            ])));
            *hand.drawn_tile_mut() = Some(Tile::PinzuRed5);

            let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

            let player_round_state = PlayerRoundState::new(Wind::East);

            assert_eq!(
                hand.kan_option(&round_context, &player_round_state),
                Some(Meld::Kan(KanType::Ankan {
                    tiles: [Tile::PinzuRed5, Tile::PinzuRed5, Tile::Pinzu5, Tile::Pinzu5]
                }))
            );
        }

        {
            let mut hand = Hand::new(TileSet::new(Vec::from([
                Tile::PinzuRed5,
                Tile::PinzuRed5,
                Tile::Pinzu5,
                Tile::Pinzu1,
                Tile::Pinzu1,
                Tile::Pinzu2,
                Tile::Souzu2,
                Tile::Souzu3,
                Tile::Souzu4,
                Tile::Souzu7,
                Tile::Souzu8,
                Tile::WindEast,
                Tile::WindEast,
            ])));
            *hand.drawn_tile_mut() = Some(Tile::Pinzu5);

            let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
            *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::West, Tile::Pinzu5));

            let player_round_state = PlayerRoundState::new(Wind::East);

            assert_eq!(
                hand.kan_option(&round_context, &player_round_state),
                Some(Meld::Kan(KanType::Ankan {
                    tiles: [Tile::PinzuRed5, Tile::PinzuRed5, Tile::Pinzu5, Tile::Pinzu5]
                }))
            );
        }
    }

    #[test]
    fn kan_options_returns_two_red_five_with_daiminkan() {
        {
            let hand = Hand::new(TileSet::new(Vec::from([
                Tile::Pinzu1,
                Tile::Pinzu1,
                Tile::Pinzu2,
                Tile::PinzuRed5,
                Tile::Pinzu5,
                Tile::Pinzu5,
                Tile::Souzu2,
                Tile::Souzu3,
                Tile::Souzu4,
                Tile::Souzu7,
                Tile::Souzu8,
                Tile::WindEast,
                Tile::WindEast,
            ])));

            let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
            *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::West, Tile::PinzuRed5));

            let player_round_state = PlayerRoundState::new(Wind::East);

            assert_eq!(
                hand.kan_option(&round_context, &player_round_state),
                Some(Meld::Kan(KanType::Daiminkan {
                    claimed_tile: Tile::PinzuRed5,
                    relative_player: PlayerRelativePosition::Toimen,
                    tiles: [Tile::PinzuRed5, Tile::PinzuRed5, Tile::Pinzu5, Tile::Pinzu5]
                }))
            );
        }

        {
            let hand = Hand::new(TileSet::new(Vec::from([
                Tile::Pinzu1,
                Tile::Pinzu1,
                Tile::Pinzu2,
                Tile::PinzuRed5,
                Tile::PinzuRed5,
                Tile::Pinzu5,
                Tile::Souzu2,
                Tile::Souzu3,
                Tile::Souzu4,
                Tile::Souzu7,
                Tile::Souzu8,
                Tile::WindEast,
                Tile::WindEast,
            ])));

            let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
            *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::West, Tile::Pinzu5));

            let player_round_state = PlayerRoundState::new(Wind::East);

            assert_eq!(
                hand.kan_option(&round_context, &player_round_state),
                Some(Meld::Kan(KanType::Daiminkan {
                    claimed_tile: Tile::PinzuRed5,
                    relative_player: PlayerRelativePosition::Toimen,
                    tiles: [Tile::PinzuRed5, Tile::PinzuRed5, Tile::Pinzu5, Tile::Pinzu5]
                }))
            );
        }
    }

    #[test]
    fn kan_options_returns_two_red_five_with_shominkan() {
        {
            let mut hand = Hand::new(TileSet::new(Vec::from([
                Tile::Pinzu1,
                Tile::Pinzu1,
                Tile::Pinzu2,
                Tile::Souzu2,
                Tile::Souzu3,
                Tile::Souzu4,
                Tile::Souzu7,
                Tile::Souzu8,
                Tile::WindEast,
                Tile::WindEast,
            ])));
            hand.melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Pinzu5,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::PinzuRed5, Tile::Pinzu5, Tile::Pinzu5],
            });
            *hand.drawn_tile_mut() = Some(Tile::PinzuRed5);

            let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

            let player_round_state = PlayerRoundState::new(Wind::East);

            assert_eq!(
                hand.kan_option(&round_context, &player_round_state),
                Some(Meld::Kan(KanType::Shominkan {
                    claimed_tile: Tile::PinzuRed5,
                    relative_player: PlayerRelativePosition::Toimen,
                    tiles: [Tile::PinzuRed5, Tile::PinzuRed5, Tile::Pinzu5, Tile::Pinzu5]
                }))
            );
        }

        {
            let mut hand = Hand::new(TileSet::new(Vec::from([
                Tile::Pinzu1,
                Tile::Pinzu1,
                Tile::Pinzu2,
                Tile::Souzu2,
                Tile::Souzu3,
                Tile::Souzu4,
                Tile::Souzu7,
                Tile::Souzu8,
                Tile::WindEast,
                Tile::WindEast,
            ])));
            hand.melds_mut().push(Meld::Pon {
                claimed_tile: Tile::Pinzu5,
                relative_player: PlayerRelativePosition::Toimen,
                tiles: [Tile::PinzuRed5, Tile::PinzuRed5, Tile::Pinzu5],
            });
            *hand.drawn_tile_mut() = Some(Tile::Pinzu5);

            let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
            *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::West, Tile::Pinzu5));

            let player_round_state = PlayerRoundState::new(Wind::East);

            assert_eq!(
                hand.kan_option(&round_context, &player_round_state),
                Some(Meld::Kan(KanType::Shominkan {
                    claimed_tile: Tile::PinzuRed5,
                    relative_player: PlayerRelativePosition::Toimen,
                    tiles: [Tile::PinzuRed5, Tile::PinzuRed5, Tile::Pinzu5, Tile::Pinzu5]
                }))
            );
        }
    }

    #[test]
    fn pon_options_returns_none_when_no_last_discard() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(hand.pon_options(&round_context, &player_round_state), None);
    }
    #[test]
    fn pon_options_returns_none_when_wall_is_exhausted() -> Result<(), IncrementWallIndexError> {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::Manzu1));
        // Exhaust the wall to create the end of a round.
        for _ in 0..(round_context.wall().last_live_tile_idx()) {
            round_context.wall_mut().increment_tile_idx()?;
        }

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(hand.pon_options(&round_context, &player_round_state), None);

        Ok(())
    }

    #[test]
    fn pon_options_returns_options_when_hand_has_one_akadora() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::Manzu5));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.pon_options(&round_context, &player_round_state),
            Some(
                [Meld::Pon {
                    claimed_tile: Tile::Manzu5,
                    relative_player: PlayerRelativePosition::Shimocha,
                    tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5]
                }]
                .into()
            )
        );
    }

    #[test]
    fn pon_options_returns_options_when_hand_has_two_akadora() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::ManzuRed5,
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        // Pretend that this game is using 4 akadora for this test.
        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::Manzu5));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.pon_options(&round_context, &player_round_state),
            Some(
                [
                    Meld::Pon {
                        claimed_tile: Tile::Manzu5,
                        relative_player: PlayerRelativePosition::Shimocha,
                        tiles: [Tile::ManzuRed5, Tile::ManzuRed5, Tile::Manzu5]
                    },
                    Meld::Pon {
                        claimed_tile: Tile::Manzu5,
                        relative_player: PlayerRelativePosition::Shimocha,
                        tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5]
                    }
                ]
                .into()
            )
        );
    }

    #[test]
    fn pon_options_returns_options_when_hand_has_one_akadora_and_discard_is_akadora() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        // Pretend that this game is using 4 akadora for this test.
        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::ManzuRed5));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.pon_options(&round_context, &player_round_state),
            Some(
                [
                    Meld::Pon {
                        claimed_tile: Tile::ManzuRed5,
                        relative_player: PlayerRelativePosition::Shimocha,
                        tiles: [Tile::ManzuRed5, Tile::ManzuRed5, Tile::Manzu5]
                    },
                    Meld::Pon {
                        claimed_tile: Tile::ManzuRed5,
                        relative_player: PlayerRelativePosition::Shimocha,
                        tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5]
                    }
                ]
                .into()
            )
        );
    }

    #[test]
    fn pon_options_returns_options_when_discard_is_akadora() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::ManzuRed5));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.pon_options(&round_context, &player_round_state),
            Some(
                [Meld::Pon {
                    claimed_tile: Tile::ManzuRed5,
                    relative_player: PlayerRelativePosition::Shimocha,
                    tiles: [Tile::ManzuRed5, Tile::Manzu5, Tile::Manzu5]
                }]
                .into()
            )
        );
    }

    #[test]
    fn pon_options_returns_options_with_three_copies_of_tile() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu6,
            Tile::Manzu6,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
        ])));

        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.last_discard_mut() = Some(LastDiscard::new(Wind::South, Tile::WindEast));

        let mut player_round_state = PlayerRoundState::new(Wind::East);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);

        assert_eq!(
            hand.pon_options(&round_context, &player_round_state),
            Some(
                [Meld::Pon {
                    claimed_tile: Tile::WindEast,
                    relative_player: PlayerRelativePosition::Shimocha,
                    tiles: [Tile::WindEast, Tile::WindEast, Tile::WindEast]
                }]
                .into()
            )
        );
    }

    #[test]
    fn score_returns_correct_struct_with_fewer_than_4_han_but_enough_fu_for_mangan() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::WindSouth,
            Tile::DragonGreen,
        ])));
        *hand.groups_mut() = Box::new([
            // Sequence: 0 fu
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            // Sequence: 0 fu
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            // Sequence: 0 fu
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            // Triplet, honor, concealed: +8 fu
            Mentsu::Koutsu(Tile::WindSouth, Tile::WindSouth, Tile::WindSouth),
            // Pair, value honor: +2 fu
            // Pair wait: +2 fu
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        *hand.win_declaration_mut() = Some(WinDeclaration::Ron {
            deal_in_player: Wind::South,
            tile: Tile::DragonGreen,
        });

        let game_rules = GameRules::new_4p_default();
        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.first_uninterrupted_turn_mut() = false;
        let mut player_round_state = PlayerRoundState::new(Wind::South);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);
        *player_round_state.ippatsu_mut() = true;

        // The fu comes from (42->50):
        //   - Winning: +20
        //   - Concealed ron: +10
        //   - Triplet, honor, concealed: +8
        //   - Pair, value honor: +2
        //   - Pair wait: +2
        let expected = HandScore::new(
            [
                Yaku::Riichi,
                Yaku::Ippatsu,
                Yaku::Yakuhai(Yakuhai::SeatWind),
            ]
            .into(),
            &hand.calculate_dora(&round_context, &player_round_state),
            [
                Fu::Winning,
                Fu::ConcealedRon,
                Fu::Ankou(FuMentsuTileClass::RoutouhaiJihai),
                Fu::Yakuhai,
                Fu::Tanki,
            ]
            .into(),
            false,
        );

        assert_eq!(
            hand.score(&game_rules, &round_context, &player_round_state),
            expected
        );
    }

    #[test]
    fn score_calculates_open_triplet_with_shanpon_wait_on_ron() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::DragonGreen,
            Tile::DragonGreen,
        ])));
        *hand.groups_mut() = Box::new([
            // Sequence: 0 fu
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            // Sequence: 0 fu
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            // Triplet, non-terminal/honor, open (from ron): +2 fu
            Mentsu::Koutsu(Tile::Souzu4, Tile::Souzu4, Tile::Souzu4),
            // Triplet, non-terminal/honor, closed: +4 fu
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            // Pair, value honor: +2 fu
            // Pair wait: +2 fu
            Mentsu::Jantou(Tile::DragonGreen, Tile::DragonGreen),
        ]);
        *hand.win_declaration_mut() = Some(WinDeclaration::Ron {
            deal_in_player: Wind::South,
            tile: Tile::Souzu4,
        });

        let game_rules = GameRules::new_4p_default();
        let mut round_context = RoundContext::new(create_wall(), Wind::East, [0; 32]);
        *round_context.first_uninterrupted_turn_mut() = false;
        let mut player_round_state = PlayerRoundState::new(Wind::South);
        *player_round_state.riichi_mut() = Some(Riichi::Riichi);
        *player_round_state.ippatsu_mut() = true;

        // Han (4):
        //   - Riichi
        //   - Ippatsu
        // The fu comes from (40):
        //   - Winning: +20
        //   - Concealed ron: +10
        //   - Triplet, non-terminal/honor, open (from ron): +2
        //   - Triplet, non-terminal/honor, closed: +4
        //   - Pair, value honor: +2
        //   - Pair wait: +2
        let expected = HandScore::new(
            [Yaku::Riichi, Yaku::Ippatsu].into(),
            &hand.calculate_dora(&round_context, &player_round_state),
            [
                Fu::Winning,
                Fu::ConcealedRon,
                Fu::Minkou(FuMentsuTileClass::Tanyaohai),
                Fu::Ankou(FuMentsuTileClass::Tanyaohai),
                Fu::Yakuhai,
                Fu::Tanki,
            ]
            .into(),
            false,
        );

        assert_eq!(
            hand.score(&game_rules, &round_context, &player_round_state),
            expected
        );
    }
}

#[cfg(test)]
mod find_groups_tests {
    use alloc::vec::Vec;

    use super::Hand;
    use crate::mentsu::Mentsu;
    use crate::tile::{Tile, TileSet};

    /// `123m 444p 888s 11z` - Incomplete hand with `457m` not in a group.
    #[test]
    fn incomplete_hand_gets_as_many_possible_groups() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Pinzu4,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            // Incomplete shape below.
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu7,
        ])));
        let expected = [
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Koutsu(Tile::Pinzu4, Tile::Pinzu4, Tile::Pinzu4),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `123 33 345m 777s 555z` - Four copies of `3m` form a pair and 2 sequences with the `3m` on both the end and start respectively.
    #[test]
    fn quadruplet_forms_two_sequences_and_pair() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::DragonWhite,
            Tile::DragonWhite,
            Tile::DragonWhite,
        ])));
        let expected = [
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu3, Tile::Manzu4, Tile::Manzu5),
            Mentsu::Jantou(Tile::Manzu3, Tile::Manzu3),
            Mentsu::Koutsu(Tile::Souzu7, Tile::Souzu7, Tile::Souzu7),
            Mentsu::Koutsu(Tile::DragonWhite, Tile::DragonWhite, Tile::DragonWhite),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `123 33m 456p 678 999s` - Three copies of `3m` form of a pair and a sequence with `3m` at the end.
    #[test]
    fn three_copies_form_pair_and_sequence() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Pinzu4,
            Tile::Pinzu5,
            Tile::Pinzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::Souzu9,
        ])));
        let expected = [
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Jantou(Tile::Manzu3, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Pinzu4, Tile::Pinzu5, Tile::Pinzu6),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Koutsu(Tile::Souzu9, Tile::Souzu9, Tile::Souzu9),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `123 333 456m 678s 11z` - Four copies of `3m` creates sequence and a triplet.
    #[test]
    fn four_copies_form_sequence_and_triplet() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Souzu6,
            Tile::Souzu7,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        let expected = [
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Koutsu(Tile::Manzu3, Tile::Manzu3, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Shuntsu(Tile::Souzu6, Tile::Souzu7, Tile::Souzu8),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `111 222 333 456m 11z` - Three triplets in succession form triplet shapes and not three sequences.
    #[test]
    fn three_triplets_in_succession_form_triplets_and_not_sequences() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        let expected = [
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Koutsu(Tile::Manzu2, Tile::Manzu2, Tile::Manzu2),
            Mentsu::Koutsu(Tile::Manzu3, Tile::Manzu3, Tile::Manzu3),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `123 22 456m 888s 111z` - Three `2m` form a sequence and a pair with the `2m` in the middle of the sequence.
    #[test]
    fn three_copies_form_sequence_and_pair_with_copies_in_middle_of_sequence() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::Souzu8,
            Tile::WindEast,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        let expected = [
            Mentsu::Shuntsu(Tile::Manzu1, Tile::Manzu2, Tile::Manzu3),
            Mentsu::Jantou(Tile::Manzu2, Tile::Manzu2),
            Mentsu::Shuntsu(Tile::Manzu4, Tile::Manzu5, Tile::Manzu6),
            Mentsu::Koutsu(Tile::Souzu8, Tile::Souzu8, Tile::Souzu8),
            Mentsu::Koutsu(Tile::WindEast, Tile::WindEast, Tile::WindEast),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `234m 234p 11 234 234s` - Mixed triple sequence and a pure double sequence. Favors this yaku when an "earlier" sequence (`123s`) is available.
    #[test]
    fn mixed_triplet_sequence_yaku_is_favored_over_earlier_sequence() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Souzu1,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
        ])));
        let expected = [
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Pinzu2, Tile::Pinzu3, Tile::Pinzu4),
            Mentsu::Jantou(Tile::Souzu1, Tile::Souzu1),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `234m 234p 234 234 55s` - Mixed triple sequence and a pure double sequence. Favors this yaku when a "later" sequence (`345s`) is available.
    #[test]
    fn mixed_triplet_sequence_yaku_is_favored_over_later_sequence() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu4,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
            Tile::Souzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
        ])));
        let expected = [
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Pinzu2, Tile::Pinzu3, Tile::Pinzu4),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Shuntsu(Tile::Souzu2, Tile::Souzu3, Tile::Souzu4),
            Mentsu::Jantou(Tile::Souzu5, Tile::Souzu5),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `555 678 999m 123 22s` - `678m` sequence sandwiched between tiles that would fit within a different sequence.
    #[test]
    fn sequence_sandwiched_between_triplets() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Manzu9,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        let expected = [
            Mentsu::Koutsu(Tile::Manzu5, Tile::Manzu5, Tile::Manzu5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Manzu9, Tile::Manzu9, Tile::Manzu9),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `340m 111 222 678 33p` - Sequence contains a red 5.
    #[test]
    fn sequence_contains_red_five() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::ManzuRed5,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Pinzu6,
            Tile::Pinzu7,
            Tile::Pinzu8,
        ])));
        let expected = [
            Mentsu::Shuntsu(Tile::Manzu3, Tile::Manzu4, Tile::ManzuRed5),
            Mentsu::Koutsu(Tile::Pinzu1, Tile::Pinzu1, Tile::Pinzu1),
            Mentsu::Koutsu(Tile::Pinzu2, Tile::Pinzu2, Tile::Pinzu2),
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu6, Tile::Pinzu7, Tile::Pinzu8),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `055 678m 999p 123 22s` - Triplet contains red 5.
    #[test]
    fn triplet_contains_red_five() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        let expected = [
            Mentsu::Koutsu(Tile::Manzu5, Tile::Manzu5, Tile::ManzuRed5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Pinzu9, Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `05 678m 999p 123 222s` - Pair contains red 5.
    #[test]
    fn pair_contains_red_five() {
        let hand = Hand::new(TileSet::new(Vec::from([
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Pinzu9,
            Tile::Souzu1,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu2,
            Tile::Souzu3,
        ])));
        let expected = [
            Mentsu::Jantou(Tile::Manzu5, Tile::ManzuRed5),
            Mentsu::Shuntsu(Tile::Manzu6, Tile::Manzu7, Tile::Manzu8),
            Mentsu::Koutsu(Tile::Pinzu9, Tile::Pinzu9, Tile::Pinzu9),
            Mentsu::Shuntsu(Tile::Souzu1, Tile::Souzu2, Tile::Souzu3),
            Mentsu::Koutsu(Tile::Souzu2, Tile::Souzu2, Tile::Souzu2),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `112233p 445566s 11z`
    #[test]
    fn ryanpeikou_returns_3_sequences() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Pinzu1,
            Tile::Pinzu1,
            Tile::Pinzu2,
            Tile::Pinzu2,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu4,
            Tile::Souzu4,
            Tile::Souzu5,
            Tile::Souzu5,
            Tile::Souzu6,
            Tile::WindEast,
            Tile::WindEast,
        ])));
        hand.drawn_tile = Some(Tile::Souzu6);
        let expected = [
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Pinzu1, Tile::Pinzu2, Tile::Pinzu3),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Shuntsu(Tile::Souzu4, Tile::Souzu5, Tile::Souzu6),
            Mentsu::Jantou(Tile::WindEast, Tile::WindEast),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    /// `111 234 567 789m 22s`
    #[test]
    fn all_suit_tiles_present_but_not_all_sequences_for_ittsuu() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu1,
            Tile::Manzu2,
            Tile::Manzu3,
            Tile::Manzu4,
            Tile::Manzu5,
            Tile::Manzu6,
            Tile::Manzu7,
            Tile::Manzu7,
            Tile::Manzu8,
            Tile::Manzu9,
            Tile::Souzu2,
            Tile::Souzu2,
        ])));
        hand.drawn_tile = Some(Tile::WindEast);
        let expected = [
            Mentsu::Koutsu(Tile::Manzu1, Tile::Manzu1, Tile::Manzu1),
            Mentsu::Shuntsu(Tile::Manzu2, Tile::Manzu3, Tile::Manzu4),
            Mentsu::Shuntsu(Tile::Manzu5, Tile::Manzu6, Tile::Manzu7),
            Mentsu::Shuntsu(Tile::Manzu7, Tile::Manzu8, Tile::Manzu9),
            Mentsu::Jantou(Tile::Souzu2, Tile::Souzu2),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }

    #[test]
    fn chiitoitsu_returns_seven_pairs() {
        let mut hand = Hand::new(TileSet::new(Vec::from([
            Tile::Manzu2,
            Tile::Manzu2,
            Tile::ManzuRed5,
            Tile::Manzu5,
            Tile::Manzu8,
            Tile::Manzu8,
            Tile::Pinzu3,
            Tile::Pinzu3,
            Tile::Souzu7,
            Tile::Souzu7,
            Tile::Souzu9,
            Tile::Souzu9,
            Tile::WindWest,
        ])));
        hand.drawn_tile = Some(Tile::WindWest);

        let expected = [
            Mentsu::Jantou(Tile::Manzu2, Tile::Manzu2),
            Mentsu::Jantou(Tile::ManzuRed5, Tile::Manzu5),
            Mentsu::Jantou(Tile::Manzu8, Tile::Manzu8),
            Mentsu::Jantou(Tile::Pinzu3, Tile::Pinzu3),
            Mentsu::Jantou(Tile::Souzu7, Tile::Souzu7),
            Mentsu::Jantou(Tile::Souzu9, Tile::Souzu9),
            Mentsu::Jantou(Tile::WindWest, Tile::WindWest),
        ];

        assert_eq!(*hand.find_groups(), expected);
    }
}
