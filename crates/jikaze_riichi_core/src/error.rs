/*
 * SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * SPDX-License-Identifier: AGPL-3.0-or-later
 */

/*
 * Jikaze - Mahjong game
 * Copyright (C) 2024 gondolyr <gondolyr+code@posteo.org>
 *
 * This program is free software: you can redistribute it and/or modify
 * it under the terms of the GNU Affero General Public License as
 * published by the Free Software Foundation, either version 3 of the
 * License, or (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU Affero General Public License for more details.
 *
 * You should have received a copy of the GNU Affero General Public License
 * along with this program.  If not, see <https://www.gnu.org/licenses/>.
 */

//! This module contains error types.

use alloc::fmt;
use alloc::string::String;
use alloc::vec::Vec;

use crate::hand::Hand;
use crate::meld::Meld;
// Re-export error for consistency.
pub use crate::player::relative_position::PlayerRelativePositionError;

/// Represents an error while incrementing the number of revealed dora indicators.
#[derive(Debug)]
pub struct IncrementDoraIndicatorError(u8);

impl core::error::Error for IncrementDoraIndicatorError {
    fn source(&self) -> Option<&(dyn core::error::Error + 'static)> {
        None
    }
}

impl fmt::Display for IncrementDoraIndicatorError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "Number of revealed dora indicators cannot exceed {max_dora}",
            max_dora = self.0
        )
    }
}

impl IncrementDoraIndicatorError {
    /// Create a new instance.
    pub fn new(max_dora_allowed: u8) -> Self {
        Self(max_dora_allowed)
    }
}

/// Represents an error while incrementing the number of kans.
#[derive(Debug)]
pub struct IncrementKanError(u8);

impl core::error::Error for IncrementKanError {
    fn source(&self) -> Option<&(dyn core::error::Error + 'static)> {
        None
    }
}

impl fmt::Display for IncrementKanError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "Number of kans called cannot exceed {max_kan_calls}",
            max_kan_calls = self.0
        )
    }
}

impl IncrementKanError {
    /// Create a new instance.
    pub fn new(max_kan_calls_allowed: u8) -> Self {
        Self(max_kan_calls_allowed)
    }
}

/// Represents an error while incrementing the number of rinshanpai drawn.
#[derive(Debug)]
pub struct IncrementRinshanpaiDrawnError(u8);

impl core::error::Error for IncrementRinshanpaiDrawnError {
    fn source(&self) -> Option<&(dyn core::error::Error + 'static)> {
        None
    }
}

impl fmt::Display for IncrementRinshanpaiDrawnError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "Number of rinshanpai drawn cannot exceed {max_rinshanpai_drawn}",
            max_rinshanpai_drawn = self.0
        )
    }
}

impl IncrementRinshanpaiDrawnError {
    /// Create a new instance.
    pub fn new(max_rinshanpai_drawn_allowed: u8) -> Self {
        Self(max_rinshanpai_drawn_allowed)
    }
}

/// Represents an error while incrementing the tile index for the wall.
#[derive(Debug)]
pub struct IncrementWallIndexError(usize);

impl core::error::Error for IncrementWallIndexError {
    fn source(&self) -> Option<&(dyn core::error::Error + 'static)> {
        None
    }
}

impl fmt::Display for IncrementWallIndexError {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(
            f,
            "Current tile index cannot exceed the last live tile index ({max_wall_idx})",
            max_wall_idx = self.0
        )
    }
}

impl IncrementWallIndexError {
    /// Create a new instance.
    pub fn new(max_wall_idx: usize) -> Self {
        Self(max_wall_idx)
    }
}

/// Represents an error applying a meld to the hand.
#[derive(Debug)]
pub struct InvalidMeld(Meld);

impl core::error::Error for InvalidMeld {
    fn source(&self) -> Option<&(dyn core::error::Error + 'static)> {
        None
    }
}

impl fmt::Display for InvalidMeld {
    fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
        write!(f, "Meld could not be applied: {meld:?}", meld = self.0)
    }
}

impl InvalidMeld {
    /// Create a new instance.
    pub fn new(meld: &Meld) -> Self {
        Self(*meld)
    }
}

/// Error that may occur while parsing a serialized hand in MPSZ notation.
#[derive(Debug, Clone)]
pub struct ParseMpszHandError {
    /// Portions of the hand that were able to be parsed. Loss of data is expected here but is
    /// available in the event a consumer wishes to use what is salvageable.
    hand: Option<Hand>,
    /// Human-readable error messages for display.
    messages: Vec<String>,
}

impl core::error::Error for ParseMpszHandError {
    fn source(&self) -> Option<&(dyn core::error::Error + 'static)> {
        None
    }
}

impl fmt::Display for ParseMpszHandError {
    fn fmt(&self, f: &mut fmt::Formatter<'_>) -> fmt::Result {
        write!(
            f,
            "The following errors were found while parsing the serialized hand:\n    - {}",
            self.messages.join("\n    - ")
        )
    }
}

impl ParseMpszHandError {
    /// Create a new instance.
    pub const fn new(hand: Option<Hand>, messages: Vec<String>) -> Self {
        Self { hand, messages }
    }

    /// Get the hand that was salvageable, if one exists.
    pub const fn hand(&self) -> &Option<Hand> {
        &self.hand
    }

    /// Get the human-readable error messages.
    pub const fn messages(&self) -> &Vec<String> {
        &self.messages
    }
}
