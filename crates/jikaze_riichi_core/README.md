<!--
SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

This workspace contains the source code of the core gameplay logic for [Riichi Mahjong (Japanese Mahjong)](https://wikipedia.org/wiki/Japanese_Mahjong).

# Symbol Glossary

This section describes the symbols used in the documentation.

**Note**: While Unicode characters exist for mahjong tiles, they are difficult to read when their size is small.

| Symbol               | Meaning                                                                                                       | Example                                                                                      |
|:--------------------:|---------------------------------------------------------------------------------------------------------------|----------------------------------------------------------------------------------------------|
| `一ニ三四伍六七八九` | Manzu (character) tiles in respective order of 1-9.                                                           | `一ニ三 九九九 ⑦⑧⑨ 11 789` - Junchan (All sets contain at least one terminal).               |
| `①②③④⑤⑥⑦⑧⑨`          | Pinzu (circle) tiles in respective order of 1-9.                                                              | `①②③④④④⑤⑤⑦⑦⑧⑧⑨⑨` - Chinitsu (Full flush).                                                    |
| `123456789`          | Souzu (bamboo) tiles in respective order of 1-9.                                                              | `一ニ三 ①②③ 123` - Sanshokudojun (Three colored sequence).                                   |
| `東南西北`           | Wind tiles in respective order of "East", "South", "West", and "North".                                       | `④⑤⑥ 東東東 南南南 西西 北⮦北北` - Shousuushii (Little four winds).                          |
| `白發中`             | Dragon tiles in respective order of "white", "green", and "red".                                              | `四伍六 ③③ 白白白 發⮦發發 ⮦中中中` - Daisangen (Big three dragons).                          |
| `⮦`                  | Tile following this is rotated anti-clockwise (counter-clockwise) for a called set (chi, pon, kan) or riichi. | `⮦②①③` - The `②` is rotated to denote it was taken from another player's discard.            |
| `█`                  | Upright anonymous tile for called chi, pon, kan. Also used for a concealed kan to denote a facedown tile.     | `③③③ 222 3 777 █11█` - Demonstrates an example of a concealed kan call of four 1 souzu.      |
| `▄`                  | Rotated anonymous called tile for chi, pon, or kan.                                                           | `④ ▄██ ▄██ ▄██ ▄██` - Demonstrates an example of "hanada tanki" with multiple calls.         |
