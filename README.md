<!--
SPDX-FileCopyrightText: 2024 gondolyr <gondolyr+code@posteo.org>

SPDX-License-Identifier: AGPL-3.0-or-later
-->

<h1 align="center">
  <img src="docs/images/jikaze_logo.svg" alt="Jikaze Logo" width="150">
  <br>
  Jikaze 「自風」
</h1>

<h3 align="center">Mahjong for everybody</h3>

<ruby>Jikaze <rp>(</rp><rt>/dʑi kaze/</rt><rp>)</rp></ruby> 「<ruby>自 <rp>(</rp><rt>じ</rt><rp>)</rp> 風 <rp>(</rp><rt>かぜ</rt><rp>)</rp></ruby>」 is a Mahjong game.

Currently, the Japanese variant (Riichi Mahjong) is the only variant that is supported.

## What is Jikaze?

Jikaze is a client for people to play Mahjong with each other.
It is also a server for people that would like to set up their own instance.
The goal of Jikaze is to respect your privacy and to make playing Mahjong accessible.

### Build communities

We believe Mahjong is a fun game and want to encourage people to bond over this game.
We hope that by providing you the tools to create your own communities, you can help share the joys of this game.

We are also hopeful that people are motivated to participate in this project and decide to contribute through various means, whether that would be through code, art, suggestions and bug reports, or other ways.
We aim to be inclusive of all people, without discrimination based on ethnicity, gender, sexual orientation, disabilities, nationality, political background, and faith.

### Trust that you can verify

By respecting the [four freedoms of free software][free software], you don't need to trust us without proof that the games are fair.
That means that you may inspect the code and host your own server to ensure your games are fair.

## Project Goals

The list below is an unordered, non-exhaustive, list of goals for the project:

- [ ] Ranked and unranked matchmaking.
- [ ] Allow clients to connect directly to other clients.
- [ ] Log export.
- [ ] Self-hostable servers.
- [ ] Add support for other Mahjong variants.

### Non-Goals

These are features that I (gondolyr) am not intending to work on.

- Tile matching version (i.e. Mahjong solitaire).
- Cross-server compatibility.
- Support every ruleset.

## Links

- Repository: <https://codeberg.org/amenoasa/jikaze>
- [Matrix](https://matrix.org/): [#jikaze:matrix.org](https://matrix.to/#/#jikaze:matrix.org)

## Prerequisites

- [Rust](https://www.rust-lang.org/)

## Building

_TBD_

## Testing

```sh
cargo test
```

There are two workspaces that have tests:

- `jikaze_riichi_core`
- `jikaze_store`

If you wish to only test a package, you can run:

```sh
cargo test -p <PACKAGE>
```

For example,

```sh
cargo test -p jikaze_riichi_core
```

## Including as a Library

The `jikaze_riichi_core` crate contains the core functionality to build Riichi Mahjong applications with.

Add the following to the `Cargo.toml`:

```toml
jikaze_riichi_core = { git = "https://codeberg.org/amenoasa/jikaze.git" }
```

## Generating Documenation

The `jikaze_riichi_core` crate has extensive documentation available for anyone wishing to include it as a dependency in their projects.

```sh
cargo doc --document-private-items -p jikaze_riichi_core
```

The documentation can be found in the `target/doc/jikaze_riichi_core/` directory. Open up the `index.html` file to get started.

## Assets

The assets for this game are being developed in the companion project [`mahjong_assets`][mahjong_assets]. The source project files can be found there and are licensed under the [Creative Commons Attribution 4.0 International License][cc-by-4.0].

## Why is Jikaze not on crates.io?

I, [gondolyr], will publish there once it stops [requiring a GitHub login](https://github.com/rust-lang/crates.io/issues/326).

This makes a commercial entity the gatekeeper of a large chunk of the Rust community, which is a political bug. I disagree to host Free Software packages and choose a gatekeeper as [hostile](https://ghuntley.com/fracture/) to [Free](https://www.theverge.com/2021/10/23/22742282/microsoft-dotnet-hot-reload-u-turn-response) [Software](https://techrights.org/o/2009/11/15/viral-microsoft-fud/) as Microsoft and this is my protest.

## License

Jikaze is [free software] and is licensed under the [AGPLv3].

This project is compliant with the [REUSE] specification to conveniently convey copyright and licensing information.

### Dependencies' Licenses

This project makes use of [cargo-deny](https://github.com/EmbarkStudios/cargo-deny) to track compatibility with dependencies' licenses.
The `deny.toml` file contains a non-exhaustive list of compatible licenses.

To check license compability, run the following command:

```sh
cargo deny check licenses
```

## Sponsor

Sponsor development with the following methods:

- [Open Collective](https://opencollective.com/gondolyr)
- [Liberapay](https://liberapay.com/gondolyr)

Money offered through these mediums come with no warranty and support and are without any strings attached.

Should an organization wish to sponsor the project with terms, it will be handled in a legal manner with contractual obligations if accepted.

[AGPLv3]: https://www.gnu.org/licenses/agpl-3.0.html
[cc-by-4.0]: https://creativecommons.org/licenses/by/4.0/
[free software]: https://www.gnu.org/philosophy/free-sw.html
[mahjong_assets]: https://codeberg.org/amenoasa/mahjong_assets
[REUSE]: https://reuse.software/
